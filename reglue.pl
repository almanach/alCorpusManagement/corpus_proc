#!/usr/bin/env perl

use strict;

use POSIX qw(strftime);

## Re-glue sentence fragments when the sentence was broken at the level of dags 
## (for long sentences)

use AppConfig qw/:argcount :expand/;

use XML::Twig;


my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
			   );

$config->args();

my $verbose = $config->verbose;


my $xml = XML::Twig->new(
			 twig_handlers => {
					   'E' => \&easy_sentence_handler,
					   'Sentence' => \&passage_sentence_handler,
					  },
			 pretty_print => 'indented'
			);

my $last;

$xml->parse(\*STDIN);

$xml->flush;
 
sub sid {
  my $id = shift;
  $id =~ /^(E\d+)(?:\.\d+)?[FT]\d+/;
  return $1;
}

sub easy_sentence_handler {
  my ($t,$sentence) = @_;
  my $sid = $sentence->att('id');
  my $truesid = $sid;
  $truesid =~ s/E(\d+)\.\d+/E$1/;
  if ($truesid ne $sid) {
    ## aggregate current sentence with last one, 
    ## after renaming of relations, groups and forms
    verbose("To be glued sid=$sid truesid=$truesid");
    my $groups = 0;
    my $rels = 0;
    my $aggregate = 0;
    my $rename = 0;
    ## 1. check that the last sentence is indeed the right one !
    if ((defined $last) && 
	(($last->att('id') eq $truesid) 
	 || (sid($last->last_descendant('F')->att('id')) eq $truesid)
	)){
      $aggregate = 1;
      $rename = 1;
      ## 2. get the current number of groups and relations
      foreach my $g ($last->children('Groupe')) {
	my $id = $g->att('id');
	$id =~ /G(\d+)$/;
	$groups = $1 if ($1 > $groups);
      }
      foreach my $r ($last->descendants('relation')) {
	my $id = $r->att('id');
	$id =~ /R(\d+)$/;
	$rels = $1 if ($1 > $rels);
      }
      verbose("\tshift groups by $groups and rels by $rels");
    } else {
      verbose("\tmissing anchoring sentence $truesid");
      $sentence->set_att(id => $truesid );
##      $last->flush if (defined $last);
      $last = $sentence;
      $rename = 1;
    }
    my $anchorpoint;
    if ($aggregate) {
      unless ($last->children('relations')) {
	XML::Twig::Elt->new('relations')->paste('last_child' => $last);
      }
      $anchorpoint = $last->first_child('relations');
    }
    ## 3. rename in current sentence
    ## 4. aggregate
    foreach my $comp ($sentence->children()){
      my $gi = $comp->gi;
      last if ($gi eq 'relations' || $gi eq 'relation');
      if ($gi eq 'F') {
	## rename
	my $fid = $comp->att('id');
	$fid =~ s/(E\d+)\.\d+(F\d+)/$1$2/o;
	$comp->set_att(id=>$fid);
      } elsif ($gi eq 'Groupe') {
	## rename
	my $gid = $comp->att('id');
	$gid =~ /G(\d+)/;
	$gid = $truesid.'G'.($1+$groups);
	$comp->set_att(id=>$gid);
	foreach my $f ($comp->descendants('F')) {
	  my $fid = $f->att('id');
	  $fid =~ s/(E\d+)\.\d+(F\d+)/$1$2/o;
	  $f->set_att(id=>$fid);
	}
      }
      if ($aggregate) {
	# displace
	$comp->cut;
	$comp->paste( before => $anchorpoint );
      }
    }
    if ($rename ) {
      foreach my $r ($sentence->descendants('relation')) {
	my $rid = $r->att('id');
	$rid =~ /R(\d+)/;
	$rid = $truesid.'R'.($1+$rels);
	$r->set_att(id=>$rid);
	foreach my $c ($r->children) {
	  next unless $c->att('xlink:href');
	  my $loc = $c->att('xlink:href');
	  $loc =~ s/(E\d+)\.\d+/$1/o;
	  if ($loc =~ /G(\d+)/) {
	    $loc = $truesid.'G'.($1+$groups);
	  }
	  $c->set_att('xlink:href' => $loc);
	}
	# displace
	if ($aggregate) {
	  $r->cut;
	  $r->paste( last_child => $anchorpoint );
	}
      }
    }
    # remove sentence from flow
    $sentence->cut if ($aggregate);
  } elsif (defined $last) {
    ## emit last sentnce
    ## update last
##    $last->flush;
    $last = $sentence;
  } else {
    ## update last
    $last = $sentence;
  }
}


sub passage_sentence_handler {
  my ($t,$sentence) = @_;
  my $sid = $sentence->att('id');
  my $truesid = $sid;
  $truesid =~ s/E(\d+)\.\d+/E$1/;
  if ($truesid ne $sid) {
    ## aggregate current sentence with last one, 
    ## after renaming of relations, groups and forms
    verbose("To be glued sid=$sid truesid=$truesid");
    my $groups = 0;
    my $rels = 0;
    my $forms = 0;
    my $aggregate = 0;
    my $rename = 0;
    ## 1. check that the last sentence is indeed the right one !
    if ((defined $last) && 
	(($last->att('id') eq $truesid) 
	 || (sid($last->last_descendant('T')->att('id')) eq $truesid)
	)){
      $aggregate = 1;
      $rename = 1;
      ## 2. get the current number of groups and relations
      foreach my $g ($last->descendants('W')) {
	my $id = $g->att('id');
	$id =~ /[FW](\d+)$/;
	$forms = $1 if ($1 > $forms);
      }
      foreach my $g ($last->descendants('G')) {
	my $id = $g->att('id');
	$id =~ /G(\d+)$/;
	$groups = $1 if ($1 > $groups);
      }
      foreach my $r ($last->descendants('R')) {
	my $id = $r->att('id');
	$id =~ /R(\d+)$/;
	$rels = $1 if ($1 > $rels);
      }
      verbose("\tshift forms by $forms, groups by $groups and rels by $rels");
    } else {
      verbose("\tmissing anchoring sentence $truesid");
      $sentence->set_att(id => $truesid );
      ##      $last->flush if (defined $last);
      $last = $sentence;
      $rename = 1;
    }
    my $anchorpoint;
    if ($aggregate) {
      $anchorpoint = $last->last_child(qr/^(T|W|G)$/);
    }
    ## 3. rename in current sentence
    ## 4. aggregate
    my $map = { 'F' => $forms, 
		'G' => $groups,
		'R' => $rels
	      };
    foreach my $c ($sentence->children(qr/^(T|W|G)$/)) {
      if (defined $anchorpoint) {
	$c->move( after => $anchorpoint );
	$anchorpoint = $c;
      } else {
	$c->move( last_child => $last )
      }
      xrename($c,$map);
    }
    foreach my $c ($sentence->children('R')) {
      $c->move( last_child => $last );
      xrename($c,$map);
    }
    # remove sentence from flow
    $sentence->cut if ($aggregate);
  } elsif (defined $last) {
    ## emit last sentnce
    ## update last
##    $last->flush;
    $last = $sentence;
  } else {
    ## update last
    $last = $sentence;
  }
}

sub xrename {
  my ($xml,$map) = @_;
  my $id = $xml->att('id');
  my $kind = $xml->gi;
  $xml->set_att(id => label_rename($id,$map)) if ($id);
  if ($kind eq 'W') {
    my @tokens = split(/\s+/,$xml->att('tokens'));
    @tokens = map {label_rename($_,$map)} @tokens;
    $xml->set_att(tokens => join(' ',@tokens));
  } elsif ($kind eq 'G') {
    foreach my $c ($xml->children) {
      xrename($c,$map);
    }
  } elsif ($kind eq 'R') {
    foreach my $c ($xml->children) {
      xrename($c,$map);
    }
  } elsif ($xml->att('ref')) {
    my $ref = label_rename($xml->att('ref'),$map);
    $xml->set_att(ref => $ref);
  }
}

sub label_rename {
  my ($label,$map) = @_;
  $label =~ s/^(E\d+)\.\d+/$1/o;
  $label =~ s/([FGR])(\d+)/$1.($2+$map->{$1})/oe;
  return $label;
}

sub verbose {
  return unless ($config->verbose);
##  my $date = strftime "%A %d %B, %H:%M:%S", gmtime;
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print STDERR "$date @_\n";
}
