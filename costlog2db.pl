#!/usr/bin/env perl

use strict;
use AppConfig qw/:argcount :expand/;
use List::Util qw/max min sum/;
use DBI;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
#			    "th=d@" => {DEFAULT => [30] },
			    "th=d@" => {DEFAULT => [] },
#			    "size=d@" => {DEFAULT => [70]},
#			    "size=d@" => {DEFAULT => [30]},
#			    "size=d@" => {DEFAULT => [10]},
			    "size=d@" => {DEFAULT => []},
#			    "size=d@" => {DEFAULT => [5]},
			    "lastcost=f", # to follow cost evolutions
			    "origcost=f", # to follow cost evolutions
			    'features=f' => {DEFAULT => 'features.conf'},
			    'dbdir=d' => {DEFAULT => '/local/alpage/frmg/'},
			    'dummy!' => {DEFAULT => 0},
			    'perceptron!' => {DEFAULT => 0},
			    'model!' => {DEFAULT => 0} # produce new model format (experimental)
                           );

$config->args();

my @sizes = @{$config->size};

print STDERR "costlog2db size=@sizes\n";

my $dbdir = $config->dbdir;

my $perceptron = $config->perceptron;

my $model={};

my $features={};

my %sql_tables = ( 
		  rlt => { rule => 'TEXT',
			   label => 'TEXT',
			   type => 'TEXT'
			 },
		 );

my %feature_sql_map = ();

my $generalizers = {};

my $fvals = {};

if ($config->features && -f $config->features) {
  my $file = $config->features;
  open(F,"<$file") || die "can't open features file '$file': $!";
  while(<F>) {
    if (my ($key,$foreign) = /^#foreign\s+(\S+)\s+(\S+)/) {
      ## definition of a SQL foreign key
      $feature_sql_map{$key} = { foreign => $foreign };
      $sql_tables{$foreign} ||= { v => 'TEXT' };
      next;
    }
    if (my ($key) = /^#integer\s+(\S+)/) {
      $feature_sql_map{$key} = { type => 'INTEGER' };
      next;
    }
    if (/^#\s*\+dummy/) {
      $config->dummy(1);
    }
    if (/^#\s*generalizer\s+(\S+)\s+(\S+)/) {
      $generalizers->{$1}{$2} = 1;
      next;
    }
    (/^\s*\#/ || /^\s*$/) and next; # comments and blank lines
    chomp;
    s/^\s+//o;
    s/\#.*$//o;			# comment on end of line
    s/\s+$//o;
    my $fs = $_;
    my @fs = split(/\s+/,$fs);
    print "registering fs='$fs'\n";
    if ($fs[0] eq 'empty') {	# empty feature set
      print "*** empty feature set\n";
      @fs = ();
      $fs='';
    }
    my $entry = $features->{$fs} ||= { f => {map {$_ => 1} @fs}, 
				       fx => \@fs, 
				       generalizers => [],
				       n => scalar(@fs),
				       foreigns => {},
				       bfset => grep {/^b/} @fs # feature set with at least one brother feature
				     };
    handle_features($entry,@fs);
  }
  foreach my $fs1 ( sort {$features->{$a}{n} <=> $features->{$b}{n}}
		    keys %$features
		  ) {
    my $entry1 = $features->{$fs1};
    my $l1 = $entry1->{n};
  LOOP:
    foreach my $fs2 ( sort {$features->{$b}{n} <=> $features->{$a}{n}} 
		      grep {$features->{$_}{n} < $l1} 
		      keys %$features
		    ) {
      ($fs1 eq $fs2) and next;
      my $entry2 = $features->{$fs2};
#      grep {! (exists $entry1->{f}{$_} || exists_generalizer($_,$entry1)) } keys %{$entry2->{f}} and next;
      grep {! (exists $entry1->{f}{$_}) } 
	keys %{$entry2->{f}} and next;
      ## $fs2 is a subset of $fs1
      push(@{$entry1->{generalizers}},$fs2);
    }
  }
  close(F);
}

sub exists_generalizer {
  my ($f,$entry) = @_;
  return grep {$generalizers->{$f}{$_}} keys %{$entry->{f}};
}

my $dummy = $config->dummy;

sub handle_features {
  my ($entry,@fs) = @_;
  my $from = join('_','fset',@fs);
  my @where = ();
  foreach my $key (@fs) {
    my $info = $feature_sql_map{$key};
    $info or die "missing feature SQL info '$key'";
    if (0 && exists $info->{foreign}) {
      push(@where,"$key = (select rowid from $info->{foreign} where v=?)");
      $entry->{foreigns}{$key} = $feature_sql_map{$key}{foreign};
    } else {
      push(@where,"$key = ?");
    }
  }
  my $where = join("\n\tand ",
#		   "rlt= (select rowid from rlt where rule=? and label=? and type=?)",
		   "rlt= ?",
		   @where);
  $entry->{stmt} = <<EOF;
select w from $from
where $where
EOF
  my $table = $entry->{table} = $from;
  
  my $insert_values = join(',' , '?', '?', map {"?"} @fs);
  my $insert_fields = join(',' , 'w', 'rlt',@fs);
  
  $entry->{insert} = "insert into $table ($insert_fields) values ($insert_values)";
}

my $oldcost = {};

sub costfile_process {
  my ($file) = @_;
  $file or return;
  print STDERR "processing oldfile $file\n";
  if ($file =~ /\.gz$/) {
    open(COST,"-|","gzcat file") || die "can't open cost file '$file': $!";
  } elsif ($file =~ /\.bz2$/) {
    open(COST,"-|","bzcat file") || die "can't open cost file '$file': $!";
  } else {
    open(COST,"<",$file) || die "can't open cost file '$file': $!";
  }
  while(<COST>) {
    chomp;
    next unless (s/^\%\%\s+//);
    my ($rule,$n,$w,$v,$keptgood,$keptbad,$shouldkeep,$delgood,$delbad,$shoulddel,@ctx) = split(/\t/,$_);
    my ($xrule) = $rule =~ /^<(.+?)>$/o;
    ($xrule eq '+dummy' && !$dummy) and next;
    if ($ctx[0] =~ /^gain=(\S+)$/) {
      my $gain = $1;
      shift @ctx;
    }
    my $ctx = join("\t",@ctx);
    $shouldkeep =~ s/^\{(.+)\}$/$1/;
    $shoulddel =~ s/^\{(.+)\}$/$1/;
    my @w = split(m{/},$w);
    $oldcost->{"$xrule\t$ctx"} = [
				  $n, # 0: n
				  $v, # 1: v
				  \@w, # 2: w
				  $keptgood, # 3: keptgood
				  $keptbad,  # 4: keptbad
				  $shouldkeep, # 5: shouldkeep
				  $delgood,    # 6: delgood
				  $delbad,     # 7: delbad
				  $shoulddel,  # 8: shoulddel
				  $keptgood + $delgood # 9: allgood => 3
				 ];
  }
  close(COST);
}

costfile_process($config->lastcost);

my $cost = [];
my $cost2 = {};

my $dbkey_ctr=1;
my $dbkeys = {};

print STDERR "Processing current costs\n";

while (<>) {
  chomp;
  next unless (s/^##\S*\s+// || /^ctx/);
  my ($ctxset,$rule,$n,$v,$keptgood,$keptbad,$shouldkeep,$delgood,$delbad,$shoulddel,
      $id,$previd,
      @ctx) = split(/\t/,$_);
  next if ($rule =~ /^all_/);
  ($rule eq '<+dummy>' && !$dummy) and next;
  my $fs = join(" ",map {/^\+(\S+?)=/ and $1} grep {/^\+/} @ctx);
  exists $features->{$fs} or next;
#  $fs or print STDERR "use empty feature set '$fs' with @ctx\n";
#  print STDERR "\thandling $rule $fs\n";
  $shouldkeep =~ s/^\{(.+)\}$/$1/;
  $shoulddel =~ s/^\{(.+)\}$/$1/;
  my ($xrule) = $rule =~ /^<(.+?)>$/o;
  ($rule eq 'brother') and $xrule = $rule;

  $rule eq 'brother' and next; 

  my $entry =  [ $n,		# 0: n
		 $v,		# 1: v
		 $xrule,	# 2: rule
		 join("\t",@ctx), # 3: ctx
		 $keptgood,	  # 4: keptgood
		 $keptbad,	  # 5: keptbad
		 $shouldkeep,	  # 6: shouldkeep
		 $delgood,	  # 7: delgood
		 $delbad,	  # 8: delbad
		 $shoulddel,	  # 9: shoulddel
		 $keptgood + $delgood, # 10: allgood  => 4
		 $features->{$fs},     # 11: fs       => 5
		 0,		       # 12: delta    => 6
		 [0],		       # 13: lastw    => 7
		 0,		       # 14: updated  => 8
		 {},		       # 15: cinfo    => 9
		 'empty',	       # 16: dkey     => 10
		 'empty',	       # 17: dvalue   => 11
		 0		       # 18: gain
	       ];
  push(@$cost, $entry);

  my $l="$xrule\t$entry->[3]";	# ctx

#  my $x = ($shouldkeep-$shoulddel);
  #  the delta combine some fixed part with the avg absolute weight value for the rule
  my $x = ($shouldkeep-$shoulddel);
#  my $y = min(5,max(1,abs($v/100)));
  my $y = min(5,max(1,abs($v/200)));
  ##  my $y = $x * $n;
  if (0 && max($shoulddel,$shouldkeep) < -20) {
    # do nothing
    # very bad case
  } elsif (0 && $rule eq 'brother') {
    $entry->[12] += 15 * ($keptgood-$keptbad);
  } elsif ($perceptron) {
    ## new way for computing the delta, closer to the perceptron
    ## to be tried ! 12/10/24
    $entry->[12] += 10 * ($delbad-$keptbad);
  } elsif ($x > 2) {		# should favor keep: increase weight
#    $entry->{delta} += $delbad * $x;
#    $entry->[12] += 0.5 * (min($delbad,10)+$delbad) * $x ;
    $entry->[12] += 0.5 * (min($delbad,10)+$delbad) * $x ;
  } elsif ($x < -2) {		# should favor deletion: decrease weight
#    $entry->{delta} += $keptbad * $x;
#    $entry->[12] += 0.5 * (min($keptbad,10)+$keptbad) * $x;
    $entry->[12] += 0.5 * (min($keptbad,10)+$keptbad) * $x;
  }

##  ($rule eq 'brother') and $entry->[12] *= 1.5;

  if (my $lastentry = $oldcost->{$l}) { # last oldcost
    ## print STDERR "using orig cost for $l\n";
    $entry->[13] = $lastentry->[2];
    $entry->[18] = $entry->[10] - $lastentry->[9]; # gain
    0 and $entry->[0] = max($lastentry->[0],$entry->[0]);
#    if (0 && $entry->[10] < $lastentry->[9] - 3) { # allgood
    if (0 && $entry->[18] < -10) {
      ## we backtrack, if performance decrease
      ## we try a step twice smaller
      $entry->[13][-1] = max(1,int(0.8*$entry->[13][-1]));
      $entry->[12] = 0;
      $entry->[0] = $lastentry->[0];
      $entry->[1]= $lastentry->[1];
      $entry->[4] = $lastentry->[3];
      $entry->[5] = $lastentry->[4];
      $entry->[6] = $lastentry->[5];
      $entry->[7] = $lastentry->[6];
      $entry->[8] = $lastentry->[7];
      $entry->[9] = $lastentry->[8];
      $entry->[10] = $lastentry->[9];
      $entry->[14] = 1;	# no update in this case
    } elsif (0 && $rule eq 'brother') {
      $entry->[12] *= max(0,$entry->[18]) / 6;
    }
    $lastentry->[0] = 0;
  }

  my $cinfo = $entry->[15] = {}; # cinfo
  my @fset = ();
  my @fvset = ();
  foreach my $u (split(/\t/,$entry->[3])) { # ctx
    my ($f,$fv) = split(/=/,$u);
    my $isset = $f =~ s/^\+//o;
    $cinfo->{$f} = $fv;
    if ($isset) {
      push(@fset,$f);
      push(@fvset,lp_val($f => $fv));
      $f eq 'f2' and $entry->[12] /= 10; # delta
    }
  }
  $entry->[16] = scalar(@fset) ? join('_',@fset) : 'empty';
  $entry->[17] = scalar(@fvset) ? join(',',@fvset) : 'empty';
  #  print STDERR "resgistering '$l'\n";
  $cost2->{$l} = $entry;
}

if (0) {
  foreach my $key (grep {$oldcost->{$_}[0]}
		   keys %$oldcost) {
    ## handling old entries not present in new round
    my $oldentry = $oldcost->{$key};
    my ($xrule,@ctx) = split(/\t+/,$key);
    my $fs = join(" ",map {/^\+(\S+?)=/ and $1} grep {/^\+/} @ctx);
    exists $features->{$fs} or next;
    my $entry =  [ $oldentry->[0],		# 0: n
		   $oldentry->[1],		# 1: v
		   $xrule,	# 2: rule
		   join("\t",@ctx), # 3: ctx
		   $oldentry->[3],	  # 4: keptgood
		   $oldentry->[4],	  # 5: keptbad
		   $oldentry->[5],	  # 6: shouldkeep
		   $oldentry->[6],	  # 7: delgood
		   $oldentry->[7],	  # 8: delbad
		   $oldentry->[8],	  # 9: shoulddel
		   $oldentry->[9], # 10: allgood  => 4
		   $features->{$fs},     # 11: fs       => 5
		   0,		       # 12: delta    => 6
		   $oldentry->[2],       # 13: lastw    => 7
		   0,		       # 14: updated  => 8
		   {},		       # 15: cinfo    => 9
		   'empty',	       # 16: dkey     => 10
		   'empty',	       # 17: dvalue   => 11
		   0		       # 18: gain
		 ];
    push(@$cost, $entry);
    my $cinfo = $entry->[15] = {}; # cinfo
    my @fset = ();
    my @fvset = ();
    foreach my $u (@ctx) { # ctx
      my ($f,$fv) = split(/=/,$u);
      my $isset = $f =~ s/^\+//o;
      $cinfo->{$f} = $fv;
      if ($isset) {
	push(@fset,$f);
	push(@fvset,lp_val($f => $fv));
	$f eq 'f2' and $entry->[12] /= 10; # delta
      }
    }
    $entry->[16] = scalar(@fset) ? join('_',@fset) : 'empty';
    $entry->[17] = scalar(@fvset) ? join(',',@fvset) : 'empty';
    #  print STDERR "resgistering '$l'\n";
    $cost2->{$key} = $entry;
  }
}

## update weights for overlapping features
update($_) foreach (@$cost);

sub update {
  my $entry = $_[0];
  $entry->[14] and return; # udpated
  $entry->[14] = 1;
  my $rule = $entry->[2];
  my $cinfo = $entry->[15];
  foreach my $gen (@{$entry->[11]{generalizers}}) { # fs
    my $genx = $features->{$gen};
    my $l = join("\t",
		 $rule,
		 "label=$cinfo->{label}",
		 "type=$cinfo->{type}",
		 map {"+$_=$cinfo->{$_}"} @{$genx->{fx}}
		);
#    print STDERR "testing '$entry->{ctx}' with '$l'\n";
    if (my $entry2 = $cost2->{$l}) {
      update($entry2);
#      print STDERR "\tremoving $entry2->{delta} '$l' from '$entry->{ctx}'\n";
      $entry->[12] -= $entry2->[12]; # delta
    }
  }
  if ($dummy && $rule ne '+dummy') {
    ## update wrt dummy rule, that acts as a kind of baseline
    my $l = join("\t",'+dummy',$entry->[3]);
    if (my $entry2 = $cost2->{$l}) {
      update($entry2);
      #      print STDERR "\tremoving $entry2->{delta} '$l' from '$entry->{ctx}'\n";
      $entry->[12] -= $entry2->[12]; # delta
    }
  }
}

my $done = {};

@{$config->size} or die "missing size";

foreach my $size (@{$config->size}) {
  my @entries = grep {$_->[0] > $size || $_->[2] eq 'brother'} @$cost; # n
  foreach my $th (@{$config->th}) {
    next if $done->{$size}{$th};
    $done->{$size}{$th} = 1;
#    my $xfactor = $th * (13/10000) ;
    my $xfactor = $th * (13/10000) * 0.1;
    my $costfile = "cost.db.$th.$size";
    my $altcostfile = "cost.db.alt.$th.$size";
    my $dbfile = "cost.$th.$size.dat";
    my $modelfile = "cost.$th.$size.mdl";

    $model = {};

    my $xdbfile = $dbfile;
    $dbdir and $xdbfile = "$dbdir/$xdbfile";

    open(DCOST,">$costfile") || die "can't open cost file '$costfile': $!";
    open(ALTDCOST,">$altcostfile") || die "can't open alt cost file '$altcostfile': $!";
    my $date = `date`;
    chomp($date);
    print DCOST <<EOF;
%% produced by $0 at $date
%% params th=$th size=$size

use_feature_cost.
EOF

    print ALTDCOST <<EOF;
%% produced by $0 at $date
%% params th=$th size=$size

use_feature_cost.
feature_cost_table('$xdbfile').

EOF
    
    print ALTDCOST <<EOF;
feature_cost_prepare(rlt,'select rowid from rlt where rule=? and label=? and type=?').
EOF

    foreach my $fs (sort {$a <=> $b} keys %$features) {
      my $entry = $features->{$fs};
      my $key = join('_','features',@{$entry->{fx}});
      print ALTDCOST <<EOF;
feature_cost_prepare($key,'$entry->{stmt}').
EOF
    }
    close(ALTDCOST);

    my $dbh = DBI->connect("dbi:SQLite:$dbfile","","",{RaiseError => 1, AutoCommit => 0});

    $dbh->do(<<EOF);
PRAGMA foreign_keys=ON;

EOF

    print STDERR "creating database tables size=$size th=$th\n";

    foreach my $table (keys %sql_tables) {
      my $info = $sql_tables{$table};
      my $fields = join(",\n",map { "$_ $info->{$_}"} keys %$info); 
      my $fields2 = join(",",map { "$_"} keys %$info); 
      $dbh->do("DROP TABLE IF EXISTS $table;");
      $dbh->do(<<EOF);
CREATE TABLE $table ( $fields, PRIMARY KEY ($fields2), UNIQUE ($fields2) );
EOF
    }

    $dbh->commit;

    foreach my $fs (keys %$features) {
      my $entry = $features->{$fs};
      my $table = $entry->{table};
      my @lines = ( "w INTEGER",
		    "rlt INTEGER", 
#		    map {"$_ INTEGER"} @{$entry->{fx}} 
		    map {"$_ ". (exists $feature_sql_map{$_} ? "TEXT" : "INTEGER")} @{$entry->{fx}}
		  );
      my $lines = join(",\n",@lines);
      my $fields = join(',','rlt',@{$entry->{fx}});
      $dbh->do("DROP TABLE IF EXISTS $table;");
      my $cmd = <<EOF;
CREATE TABLE $table (
     $lines,
     PRIMARY KEY($fields),
     UNIQUE ($fields)
);
EOF
##      print STDERR "SQL $cmd\n";
      $dbh->do($cmd);
    }

    $dbh->commit;

    $fvals = {};

    print STDERR "populating the tables size=$size th=$th\n";

    emit_db_cost($_,$xfactor,$dbh) foreach (@entries);
    
    close(DCOST);

    $dbh->commit;
    $dbh->disconnect;
    
    model_save($modelfile);

  }
}

sub lp_val {
  my ($f,$s) = @_;
  if (defined $s) {
    if ($f eq 'delta') {
      return $s;
    } elsif (($s =~ /^[a-z0-9]+$/ || $s =~ /^[+-]\d+$/) && $f !~ /tree/) {
      return $s;
    } elsif (0 && $s eq '_' && grep {$f eq $_} qw{tlemma slemma tform sform tform2}) {
      # lemma and forms
      return "''";
    } else {
      $s =~ s/'/''/og;
      return "'$s'";
    }
  } else {
    return '_';
  }
}

sub emit_db_cost {
  my ($entry,$xfactor,$dbh) = @_;
  my $w = int($entry->[12] * $xfactor); # delta
  my $lastw = $entry->[13];
  my $xw = sum $w,@$lastw;
##  abs($w) > 3 or return;
  $xw or return;
  my @w = grep {$_} @$lastw,$w;
  if (@w > 2) {
    my $w1 = pop(@w);
    my $w2 = pop(@w);
    @w = ((sum @w),$w2,$w1);
  }
  my $cinfo = $entry->[15]; # cinfo
  my $xi = sprintf("<%s>\t%d\t%s\t%+.1f\t%.1f\t%.1f\t{%+.1f}\t%.1f\t%.1f\t{%+.1f}\tgain=%d\t%s",
		   $entry->[2],	# rule
		   $entry->[0],	# n
		   join("/", map {sprintf "%+.1f", $_} @w),
		   $entry->[1],	# v
		   $entry->[4], # keptgood
		   $entry->[5], # keptbad
		   $entry->[6], # shouldkeep
		   $entry->[7], # delgood
		   $entry->[8], # delbad
		   $entry->[9], # shoulddel
		   $entry->[18], # gain info
		   $entry->[3] # ctx
		  );

  if ($entry->[16]) {
    print DCOST <<EOF;
%% $xi
feature_cost_$entry->[16]('$entry->[2]','$cinfo->{label}','$cinfo->{type}',$entry->[17],$xw).

EOF
  } else {
    print DCOST <<EOF;
%% $xi
feature_cost('$entry->[2]','$cinfo->{label}','$cinfo->{type}',$xw).

EOF
  }

  ## SQLite insert
  my $fsentry = $entry->[11];	# fs
  my $rule = $entry->[2];
  my @values = ($xw,
		register_rlt($dbh,
			     $rule,
			     $cinfo->{label},
			     $cinfo->{type})
	       );
  # my @xvalues = ( $xw,
  # 		  $rule,
  # 		  $cinfo->{label},
  # 		  $cinfo->{type}
  # 		);
  foreach my $f (@{$fsentry->{fx}}) {
    my $v = $cinfo->{$f};
    $feature_sql_map{$f}{foreign} and register_foreign_fval($dbh,$f,$v);
    push(@values, $v);
  }
  ##  print "sth prepare $fsentry->{insert}\n";
  #  ($entry->[2] eq 'brother') and print "@xvalues (@values)\n";
  $dbh->prepare_cached($fsentry->{insert})->execute(@values);

  ## Model emit
  my $where = $model;
  foreach my $f (qw{label type},@{$fsentry->{fx}}) {
    $where = model_add($cinfo->{$f},0,$where);
  }
  my $template = join('_',@{$fsentry->{fx}});
  $where = model_add($template,0,$where);
  $where = model_add($rule,$xw,$where);
}




sub model_add {
  my ($f,$w,$where) = @_;
  my $entry = $where->{$f} ||= { w => 0, next => {} };
  $entry->{w} += $w;
  return $entry->{next};
}

sub model_save {
  my $file = shift;
  open(MODEL,">$file") || die "can't open model file '$file': $!";
  model_save_aux($model,0);
  close(MODEL);
}

sub model_save_aux {
  my ($where,$depth) = @_;
  foreach my $f (sort keys %$where) {
    my $entry = $where->{$f};
    my $w = sprintf("%.2f",$entry->{w});
    my $kind = ($f =~ /^\d+$/) ? 'int' : 'smb';
    print MODEL <<EOF;
$kind:d$depth:w$w:$f
EOF
    exists $entry->{next} and model_save_aux($entry->{next},$depth+1);
  }
}

sub register_rlt {
  my ($dbh,$rule,$label,$type) = @_;
  my $id = $fvals->{rlt}{$rule}{$label}{$type};
  unless ($id) {
    my $sth = $fvals->{cache}{rlt} ||= $dbh->prepare('insert into rlt (rule,label,type) values (?,?,?)');
    $sth->execute($rule,$label,$type);
    $id = $fvals->{rlt}{$rule}{$label}{$type} = $dbh->func('last_insert_rowid');
    # my $sth2 = $dbh->prepare_cached('select rowid from rlt where rule=? and label=? and type=?');
    # $sth2->execute($rule,$label,$type);
    # while ( my @row = $sth2->fetchrow_array) {
    #  print STDERR "rlt register $rule $label $type => $id vs @row\n";
    # };
  }
  return $id;
}

sub register_foreign_fval {
  my ($dbh,$f,$v) = @_;
  my $foreign = $feature_sql_map{$f}{foreign};
  my $id = $fvals->{$foreign}{$v};
  unless ($id) {
    my $sth = $fvals->{cache}{$foreign} 
      ||= $dbh->prepare("insert into $foreign (v) values (?)");
    $sth->execute($v);
    $id = $fvals->{$foreign}{$v} = $dbh->func('last_insert_rowid');
  }
  return $id;
}
