#!/usr/bin/env perl

use strict;
use IO::All;

use AppConfig qw/:argcount :expand/;

my $easydir= "$ENV{HOME}/EasyCampaign";

my $config = AppConfig->new(
			    'verbose!' => {DEFAULT => 1},
			    'segdir=f' => {DEFAULT => "$easydir/corpus/corpus_ref"},
			    'dagdir=f' => {DEFAULT => "$easydir/dag"}
			   );

$config->args();

my $segdir = $config->segdir();
my $dagdir = $config->dagdir();

my $sid;
my $corpus;
my $old=0;
my $file;
my $seg;

my $gid;
my $rid;

my $gcounter;
my $gcounter;

my %info;

my $log;
my $f;
my $fid;

my %sentences=();

my @files = @ARGV;

foreach my $file (@files) {
  print "Processing $file\n";
  if ($file =~ /\.bz2$/) {
    open(FILE,"bzcat $file|") || die "can't open $file: $!";
  } else {
    open(FILE,"<$file") || die "can't open $file: $!";
  }
  while (<FILE>) {
    if (/\<DOCUMENT\s+(fichier|file)="(.+?)"/) {
      $file=$2;
      $corpus=$file;
      $corpus =~ s/\.\S+$//og;
      $old=0;
      close(LOG) if ($log);
      ##      print "CORPUS $corpus\n";
      $log= "$corpus.patch";
      open(LOG,">$log") || die "can't log to $log: $!";
      check_shift($file);
      $sid=0;
      %info = ();
      %sentences = ();
      next;
    }
    
    if (m!</E>!) {
      foreach my $xsid (sort {sid_order($a,$b)} keys %{$info{break}}) {
	my $data = $info{break}{$xsid};
	foreach my $gid (keys %{$data->{gids}}) {
	  print LOG "rename group $sid$gid $data->{gids}{$gid}\n";
	}
	foreach my $rid (keys %{$data->{rids}}) {
	  print LOG "rename relation $sid$rid $data->{rids}{$rid}\n";
	}
      }
    %info=();
      next;
    }
    
    if (/<E .*id="(E\d+(?:\.\d+)?)"/) {
      $sid=$1;
    register_sentence($sid);
      next;
    }
    ++$f, $fid=$1 if (/\<F/ && /id="(E\d+(?:\.\d+)?F\d+)"/);
    $gid=$1, $f=0, next if (/\<Groupe/ && /id="E\d+(?:\.\d+)?(G\d+)"/); 
    $rid=$1, next if (/\<relation/ && /id="E\d+(?:\.\d+)?(R\d+)"/); 
    if (m!</Groupe>!) {
      ## Detecting void groups
      print LOG "void $sid$gid\n" unless ($f);
      $gid=0;
      next;
    }
    if ($f && /(E\d+(?:\.\d+)?F\d+)\|/ && $1 ne $fid) {
      print LOG "rename token $fid $1\n";
      next;
    }
    $rid=0, next if (m!</relation>!); 
    if (/(?:id|href)="(E\d+(?:\.\d+)?)F\d+"/ && $1 ne $sid) {
      my $xsid=$1;
      next if (sid_order($xsid,$sid) == -1);
      unless (exists $info{break}{$xsid}) {
	register_sentence($xsid);
	$info{break}{$xsid} = { group => 1, rel =>1, gids => {}, rids => {} };
	##      print "break $corpus $sid $xsid\n";
	print LOG "break $sid $xsid\n";
	my $delta = sid_minus($xsid,$sid);
	print LOG "## large delta $delta from $sid to $xsid\n" if ($delta > 5);
      }
      if ($gid && !exists $info{break}{$xsid}{gids}{$gid}) {
	print LOG "## group $gid to be splitted between two sentences $sid and $xsid\n" if ($f > 1);
	update_displaced_group($gid,$sid,$xsid);
      }
      if ($rid && !exists $info{break}{$xsid}{rids}{$rid}) {
	update_displaced_rel($rid,$sid,$xsid);
      }
      next;
    }
    
    # taking into account displaced groups occuring within relations
    if (/href="E\d+(?:\.\d+)?(G\d+)"/ && exists $info{displaced}{$1}) {
      my $xsid=$info{displaced}{$1};
      if ($rid && !exists $info{break}{$xsid}{rids}{$rid}) {
	update_displaced_rel($rid,$sid,$xsid);
	print LOG "## $sid$rid renamed through displaced group $1\n";
      }
      next;
    }
  }
  close(FILE);
}

sub update_displaced_group {
  my $gid = shift;
  my $sid = shift;
  my $xsid = shift;
  if (exists $info{displaced}{$gid} && $info{displaced}{$gid} ne $xsid) {
    print LOG "## cross-sentence group $sid$gid\n";
    return if (sid_order($xsid,$info{displaced}{$gid}) == -1);
    delete $info{break}{$info{displaced}{$gid}}{gids}{$gid};
    $info{displaced}{$gid} = $xsid;
  } else {
    $info{displaced}{$gid} = $xsid;
  }
  $info{break}{$xsid}{gids}{$gid} = $xsid."G".($info{break}{$xsid}{group}++);
}

sub update_displaced_rel {
  my $rid = shift;
  my $sid = shift;
  my $xsid = shift;
  if (exists $info{displaced}{$rid} && $info{displaced}{$rid} ne $xsid) {
    print LOG "## cross-sentence relation $sid$rid\n";
    return if (sid_order($xsid,$info{displaced}{$rid}) == -1);
    delete $info{break}{$info{displaced}{$rid}}{rids}{$rid};
    $info{displaced}{$rid} = $xsid;
  } else {
    $info{displaced}{$rid} = $xsid;
  }
  $info{break}{$xsid}{rids}{$rid} = $xsid."R".($info{break}{$xsid}{rel}++);
}


sub register_sentence {
  my $sid = shift;
  print LOG "## duplicate $sid\n" if (exists $sentences{$sid});
  $sentences{$sid} = 1;
}

sub sid_order {
  my ($s1,$s2) = @_;
  my ($k1) = ($s1 =~ /^E(\d+(?:\.\d+)?)/);
  my ($k2) = ($s2 =~ /^E(\d+(?:\.\d+)?)/);
  return ($k1 <=> $k2);
}

sub sid_minus {
  my ($s1,$s2) = @_;
  my ($k1) = ($s1 =~ /^E(\d+(?:\.\d+)?)/);
  my ($k2) = ($s2 =~ /^E(\d+(?:\.\d+)?)/);
  return ($k1 - $k2);
}

close(LOG);

sub check_shift {
  my $file = shift;
  my %first_ref = ();
  my %first_dag = ();
##  print "Check shift on $corpus for $file\n";
  open(SEG,"<$segdir/$file");
  while (<SEG>) {
##    print "SEG $_";
    if (m!<F ID="E(\d+(?:\.\d+)?)F1">\s*(.*?)\s*</F>!i) {
      my ($sid,$w) = ($1,$2);
      $w =~ s/�/-/og;
      $first_ref{$sid} = $w; 
    }
  }
  close(SEG);
  if (-r "$dagdir/$file.udag.bz2") {
    open(DAG,"bzcat $dagdir/$file.udag.bz2|");
  } else {
    open(DAG,"<$dagdir/$file.udag");
  }
  while (<DAG>) {
##    print "DAG $_";
    if (m!<F id="E(\d+(?:\.\d+)?)F1">\s*(.*?)\s*</F>!i) {
      my ($sid,$w) = ($1,$2);
      $w =~ s/_/ /og;
      $w =~ s/\s*UNDERSCORE/_/og;
      $first_dag{$sid} = $w;
    }
  }
  close(DAG);
  my $shift=0;
  foreach my $sid (sort {$a <=> $b} keys %first_dag) {
    my $lshift=0;
##    print "Try Align sentence $sid ".($sid+$shift+$lshift)."  '$first_dag{$sid}' '$first_ref{$sid+$shift+$lshift}'\n";
    while (!(index($first_ref{$sid+$shift+$lshift},$first_dag{$sid},0) == 0)) {
      $lshift++;
      $lshift=0, last if ($lshift > 5);
    }
##    print "Align sentence $sid ".($sid+$shift+$lshift)."  '$first_dag{$sid}' '$first_ref{$sid+$shift+$lshift}'\n";
    if ($lshift) {
      $shift += $lshift;
##      print "shift $corpus E$sid +$shift\n";
      print LOG "shift E$sid +$shift\n";
    }
  }
}

