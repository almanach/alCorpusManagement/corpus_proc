#!/usr/bin/env perl

use strict;
use XML::Twig;
use File::Basename;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "ref|r=f"  => {DEFAULT => "/Users/clerger/Work/EASy/ref290109"},
			    "freqform=f" => {DEFAULT => "concat1/freqform.txt"},
			    "freqth=i" => {DEFAULT => 50},
			    "full!" => {DEFAULT => 1},
			    "robust!" => {DEFAULT => 1},
			    "fail!" => {DEFAULT => 1},
			    "html=f" => {DEFAULT => "relaccuracy.html"},
			    "passageref!" => {DEFAULT => 0}
                           );

$config->args();

##print "Handling $corpus\n";
my $refdir = $config->ref;

my $freqform = $config->freqform;
my $freqth=$config->freqth;

my $keepfull = $config->full;
my $keeprobust = $config->robust;
my $keepfail = $config->fail;

my $html = $config->html;
my $passageref = $config->passageref;

my %forms=();
my $corpus;

my @files = @ARGV;

my @gtypes = qw/GN GA GR GP NV PV/;
my @allgtypes = ('?','mis','void');

my %rtypes = (
	      'SUJ-V' =>  { verbe => 'head',
			    sujet => 'arg1'
			  },
	      'AUX-V' => { verbe => 'head',
			   auxiliaire => 'arg1'
			 },
	      'COD-V' => { verbe => 'head',
			   cod => 'arg1'
			 },
	      'CPL-V' => { verbe => 'head',
			   complement => 'arg1'
			 },
	      'MOD-V' => { verbe => 'head',
			   modifieur => 'arg1'
			 },
	      'COMP' => { verbe => 'head',
			  complementeur => 'arg1'
			},
	      'ATB-SO' => { verbe => 'head',
			    attribut => 'arg1'
			  },
	      'MOD-N' => { nom => 'head',
			   modifieur => 'arg1'
			 },
	      'MOD-A' => { adjectif => 'head',
			   modifieur => 'arg1'
			 },
	      'MOD-R' => { adverbe => 'head',
			   modifieur => 'arg1'
			 },
	      'MOD-P' => { preposition => 'head',
			   modifieur => 'arg1'
			 },
	      'COORD' => { coordonnant => 'head',
			   'coord-g' => 'arg1',
			   'coord-d' => 'arg2'
			 },
	      'APPOS' => { appose => 'head',
			   premier => 'arg1'
			 },
	      'JUXT' => { suivant => 'head',
			  premier => 'arg1'
			}
	     );

my @reltypes = keys %rtypes;
my @allreltypes = ('missing',
		   'SUJ-V',
		   'AUX-V',
		   'COD-V',
		   'CPL-V',
		   'MOD-V',
		   'COMP',
		   'ATB-SO',
		   'MOD-N',
		   'MOD-A',
		   'MOD-R',
		   'MOD-P',
		   'COORD-G',
		   'COORD-D',
		   'APPOS',
		   'JUXT'
		  );

my $accinfo = { n => 0, 
		correct => 0,
		full => { n => 0, correct => 0, confusion => {}},
		robust => { n => 0, correct => 0, confusion => {}},
		fail => { n => 0, correct => 0, confusion => {}},
		confusion => {},
		corpus => {}
	      };

my $refinfo = {};
my $reftokens = {};

open(TOK,">reltokens.log") || die "can't open 'reltokens.log': $!";

foreach my $file (@files) {
  $corpus = fileparse($file,qr{.xml},qr{.xml.bz2});
  $accinfo->{corpus}{$corpus} = { n => 0,
				  correct => 0,
				  full => { n => 0, correct => 0 },
				  robust => { n => 0, correct => 0 },
				  fail => { n => 0, correct => 0 }
				};
  my $ref = "$refdir/$corpus.xml";
  print TOK "## Processing corpus $corpus\n";
  process_ref_corpus($ref);
  process_hyp_corpus($file);
  process_missing_hyp();
}

close(TOK);

open(HTML,">$html") || die "can't open $html: $!";
print HTML header,
  start_html('EASy files and stats'),
  h1('Relation accuracy');


sub perc {
  my ($a,$b) = @_;
  return '-' if ($b == 0);
  return sprintf("%.2f%%",$a / $b * 100);
}

my $acc = perc($accinfo->{correct},$accinfo->{n});
my $facc = perc($accinfo->{full}{correct},$accinfo->{full}{n});
my $racc = perc($accinfo->{robust}{correct},$accinfo->{robust}{n});
my $nacc = perc($accinfo->{fail}{correct},$accinfo->{fail}{n});

my @table = (td(['all',$acc,$facc,$racc,$nacc,$accinfo->{n}]));

foreach my $c (sort keys %{$accinfo->{corpus}}) {
  my $info = $accinfo->{corpus}{$c};
  my $acc = perc($info->{correct},$info->{n});
  my $facc = perc($info->{full}{correct},$info->{full}{n});
  my $racc = perc($info->{robust}{correct},$info->{robust}{n});
  my $nacc = perc($info->{fail}{correct},$info->{fail}{n});
  push(@table,td([$c,$acc,$facc,$racc,$nacc,$info->{n}]));
}

print HTML table( {-border => 1},
		  Tr( [th(['','all','full','robust','fail','#tok']),
		       @table
		      ] ) );

print HTML h1("General confusion table");
confusion_table($accinfo->{confusion});

print HTML h1("Confusion table for mode=full");
confusion_table($accinfo->{full}{confusion});

print HTML h1("Confusion table for mode=robust");
confusion_table($accinfo->{robust}{confusion});

print HTML h1("Confusion table for mode=fail");
confusion_table($accinfo->{fail}{confusion});

sub confusion_table {
  my $table = shift;
  my $tb = [th(['classified as =>', @allreltypes,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  my $rtotal = {};
  foreach my $r (@allreltypes) {
    foreach my $t (@allreltypes) {
      my $info = $table->{$t}{$r} || {};
      foreach my $map (keys %$info) {
	$rtotal->{$r} += $info->{$map};
      }
    }
  }
  foreach my $t (@allreltypes) {
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    foreach my $r (@allreltypes) {
      foreach my $map (keys %{$info->{$r}}) {
	$total += $info->{$r}{$map};
	$sum->{$r} += $info->{$r}{$map};
      }
    }
    foreach my $r (@allreltypes) {
      my @y = ();
      foreach my $map (keys %{$info->{$r}}) {
	my $n = $info->{$r}{$map};
	my $ratio = sprintf("%.1f%%",100*$n/$total);
	my $prec = sprintf("%.1f%%",100*$n/($rtotal->{$r} || 1 ));
##	my $x = perc($n,$total);
	$n = "$map:$n ($ratio $prec)";
	$n = "<span style='color: red;'>$n</span>" if ($map eq 'match');
	##	$n = "<span style='background-color: blue;'>$n</span>" if ($n > 150 && $r ne $t);
	push(@y,"$n");
      }
      push(@x,"<span title='$t $r'>".join('<br/>',@y)."</span>");
    }
    push(@$tb,td([$t,@x,$total,$t]));
    $ttotal += $total;
  }
  my @x = ('total');
  my $rtotal = 0;
  foreach my $r (@allreltypes) {
    push(@x,$sum->{$r});
    $rtotal += $sum->{$r};
  }
  push(@x,"$rtotal/$ttotal");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['classified as =>', @allreltypes,'total','']));
  print HTML table({-border => 1}, Tr($tb));
}

print HTML end_html;
close(HTML);

sub process_ref_corpus {
  my $ref = shift;
  $refinfo = {};
  $reftokens = {};

  my $refxml = XML::Twig->new(
			      twig_handlers => {
						'E' => \&ref_sentence_handler,
						'Sentence' => \&passageref_sentence_handler,
					       }
			     );
  
  $refxml->parsefile($ref);
  $refxml->purge;
}

sub ref_sentence_handler {
  my ($t,$s) = @_;
  foreach my $t (qw/hypconsts hyprels/) {
    if (my $x = $s->first_child($t)) {
      $x->delete;
    }
  }
  foreach my $w ($s->descendants('F')) {
    my $txt = $w->trimmed_text();
    my $group = $w->parent('Groupe');
    my $gtype = 'void';
    my $id = $w->att('id');
    next if (exists $refinfo->{$id});
    if (defined $group) {
      $gtype =$group->att('type');
    }
    my $x = $refinfo->{$id} = { form => $txt, 
				gtype => $gtype,
				rels => []
			      };
    if ( (defined $group) 
	 && ($group->att('type') eq 'NV')
	 && ( $txt =~ /^(-t-|-)?je|tu|il|elle|on|l'on|nous|vous|ils|elles|�a$/io
	      || $txt =~ /'(il|elle|on|ils|elles)$/o
	    )
       ) {
      ## find subject in NV group
      my $gid = $group->att('id');
      $refinfo->{$gid} = { repsubj => $id }
    }
    if ((defined $group) && ($group->last_child('F')->att('id') eq $id)) {
      my $gid = $group->att('id');
      ## we assume the head of a chunk is the last form
      ## we will try to refine this hyp. later
      ## for instance for interrogative NV or impertaive NV with post-v clitics
      ## some GN with post-n modifiers (eg Partie II)
      $refinfo->{$gid} = { rep => $id };
      if ( ($txt =~ /^[\"\).?!,;]$/ 
	    ##	    || $txt =~ /^\d+$/
	   )
	  && $w->prev_sibling('F')
	 ) {
	my $tmp = $w->prev_sibling('F');
	$refinfo->{$gid}{rep} = $tmp->att('id');
      }
      if ($group->att('type') eq 'NV') {
	my $tmp = $w;
	while ($tmp 
	       && $tmp->trimmed_text() =~ /^(-t-|-)?(je|tu|il|elle|on|l'on|nous|vous|ils|elles|�a)$/o) {
	  $tmp = $tmp->prev_sibling('F');
	}
	if ($tmp && $tmp != $w) {
	  $refinfo->{$gid}{rep} = $tmp->att('id');
	}
      }
    }
  }
  foreach my $r ($s->descendants('relation')) {
    my $rid = $r->att('id');
    my $rtype = $r->att('type');
    my $rinfo = $rtypes{$rtype};
    next unless ($rinfo);
    my $x =  { type => $rtype  };
    foreach my $role ($r->children) {
      my $target = $role->att('xlink:href');
      next unless ($target && $target =~ /^E\d+/);
      my $rname = $role->name;
      $x->{$rinfo->{$rname}} = 
	($target =~ /F\d+/) ? $target : 
	  ($rname eq 'sujet' && $refinfo->{$target}{repsubj}) ?
	    $refinfo->{$target}{repsubj}
	      : $refinfo->{$target}{rep};
    }
    foreach my $arg (qw/arg1 arg2/) {
      next unless $x->{$arg};
      my $target = $refinfo->{$x->{$arg}};
      push(@{$target->{rels}},
	   { type => xrtype($rtype,$arg),
	     head => $x->{head},
	     rid => $rid
	   });
    }
  }
  $t->purge;
}


sub passageref_sentence_handler {
  my ($t,$s) = @_;
  my $tokens = {};
  $passageref=1;
  foreach my $tok ($s->children('T')) {
    my $txt = $tokens->{$tok->id} = $tok->trimmed_text();
    $reftokens->{$tok->id} = { form => $txt, w => [] };
  }
  foreach my $w ($s->descendants('W')) {
    my @toks = split(/\s+/,$w->att('tokens'));
    my $txt = join(' ',map {$tokens->{$_}} @toks);
    my $group = $w->parent('G');
    my $gtype = 'void';
    my $id = $w->att('id');
    next if (exists $refinfo->{$id});
    defined $group and $gtype =$group->att('type');
    my $x= $refinfo->{$id} = { form => $txt, 
			       gtype => $gtype,
			       rels => []
			     };
    push(@{$reftokens->{$_}{w}},$x) foreach (@toks);
    if ( (defined $group) 
	 && ($group->att('type') eq 'NV')
	 && ( $txt =~ /^(-t-|-)?je|tu|il|elle|on|l'on|nous|vous|ils|elles|�a$/io
	      || $txt =~ /'(il|elle|on|ils|elles)$/o
	    )
       ) {
      ## find subject in NV group
      my $gid = $group->att('id');
      $refinfo->{$gid} = { repsubj => $id }
    }
    if ((defined $group) && ($group->last_child('W')->att('id') eq $id)) {
      my $gid = $group->att('id');
      ## we assume the head of a chunk is the last form
      ## we will try to refine this hyp. later
      ## for instance for interrogative NV or impertaive NV with post-v clitics
      ## some GN with post-n modifiers (eg Partie II)
      $refinfo->{$gid} = { rep => $id };
      if ( ($txt =~ /^[\"\).?!,;]$/ 
	    ##	    || $txt =~ /^\d+$/
	   )
	  && $w->prev_sibling('W')
	 ) {
	my $tmp = $w->prev_sibling('W');
	$refinfo->{$gid}{rep} = $tmp->att('id');
      }
      if ($group->att('type') eq 'NV') {
	my $tmp = $w;
	my $tmptxt = join(' ',map {$tokens->{$_}} split(/\s+/,$tmp->att('tokens')));
	while ($tmp 
	       && $tmptxt =~ /^(-t-|-)?(je|tu|il|elle|on|l'on|nous|vous|ils|elles|�a)$/o) {
	  $tmp = $tmp->prev_sibling('W');
	}
	if ($tmp && $tmp != $w) {
	  $refinfo->{$gid}{rep} = $tmp->att('id');
	}
      }
    }
  }
  foreach my $r ($s->children('R')) {
    my $rid = $r->att('id');
    my $rtype = $r->att('type');
    my $rinfo = $rtypes{$rtype};
    next unless ($rinfo);
    my $x =  { type => $rtype  };
    foreach my $role ($r->children) {
      my $target = $role->att('ref');
      next unless ($target && $target =~ /^E\d+/);
      my $rname = $role->name;
      my $xrole = $rinfo->{$rname};
##      print STDERR "REF REL $rid type=$rtype role=$rname xrole=$xrole target=$target\n";
      $x->{$rinfo->{$rname}} = 
	($target =~ /F\d+/) ? $target : 
	  ($rname eq 'sujet' && $refinfo->{$target}{repsubj}) ?
	    $refinfo->{$target}{repsubj}
	      : $refinfo->{$target}{rep};
    }
    foreach my $arg (qw/arg1 arg2/) {
      next unless $x->{$arg};
      my $target = $refinfo->{$x->{$arg}};
      push(@{$target->{rels}},
	   { type => xrtype($rtype,$arg),
	     head => $x->{head},
	     rid => $rid
	   });
    }
  }
  $t->purge;
}


sub xrtype {
  my $rtype = shift;
  my $arg = shift;
  if ($rtype eq 'COORD') {
    return ($arg eq 'arg1') ? 'COORD-G' : 'COORD-D';
  } else {
    return $rtype;
  }
}

sub process_hyp_corpus {
  my $file = shift;
##  print "Process hyp file $file\n";
  my $xml = XML::Twig->new(
			   twig_handlers => {
					     'Sentence' => \&sentence_handler
					    }
			  );
  if ($file =~ /\.bz2$/) {
    open(FILE,"bzcat $file|") || die "can't open $file: $!";
  }   elsif ($file =~ /\.gz$/) {
    open(FILE,"gzcat $file|") || die "can't open $file: $!";
  } else {
    open(FILE,"<$file") || die "can't open $file: $!";
  }
  $xml->parse(\*FILE);
  $xml->purge;
  close(FILE);
}

sub sentence_handler {
  my ($t,$s) = @_;
  unless ($s->children('G')) {
    $t->purge;
    return;
  }
  my $sid = $s->id;
  my $mode = $s->att('mode');
  $mode eq 'corrected' and $mode = 'full';
  return unless (($keepfull && ($mode eq 'full')) 
		 || ($keeprobust && ($mode eq 'robust')));
  my $tokens = {};
  my @tokens = ();
  my $forms = {};
  my $groups = {};
  my $state = 'init';
  foreach my $tok ($s->children('T')) {
    my $id = $tok->id;
    my $refid = $tok->id;
    $passageref or $refid =~ s/T/F/;
    $refid =~ s/\.\d+//;
    my $ref = $passageref ? $reftokens->{$refid} : $refinfo->{$refid};
    my $gtype = 'mis';
    defined $ref or print "NO REF $corpus $id refid=$refid\n";
    if ($ref) {
      $passageref and $ref = $ref->{w}[0];
      $gtype = $ref->{gtype};
    }
    $ref->{seen} = 1;
    $refid =~ s/T/F/;
    my $tt = $tokens->{$tok->id} 
      = { form => quote($ref->{form} || $tok->trimmed_text()), 
	  w => [],
	  id => "${corpus}.$refid",
	  refid => $refid,
	  gtype => $gtype,
	  refrels => $ref->{rels},
	  rels => [],
	  uid => $tok->id,
	  ws => []
	};
    push(@tokens,$tt);
  }

  # reused from passage2easy, to handle some mutiple-use of tokens
  my $ws={};
  foreach my $w ($s->descendants('W')) {
    my @toks = split(/\s+/,$w->att('tokens'));
    my $wid = $w->att('id');
    my $info = $ws->{$wid} = { tokens => [@toks], xml => $w, id => $wid };
    push(@{$tokens->{$_}{ws}},$info) foreach (@toks);
  }
  foreach my $tt (@tokens) {
    my @ws = @{$tt->{ws}};
    my $first = shift @ws;
    next unless (@ws);
    next if (grep {@{$_->{tokens}}==1} @ws);
    ## try to some something for Ts in several Ws
    my @toks = @{$first->{tokens}};
    my $u = shift @toks;
    if (@toks && $u == $tt->{uid}) {
      ## T belong to a multi-T W and to several W
      ## we keep T in the first W and remove it from the other Ws
      $first->{tokens} = [$u];
      shift @{$tokens->{$_}{ws}} foreach (@toks);
      shift @{$_->{tokens}} foreach (@ws);
    }
  }

  foreach my $w ($s->descendants('W')) {
    ## my @toks = split(/\s+/,$w->att('tokens'));
    my $group = $w->parent('G');
    my $id = $w->id;
    my @toks = @{$ws->{$id}{tokens}};
    my $gtype = 'void';
    (defined $group) and $gtype = $group->att('type');
    my $pos = $w->att('pos');
    if (defined $pos && ($pos eq 'v')) {
      if (my $mstag = $w->att('mstag')) {
	$mstag =~ /mode\.(\S+)/;
	my $mode = $1;
	if ($mode && grep {$_ eq $mode} qw/participle infinitive gerundive/) {
	  $pos .= ".$mode";
	}
      }
    }
    foreach my $tid (@toks) {
      my $tt = $tokens->{$tid};
      my $is_first = ($tid eq $toks[0]);
      my $is_last = ($tid eq $toks[-1]);
      my $lgtype = $gtype;
      my $info = { cat => $pos || '?',
		   gtype => $lgtype,
		   lemma => quote($w->att('lemma') || '?'),
		   form => $w->att('form') || ""
		 };
      push(@{$tt->{w}}, $info);
      $forms->{$id} = { rep => $tid } if ($is_last);
    }
  }
  foreach my $r ($s->children('R')) {
    my $rid = $r->att('id');
    my $rtype = $r->att('type');
    my $rinfo = $rtypes{$rtype};
    next unless ($rinfo);
    my $x =  { type => $rtype  };
    foreach my $role ($r->children) {
      my $target = $role->att('ref');
      next unless ($target && $target =~ /^E\d+/);
      my $rname = $role->name;
      $x->{$rinfo->{$rname}} = $forms->{$target}{rep};
    }
    my $head =  $tokens->{$x->{head}};
    my $hrefid = $head->{refid};
    my $hform = $head->{form};
    foreach my $arg (qw/arg1 arg2/) {
      next unless $x->{$arg};
      next if ($x->{$arg} eq $x->{head}); # some rels have same target and heads, because of tokens: we discard
      my $target = $tokens->{$x->{$arg}};
      push(@{$target->{rels}},
	   { type => xrtype($rtype,$arg),
	     head => $hrefid,
	     hform => $hform,
	     rid => $rid
	   });
    }
  }
  while (@tokens) {
    my $tok = shift @tokens;
    my $atts = get_atts($tok);
    my $refrels = $tok->{refrels};
    my $hyprels = $tok->{rels};
    my $refgtype = $tok->{gtype};
    my $hypgtype;
    foreach my $w (@{$tok->{w}}) {
      $hypgtype = $w->{gtype};
    }
    my $cat = join('_',map {$_->{cat}} @{$tok->{w}});
    my @maps = ();
    my $register = sub {
      my ($status,$ref,$hyp) = @_;
      my $reft = $ref ? $ref->{type} : "missing";
      my $refh = $ref ? $ref->{head} : 0;
      my $hypt = $hyp ? $hyp->{type} : "missing";
      my $hyph = $hyp ? $hyp->{head} : 0;
      my $refw = $refh ? $refinfo->{$refh}{form} : '*';
      my $hypw = $hyph ? $hyp->{hform} : '*';
      $hyp->{map} = [$status,$ref] if ($hyp);
      $ref->{map} = [$status,$hyp] if ($ref);
      push(@maps,[$status,
		  $reft,$hypt,
		  $refh,$hyph,
		  $refw,$hypw
		 ]);
    };
    foreach my $refrel (@$refrels) {
      my $reft = $refrel->{type};
      my $refh = $refrel->{head};
      foreach my $hyprel (grep {! exists $_->{map}} @$hyprels) {
	my $hypt = $hyprel->{type};
	my $hyph = $hyprel->{head};
##	print STDERR "reft=$reft refh=$refh hypt=$hypt hyph=$hyph\n";
	if (($reft eq $hypt) && ($refh eq $hyph)) {
	  $register->('match',$refrel,$hyprel);
	  last;
	} 
      }
    }
    foreach my $refrel (grep {! exists $_->{map}} @$refrels) {
      my $reft = $refrel->{type};
      my $refh = $refrel->{head};
      foreach my $hyprel (grep {! exists $_->{map}} @$hyprels) {
	my $hypt = $hyprel->{type};
	my $hyph = $hyprel->{head};
	if ($refh eq $hyph) {
	  $register->('samehead',$refrel,$hyprel);
	  last;
	} 
      }
    }
    foreach my $refrel (grep {! exists $_->{map}} @$refrels) {
      my $reft = $refrel->{type};
      my $refh = $refrel->{head};
      foreach my $hyprel (grep {! exists $_->{map}} @$hyprels) {
	my $hypt = $hyprel->{type};
	my $hyph = $hyprel->{head};
	if ($reft eq $hypt) {
	  $register->('sametype',$refrel,$hyprel);
	  last;
	} 
      }
    }
    foreach my $refrel (grep {! exists $_->{map}} @$refrels) {
      my $reft = $refrel->{type};
      foreach my $hyprel (grep {! exists $_->{map}} @$hyprels) {
	my $hypt = $hyprel->{type};
	$register->('mismatch',$refrel,$hyprel);
	last;
      }
    }
    $register->('onlyref',$_,0) foreach (grep {! exists $_->{map}} @$refrels);
    $register->('onlyhyp',0,$_) foreach (grep {! exists $_->{map}} @$hyprels);
    print TOK "$tok->{id} $tok->{form} $cat $mode $refgtype $hypgtype\n";
    foreach my $map (@maps) {
      print TOK "$tok->{id} ### $mode $tok->{form} $cat @$map\n";
      $accinfo->{n}++;
      $accinfo->{$mode}{n}++;
      $accinfo->{corpus}{$corpus}{n}++;
      $accinfo->{corpus}{$corpus}{$mode}{n}++;
      $accinfo->{confusion}{$map->[1]}{$map->[2]}{$map->[0]}++;
      $accinfo->{$mode}{confusion}{$map->[1]}{$map->[2]}{$map->[0]}++;
    }
  }
  $t->purge;
}

sub process_missing_hyp {
  ## process sentence in the ref missing in hyp
  my $register = sub {
    my ($ref) = @_;
    my $reft = $ref->{type};
    my $refh = $ref->{head};
    my $refw = $refinfo->{$refh}{form};
    return [ 'onlyref',
	     $reft,'missing',
	     $refh,0,
	     $refw,'*'
	   ];
  };

  foreach my $refid (sort {sid_sort($a,$b)} keys %$refinfo) {
    my $ref = $refinfo->{$refid};
    next if ($ref->{seen});
    foreach my $refrel (@{$ref->{rels}}) {
      my $reft = $refrel->{type};
      my $refh = $refrel->{head};
      my $map = $register->($refrel);
      my $mode = 'fail';
      print TOK "${corpus}.${refid} ### fail $ref->{form} - @$map\n";
      $accinfo->{n}++;
      $accinfo->{$mode}{n}++;
      $accinfo->{corpus}{$corpus}{n}++;
      $accinfo->{corpus}{$corpus}{$mode}{n}++;
      $accinfo->{confusion}{$map->[1]}{$map->[2]}{$map->[0]}++;
      $accinfo->{$mode}{confusion}{$map->[1]}{$map->[2]}{$map->[0]}++;
    }
  }
}
 
sub sid_sort {
  my ($a,$b) =@_;
  my ($sa,$fa) = ($a =~ /E(\d+)F(\d+)/);
  my ($sb,$fb) = ($b =~ /E(\d+)F(\d+)/);
  return ($sa <=> $sb || $fa <=> $fb); 
}

sub get_atts {
  my $tok = shift;
  my @cats = @{$tok->{w}};
  my $cat = join('+',map {$_->{cat}} @cats);
  $cat ||= '?';
  my $lemma = join('+',map {$_->{lemma}} @{$tok->{w}});
  my $gtype = join('+',map {$_->{gtype}} @{$tok->{w}});
  my $form = $tok->{w}[0]{form};
  if (defined $form && exists $forms{$form}) {
    $form = quote($form);
    $form = "$form";
  } else {
    $form = '$$';
  }
  return [$form,"$cat"];
}

sub quote {
  my $s = shift;
##  $s =~ s/'/\\'/og;
  return $s;
}
