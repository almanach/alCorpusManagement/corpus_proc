#!/usr/bin/perl

# to build a confusion matrix from a filtered reltokens.log

## example: 
## matrice_builder.pl -p ' form:^que$ '

use strict;
use File::Basename;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "pattern|p=s@" => {DEFAULT => []},
			    "diff=s",
			    "reflog=f",
			    "hyplog=f",
			    "html=f" => {DEFAULT => "filtered.html"},
			    "difflog=f" => {DEFAULT => "diffreltokens.log"}
                           );

$config->args();

my $file = shift || 'reltokens.log';
my $html = $config->html;
my $diff = find_diff_file();

my %map = (
	   'mode' => '$mode',
	   'cat' => '$cat',
	   'form' => '$form',
	   'hyp' => '$thyp',
	   'ref' => '$tref',
	   'id' => '$id',
	   type => '$type',
	   refgov => '$refgov',
	   hypgov => '$hypgov'
	  );

my @gtypes = qw/GN GA GR GP NV PV/;


my @allreltypes = (
		   'missing',
		   'SUJ-V',
		   'AUX-V',
		   'COD-V',
		   'CPL-V',
		   'MOD-V',
		   'COMP',
		   'ATB-SO',
		   'MOD-N',
		   'MOD-A',
		   'MOD-R',
		   'MOD-P',
		   'COORD-G',
		   'COORD-D',
		   'APPOS',
		   'JUXT'
		  );

my %reltypes = ();
my %gtype = ();

my $mapexp = qr{match|mismatch|sametype|samehead|onlyref|onlyhyp|missing}o;

my @patterns = map {compile_pattern($_)} @{$config->pattern};

## print "PATTERN @patterns\n";
print "diff file=$diff\n" if ($diff);

if ($diff) {
  my $difflog = $config->difflog;
  open(DIFF,">",$difflog) || die "can't open '$difflog':$!";
  my $current = filter($file);
  my $tmp = {};
  foreach my $entry (@$current) {
    chomp $entry;
    $entry =~ /^(\S+)\s/;
    my $key = $1;
    $key =~ s/(E\d+)\.\d+/$1/;
    $tmp->{$key} ||= [];
    push(@{$tmp->{$key}},$entry);
  }
  my $old = filter($diff,$tmp);
  my $seen_and_keep = {}; 
  my $added = {};
  foreach my $entry (@$old) {
    chomp $entry;
    $entry =~ /^(\S+)\s/;
    my $key = $1;
    $key =~ s/(E\d+)\.\d+/$1/;
    ##    print "-- key='$key' entry=$entry\n\ttmp=$tmp->{$key}\n";
    my $new;
    $tmp->{$key} ||= [];
    my @news = @{$tmp->{$key}};
    ## we check if $entry is present in @news
    ## if not, we take the first unseen in @news
    foreach my $x (@news) {
      next if $seen_and_keep->{$x};
      $new = $x, last if ($x eq $entry);
      $new ||= $x;
    }
    ## if none,  we add a new one in @news and select it
    unless ($new) {
      $new = $entry;
      $new =~ s/($mapexp)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)$/onlyref $2 missing $4 0 $6 */o;
      push(@{$tmp->{$key}},$new);
      @news = @{$tmp->{$key}};
    }
    ## we mark $new as seen
    $seen_and_keep->{$new} = 1;
    my $ok_old = ok($entry);
    my $ok_new = ok($new);
    ## we are only interested in status changes, some positive, some negative 
    if ($ok_old eq $ok_new) {
      @news = grep {$_ ne $new} @news;
      $tmp->{$key} = [@news];
      delete $tmp->{$key} unless (@news);
      next;
    }
    print DIFF "$new --- $entry\n";
  }
  my @table0 = map {@$_} values %$tmp;
  print "Table0 size: ",scalar(@table0),"\n";
  if (1) {
    ## look at the new entries that not associated with old ones
    my $added=0;
    foreach my $key (keys %$tmp) {
      my @news = ();
      foreach my $new (@{$tmp->{$key}}) {
	if ($seen_and_keep->{$new}) {
	  ## this entry has been mapped with an old one: we keep it
	  push(@news,$new);
	  next;
	}
	## this entry was not mapped with an old one
	## don't try to correlate if onlyref
	next if ($new =~ / onlyref /);
	## otherwise, add a pseudo-old entry
	$seen_and_keep->{$new} = 1;
	my $old = $new;
	$old =~ s/($mapexp)\s+(\S+?)\s+(\S+?)\s+(E\d+F\d+|0)\s+(E\d+F\d+|0)\s+(\S+)\s+(\S+)$/onlyref $2 missing $4 0 $6 */o;
	my $ok_old = ok($old);
	my $ok_new = ok($new);
	push(@news,$new);
	$added++;
	print DIFF "$new --- $old\n";
      }
      if (@news) {
	$tmp->{$key} = [@news];
      } else {
	delete $tmp->{$key};
      }
    }
    print "Added $added pseudo old\n";
  }
  close(DIFF);
  my @table = map {@$_} values %$tmp;
  print "Table size: ",scalar(@table),"\n";
  emit_table([ @table  ]);
} else {
  emit_table(filter($file));
}

sub ok {
  my $line = shift;
  return ($line =~ /\s+match\s+(\S+)\s+\1\s+/) ? 'ok' : 'no';
}

sub filter {
  my $file = shift;
  my $keep = shift || {};
  my @pending = ();
  my @tmp = @patterns;
  my @results = ();
  open(FILE,"<$file") || die "can't open $file: $!";
  while (my $line = <FILE>) {
    next unless ($line =~ / \#\#\#\s+(full|robust|fail|corrected)/);
  TEST: 
    if (@tmp) {
      if ($tmp[0]->($line)) {
	shift @tmp;
	push(@pending,$line);
	unless (@tmp) {
	  push(@results,@pending);
	  @pending = ();
	  @tmp = @patterns;
	}
      } else {
	if ($keep 
	    && $line =~ /^(\S+)\s/
	    && $keep->{$1}
	    && @{$keep->{$1}}
	   ) {
	  push(@results,$line);
	}
	if (@pending) {
	  @pending = ();
	  @tmp = @patterns;
	  goto TEST;
	}
      }
    }
  }
  print "Filter ".scalar(@results)."\n";
  return \@results;
}

sub compile_pattern {
  my $s = shift;
  $s =~ s/^\s+//o;
  $s =~ s/\s+$//o;
  my $pattern = <<EOF;
sub {
  my \$line = shift;
  my (\$id,\$mode,\$form,\$cat,\$type,\$tref,\$thyp,\$refgov,\$hypgov) = 
                    \$line =~ m{^(\\S+F\\d+)\\s+\\#{3}\\s+(robust|full|fail)\\s+
                                        (.+?)\\s+(\\S+)\\s+
                                       ($mapexp)\\s+(\\S+)\\s+(\\S+)\\s+
                                       (?:\\S+)\\s+(?:\\S+)\\s+
                                      (\\S+)\\s+(\\S+)
                                     }xo;
EOF
  my @tests = split(/\s+/,$s);
  foreach my $test (@tests) {
    my ($f,$t) = split(/[:=]/,$test);
##    print "test f='$f' t='$t'\n";
    if ($f eq 'form') {
      $t =~ s/_/ /og;
      $pattern .= "\n\t(\$form =~ /$t/) or return 0;";
    } elsif (($f eq 'hyp') && $t eq 'mismatch') {
      $pattern .= "\n\t(\$tref ne \$thyp) or return 0;";
    } elsif (($f eq 'hyp') && $t eq 'match') {
      $pattern .= "\n\t(\$tref eq \$thyp) or return 0;";
    } else {
      $pattern .= "\n\t($map{$f} =~ /$t/) or return 0;";
    }
  }
  $pattern .= "\n\treturn 1; }";
  my $fun = eval($pattern);
#  print "pat=$pattern fun=$fun\n";
  return $fun;
}

sub emit_table {
  ## build the matrix
  my $lines = shift;
  my $table = {};
  my $col = {};
  my $row = {};
  my $keep = {};
    foreach my $line (@$lines) {
    my ($type,$tref,$thyp) = 
      $line =~ /(?:robust|full|fail|corrected)\s+(?:.+?)\s+(?:\S+)\s+($mapexp)\s+(\S+)\s+(\S+)/o;
    $table->{$tref}{$thyp}{$type}++;
    $col->{$thyp}++;
    $row->{$tref}++;
    $keep->{$thyp}++;
    $keep->{$tref}++;
    $reltypes{$tref}++;
    $reltypes{$thyp}++;
  }
  foreach my $k (keys %$keep) {
    print "type $k $keep->{$k}\n";
  }
  ## emit the matrix
  my @pat = @{$config->pattern};
  open(HTML,">$html") || die "can't open $html: $!";
  print HTML header,
    start_html('EASy filtered confusion table'),
      h1("Filtered confusion table for '@pat'");

  if ($diff) {
    print HTML  p("differential bewteen with $diff");
  }

  my @reltypes = ();
  @allreltypes = sort {$reltypes{$b} <=> $reltypes{$a}} keys %reltypes;

  foreach my $r (@allreltypes) {
    next unless ($keep->{$r});
    push(@reltypes,$r);
  }

  my @coltypes = grep {$col->{$_}} @allreltypes;
  my @rowtypes = grep {$row->{$_}} @allreltypes;

  my $tb = [th(['classified as =>', @coltypes,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  my $tok = 0;
  my $oksum = {};

  my $rtotal = {};
  foreach my $r (@coltypes) {
    foreach my $t (@rowtypes) {
      my $info = $table->{$t}{$r} || {};
      foreach my $map (keys %$info) {
	$rtotal->{$r} += $info->{$map};
      }
    }
  }

  foreach my $t (@rowtypes) {
    next unless ($keep->{$t});
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    my $ok = 0;
    foreach my $r (@coltypes) {
      next unless ($keep->{$r});
      foreach my $map (keys %{$info->{$r}}) {
	$total += $info->{$r}{$map} || 0;
	$sum->{$r} += $info->{$r}{$map} || 0;
      }
    }
    foreach my $r (@coltypes) {
      next unless ($keep->{$r});
      my $n = $info->{$r} || 0;
      my @y = ();
      ##      my $total = 0;
      foreach my $map (keys %{$info->{$r}}) {
	my $nn = $info->{$r}{$map};
	my $ratio = perc($nn,$total);
	my $prec = perc($nn,$rtotal->{$r});
	my $n = "$map:$nn (r=$ratio p=$prec)";
##      my $x = perc($n,$total);
	if (($r eq $t) && ($map eq 'match')) {
	  $ok += $nn;
	  $oksum->{$r} += $nn;
	  my $fm = ($total + $rtotal->{$r}) ? 2 * $nn / ($total + $rtotal->{$r}) : 1;
##	  if ($diff && $total && ($nn/$total > 0.5) && $rtotal->{$r} && ($nn/$rtotal->{$r} > 0.5)) {
	  if ($diff && ($fm >= 0.5)) {
	    $n = "<span style='color: green;'>$n</span>" ;
	  } else {
	    $n = "<span style='color: red;'>$n</span>" ;
	  }
	}
	$n = "<span style='background-color: blue;'>$n</span>" if ($n > 150 && $r ne $t);
	if (0 && $n == 0 && ($r ne $t)) {
	  $n = "" ;
	} 
	push(@y,"$n");
      }
      push(@x,"<span title='$t $r'>".join('<br/>',@y)."</span>");
    }
    push(@$tb,td([$t,@x,"$total ($ok)",$t]));
    $ttotal += $total;
    $tok += $ok;
  }
  my @x = ('total');
  my $rtotal = 0;
  my $rok = 0;

  foreach my $r (@coltypes) {
    next unless ($keep->{$r});
    $oksum->{$r} ||= 0;
    push(@x,"$sum->{$r} ($oksum->{$r})");
    $rtotal += $sum->{$r};
    $rok += $oksum->{$r};
  }
  push(@x,"$rtotal/$ttotal ($tok/$rok)");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['classified as =>', @coltypes,'total','']));
  print HTML table({-border => 1}, Tr($tb));

  print HTML end_html;
  close(HTML);
}

sub perc {
  my ($a,$b) = @_;
  return '-' if ($b == 0);
  return sprintf("%.2f%%",$a / $b * 100);
}

sub find_diff_file {
  my $diff = $config->diff;
  return unless ($diff);	# we are running for a diff
  if ($diff eq 'last') {
    ## try tp find last run for a diff
    my $where = `pwd`;
    chomp $where;
    $where =~ /(\d+)$/;
    my $current = $1;
    my $prev = $current-1;
    $diff = "../results.easyref$prev/$file";
  } elsif ($diff =~ /^\d+$/) {
    $diff = "../results.easyref$diff/$file";
  } elsif (-d $diff) {
    $diff .= "/$file";
  }
  return $diff if (-r $diff);
}
