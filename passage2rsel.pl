#!/usr/bin/perl

## try to extract restriction selections from passage files

use strict;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    {CREATE => 1}
			   );

$config->args();

my @files = @ARGV;

## table of restr of the form (lemmaSource,prep,lemmaTarget)
my $restr = {};

my $dico = {};
my $rdico = {};
my $n = 0;

foreach my $file (@files) {
  process($file);
}

sub process {
  my $file = shift;
  print "Processing $file\n";
  if ($file =~ /\.bz2$/) {
    unless (open(FILE,"bunzip2 -c $file|")) {
      warn "can't process $file: $!";
      return;
    }
  } elsif ($file =~ /\.gz$/) {
    unless (open(FILE,"gunzip -c $file|")) {
      warn "can't process $file: $!";
      return;
    }
  } else {
    unless (open(FILE,"<$file")) {
      warn "can't process $file: $!";
      return;
    }
  }
  # list of potential targets (within GP and PV, keep the prep)
  # the list is reset for each sentence
  my $ws = {};	
  my $preps = {};			# for reach GP or PV, keep prep
  my $current ;				# current group
  my $rel;				# current relation
  while (my $line = <FILE>) {
    ##    print $line;
    if ($line =~ /pos="preposition"/ 
	|| $line =~ /pos="prep"/ 
       ) {
      $line =~ /lemma="(.+?)"/;
      $preps->{$current} = $1;
      next;
    }
    if ($line =~ m{<G.*type="(PV|GP)"} ) {
      $line =~ m{id="(.+?)"};
      $current = $1;
      next;
    }
    if ($current && $line =~ m{</G>}) {
      $current = undef;
      next;
    }
    if ($line =~ m{<W.*id="(.+?)"}) {
      my $wid = $1;
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      $ws->{$wid} = { 
		     lemma => $lemma,
		     pos => $pos,
		     group => $current
		    };
      next;
    }
    if ($line =~ m{<R.*id="(.+?)"}) {
      $rel = $1;
      next;
    }
    if ($line =~ m{</R>}) {
      $rel = undef;
      next;
    }
    if ($rel && $line =~ m{<(?:complement|modifieur).*ref="(.+?)"} && $ws->{$1}{group}) {
      my $tid = $1;
      $line = <FILE>;
      $line =~ /ref="(.+?)"/;	# governor
      my $sid = $1;
      my $tlemma = $ws->{$tid}{lemma};
      my $tpos = $ws->{$tid}{pos};
      my $slemma = $ws->{$sid}{lemma};
      my $spos = $ws->{$sid}{pos};
      my $prep = $preps->{$ws->{$tid}{group}};
      $restr->{dico_id($slemma,$spos)}{dico_id($prep,'prep')}{dico_id($tlemma,$tpos)}++;
      print <<EOF;
$slemma\t$spos\t$prep\t$tlemma\t$tpos
EOF
      next;
    }
    if ($line =~ m{</Sentence>}) {
      $ws = {};
      $preps = {};
      $current = undef;
      $rel = undef;
    }
  } 
  close(FILE);
}

sub dico_id {
  my $v = shift;
  if (my $x = $dico->{$v}) {
    return $x;
  } else {
    my $x = $n++;
    $dico->{$v} = $x;
    $rdico->{$x} = $v;
    return $x;
  }
}
