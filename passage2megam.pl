#!/usr/bin/perl

use strict;
use XML::Twig;
use File::Basename;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "ref|r=f"  => {DEFAULT => "/Users/clerger/Work/EASy/ref290109"},
			    "freqform=f" => {DEFAULT => "concat1/freqform.txt"},
			    "freqth=i" => {DEFAULT => 50},
                           );

$config->args();

##print "Handling $corpus\n";
my $refdir = $config->ref;

my $freqform = $config->freqform;
my $freqth=$config->freqth;
my %forms=();
my $corpus;

my @files = @ARGV;

open(FORM,"<$freqform") || die "can't open $freqform:$!";
while(<FORM>) {
  chomp;
  my ($f,$form) = /(\d+)\s+(.*)/;
  next unless ($f > 100);
  $forms{$form} = $f;
}
close(FORM);

my @gtypes = qw/GN GA GR GP NV PV/;
my @allgtypes = ('?','void');

foreach my $gt (@gtypes) {
  push(@allgtypes,$gt,"B_$gt","E_$gt","BE_$gt");
}

my %gtypes = ();
my $i=0;

foreach my $gt (@allgtypes) {
  $gtypes{$gt} = $i++;
}

my $refinfo = {};

foreach my $file (@files) {
  $corpus = fileparse($file,qr{.xml});
  my $ref = "$refdir/$corpus.xml";
  process_ref_corpus($ref);
  process_hyp_corpus($file);
}

sub process_ref_corpus {
  my $ref = shift;
  $refinfo = {};

  my $refxml = XML::Twig->new(
			      twig_handlers => {
						'E' => \&ref_sentence_handler
					       }
			     );
  
  $refxml->parsefile($ref);
  $refxml->purge;
}

sub ref_sentence_handler {
  my ($t,$s) = @_;
  foreach my $w ($s->descendants('F')) {
    my $txt = $w->trimmed_text();
    my $group = $w->parent('Groupe');
    my $gtype = 'void';
    my $id = $w->id;
    if (defined $group) {
      my $first = $group->first_child('F')->id;
      my $last = $group->last_child('F')->id;
      my $prefix = "";
      $prefix .= "B" if ($first eq $id);
      $prefix .= "E" if ($last eq $id);
      $prefix .= "_" if ($prefix);
      $gtype = $prefix.$group->att('type');
    }
    $refinfo->{$w->id} = { form => $txt, 
			   gtype => $gtype };
  }
  $t->purge;
}


sub process_hyp_corpus {
  my $file = shift;
  my $xml = XML::Twig->new(
			   twig_handlers => {
					     'Sentence' => \&sentence_handler
					    }
			  );

  $xml->parsefile($file);
  $xml->purge;
}

sub sentence_handler {
  my ($t,$s) = @_;
  unless ($s->children('G')) {
    $t->purge;
    return;
  }
  my $sid = $s->id;
  my $mode = $s->att('mode');
  my $tokens = {};
  my @tokens = ();
  my $forms = {};
  my $groups = {};
  foreach my $tok ($s->children('T')) {
    my $refid = $tok->id;
    $refid =~ s/T/F/;
    $refid =~ s/\.\d+//;
    my $ref = $refinfo->{$refid};
    my $tt = $tokens->{$tok->id} 
      = { form => quote($ref->{form} || $tok->trimmed_text()), 
	  w => [],
	  id => "${corpus}.$refid",
	  gtype => $ref->{gtype} || '?'
	};
    push(@tokens,$tt);
  }
  foreach my $w ($s->descendants('W')) {
    my @toks = split(/\s+/,$w->att('tokens'));
    my $group = $w->parent('G');
    my $gtype = $group ? $group->{type} : 'void';
    my $pos = $w->att('pos');
    if (defined $pos && ($pos eq 'v')) {
      if (my $mstag = $w->att('mstag')) {
	$mstag =~ /mode\.(\S+)/;
	my $mode = $1;
	if ($mode && grep {$_ eq $mode} qw/participle infinitive gerundive/) {
	  $pos .= ".$mode";
	}
      }
    }
    my $info = { cat => $pos || '?',
		 gtype => $gtype,
		 lemma => quote($w->att('lemma') || '?'),
		 form => $w->att('form') || ""
	       };
    foreach my $tid (@toks) {
      my $tt = $tokens->{$tid};
      push(@{$tt->{w}}, $info) unless (@{$tt->{w}} == 2);
    }
  }
##  my $prevatts = ['^','start','void'];
  my $prevatts = ['^','start','void'];
  while (@tokens) {
    my $tok = shift @tokens;
    my $atts = get_atts($tok);
    my $gtype = $tok->{gtype};
    my $nextatts;
    if (@tokens) {
      $nextatts = get_atts($tokens[0]);
    } else {
##      $nextatts = ['$','end'];
      $nextatts = ['$','end'];
    }
    my $nnextatts;
    if (scalar(@tokens>1)) {
      $nnextatts = get_atts($tokens[1]);
    } else {
      $nnextatts = ['$','end'];
    }
    my @atts = (@$atts,@$prevatts,@$nextatts,@$nnextatts,$mode);
    my $ngtype = $gtypes{$gtype};
    my @all = ($ngtype || 0); ## WARNING
    foreach my $k (qw/form pos prev_form prev_pos prev_gtype next_form next_pos next_next_form next_next_pos mode/) {
      my $x = shift @atts;
      push(@all,"${k}_$x");
    }
    print join(' ',@all),"\n";
    $prevatts = $atts;
    push(@$prevatts,$gtype);
  }
  $t->purge;
}

sub get_atts {
  my $tok = shift;
  my @cats = @{$tok->{w}};
  my $cat = join('+',map {$_->{cat}} @cats);
  $cat ||= '?';
  my $lemma = join('+',map {$_->{lemma}} @{$tok->{w}});
  my $gtype = join('+',map {$_->{gtype}} @{$tok->{w}});
  my $form = $tok->{w}[0]{form};
  if (defined $form && exists $forms{$form}) {
    $form = quote($form);
    $form = "$form";
  } else {
    $form = '$$';
  }
  return [$form,"$cat"];
}

sub quote {
  my $s = shift;
##  $s =~ s/'/\\'/og;
  return $s;
}
