#!/usr/bin/env perl

# return informations about time in logs

use strict;
use POSIX;
use Text::Table;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "robust!" => {DEFAULT => 0},
			    "ok!" => {DEFAULT => 0},
                           );

$config->args();

my $info = {};

sub register {
  my ($type,$time) = @_;
  my $x = $info->{$type} ||= { n => 0, all => 0, times => [], timeout => 0};
  $x->{n}++;
  $x->{all} += $time;
  push(@{$x->{times}},$time);
}

sub register_timeout {
  my $type = shift;
  $info->{$type}{timeout}++;
}

sub register_null {
  my $type = shift;
  $info->{$type}{null}++;
}


@ARGV = map {s/(.*\.bz2)$/bunzip2 -c $1 |/o; $_} grep {!/^-/} @ARGV;

##print "@ARGV\n";

sub keep_record {
  my $type = shift;
  if ($config->robust) {
    return $type eq 'robust';
  } elsif ($config->ok) {
    return $type eq 'ok';
  } else {
    return 1;
  }
}

my $active = 0;

while (<>){
  if (/^(ok|fail|robust)\s+(\d+(?:\.\d+)?)\s+(.*)/) {
    $active =  keep_record($1);
    $active or next;
    register( size => scalar(split(/\s+/,$3)) );
    register( $1 => 1);
  }
  if (/<ambiguity>\s+(-?\d+(?:\.\d+)?)/) {
    $active or next;
    register(amb=>$1);
    register(ambderiv=>$1) if /(\d+(?:\.\d+)?)\s+avgderivs/;
    register(amball=>$1) if /(\d+)\s+derivs/;
    next;
  }
  if (/<(\w+?)>\s+(\d+(?:\.\d+)?)(.*)/) {
    $active or next;
    my ($type,$t,$timeout) = ($1,$2,$3);
    next unless $type =~ /time/;
    register($type=>$t);
    register_timeout($type) if ($timeout =~ /timeout/);
    register_null($type) if ($timeout =~ /null\s+output/);
    next;
  }
}

sub analyze {
  my $x = shift;
  $x->{avg} = $x->{all}/$x->{n};
  $x->{times} = [sort {$b <=> $a} @{$x->{times}}];
  $x->{median} = $x->{times}[floor($x->{n}/2)];
  $x->{most} = $x->{times}[floor($x->{n}*0.01)];
  $x->{most90} = $x->{times}[floor($x->{n}*0.1)];
  $x->{most999} = $x->{times}[floor($x->{n}*0.001)];
  $x->{tx} = ($x->{timeout}/$x->{n})*100;
  $x->{nx} = ($x->{null}/$x->{n})*100;
}

foreach my $x (values %$info) {
  analyze($x);
}

sub display {
  my ($label,$x) = @_;
  printf "%10s: n=%d avg=%.2f all=%.1f max=%.2f median=%.2f 90=%.2f 99=%.2f 99.9=%.2f\n",
    $label, $x->{n},$x->{avg},$x->{all},$x->{times}[0],$x->{median},$x->{most90},$x->{most},$x->{most999};
}

if(0) {
  foreach my $k (qw/time size amb ambderiv amball ftime timeamb timexmldep timedepxml timeeasy timepassage timeconll timex time1 timeproc/) {
    my $v = $info->{$k};
    display($k,$v) if (defined $v);
  }
}

my $tb = Text::Table->new(qw/type n avg median %90 %99 %99.9 max all timeout null/);
foreach my $k (qw/time size amb ambderiv amball ftime timeamb timexmldep timedepxml timedis_xmldep timeeasy timepassage timeconll timex timefread time1 timeproc/){
  my $x = $info->{$k};
  next unless (defined $x);
  my @y = ($x->{avg},
	   $x->{median},
	  );
  my @x = ($x->{most90},
	   $x->{most},
	   $x->{most999},
	   $x->{times}[0],
	   $x->{all}
	  );
  $tb->load([ $k, 
	      $x->{n},
	      (map {sprintf("%.3f",$_)} @y),
	      (map {sprintf("%.2f",$_)} @x),
	      sprintf("%.2f%%",$x->{tx}),
	      sprintf("%.2f%%",$x->{nx}),
	    ]
	   );
}

my $all_n = $info->{size}{n};

foreach my $k (qw/ok robust fail/) {
  my $n = $info->{$k}{n};
  $tb->load([$k,
	     $all_n,
	     sprintf("%.1f%%",100*$n/($all_n || 1)),
	     "-", "-", "-", "-", "-",
	     $n,
	     "-", "-" ] );
}

print $tb;

if (exists $info->{timeproc}) {
  my $delta = $info->{timeproc}{all};
  foreach my $k (keys %$info) {
    next unless ($k =~ /time/);
    next if ($k eq 'timeproc');
    next if ($k eq 'time1');
    next if ($k eq 'timex');
    $delta -= $info->{$k}{all};
  }
  
  my $avgdelta = $delta / ($info->{timeproc}{n} || 1);
  print "Delta time: $delta  avg: $avgdelta\n";
}

if (exists $info->{time1}) {
  my $delta1 = $info->{time1}{all};
  foreach my $k (qw{time ftime}) {
    next unless (exists $info->{$k});
    $delta1 -= $info->{$k}{all};
  }

  my $avgdelta1 = $delta1 / ($info->{time1}{n} || 1);
  print "Delta time1: $delta1  avg: $avgdelta1\n";
}


if (exists $info->{timex}) {
  my $deltax = $info->{timex}{all};
  foreach my $k (qw{time}) {
    next unless (exists $info->{$k});
    $deltax -= $info->{$k}{all};
  }

  my $avgdeltax = $deltax / ($info->{timex}{n} || 1);
  print "Delta timex: $deltax  avg: $avgdeltax\n";
}

__END__
