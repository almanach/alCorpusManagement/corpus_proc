#!/usr/bin/env perl

use strict;
use Carp;
use DBI;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    { CREATE => 1},
                            "verbose!"        => {DEFAULT => 1},
                            "corpus=f@"       => {DEFAULT => []},
                            "run!"            => {DEFAULT => 1},
                            "lock=f"          => {DEFAULT => 'lock'},
                            "results=f"       => {DEFAULT => 'results'},
                            "callparser=f"    => {DEFAULT => ''},
                            "parse_options=s" => {DEFAULT => ''},
                            "key=s"           => {DEFAULT => ''},
                            "host=s@"         => {DEFAULT => []},
                            "hostallow!"      => {DEFAULT => 1},
			    "mafdocument!"    => {DEFAULT => 0},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "dagdir=f"        => {DEFAULT => ''},
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my %status = ( unparsed => -1,
	       ok       => 1,
	       fail     => 0,
	       robust => 2,
	       corrected => 3,
	     );

my $results = $config->results;
my $verbose = $config->verbose;
my @corpus  = @{$config->corpus};
my $dag     = $config->dagdir;
my $dagext  = $config->dagext;


unless (@corpus) {
  my ($info) = grep {-r $_} map {"$_/info.txt"} ($results,$dag);
  if (!-r $info) {
    die "can't find info.txt in $dag or locally: $!";
  }
  
  open(INFO,"$info") or die "can't open $info.txt: $!";
  
  while(<INFO>){
    chomp;
    /##/ and next;
    my ($corpus,$size) = split(/\s+/,$_);
    push(@corpus,$corpus);
  }
  close(INFO);
}


create_table();

my $dbh = DBI->connect("dbi:SQLite:$results/dblog.dat", "", "",
		       {RaiseError => 1, AutoCommit => 0});

my $sth=$dbh->prepare(<<EOF);
UPDATE log set 
      status = ? , 
      time = ? , 
      timefread = ? , 
      timeamb = ? , 
      timexmldep = ? , 
      timepassage = ? , 
      timeout = ? , 
      ambiguity=? , 
      host= ? WHERE corpus = ? AND id = ?
EOF
foreach my $corpus (@corpus) {
  my $log = "$results/$corpus.log";
  if (-r "$log.bz2") {
    open(LOG,"bzcat $log.bz2|") or die "can't open log file: $!";
  } elsif (-r $log) {
    open(LOG,"<$log") or die "can't open log file: $!";
  } else {
    next;
  }
  my $id;
  my $status;
  my $time;
  my $ambiguity;
  my $host;
  my $timeout=0;
  my $timepassage=0;
  my $timefread=0;
  my $timeamb=0;
  my $timexmldep=0;
  while (<LOG>) {
    if (/^(fail|ok|robust|corrected)\s+(\d+(?:\.\d+)?)/) {
      $id and 
	$sth->execute($status,
		      $time || 'NULL',
		      $timefread || 'NULL',
		      $timeamb || 'NULL',
		      $timexmldep || 'NULL',
		      $timepassage || 'NULL',
		      $timeout,
		      $ambiguity || 'NULL',
		      $host,
		      $corpus,
		      $id
		     );
      $id        = $2;
      $status    = $status{$1};
      $time      = undef;
      $timepassage = undef;
      $timefread = undef;
      $timeamb = undef;
      $timexmldep = undef;
      $ambiguity = undef;
      $host      = undef;
      $timeout   = 0;
      next;
    }
    $host = $1, next if (/^\s*<host>\s+(\S+)/);
    if (/^\s*<time>\s+(\S+)/) {
      $time    = $1;
      $timeout = 1 if /timeout/;
      next;
    }
    $ambiguity = $1, next if (/^\s*<ambiguity>\s+(\S+)/);
    /<timepassage>\s+(\S+)/ and $timepassage=$1;
    /<timefread>\s+(\S+)/ and $timefread=$1;
    /<timeamb>\s+(\S+)/ and $timeamb=$1;
    /<timexmldep>\s+(\S+)/ and $timexmldep=$1;
  }
  $id and $sth->execute($status,
			$time || 'NULL',
			$timefread || 'NULL',
			$timeamb || 'NULL',
			$timexmldep || 'NULL',
			$timepassage || 'NULL',
			$timeout,
			$ambiguity || 'NULL',
			$host,$corpus,$id);
  $dbh->commit;
  close(LOG);
}

$dbh->commit;
$dbh->disconnect;

## PRAGMA default_synchronous = OFF

sub create_table {
  return if (-e "$results/dblog.dat") ;
  my $dbh = DBI->connect("dbi:SQLite:$results/dblog.dat", "", "",
			 {RaiseError => 1, AutoCommit => 0});
    # (re)create it
    $dbh->do(<<EOF );
CREATE TABLE log (corpus TEXT,
                  id INTEGER,
                  status INTEGER,
                  length INTEGER,
                  edges INTEGER,
                  time FLOAT,
                  timepassage FLOAT,
                  timefread FLOAT,
                  timeamb FLOAT,
                  timexmldep FLOAT,
                  timeout INTEGER,
                  ambiguity FLOAT,
                  host TEXT,
                  PRIMARY KEY(corpus,id), 
                  UNIQUE(corpus,id)
)
EOF

  # insert initial values
  my $sth=$dbh->prepare('INSERT INTO log(corpus,id,status,length,edges) VALUES (?,?,?,?,?)');
  foreach my $corpus (@corpus) {
    ##    print "Processing DAG '$dag/$corpus.$dagext.bz2'\n";
    if (-f "$dag/$corpus.$dagext.bz2") {
      open(DAG,"bzcat $dag/$corpus.$dagext.bz2|") or die "can't open file: $!";
    } else {
      open(DAG,"<$dag/$corpus.$dagext") or die "can't open file: $!";
    }
    my $sid;
    my $n     = 0;
    my $edges = 0;
    my $lastsid;
    while (<DAG>) {
      if (/^##\s*DAG\s+END/io){
	## insert table
	$sth->execute("$corpus",$sid,$status{'unparsed'},$n,$edges);
	next;
      }
      if (/^##\s*DAG\s+BEGIN/io) {
	<DAG> =~ /id="E(\d+)F\d+"/;
	$lastsid = $sid;
	$sid     = $1;
	if (defined $lastsid && $sid == $lastsid) {
	  print STDERR "Warning: duplicate $corpus $sid\n";
	  $sid++;
	}
	$n = 0;
	$edges = 0;
      }
      my ($tmp) = /id="E\d+F(\d+)"/;
      $edges++;
      $n = $tmp if ($tmp > $n);
    }
    close(DAG);
    $dbh->commit;
  }
  $dbh->disconnect;
}


__END__

# quietly drop the table if it already existed
eval {
  local $dbh->{PrintError} = 0;
  $dbh->do("DROP TABLE log");
};


=head1 NAME

log2db.pl -- Loading a log file inside a database to compute more easily statistics

=head1 SYNOPSIS 	 
  	 
./log2db.pl

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
