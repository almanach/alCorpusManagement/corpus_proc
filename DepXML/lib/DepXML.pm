package DepXML;

##use 5.010001;
use strict;
use warnings;
use DepXML::Node;
use DepXML::Edge;
use DepXML::Cluster;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use DepXML ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';


our $twig;

sub new {
  my ($this) = @_;
  my $class = ref($this) || $this;

  $twig = XML::Twig->new(
			 pretty_print => 'indented',
			);
  my $self = { 
	      ids => {},
	      derivs => {},
	      nodes => {},
	      edges => {},
	      words => {},
	      clusters => {},
	      xml => $twig 
	     };

  bless $self, $class;

  ##  $xml->purge;
  $twig->purge;
#  $twig->setTwigHandler('dependencies' => sub { $self->dep_handler($_) });
##  $twig->setTwigHandler('node' => sub { $self->word_index($_) });
  
  return $self;
}

sub reset {
  undef $twig;
}

sub word_index {
  my ($self,$node) = @_;
  $self->{words}{$node->att('lemma')} ||= 1;
}

sub activate {
  my $self = shift;
  $self->dep_handler($self->{xml}->root);
}

sub dep_handler {
  my ($self,$dep) = @_;
  my @nodes = ();
  foreach my $e ($dep->children) {
    my $name = $e->name;
    my $id = $e->att('id');
    if ($name eq 'node') {
      DepXML::Node->new($e,$self);
      push(@nodes,$e);
      if ($e->att('deriv')) {
	foreach my $deriv (split(/\s+/,$e->att('deriv'))) {
	  $self->register_deriv($deriv,node => $id);
	}
      }
    } elsif ($name eq 'edge') {
      DepXML::Edge->new($e,$self);
    } elsif ($name eq 'op') {
      my $span = [split(/\s+/,$e->att('span'))];
      foreach my $deriv (split(/\s+/,$e->att('deriv'))) {
	$self->register_deriv($deriv, op => $id);
	$self->register_deriv($deriv, op => $span);
      }
    } elsif ($name eq 'hypertag') {
      foreach my $deriv (split(/\s+/,$e->att('derivs'))) {
	$self->register_deriv($deriv, ht => $id);
      }
    } elsif ($name eq 'cluster') {
      DepXML::Cluster->new($e,$self);
    }
  }
  
  if (0) {
    @nodes = map {$_->[0]}
      sort {$a->[2] <=> $b->[2] || $a->[3] <=> $b->[3] }
	map { [$_,split(/_/,$_->att('cluster'))] }
	  @nodes;
    my $prev;
    foreach my $current (@nodes) {
      if ($prev) {
	$prev->set_att('#lnext' => $current->att('id'));
	$current->set_att('#lprev' => $prev->att('id'));
      }
	$prev = $current;
    }
  }
  
}

sub parse {
  my ($self,$file) = @_;
  $self->xml->parse($file);
  $self->dep_handler($self->{xml}->root);
}

sub log {
  my $self  = shift;
  my $level = shift;
  my $msg   = shift;
  warn "$level: $msg";
}

sub xml {
  shift->{xml};
}

sub root {
  shift->{xml}->root;
}

sub register {
  my $self = shift;
  my $o = shift;
  $o->add_id;
  $self->{ids}{$o->att('id')} = $o;
}

sub id2obj {
  my $self = shift;
  my $id = shift;
  return $self->{ids}{$id};
}

sub register_deriv {
  my $self = shift;
  my $deriv = shift;
  my $key = shift;
  my $v = shift;
  my $derivs = $self->{derivs} ||= {};
  my $info = $derivs->{$deriv} ||= { id => $deriv };
  $info->{$key} = $v;
}

sub deriv2node {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{node}) {
    my $nid = $info->{node};
    return $nid, $self->id2obj($nid);
  }
}

sub deriv2span {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{span}) {
    return $info->{span};
  }
}

sub deriv2op {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{op}) {
    my $oid = $info->{op};
    return $oid, $self->id2obj($oid);
  }
}

sub deriv2ht {
  my $self = shift;
  my $deriv = shift;
  my $info = $self->{derivs}{$deriv};
  if ($info && exists $info->{ht}) {
    my $hid = $info->{ht};
    return $hid, $self->id2obj($hid);
  }
}

sub node2op {
  my $self = shift;
  my $node = shift;             # id or XML
  $node = $self->id2obj($node) if (ref($node) eq 'SCALAR');
  my $deriv = $node->att('deriv');
  return $deriv && $self->deriv2op($deriv);
}

sub node2ht {
  my $self = shift;
  my $node = shift;             # id or XML
  $node = $self->id2obj($node) if (ref($node) eq 'SCALAR');
  my $deriv = $node->att('deriv');
  return $deriv && $self->deriv2ht($deriv);
}

sub xnode {
  my $self = shift;
  my $node = shift;             # id or XML
  my $nid = (ref($node) eq 'SCALAR') ? $node : $node->att('id');
  return $self->{nodes}{$nid};
}

sub xedge {
  my $self = shift;
  my $edge = shift;             # id or XML
  my $eid = (ref($edge) eq 'SCALAR') ? $edge : $edge->att('id');
  return $self->{edges}{$eid};
}

sub xcluster {
  my $self = shift;
  my $cluster = shift;             # id or XML
  my $cid = (ref($cluster) eq 'SCALAR') ? $cluster : $cluster->att('id');
  return $self->{clusters}{$cid};
}


sub find_nodes {
  my $self = shift;
  my $cond = shift;             # sub {}
  my @nodes = ();
  while (my ($k,$n) = each %{$self->{nodes}}) {
    push(@nodes,$n) if ($cond->($n));
  }
  return @nodes;
}

sub find_edges {
  my $self = shift;
  my $cond = shift;             # sub {}
  my @edges = ();
  while (my ($k,$e) = each %{$self->{edges}}) {
    push(@edges,$e) if ($cond->($e));
  }
  return @edges;
}

sub has_lemma {
  my ($self,$lemma) = @_;
  return exists $self->{words}{$lemma};
}

# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

DepXML - Perl extension for dealing with DepXML files, in particular
with a small query language

=head1 SYNOPSIS

  use DepXML;
  blah blah blah

=head1 DESCRIPTION


=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 AUTHOR

Eric de la Clergerie E<lt>Eric.de_la_clergerie@inria.frE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by INRIA

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.1 or,
at your option, any later version of Perl 5 you may have available.

=cut
