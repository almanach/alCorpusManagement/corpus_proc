package DepXML::Edge;
use Scalar::Util qw{weaken};

sub new {
  my ($this,$xml,$cas) = @_;
  my $class = ref($this) || $this;
  my $id = $xml->att('id');
  my $self = { 
	      id => $id,
	      cas => $cas,
	      xml => $xml,
	      source => $cas->{ids}{$xml->att('source')},
	      target => $cas->{ids}{$xml->att('target')},
	     };
  bless $self, $class;
  $cas->{ids}{$id} = $self;
  $cas->{edges}{$id} = $self;
  weaken($self->{cas});
  weaken($self->{xml});
  if (defined $self->{source}) {
    push(@{$self->{source}{out}},$self);
    weaken($self->{source});
  }
  if (defined $self->{target}) {
    push(@{$self->{target}{in}},$self);
    weaken($self->{target});
  }
  return $self;
}

sub apply {
  my $self = shift;
  my $query = shift;
  $query->($self);
}

foreach my $x (qw{id cas xml source target}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      return shift->{$x};
    }
  }
}

sub deriv {
  my $self = shift;
  unless (exists $self->{deriv}) {
    foreach my $deriv ($self->{xml}->children('deriv')) {
      my @derivs = split(/\s+/,$deriv->att('names'));
      if (@derivs) {
      $self->{deriv} = [@derivs];
    } elsif ($self->{source}) {
      $self->{deriv} = $self->{source}{deriv};
    } else {
      $self->{deriv} = [];	# should not arise
    }
    }
  }
  return $self->{deriv};
}

foreach my $x (qw{label type}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      my $self = shift;
      return $self->{$x} ||= $self->{xml}->att($x);
    }
  }
}

sub sprint {
  my $self = shift;
  my $source = $self->source->sprint;
  my $target = $self->target->sprint;
  my $label = $self->label;
  my $type = $self->type;
  my $id = $self->id;
  return "$source --[$id/$label/$type]--> $target";
}

foreach my $type (qw/subst adj lexical skip/) {
  no strict 'refs';
  *{__PACKAGE__."::is_$type"} = sub {
    my $self = shift;
    my $ltype = $self->type;
    return $ltype && ($type eq $ltype);
  };
  *{__PACKAGE__."::is_not_$type"} = sub {
    my $self = shift;
    my $ltype = $self->type;
    return $ltype && ($type ne $ltype);
  };
}

foreach my $label (qw/subject
		      impsubj
		      object
		      xcomp
		      S
		      arg
		      preparg
		      scomp
		      comp
		      Infl
		      V
		      V1
		      v
		      vmod
		      Modifier
		      N2
		      N2app
		      Nc2
		      prep
		      csu
		      prel
		      pri
		      pro
		      Root
		      Punct
		      S2
		      SRel
		      SubS
		      varg
		      start
		      wh
		      starter
		      advneg
		      aux
		      ce
		      number
		      ncpred
		      CS
		      PP
		      void
		      coo
		      coo2
		      coord2
		      coord3
		      coord
		      np
		      incise
		      det
		      time_mod
		      quoted_S
		      audience
		      clneg
		     /) {
  no strict 'refs';
  *{__PACKAGE__."::is_$label"} = sub {
    my $self = shift;
    my $llabel = $self->label;
    return $llabel && ($label eq $llabel);
  }
}

1;
