package DepXML::Node;
use DepXML::Query::Compile;
use Scalar::Util qw{weaken};

sub new {
  my ($this,$xml,$cas) = @_;
  my $class = ref($this) || $this;
  my $id = $xml->att('id');
  my $self = { 
	      id => $id,
	      cas => $cas,
	      xml => $xml,
	      in => [],
	      out => [],
	     };
  bless $self, $class;
  $cas->{ids}{$id} = $self;
  ##  weaken($cas->{ids}{$id});
  $cas->{nodes}{$id} = $self;
  ## weaken($cas->{nodes}{$id});
  $cas->{words}{$xml->att('lemma')} ||= 1;
  weaken($self->{cas});
  weaken($self->{xml});
  return $self;
}

sub apply {
  my $self = shift;
  my $query = shift;
  $query->($self);
}

sub deriv {
  my $self = shift;
  return $self->{deriv} ||= [ split(/\s+/,$self->{xml}->att('deriv'))];
}

foreach my $x (qw{id cas xml}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      return shift->{$x};
    }
  }
}

foreach my $x (qw{lemma cat xcat form tree cluster}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      my $self = shift;
      return $self->{$x} ||= $self->{xml}->att($x);
    }
  }
}

foreach my $x (qw{active passive}) {
  { no strict 'refs';
    *{__PACKAGE__."::is_$x"} = sub {
      my $self = shift;
      my $tree = $self->tree;
      return $tree =~ /categorization_$x/;
    }
  }
}

foreach my $x (qw{in out}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      my $self = shift;
      return @{$self->{$x}};
    };
    *{__PACKAGE__."::all_$x"} = sub {
      my $self = shift;
      return @{$self->{$x}};
    };
  }
}

sub first_out {
  my $self = shift;
  return $self->{out}[0];
}

sub last_out {
  my $self = shift;
  return $self->{out}[-1];
}

sub is_root {
  !shift->in;
}

sub sprint {
  my $self = shift;
  my $id = $self->id;
  my $cat = $self->cat;
  my $form = $self->form;
  my $lemma = $self->lemma;
  return "$id/$cat/$lemma/$form";
}

sub parent_with_cond {
  my $self = shift;
  my $cond = shift || {};
  my $edge_cond = $cond->{edge} || sub { 1; };
  my $node_cond = $cond->{node} || sub { 1; };
  if (my $in=$self->in) {
    my $parent = $in->source;
    return $parent if ($edge_cond->($in) && $node_cond->($parent));
  }
}

sub children_with_cond {
  my $self = shift;
  my $cond = shift || {};		# undef or sub { }
  my $edge_cond = $cond->{edge};
  my $node_cond = $cond->{node};
  my @out = $self->all_out;
  if (ref($edge_cond) eq 'CODE') {
    @out = grep {$edge_cond->($_)} @out;
  }
  my @children = map {$_->target} @out;
  if (ref($node_cond) eq 'CODE') {
    @children = grep {$node_cond->($_)} @children;
  }
  return @children;
}

sub descendants_with_cond {
  my $self = shift;
  my $cond = shift || {};		# undef or sub { }
  my $edge_cond = $cond->{edge};
  my $node_cond = $cond->{node};
  my $last_cond = $cond->{last};
  my @descendants = $self->children($last_cond->{cond});
  foreach my $middle ($self->children($cond)) {
##    next if (grep {$_ eq $middle} @descendants); # should not occur
    push(@descendants,$middle->descendants($cond));
  }
  return @descendants;
}

sub self_and_descendants_with_cond {
  my $self = shift;
  my $cond = shift || {};		# undef or sub { }
  my $edge_cond = $cond->{edge};
  my $node_cond = $cond->{node};
  my $last_cond = $cond->{last} ||= {};
  my $last_node_cond = $last_cond->{node} ||= sub { 1; };
  my @descendants = ();
  push(@descendants,$self) if ($last_cond->{node}->($$self));
  push(@descendants,$self->descendants($cond));
  return @descendants;
}

sub first_ancestor_with_cond {
  my $self = shift;
  my $cond = shift || {};
  my $edge_cond = $cond->{edge} ||= sub { 1; };
  my $node_cond = $cond->{node} ||= sub { 1; };
  my $first_cond = $cond->{first};
  my $first_edge_cond = $first_cond->{edge} ||= sub { 1; };
  my $first_node_cond = $first_cond->{node} ||= sub { 1; };
  foreach my $in ($self->in) {
    my $parent = $in->source;
    if ($first_edge_cond->($in) && $first_node_cond->($parent)) {
      return $parent;
    } elsif ($edge_cond->($in) && $node_cond->($parent)) {
      $parent->first_ancestor($cond);
    }
  }
}

sub ancestors_with_cond {
  my $self = shift;
  my $cond = shift || {};
  my $edge_cond = $cond->{edge} ||= sub { 1; };
  my $node_cond = $cond->{node} ||= sub { 1; };
  my $first_cond = $cond->{first};
  my $first_edge_cond = $first_cond->{edge} ||= sub { 1; };
  my $first_node_cond = $first_cond->{node} ||= sub { 1; };
  my @ancestors = ();
  foreach  my $in ($self->in) {
    my $parent = $in->source;
    if ($first_edge_cond->($in) && $first_node_cond->($parent)) {
      push(@ancestors,$parent);
    } 
    if ($edge_cond->($in) && $node_cond->($parent)) {
      push(@ancestors,$parent->ancestors($cond));
    }
  }
  return @ancestors;
}

foreach my $cat (qw/v nc np prep aux csu coo S N2 V CS det pro prel pri incise epsilon adj adv advneg VMod/) {
  no strict 'refs';
  *{__PACKAGE__."::is_$cat"} = sub {
    my $self = shift;
    my $lcat = $self->cat;
    return $lcat && ($cat eq $lcat);
  };
  *{__PACKAGE__."::is_not_$cat"} = sub {
    my $self = shift;
    my $lcat = $self->cat;
    return $lcat && ($cat ne $lcat);
  };
}

foreach my $x (qw/left right fids/) {
  no strict 'refs';
  *{__PACKAGE__."::$x"} = sub {
    my $self = shift;
    my $cluster = $self->{cas}{clusters}{$self->cluster};
    return $cluster && $cluster->$x;
  }
}

sub is_empty {
  my $self = shift;
  my $form = $self->form;
  return !$form;
}

sub get_subject {
  (shift->apply( dpath all_out 
		( is_subject target
		  union is_Infl is_adj target is_aux get_subject )))[0];
}

sub arg_object {
  shift->apply(dpath all_out is_object target);
}

sub is_full_parse {
  my $self = shift;
  $self->{xml}->parent->att('mode') eq 'full';
}

sub parent {
  shift->apply(dpath in source);
}

sub children {
  shift->apply(dpath all_out target);
}

sub ancestors {
  shift->apply(dpath parent (parent)* );
}

sub descendants {
  shift->apply(dpath children (children)*);
}

1;
