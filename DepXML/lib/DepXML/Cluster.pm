package DepXML::Cluster;
use DepXML::Query::Compile;
use Scalar::Util qw{weaken};

sub new {
  my ($this,$xml,$cas) = @_;
  my $class = ref($this) || $this;
  my $id = $xml->att('id');
  my $self = { 
	      id => $id,
	      cas => $cas,
	      xml => $xml,
	      in => [],
	      out => [],
	     };
  bless $self, $class;
  $cas->{ids}{$id} = $self;
  $cas->{clusters}{$id} = $self;
  weaken($self->{cas});
  weaken($self->{xml});
  return $self;
}

sub apply {
  my $self = shift;
  my $query = shift;
  $query->($self);
}

foreach my $x (qw{id cas xml}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      return shift->{$x};
    }
  }
}

foreach my $x (qw{left right token lex}) {
  { no strict 'refs';
    *{__PACKAGE__."::$x"} = sub {
      my $self = shift;
      return $self->{$x} ||= $self->{xml}->att($x);
    }
  }
}

sub fids {
  my $self = shift;
  return $self->{fids} ||= [map {/^(E\S+?F\d+)/ and $1} split(/\s+/,$self->lex) ];
}

1;
