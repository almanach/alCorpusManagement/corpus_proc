package DepXML::Query::Compile;
use strict;
use warnings;
use Devel::Declare qw{};
use B::Hooks::EndOfScope;
use List::MoreUtils qw{any all none};

## reuse from NewsProcess::Query::Compile
## to express queries over dependencies

our ($Declarator, $Offset);

sub import {
  my $class = shift;
  my $caller = caller;

  ##  print "Importing caller=$caller\n";

  Devel::Declare->setup_for(
			    $caller,
			    { 'dpath' => {const => \&dpath_parser} }
			   );

  no strict 'refs';
  *{$caller.'::dpath'} = sub (&) { shift };
#  *{$caller.'::check'} = sub (&) {};
}

sub skip_declarator {
  $Offset += Devel::Declare::toke_move_past_token($Offset);
}

sub skipspace {
  $Offset += Devel::Declare::toke_skipspace($Offset);
}

sub strip_name {
  skipspace;
  if (my $len = Devel::Declare::toke_scan_word($Offset, 1)) {
    my $linestr = Devel::Declare::get_linestr();
    my $name = substr($linestr, $Offset, $len);
    substr($linestr, $Offset, $len) = '';
    Devel::Declare::set_linestr($linestr);
    return $name;
  }
  return;
}

sub shadow {
    my $pack = Devel::Declare::get_curstash_name;
    Devel::Declare::shadow_sub("${pack}::${Declarator}", $_[0]);
}

sub make_proto_unwrap {
    my ($proto) = @_;
    return (defined($proto) && length($proto)) ? "($proto);" : "";
}

sub inject {
  my $inject = shift;
  my $linestr = Devel::Declare::get_linestr;
  substr($linestr, $Offset, 0) = $inject;
  Devel::Declare::set_linestr($linestr);
}

sub inject_if_block {
    my $inject = shift;
    skipspace;
    my $linestr = Devel::Declare::get_linestr;
    if (substr($linestr, $Offset, 1) eq '{') {
        substr($linestr, $Offset+1, 0) = $inject;
        Devel::Declare::set_linestr($linestr);
    }
}

sub inject_before_block {
    my $inject = shift;
    skipspace;
    my $linestr = Devel::Declare::get_linestr;
    if (substr($linestr, $Offset, 1) eq '{') {
        substr($linestr, $Offset, 0) = $inject;
        Devel::Declare::set_linestr($linestr);
    }
}

sub inject_scope {
    on_scope_end {
        my $linestr = Devel::Declare::get_linestr;
        my $offset = Devel::Declare::get_linestr_offset;
        substr($linestr, $offset, 0) = ';';
        Devel::Declare::set_linestr($linestr);
    };
}

sub dpath_parser {
  local ($Declarator, $Offset) = @_;

  my $linestr = Devel::Declare::get_linestr();

  my $strip = sub {
    my $len = shift;
    $linestr = Devel::Declare::get_linestr();
    substr($linestr, $Offset, $len) = '';
    Devel::Declare::set_linestr($linestr);
    $linestr = Devel::Declare::get_linestr();
  };

  ## print "Query compile: starting declarator=$Declarator offset=$Offset str='$linestr'\n";

  my $old = $Offset;
  skip_declarator;

  return if $Offset == $old;

  my $counter = 0;

  my $genvar = sub { $counter++; return '@x'.$counter };
  my $genfun = sub { $counter++; return "fun$counter" };
  ##  my $var = $genvar->();
  my $var = '@_';
  my $assign = '@_';
  my $dpath = $var;
  my @union = ();
  my @stack = ();
  my $move_or_stay = 'move';

  while (1) {
##    my $x = substr($linestr,$Offset,3);
##    print "Query compile: at offset=$Offset local='$x' str='$linestr'\n";
    $linestr = Devel::Declare::get_linestr();
    skipspace;
    if (my $move = strip_name) {
      if ($move eq 'union') {
	##	print "found union\n";
	push(@union,$dpath);
	$dpath = "$var";
      } elsif ($move =~ /^is_/) {
	## print "found test $move\n";
	$dpath = qq{ grep {\$_ && \$_->$move} $dpath };
      } elsif ($move =~ /^has_(\S+)$/) {
	## print "found test $move\n";
	$dpath = qq{ grep {\$_ && \$_->$1} $dpath };
      } elsif ($move eq 'true') {
	# noop
      } elsif ($move =~ /^(\S+?)_in_(\S+)/) {
	use Regexp::List;
	my $where = $1;
	my $ra = Regexp::List->new;
	$ra = $ra->list2re(split(/_/,$2));
	##	print "Member $where $ra\n";
	$dpath = qq{ grep {\$_ && \$_->$where =~ /^$ra\$/ } $dpath };
      } else {
	## print "move '$move'\n";
	$dpath = qq{ map {\$_ && \$_->$move} $dpath };
      } 
      next;
    }
    $linestr = Devel::Declare::get_linestr();
    skipspace;
    if (substr($linestr, $Offset, 1) eq '(') {
      ## handle expressions
      ## print "open expression\n";
      $strip->(1);
      push(@stack, 
	   { var => $var,
	     union => [@union],
	     assign => $assign,
	     type => $move_or_stay
	   });
      $assign = "$dpath";
      my $nvar = $genvar->();
      $dpath = $nvar;
      $var = $nvar;
      $move_or_stay = 'move';
      @union = ();
      next;
    } elsif  ((length($linestr) > $Offset+2)
	      && substr($linestr, $Offset, 2) eq '.(') {
      ## filter current object set wrt to the subexpression
      $strip->(2);
      push(@stack, 
	   { var => $var,
	     union => [@union],
	     assign => $assign,
	     type => $move_or_stay
	   });
      $assign = "$dpath";
      my $nvar = $genvar->();
      $dpath = $nvar;
      $var = $nvar;
      $move_or_stay = 'reduce';
      @union = ();
      next;
    } elsif  ((length($linestr) > $Offset+3)
	      && substr($linestr, $Offset, 3) eq '.x(') {
      ## keep current object set if it exists some object in it satisfying the subexpr
      $strip->(3);
      push(@stack,
	   { var => $var,
	     union => [@union],
	     assign => $assign,
	     type => $move_or_stay
	   });
      $assign = "$dpath";
      my $nvar = $genvar->();
      $dpath = $nvar;
      $var = $nvar;
      $move_or_stay = 'exists';
      @union = ();
      next;
    }  elsif  ((length($linestr) > $Offset+3)
	       && substr($linestr, $Offset, 3) eq '.n(') {
      ## keep current object set if it no object in it satisfies the subexpr
      $strip->(3);
      push(@stack,
	   { var => $var,
	     union => [@union],
	     assign => $assign,
	     type => $move_or_stay
	   });
      $assign = "$dpath";
      my $nvar = $genvar->();
      $dpath = $nvar;
      $var = $nvar;
      $move_or_stay = 'none';
      @union = ();
      next;
    }  elsif  ((length($linestr) > $Offset+3)
	       && substr($linestr, $Offset, 3) eq '.a(') {
      ## keep current object set if all objects in it satisfy the subexpr
      $strip->(3);
      push(@stack,
	   { var => $var,
	     union => [@union],
	     assign => $assign,
	     type => $move_or_stay
	   });
      $assign = "$dpath";
      my $nvar = $genvar->();
      $dpath = $nvar;
      $var = $nvar;
      $move_or_stay = 'all';
      @union = ();
      next;
    }

    $linestr = Devel::Declare::get_linestr();
    skipspace;
    if (@stack 
	&& (length($linestr) > $Offset+1)
	&& (substr($linestr, $Offset, 2) eq ')*')) {
      $strip->(2);
      if (@union) {
	$dpath = '('.join(',',map {"($_)" } @union,$dpath).')';
	my $uvar = $genvar->();
	$dpath = qq( my $uvar=$dpath; $uvar; );
      }
      my $fun = $genfun->();
      $dpath = qq{ do { sub $fun { my $var=\@_; return $var ? ($var, $fun($dpath)) : (); }; $fun($assign) } };
      my $info = pop @stack;
      @union = @{$info->{union}};
      $var = $info->{var};
      $assign = $info->{assign};
      $move_or_stay = $info->{type};
      next;
    } elsif (@stack && (substr($linestr, $Offset, 1) eq ')')) {
      ## close expression
      ## print "close expression\n";
      $strip->(1);
      if (@union) {
	$dpath = '('.join(',',map {"($_)" } @union,$dpath).')';
	my $uvar = $genvar->();
	$dpath = qq( my $uvar=$dpath; $uvar; );
      }
      if ($move_or_stay eq 'move') {
	$dpath = qq{ do { my $var=$assign; $dpath } };
      } elsif ($move_or_stay eq 'reduce') {
	$dpath = qq{ do { grep {my $var=(\$_); $dpath} $assign } };
      } elsif ($move_or_stay eq 'exists') {
	$dpath = qq{ do { my $var=$assign; $dpath and $var} }; 
      } elsif ($move_or_stay eq 'all') {
	$dpath = qq{ do { my $var=$assign; (all {my $var=(\$_); $dpath} $var) and  $var} }; 
      } elsif ($move_or_stay eq 'none') {
	$dpath = qq{ do { my $var=$assign; !$dpath and $var} }; 
      }
      my $info = pop @stack;
      @union = @{$info->{union}};
      $var = $info->{var};
      $assign = $info->{assign};
      $move_or_stay = $info->{type};
      next;
    } 
    $linestr = Devel::Declare::get_linestr();
    skipspace;
    if (substr($linestr, $Offset, 1) eq '{') {
      ## handle checks
      my $length = Devel::Declare::toke_scan_str($Offset);
      my $check = Devel::Declare::get_lex_stuff();
      ## print "check expression '$check'\n";
      Devel::Declare::clear_lex_stuff();
      $strip->($length);
      $dpath = qq{ grep {\$_ && $check} $dpath };
      next;
    } 
    $linestr = Devel::Declare::get_linestr();
    skipspace;
    if (substr($linestr, $Offset, 1) eq '[') {
      ## handle checks
      my $length = Devel::Declare::toke_scan_str($Offset);
      my $index = Devel::Declare::get_lex_stuff();
      if ($index eq 'first') {
	$index = 0;
      } elsif ($index eq 'last') {
	$index = -1;
      }
##      print "check expression '$check'\n";
      Devel::Declare::clear_lex_stuff();
      $strip->($length);
      $dpath = qq{ ( $dpath ) [$index] };
      next;
    } 
    last;
  }

  if (@union) {
    $dpath = '('.join(',',map {"($_)"} @union,$dpath).')';
    my $uvar = $genvar->();
    $dpath = qq( my $uvar=$dpath; $uvar; );
  }
  $dpath = qq( sub{ $dpath } );

  $ENV{DPATH_DEBUG} and print STDERR "DPath code is '$dpath'\n";

  inject($dpath);

}

1;
