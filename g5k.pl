#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw(strftime :sys_wait_h);

use lib ("$ENV{HOME}/lib/perl/5.8.8",
	 "$ENV{HOME}/perl/lib/perl/5.8.8",
	 "$ENV{HOME}/perl/share/perl/5.8.8",
	 "$ENV{HOME}/perl/share/perl/5.8.8/auto"
	 );

use AppConfig qw/:argcount :expand/;
use File::Rsync;

my $config = AppConfig->new(
			    { CREATE => 1 },
    "host|h=s@"         => {DEFAULT => []},
    "port|p=s"          => {DEFAULT => 8999},
    "corpus=f@"         => {DEFAULT => []},
    "exclude=f@"        => {DEFAULT => []}, ## exclude corpus
    "parser=s"          => {DEFAULT => "frmgtel"},
    "rparser=s"         => {DEFAULT => "frmgtelr"},
    "input|in|i|t=s"    => {DEFAULT => ''},
    "verbose|v!"        => {DEFAULT => 1},
    "stats|s!"          => {DEFAULT => 0},

    "forest!"           => {DEFAULT => 0},
    "display|d=s"       => {DEFAULT => ''},
    "grammar!"          => {DEFAULT => 0},
    "tagger!"           => {DEFAULT => 0},
    "xmldep!"           => {DEFAULT => 0},
    "lpdep!"            => {DEFAULT => 0},
    "dotdep!"           => {DEFAULT => 0},
    "easy!"             => {DEFAULT => 0},
    "passage!"             => {DEFAULT => 0},
    "easyhtml!"         => {DEFAULT => 0},

    "errfile=s"         => {DEFAULT => "errors"},
    "setsid!"           => {DEFAULT => 0},
    "log_file=f"        => {DEFAULT => '/tmp/dispatch.log'},
    "status!"           => {DEFAULT => 0},
    "time!"             => {DEFAULT => 0},
    "robust!"           => {DEFAULT => 0},
    "easyinput"         => {DEFAULT => 0},
    "timeout=i"         => {DEFAULT => 200},

    "colldir=f"         => {DEFAULT => "./results"},
    "collsave!"         => {DEFAULT => 0},
    "collbase=f"        => {DEFAULT => 'sentence'},,

    "allparse!"         => {DEFAULT => 0},
    "showdag!"          => {DEFAULT => 0},
    "run!"              => {DEFAULT => 1},
    "final!"            => {DEFAULT => 1},
    "disambiguate|dis!" => {DEFAULT => 0},
    "date!"             => {DEFAULT => 0},
    "printhost!"        => {DEFAULT => 0},
    "results=f"         => {DEFAULT => 'results'},
    "dagdir=f"          => {DEFAULT => 'dag'},
    "dagext=s"          => {DEFAULT => "udag"},
    "lock=f"            => {DEFAULT => 'lock'},

    "compress!"         => {DEFAULT => 0},
    "kill=i"            => {DEFAULT => 0},
    "g5k!"              => {DEFAULT => 0},
			    "adj_max=i" => {DEFAULT => 6},
			    "backdir=s",
			    "tar!" => {DEFAULT => 0},
			    "lexer_options=s" => {DEFAULT => ""},
                            "rules=f",
			    "g5storage!" => {DEFAULT => 0},
			    "jobid=s" => {DEFAULT => $ENV{OAR_JOBID}}
    );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    or die "can't open or process configuration file $conffile, stopped";
}

$config->args();

sub verbose {
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
}

##sleep(30);

my $me = `whoami`;

verbose "launching g5k script as '$me' ...";

my $g5storage = $config->g5storage;
my $jobid = $config->jobid;

my $dagdir  = $config->dagdir;
my $results = $config->results;
#my $login   = $config->login;
#my $login   = "edelaclergerie";
my $master_node;
my @hosts;
my $base = "ParserRuns";
my $pid;

my $clusters = {
		'azur' => 2,	# sophia 2 2GB 1GB
		'helios' => 4,	# sophia 4 4GB 1GB
		'sol' => 4,	# sophia 4 4GB 1GB
		'suno' => 8,	# sophia 8 32GB 4GB  **
#		'gdx' => 2,	# orsay
#		'netgdx' => 2,	# orsay
		'borderplage' => 2, # bordeaux 2 2GB 1GB         (51)
		'bordereau' => 4,   # bordeaux 4 4GB 1GB         (93)
		'borderline' => 8,  # bordeaux 8  32GB 4GB ***   (10)
#		'bordemer' => 2, # bordeaux
		'adonis' => 8,	    # grenoble 8 24GB 3GB **     (10)
		'genepi' => 8,	    # grenoble 8 8GB 1GB         (34)
		'edel' => 8,	    # grenoble 8 24GB 3GB **     (72)
		'griffon' => 5,	    # Nancy 8 16GB 2GB **        (92)
		'graphene' => 4,    # Nancy 4 16GB 4GB ***      (144)
#		'grelon' => 4,	    # Nancy
#		'grillon' => 2,	    # Nancy
		'paradent' => 8,    # Rennes 8 32GB 4GB ***      (64)
		'parapluie' => 16,  # Rennes 24 48GB 2GB ***     (25)
		'parapide' => 6,    # Rennes 8 24GB 3GB ***      (40)
#		'paramount' => 4,  # Rennes
#		'paraquad' => 4,   # Rennes
#		'paravent' => 2,    # Rennes
		'pastel' => 4, # Toulouse 4 8GB 2GB     *        (140)
		'violette' => 2, # Toulouse 2 2GB 1GB
		'chicon' => 4,	 # Lille 4 4GB 1GB               (26)
		'chimint' => 8,	 # Lille 8 16GB 2GB     *        (20)
		'chinqchint' => 8, # Lille 8 8GB 1GB             (46)
		'chirloute' => 8,  # Lille 8 8GB 1GB             (8)
		'hercule' => 8,   # Lyon 12 32GB 2.xGB ***       (4)
		'orion' => 8,   # Lyon 12 32GB 2.xGB   ***       (4)
		'sagittaire' => 2,   # Lyon 2 2GB 1GB           (79)
#		'capricorne' => 2,   # Lyon
		'taurus' => 8,   # Lyon 12 32GB 2.xGB  ***      (16)
		'stremy' => 24,	  # Reims 24 48GB 2GB  *        (44=) 
		'granduc' => 8,	  # Luxembourg 8 16GB 2GB  *    (22=176)
	       };


END {
    kill $pid if (defined $pid);
}

$SIG{INT} = sub {
   kill $pid if (defined $pid);
   waitpid($pid,0);
};

$SIG{CHLD} = sub {
    while ((waitpid(-1,WNOHANG)) > 0) {
    }
};

### add hosts to dispatch.conf
verbose "Update hosts in $conffile";

## first remove previous recorded hosts
open(CONF,"<$conffile") or die "can't open $conffile: $!";
my @conf;
while (<CONF>) {
    push(@conf,$_) unless (/^host/ || /^## master/);
}
close(CONF);

open(CONF,">$conffile") or die "can't open $conffile: $!";
print CONF @conf;
print CONF "\n\n";		# to be sure to have newlines at the end

## append hosts
my $nodefile;
if (defined $ENV{GOOD_NODES}) {
    $nodefile = $ENV{GOOD_NODES};
    verbose "Getting good nodes from '$nodefile' and saving in '$ENV{HOME}/katapult/nodes.conf'";
    system "cp $nodefile $ENV{HOME}/katapult/nodes.conf";
} elsif (-e "$ENV{HOME}/katapult/nodes.conf") {
    $nodefile = "$ENV{HOME}/katapult/nodes.conf";
} else {
    die "no node file !";
}

open(NODES, "<$nodefile") or die "can't open nodefile $nodefile: $!";

while (<NODES>) {
    if(eof(NODES)) {
	chomp;
	$master_node = $_;
	print CONF "## master: $master_node\n";
	last;
    }
    chomp;
    my ($hostlabel) = m/^(\D+\d+)/;
    my $node = $_;
    my $n = get_number_of_cores($hostlabel);
    while ($n > 0) {
      print CONF "host ${hostlabel}_${n}\@${node}\n";
      $n--;
    }
    chomp;
    push (@hosts, $_);
}
close(CONF);
close(NODES);

verbose "using master node '$master_node' nodefile $nodefile";

sub get_number_of_cores {
  my $hostlabel = shift;
  $hostlabel =~ /^(\S+?)-/;
  my $cluster = $1;
  return $clusters->{$cluster} || 2;
}

#verbose "adding keys";

##system "ssh-add ~/.ssh/id_dsa"
##  || warn "couldn't add ssh key";


## verbose "kaadkeys";

##system "kaaddkeys -k ~/.ssh/id_rsa.pub -m $master_node"
##  || warn "couldn't export keys to master $master_node";
##system "kaaddkeys -k ~/.ssh/id_rsa.pub -f $nodefile"
##  || warn "couldn't export keys to nodes in $nodefile";


my $obj = File::Rsync->new( { archive      => 1, 
			      compress     => 1, 
			      update       => 1,
			      exclude      => ['*_*/','*_*.log'],
			      rsh          => '/usr/bin/ssh', 
			      'rsync-path' => '/usr/bin/rsync' } );


sub ssh ($) {
  my $cmd = shift;
  system("ssh $master_node \"$cmd\"") == 0
      || warn "ssh $master_node failed: $?" ;
}

sub safe_system ($) {
    my $cmd = shift;
    system($cmd) == 0
	|| warn "Pb with $cmd: $!";
}

my $front_base = "$ENV{HOME}/$base";
my $front_result = "$front_base/$results";
my $master_base = "$master_node:$base";
my $master_results = "$master_base/$results";

ssh "mkdir -p /tmp/$base.clerger";

unless ($g5storage) {
  verbose "copy $dagdir on master node";
  safe_system "rsync -av $front_base/$dagdir $master_base/";
}

verbose "copy dispatch.pl on master node";
safe_system "scp $ENV{HOME}/parserd/dispatch.pl $master_base/dispatch.pl";

verbose "copy dispatch.conf on master node";
safe_system "scp $conffile $master_base/dispatch.conf";

verbose "copy report.pl on master node";
system "scp $ENV{HOME}/corpus_proc/report.pl $master_base/report.pl";

if (-d "$front_result" && !$g5storage) {
    verbose "Resuming: copying back needed info on master node"; 
    # copy logs, info and status. not complete results !
    ssh "mkdir -p $base/$results";
    ##    system "scp -C $front_result/*.log $master_results/";
    system "scp -C $front_result/dispatch.log $master_results/";
    system "scp $front_result/info.txt $master_results/";
    ##    system "scp -r $front_result/status $master_results/";
}

verbose "start parserd on every host";
#system "taktuk -f $ENV{GOOD_NODES} broadcast exec [ /home/clerger/exportbuild/sbin/parserd_setenv restart ]";
## pb with taktuk, cf job 137068
## tmp
foreach my $h (@hosts) {
    verbose "$h";
    my $status = system("ssh -A -q -o 'BatchMode yes' -o StrictHostKeyChecking=no $h \"~/exportbuild/sbin/parserd_setenv restart\"");
    unless ($status==0) {
	warn "Couldn't start parserd on $h:$?";
	system "ssh $h rm exportbuild/var/lock/subsys/parserd";
	system("ssh -A -q -o 'BatchMode yes' -o StrictHostKeyChecking=no $h \"~/exportbuild/sbin/parserd_setenv restart\"") == 0
	    ||	warn "Really couldn't start parserd on $h:$?";
    }
}

if ($g5storage) {
  
  # $g5storage ? "/data/edelaclergerie_$jobid" : 
  
  my $g5base = "/data/edelaclergerie_$jobid";
  my $master_g5base = "$master_node:$g5base";

  verbose "mount storage space on master node $master_node:";
  system "storage5k -a mount -j jobid -p nfs -m $master_node:";

  verbose "copy dispatch.pl on master node";
  safe_system "scp $ENV{HOME}/parserd/dispatch.pl $master_g5base/dispatch.pl";

  verbose "copy dispatch.conf on master node";
  safe_system "scp $conffile $master_g5base/dispatch.conf";

  verbose "copy report.pl on master node";
  system "scp $ENV{HOME}/corpus_proc/report.pl $master_g5base/report.pl";

  verbose "run dispatcher";
  system "ssh -A -q -o 'BatchMode yes' $master_node \"source ~/exportbuild/sbin/setenv.sh; cd ~/$g5base; ./dispatch.pl \"";

} elsif ($pid  = fork()) {	
  ## import of results is only needed if one doesnt use g5storage

  sleep (300);
  while (1) {
    ## get results
    sleep (300);
    verbose "import results '$results'";
    $obj->exec( { src  => "$master_node:$base/$results", 
		  dest => "$ENV{HOME}/$base/" } ) or warn "rsync $results failed\n";
  }
  
}  else {

    verbose "run dispatcher";
    #system "taktuk -m $master_node broadcast exec [ /home/clerger/dispatch.pl ]";
    system "ssh -A -q -o 'BatchMode yes' $master_node \"source ~/exportbuild/sbin/setenv.sh; cd ~/$base; ./dispatch.pl \"";

    if (0) {
      # no need to extract the results from the master node
      # results are copied on nfs partition
      ## but the nfs partition doesn't seem to work !
      verbose "import last results '$results'";
      $obj->exec( { src  => "$master_node:$base/$results", 
		    dest => "$ENV{HOME}/$base/" } ) or die "rsync $results failed\n";
    }

}


verbose "done";


#########################################

=head1 NAME

g5k.pl - to launch the dispatcher on grid'5000

=head1 SYNOPSIS

oarsub -t deploy -p "cluster='gdx'" -l nodes=5,walltime=4 \
'./katapult  --copy-ssh-key --deploy-env sid-x86-alpage -- /home/orsay/clerger/corpus_proc/g5k.pl --config <file>'

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2008, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Isabelle Cabrera <Isabelle.Cabrera@inria.fr>

=cut
