#!/usr/bin/perl

use strict;
use Carp;
use DBI;
use IPC::Run  qw(start finish run pump);

use AppConfig qw/:argcount :expand/;
my $config = AppConfig->new(
                            "verbose|v!" => {DEFAULT => 1},
			    "mine=f",
			    "db=f"     => {DEFAULT => "dbdir/mine.dat" },
			    "iter=i"   => {DEFAULT => 200},
			    "logdir=f" => {DEFAULT => "./"},
#			    "lexed=f"  => {DEFAULT => "/usr/bin/lexed"},
			    "dicodir=f" => {DEFAULT => "/usr/local/lib/lefff-frmg"},
			    "dico=f" => {DEFAULT => "dico.xlfg"}
                           );

my $conffile = 'mine2db.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $verbose = $config->verbose;
my $iter   = $config->iter();
my $mine   = $config->mine() || "mine$iter.csv";
#my $lexed  = $config->lexed(); 
my $dicodir = $config->dicodir(); 
my $dico = $config->dico(); 
my $db     = $config->db();
my $logdir = $config->logdir();
my @logdir = ();

my $dbh;

if (@ARGV) {
  @logdir = @ARGV;
} else {
  @logdir = $logdir;
}

create_table();

fill_table();

## create table
sub create_table {
  my $dbh = DBI->connect("dbi:SQLite:$db", "", "",
                         {RaiseError => 1, AutoCommit => 0});
  $dbh->do(<<EOF );
CREATE TABLE mine (id INTEGER,key,weight FLOAT,occ INTEGER, focc INTEGER,
                  PRIMARY KEY(id),
                  UNIQUE(id)
);
EOF
  
  $dbh->do(<<EOF );
CREATE TABLE history (id INTEGER,iter INTEGER,weight FLOAT,
                  PRIMARY KEY(id,iter)
);
EOF
  
  $dbh->do(<<EOF );
CREATE TABLE sentence (id INTEGER, srank INTEGER,corpus,sid,sentence,
                  PRIMARY KEY(id,srank)
);
EOF

  $dbh->do(<<EOF);
CREATE TABLE lemma (id INTEGER, lemma);
EOF
  
 $dbh->do(<<EOF);
CREATE TABLE rank (id INTEGER, rank INTEGER, weight FLOAT,
        PRIMARY KEY(id)
        UNIQUE(id)
        UNIQUE(rank)
);
EOF

  $dbh->commit;
  $dbh->disconnect;
}

## populate table
sub fill_table {
  print STDERR "Filling table\n" if ($verbose);
  my $dbh = DBI->connect("dbi:SQLite:$db", "", "",
                         {RaiseError => 1, AutoCommit => 0});
  # insert initial values
  my $mine_sth=$dbh->prepare('INSERT INTO mine(id,key,weight,occ,focc) VALUES (?,?,?,?,?)');
  my $history_sth=$dbh->prepare('INSERT OR IGNORE INTO history(id,iter,weight) VALUES (?,?,?)');
  my $sentence_sth=$dbh->prepare('REPLACE INTO sentence(id,srank,corpus,sid,sentence) VALUES (?,?,?,?,?)');
  my $lemma_sth = $dbh->prepare('REPLACE INTO lemma(id,lemma) VALUES (?,?)');

  my $rank_sth = $dbh->prepare('INSERT INTO rank(id,rank,weight) VALUES (?,?,?)');
  open(ERROR,"<$mine") || die "can't open $mine: $!";
  my $rank=0;
  my %corpus = ();
  my %sentences = ();
  my $srank = 0;
  my $key;
  my %rank = ();
  while (<ERROR>) {
    chomp;
    next if (/^#/);
    if (/^\S+/) {
      $dbh->commit unless ($rank % 1000) ;
      s/\[\|.*?\|\]//o;
      my @k = split(/\s+/,$_);
      $rank++;
      $srank = 0;
      $key = $k[0];
      $mine_sth->execute($rank,$k[0],$k[1],$k[2],$k[3]);
      $rank{$rank} = $k[1]*($k[2] ? log($k[2]) : 0);
      my %lemma = get_lemma($key);
      foreach my $lemma (keys %lemma) {
	print STDERR "Register $rank $key $lemma\n" if ($verbose);
	$lemma_sth->execute($rank,$lemma);
      }
      next;
    }
    if (/^\s+history\s*(.*)/) {
      my $history = $1;
      chomp $history;
      my $i = 1;
      foreach my $h (reverse(split(/\s+/,$history))) {
	my ($liter,$r,$v) = split(/:/,$h);
	next unless ($liter % 10 || $liter ge ($iter - 1));
	last if ($i++ == 15);
	## print "RANK-ITER $rank $liter\n";
	$history_sth->execute($rank,$liter,$v);
      }
      next;
    }
    if (/^\s+/) {
      s/^\s+//og;
      my @k = split(/\s+/,$_);
      $srank++;
      my ($corpus,$id) = split(/\#/,$k[0]);
      $corpus{$corpus} = 1;
      $sentences{$corpus}{$id}{$rank}{$srank} = 1;
      next;
    }
  }
  close(ERROR);

  $dbh->commit;

  my $n=0;
  foreach my $id (sort {$rank{$b} <=> $rank{$a}} keys %rank) {
    $n++;
    $rank_sth->execute($id,$n,$rank{$id});
  }

  foreach my $corpus (keys %corpus) {
    my $found=0;
    foreach my $logdir (@logdir) {
      my $file = "$logdir/$corpus.log";
      if (-r $file) {
	open(LOG,"<$file") || die "can't open file $file:$!";
	$found=1;
	last;
      } elsif (-r "$file.gz") {
	open(LOG,"-|","gunzip -c $file.gz") || die "can't open file $file:$!";
	$found=1;
	last;
      } elsif (-r "$file.bz2") {
	open(LOG,"-|","bzcat $file.bz2") || die "can't open file $file:$!";
	$found=1;
	last;
      } else {
	next;
      }
    }
    next unless ($found);
    while (<LOG>) {
      if (/^(?:fail|robust)\s+(\d+)\s+(.+)/ && (exists $sentences{$corpus}{$1})) {
	chomp;
	my $sid = $1;
	my $s = $2;
	foreach my $info ($sentences{$corpus}{$sid}) {
	  foreach my $rank (keys %$info) {
	    foreach my $srank (keys %{$info->{$rank}}) {
	      ##	      print "$corpus $sid $srank $rank\n";
	      $sentence_sth->execute($rank,$srank,$corpus,$sid,$s);
	    }
	  }
	}
      }
    }
    close(LOG);
    $dbh->commit;
  }

  $dbh->disconnect;
}


sub get_lemma {
  my $key = shift;
  my ($truelex,$lex) = ($key =~ m{^(.+)/(.+?)$}o);
  my $tmplex=$lex;
  $tmplex =~ s/([^_-])_([^_-])/$1 $2/og;
  my $info = sql_lexed($tmplex);
  my @lemma = ($info =~ /pred="([^_<>"]+).*?"/og);
  my %lemma = ();
  $lemma{$_} = 1 foreach (@lemma);
  return %lemma;
}

## replacement of lexed by sqlite (December 2011)
## direct implementation of sqlite_reader.pl (in alexina-tools)
use DBI;
my $dico_sth;

sub sql_lexed {
  my ($s,$status) = @_;
  print "%% test lexed $s\n";
  unless ($dico_sth) {
    my $db = "$dicodir/$dico.dat";
    (-f $db) 
      or die "Missing sqlite databse '$db' for lefff: maybe you need to update lefff and lefff-frmg packages";
    $dbh = DBI->connect("dbi:SQLite:$db","","",{RaiseError => 1, AutoCommit => 0});
    $dbh->{sqlite_unicode} = 1;
    $dico_sth = $dbh->prepare('select value from data where key=?');
  }
  $dico_sth->execute($s);
  my %results = ();
  while (my $value = $dico_sth->fetchrow) {
    $results{$value} = 1;
  }
#  my $keys = join("\n\t",keys %results);
#  print "%% sql $s => $keys\n";
  unless ((keys %results) || $status eq 'discard') {
    return sql_lexed( (lcfirst($s) eq $s) ? '_uw' : '_Uw', $status);
  }
  return join('|', sort {$a cmp $b} keys %results);
}

1;

=head1 NAME

mine2db.pl -- Build database after error mining

=head1 SYNOPSIS

./mine2db.pl --logdir <resultsdir>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
