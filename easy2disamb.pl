#!/usr/bin/env perl

## extract information from dag Easy files (reference and dags)
## to help FRMG disambiguisation

use strict;
use warnings;
use Carp;
use POSIX qw(strftime floor);
use POSIX ":sys_wait_h";
use XML::Twig;
use List::Util qw{min max};
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            { CREATE => 1 },
                            'verbose!'        => {DEFAULT => 1},
                            'dest=f'          => {DEFAULT => 'concat'},
                            "corpus=f@"       => {DEFAULT => [] },
                            "dagdir=f"        => {DEFAULT => '/Users/clergeri/Work/ParserRuns/dag.easyref33'},
                            "dagext=s"        => {DEFAULT => "udag"},
                            "set=s"           => {DEFAULT => "frmg"},
			    "logdir=f",
			    "ref|r=f"  => {DEFAULT => "/Users/clergeri/Work/EASy/EasyRef2009-10-16"},
                            "tmpdir=f" # to be used when untaring in $results is too  slow (if nfs dir)
			   );

$config->args();

my $dest = $config->dest;
my $verbose = $config->verbose;
my $dagdir = $config->dagdir;
my $dagext = $config->dagext;
my $refdir  = $config->ref;

my $log;
my $tmpdir = $config->tmpdir || $dest;

my @groups = qw/GA GN GP GR NV PV/;
my @rels   = qw/SUJ-V AUX-V COD-V CPL-V MOD-V COMP ATB-SO MOD-N MOD-A MOD-R MOD-P COORD APPOS JUXT/;

my @corpus = @{$config->corpus};

if (!@corpus) {

  open(INFO,"<$dagdir/info.txt") || die "can't open '$dagdir/info.txt': $!";
  while(<INFO>) {
    next if /^\#/;
    my ($corpus) = split(/\s+/,$_);
    push(@corpus,$corpus)
  }
  close(INFO);
}

system("mkdir -p $dest");

if ($verbose) {
    my $logdir = $config->logdir || $dest;
    $log = "$logdir/easy2disamb.log";
    open(VERBOSE,">>$log") or die "can't open log $log: $!";
    verbose("Activate log in '$log'");
}

verbose("Start PASSAGE disamb");

my $info = {};

foreach my $corpus (@corpus) {

  my ($base) = ($corpus =~ /^(\w+)_/);
  my $dagfile = "$dagdir/$corpus.${dagext}";

  my $reffile = "$refdir/$corpus.xml";

 ## my $xdest = "$dest/$corpus";
 ## system("mkdir -p $xdest");

  my $tar = 0;

  verbose("processing $corpus");


  if (-r $dagfile) {
    open(DAG,"<$dagfile") || die "can't open dag for $corpus '$dagfile': $!";
  } elsif (-r "${dagfile}.bz2") {
    open(DAG,"bunzip2 -c ${dagfile}.bz2|") || die "can't open dag for $corpus '$dagfile': $!";
  } else {
    die "can't find DAG file '$dagfile'";
  }
  
  $info = {};
  my $forms = $info->{forms} = {};

  while (<DAG>) {
    next unless /^##\s*DAG\s+BEGIN/oi;
    my $line = <DAG>;
    my ($sid) = ($line =~ /E(\d+(?:\.\d+)?)F\d+/);
    while(1) {
      last if ($line =~ /^##\s*DAG\s+END/);
      my ($left,$right) = $line =~ /^(\d+)\s+.+?\s+(\d+)$/;
      while ($line =~ m{\G.*?(<F id="E\d+(?:\.\d+)?F\d+">[^<>]+</F>)}og) {
        my $form = $1;
        ##        print STDERR "Adding $form\n";
        my ($fid) = ($form =~ /id="(E\d+(?:\.\d+)?F\d+)"/o);
        $form =~ s/>([^<>]+)</>\n\t  $1\n\t</o;
        $form =~ s/\\(["?+])/$1/og; ## remove escaping added by dag2udag
        $form = "\t$form";
	my $xfid = $fid;
	$xfid =~ s/\.\d+//o;
        my $entry = $forms->{$xfid} ||= { form => $form, 
					  id => $fid,
					  refid => $xfid,
					  dagpos => [],
					  sid => "$sid"
					};
	push(@{$entry->{dagpos}},[$left-1,$right-1]);
      }
      $line = <DAG>;
      unless ($line) {
          # this case should not arise
          verbose("*** Unfinished DAG in '$corpus' sid=$sid");
          last;
	}
    }
  }
  close(DAG);

  foreach my $form (values %$forms) {
    $form->{min} = min map {$_->[0]} @{$form->{dagpos}};
    $form->{max} = min map {$_->[1]} @{$form->{dagpos}};
  }

  my $xml = XML::Twig->new(
                           twig_handlers => {
                                             'E' => \&sentence_handler,
                                             'hypconsts' => sub { $_->delete },
                                             'hyprels' => sub { $_->delete },
                                            },
                           pretty_print => 'indented',
                          );

  if (-r $reffile) {
    if ($reffile =~ /\.bz2$/) {
      open(FILE,"bzcat $reffile|") || die "can't open $reffile: $!";
    } else {
      open(FILE,"<$reffile") || die "can't open $reffile: $!";
    }
    print "Handling '$reffile'\n";
    $xml->parse(\*FILE);
    close(FILE);
  }

  my $destfile = "$dest/$corpus.disamb.pl";
  open(DEST,">$destfile")
    or die "can't open '$destfile': $!";
  foreach my $g (values %{$info->{groups}}) {
    next unless ($g->{sid} && $g->{first} && $g->{last});
    print DEST <<EOF;
easy!group('$g->{sid}',$g->{min},$g->{max},'$g->{type}',$g->{first}{min}:$g->{first}{max},$g->{last}{min}:$g->{last}{max}).
EOF
  }
  foreach my $r (values %{$info->{rels}}){
    next unless ($r->{source} && $r->{target});
    next unless ($r->{sid});
    print DEST <<EOF;
easy!rel('$r->{sid}',$r->{source}{min}:$r->{source}{max},$r->{target}{min}:$r->{target}{max},'$r->{type}').
EOF
  }
  close(DEST);

}

sub sentence_handler {
  my $twig      = shift;
  my $e         = shift;
  
  my $groups = $info->{groups} ||= {};
  my $rels = $info->{rels} ||= {};
  my $forms = $info->{forms} ||= {};
  my $coords = $info->{coords} ||= {};
  my $auxv = $info->{auxv} ||= {};
  my $sid = $e->att('id');

#  print "processing $sid\n";

  
  if (my $const = $e->first_child('constituants')) {
    foreach my $g ($const->children('Groupe')) {
      my $gid = $g->att('id');
      my $type = $g->att('type');
#      print "Register group $gid $type\n";
      my $group = $groups->{$gid} = { id => $gid,
				      type => $type
				    };
      my @fs = ();
      my $min = 1000;
      my $max = 0;
      foreach my $f ($g->children('F')) {
	my $xf = $forms->{$f->att('id')};
	next unless $xf;
	$min > $xf->{min} and $min = $xf->{min};
	$max < $xf->{max} and $max = $xf->{max};
	## last form is generally the head (but for some NV with clitics)
	$group->{last} = $forms->{$f->att('id')};
	unless (exists $group->{first}) {
	  $group->{first} = $forms->{$f->att('id')};
	}
      }
      $group->{sid} = $group->{last}{sid};
      $group->{min} = $min;
      $group->{max} = $max;
    }
  }
  
  if (my $rel = $e->first_child('relations')) {
    my @rels = ();
    foreach my $r ($rel->children('relation')) {
      my $rid = $r->att('id');
      my $type = $r->att('type');
      next if grep {$type eq $_} qw{JUXT};
      if ($type eq 'COORD') {
	my ($coord,$left,$right) = map {$_->att('xlink:href')} $r->children();
	next unless ($coord && $left && $right);
	next if ($left eq 'vide');
	$coord = try_fix($coord,$sid);
	$left = try_fix($left,$sid);
	$right = try_fix($right,$sid);
	$coords->{$coord} = { coord => $coord, left => $left, right => $right };
	next;
      }
      my $entry = { id => $rid,
		    type => $type
		  };
      my ($target,$source) = $r->children();
      $source = $source->att('xlink:href');
      $target = $target->att('xlink:href');
      next unless ($source && $target);
      ## sometime $source and $target id are not correct !!!
      $source = try_fix($source,$sid);
      $target = try_fix($target,$sid);
      $entry->{source} = ($source =~ /G\d+/) ? $groups->{$source}{last} : $forms->{$source};
      $entry->{target} = ($target =~ /G\d+/) ? $groups->{$target}{last} : $forms->{$target};
      $entry->{sid} = $entry->{source}{sid};
      unless ($entry->{source} && $entry->{target}) {
	print "*** pb $sid $rid source=$source target=$target\n";
	next;
      }
      $rels->{$rid} = $entry; 
      push(@rels,$entry);
      if ($type eq 'AUX-V') {
	$auxv->{ $entry->{target}{refid} } = { v => $entry->{source}{refid} };
      }
    }
    foreach my $entry (@rels) {
      my $sourceid = $entry->{source}{refid};
      my $targetid = $entry->{target}{refid};
      next unless ($sourceid && $targetid);
      if (exists $coords->{$sourceid}) {
	$entry->{source} = $forms->{$coords->{$sourceid}{left}};
      }
      if (exists $coords->{$targetid}) {
	$entry->{target} = $forms->{$coords->{$targetid}{left}};
      }
      if ($entry->{type} ne 'AUX-V' && exists $auxv->{$sourceid}) {
##	print "+++ redirect $entry->{type} $sourceid => $auxv->{$sourceid}{v} (AUX-V)\n";
	$entry->{source} = $forms->{$auxv->{$sourceid}{v}};
      }
    }
  }

}

sub try_fix {
  ## fix id if necessary !
  my ($id,$sid) = @_;
  unless ($id =~ /^E\d+/) {
    print "*** bad id $id in $sid\n";
    $id = "${sid}$id";
  }
  unless ($id =~ /\d+[FG]\d+$/) {
    print "*** bad id $id in $sid\n";
    $id =~ s/(\d+)[A-Z]+(\d+)$/$1G$2/;
  }
  return $id;
}

sub verbose {
  return unless ($config->verbose);
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
  print VERBOSE "$date @_\n" if (defined $log);
}

=head1 NAME

easy2disamb.pl -- extract disambiguisation information from Easy files

=head1 SYNOPSIS          
         
./easy2disamb.pl -dest <dest> -dagdir <dagdir> -refdir <refdir>

The resulting files are sent to directory F<dest>, DAGs are read from
<dagdir> and reference file from <refdir>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut

1;
