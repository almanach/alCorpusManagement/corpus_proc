#!/usr/bin/env perl

use strict;

## A small patch applier on FRMG data for the EASy Campaign
## Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

## To apply:
## ./correct_frmg.pl <file>
##   <file> should be of the form <base>.ext
##   and a patch file <base>.patch should exists

my $file=shift;

my $correct = $file;

$correct =~ s!^.*/!!og;
$correct =~ s!\..+$!.patch!og;

open(CORRECT,"<$correct") || die "can't open $correct: $!";

my %shift = ();
my %break = ();
my %group = ();
my %rel = ();
my %void = ();
my %token = ();

while(<CORRECT>) {
  next if /^##/;
  next if /^\s*$/;
  chomp;
  if (/^shift\s+(.\S+)\s+\+(\d+)/) {
    $shift{$1}=$2;
    next;
  }
  if (/^break\s+(\S+)\s+(\S+)/) {
    $break{$1}{$2} = 1;
    next;
  }
  if (/^rename\s+group\s+(\S+)\s+(\S+)/) {
    $group{$1}=$2;
    next;
  }
  if (/^rename\s+relation\s+(\S+)\s+(\S+)/) {
    $rel{$1}=$2;
    next;
  }
  if (/^rename\s+token\s+(\S+)\s+(\S+)/) {
    $token{$1}=$2;
    next;
  }
  if (/^void\s+(\S+)/) {
    $void{$1} = 1;
    next;
  }
  print STDERR "Not an expected correction instruction: $_\n";
}

close(CORRECT);

my @shift = sort {sid_order($a,$b)} (keys %shift);

if ($file =~ /\.bz2$/) {
  open(FILE,"bzcat $file|") || die "can't open $file: $!";
} else {
  open(FILE,"<$file") || die "can't open $file: $!";
}

my %pending = ();
my $master;

my $shift=0;

my $last= "E1"; # next sentence to emit

my $pending_fs;

my $fid;

while(<FILE>) {

  if (m!</E>! and %pending) {

    ## Close current sentence
    print $_;
    next;
  }
  
  if (m!</DOCUMENT>! && %pending) {
    ## dont forget to emit pending sentence before closing document !
    foreach my $psid (sort {sid_order($a,$b)} keys %pending) {
      emit_pending($psid);
    }
    
    print $_;
    next;
  }

  if (/<E id="(.+)">/ ) {
    my $sid=$1;

##    print STDERR "Try pending before $sid\n";

    foreach my $psid (sort {sid_order($a,$b)} keys %pending) {

##      print STDERR "Compare $psid to $sid=".sid_order($psid,$sid)."\n";

      if ($psid eq $sid) {
	print STDERR "*** Removing pending duplicate: $sid\n";
	delete $pending{$psid};
	next;
      }

      next if (sid_order($psid,$sid) == 1);
      emit_pending($psid);
    }

    emit_missing_sentences($sid);
    update_shift($sid);

    if (exists $break{$sid}) {
      ## Prepare data for sentences to be broken
      foreach my $xsid (keys %{$break{$sid}}) {
	$pending{$xsid} = { groups => '', 
			    relations => '',
			    comment => "$xsid broken from $sid"
			  };
      }
    }
    if ($shift) {
      print <<EOF
  <!-- $sid shifted by $shift -->
EOF
    }
    goto EMIT;
  }

  if (/<Groupe id="(.+?)".+>/ && exists $void{$1}) {
    ## Removing void groups
    <FILE>;
    next;
  }

  if (/<Groupe / && /id="(.+?)".+>/ && exists $group{$1}) {
    ## handling renamed group
    my $gid=$1;
    my $xgid=$group{$gid};
    my ($xsid) = ($xgid =~ /^(E\d+(?:\.\d+)?)G/o);
    ## Beware of group on the border of two sentences !
    my $orig_open = $_;
    my $overlap_state = 0;
    s/$gid/$xgid/;
    $pending{$xsid}{groups} .= text_add($_);
    $pending{$xsid}{comment} .= " $gid/$xgid";
#    print STDERR "Displace and Renaming group $gid -> $xgid in $xsid open=$_\n";
    while (<FILE>) {
      if (/\<F /) {
        $_ = handle_F($_);
      }
      if (/(E\d+(?:\.\d+)?)F/ && ($1 ne $xsid)) {
	## for overlapping group, emit once the original opening
	if ($overlap_state == 0) {
	  print text_shift($orig_open);
	  $overlap_state = 1;
	}
	## and emit forms
	print text_shift($_);
      } else {
	## Add to pending text for group if in $xsid
	$pending{$xsid}{groups} .= text_add($_);
	if (m!</Groupe>!) {
	  ## For overlapping groups, emit once the original closing
	  print text_shift("$_\n") if ($overlap_state == 1);
	  last;
	};
      }
    }
    next;
  }

  if (/\<relation / && /id="(.+?)".*?\>/ && exists $rel{$1}) {
    ## handling renamed relations
    my $rid=$1;
    my $xrid=$rel{$rid};
    my ($xsid) = ($xrid =~ /^(E\d+(?:\.\d+)?)R/o);
    s/$rid/$xrid/;
    $pending{$xsid}{relations} .= text_add($_) ;
    $pending{$xsid}{comment} .= " $rid/$xrid";
    while (<FILE>) {
      if (/href="(E\d+(?:\.\d+)?G\d+)"/ && $group{$1}) {
	my $k = $1;
	my $xk = $group{$k};
	s/$k/$xk/;
      }
      $pending{$xsid}{relations} .= text_add($_);
      last if (m!</relation>!);
    }
    next;
  }

  if (/\<F /) {
     $_ = handle_F($_);
  }

  if (/\<F /o &&  /id="(E\d+(?:\.\d+)?)F\d+"\>/o && exists $pending{$1}) {
    # handling F outside groups that are to be displaced
    my $xsid=$1;
    $pending{$xsid}{groups} .= text_add($_);
#     while (<FILE>) {
#       $pending{$xsid}{groups} .= text_add($_);
#       last if (m!</F>!);
#     }
    next;
  }

 EMIT:

  print text_shift($_);

}

sub handle_F {
  my $open = shift;

  ## Flatten on a single lines Fs
  chomp $open;
  my $content = <FILE>;
  chomp $content;
  $content =~ s/^\s+//;
  my $close = <FILE>;      
  $close =~ s/^\s+//;
  my $line = "$open$content$close";

  if ($open =~ /id="(E\d+(?:\.\d+)?F\d+)/ && exists $token{$1}) {
    ## Token renaming because of buggy embedded 'EiFj|' as content
    ## that seems to have messed with token numbering
    my $xid=$token{$1};
    $line =~ s/id="(\S+)"/id="$xid"/;
  }

   return $line;

}

sub emit_pending {
  my $psid = shift;
  ## emitting pendings sentences at the right place
  ## due to sentence breaking in some previous sentence

  emit_missing_sentences($psid);
  update_shift($psid);
  $pending{$psid}{comment} .= "\n\t$psid shifted by $shift" if ($shift);

  print text_shift(<<EOF);
  <!-- $pending{$psid}{comment} -->
  <E id="$psid">
     <constituants>$pending{$psid}{groups}     
     </constituants>
     <relations>$pending{$psid}{relations}     
     </relations>
   </E>
EOF
  delete $pending{$psid};
}

sub emit_missing_sentences {
  my $up = shift;
  while (sid_order($up,$last) == 1) {
    update_shift($last);
    my $sid = $last;
    $last = sid_incr($last);
    next;
    print text_shift(<<EOF);
  <!-- add missing sentence -->
  <E id="$sid">
     <constituants>
     </constituants>
     <relations>
     </relations>
   </E>
EOF

  }
  $last=sid_incr($last);
}

sub sid_incr {
  my $sid = shift;
  my ($k) = ($sid =~ /^E(\d+(?:\.\d+)?)/);
  $k++;
  return "E$k";
}

sub update_shift {
  my $sid = shift;
  # updating shifting if needed
  if (@shift && (sid_order($sid,$shift[0]) != -1)) {
    my $x = shift @shift;
    if ($shift{$x} > 0) {
      ## should always be the case !
      my $y=$shift{$x}-$shift;
      my $xsid = id_shift($sid);
##      print "Add missing: sid=$sid last=$last shift=$shift y=$y xsid=$xsid\n";
      while ($y) {
	print <<EOF;
  <!-- add missing sentence -->
  <E id="$xsid">
     <constituants>
     </constituants>
     <relations>
     </relations>
   </E>
EOF
	$xsid =  sid_incr($xsid);
	$y--;
      }
    }
    $shift = $shift{$x};
  }
}

sub sid_order {
  my ($s1,$s2) = @_;
  my ($k1) = ($s1 =~ /^E(\d+(?:\.\d+)?)/);
  my ($k2) = ($s2 =~ /^E(\d+(?:\.\d+)?)/);
  return $k1 <=> $k2;
}

sub id_shift {
  my $id = shift;
  return $id unless ($shift);
  $id =~ s/^E(\d+(?:\.\d+)?)/"E".($1+$shift)/ge;
  return $id;
}

sub text_shift {
  my $text = shift;
  # replace _ACC_O by {
  $text =~ s/_ACC_O/\{/og;
  # replace _ACC_C by }
  $text =~ s/_ACC_C/\}/og;
  # replace underscore by ' ' in compound token
  $text =~ s/_/ /og unless ($text =~ /\<DOCUMENT/);
  # revert protected underscore to '_'
  $text =~ s/ UNDERSCORE/_/og;
  # replace text of the form 'EiFj|' by ')' (a bug)
  $text =~ s/E\d+(?:\.\d+)?F\d+\|/\)/og;
  return $text unless ($shift);
  $text =~ s/(id|href)="(.+?)"/"$1=\"".id_shift($2).'"'/ge;
  return $text;
}

sub text_add {
  shift;
  chomp;
  return "\n$_";
}

1;

=head1 NAME

correct_frmg.pl -- A small patch applier on FRMG data for the EASy Campaign

=head1 SYNOPSIS

./correct_frmg.pl <file>

where <file> should be of the form <base>.ext
and a patch file <base>.patch should exists

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, 2008 INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
