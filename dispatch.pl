#!/usr/bin/perl

use strict;
use Carp;
use POSIX qw(strftime :sys_wait_h);
use Net::Ping;
use IO::File;
use IO::All qw/io/;
use AppConfig qw/:argcount :expand/;

sub REAPER {
  while (waitpid(-1, WNOHANG) > 0) {}
  $SIG{CHLD} = \&REAPER;
}
$SIG{CHLD} = \&REAPER;

my %corpora = ();

my $config = AppConfig->new(
			    "verbose!"        => {DEFAULT => 1},
			    "corpus=f@"       => {DEFAULT => []},
			    "run!"            => {DEFAULT => 1},
			    "lock=f"          => {DEFAULT => 'lock'},
			    "results=f"       => {DEFAULT => 'results'},
			    "dag=f"           => {DEFAULT => 'dag'},
			    "callparser=f"    => {DEFAULT => '/usr/bin/callparser'},
			    "parse_options=s" => {DEFAULT => '-collsave -time -stats -xmldep -robust'},
			    "key=s"           => {DEFAULT => ''},
			    "good_nodes!"     => {DEFAULT => 0}, ## launched with katapult on grid5000
			    "host=s@"         => {DEFAULT => []},
			    "hostallow!"      => {DEFAULT => 1},
			    "mafdocument!"    => {DEFAULT => 0},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "skel=f"
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    or die "can't open or process configuration file $conffile, stopped";
}

$config->args();

my $dispatch = $config->mafdocument 
  ? MyDispatcher::Maf->new($config) 
  : MyDispatcher->new( $config );

exit;

package MyDispatcher;
use Carp;
##use threads;
##use Event qw/loop unloop/;
use IO::Socket;
use IO::All qw/io/;
use POSIX qw(strftime);

############################################
# Purpose    : Create and launch dispatcher

sub new {
  my $this   = shift;
  my $class  = ref($this) || $this;
  my $config = shift;

  my $self = { config   => $config,
	       hosts    => {},
	       corpus   => {},
	       tasks    => [],
	       pending  => {},	# hash (host => task) waiting to be finished
	       ctask    => 0,
	       loop     => 1,
	       done     => 0,
             };

  bless $self, $class;
  my $dag = $config->dag;

  my $info = "$dag/info.txt";
  $info = "info.txt" unless (-r $info);
  if (!-r $info) {
    die "can't find $info in $dag or locally: $!, stopped";
  }

  open(INFO,"$dag/info.txt") or die "can't open $dag/info.txt: $!, stopped";

  while(<INFO>){
    chomp;
    my ($corpus,$size) = split(/\s+/,$_);
    print "$corpus $size\n";
    $self->{corpus}{$corpus} = { min => 0, max => 0, size => $size };
  }
  close(INFO);

  if (-f "$dag/exclude.txt") {
    open(EXCLUDE,"<$dag/exclude.txt") or die "can't open exclude file: $!, stopped";
    while( <EXCLUDE> ) {
      my ($corpus,$i) = /^(\S+)\.E(\d+(?:\.\d+)?)\s/;
      $self->{corpus}{$corpus}{exclude}{$i} = 1;
    }
    close(EXCLUDE);
  }

  unless (@{$config->corpus}) {
      print "sort keys in corpus\n"; # for test
    $config->corpus($_) foreach (sort keys %{$self->{corpus}});
  }
  
  $self->{lcorpus} = $config->corpus;
  $self->verbose("Corpus list @{$self->{lcorpus}}");
  
  my $results = $self->results;
  my $lock = $self->lock;

  my $log = "$results/dispatch.log".$config->key;
  my $sentences = "$results/sentences.log";
  
  ## Preparation
  $self->verbose("Preparing results directory '$results'");
  
  #system("mkdir -p $results $results/$lock $results/$lock.allow $results/status");
  mkdir $results;
  mkdir "$results/$lock";
  mkdir "$results/$lock.allow";
  mkdir "$results/status";

  #system("cp $conffile $results/");

  ##  system("touch $results/status/running");

  if ($config->verbose) {
    open(VERBOSE,">>$log") or die "can't open dispatch log $log: $!, stopped";
    VERBOSE->autoflush(1);
    $self->verbose("Activate dispatch log in $log");
    $self->{log} = $log;
  }
  open(SENTENCES,">>$sentences") or die "can't open sentences log $sentences: $!, stopped";
  SENTENCES->autoflush(1);

  $self->verbose("Parse options: ", $config->parse_options);

  ## to be completed
  my $skel = $config->skel;
  if (0 && defined $skel && -d $skel && -r $skel) {
    $self->verbose("Using skel dir $skel to prepare '$results'");
  }



  ## setting some methods

  my @hosts = @{$self->{config}->host()};

  ## if $GOOD_NODES is provided by katapult on grid5000
  if ($config->good_nodes) {
      open(NODES, "<$ENV{GOOD_NODES}") or die "can't open nodefile $ENV{GOOD_NODES}: $!";
      while (defined(my $h=<NODES>)) {
	  chomp $h;
	  push (@hosts, $h);
      }
      ## also start parserd on each host
      system "taktuk -f $ENV{GOOD_NODES} broadcast exec [ /home/icabrera/exportbuild/sbin/parserd_service start ]";
  }

  # excute callparser on every host
  foreach my $host (@hosts) {
    $self->add_host($host);
  }

  # while hosts are not closed
  while ($self->{loop}) {
    #      $self->verbose("Restarting socket");
    eval {
	# method accept opens a server socket (LISTEN => 1, REUSE => 1). 
	# Returns an IO::All socket object that you are listening on.
	$self->{socket} = io(':44555')->accept->(
					sub { $self->next_dag($_[0],$_); }
					);
    };
    print STDERR "dispatch server: $@" if $@;

    unless ($self->{done}) {
      ## try to revive dead child unless all corpus processed
      foreach my $host (@hosts) {
	my $info = $self->{'hosts'}{$host};
	my $pid  = $info->{pid};
	unless (defined $pid && kill 0 => $pid) {
	  delete $info->{pid};
	  kill 9, $pid if (defined $pid);
	  my ($hostlabel) = ($host =~ m/(\S+)\@/);
	  $self->{'restart$hostlabel'} = 1;
	  $self->verbose("Need to restart $host");
	  $self->add_host($host);
	}
      }
    }
  }

  ##  system("rm $results/status/running");

}

############################################
# Purpose    : execute callparser on a host, doing a fork
# Parameters : $host

sub add_host {
  my ($self, $host) = @_;
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date adding $host\n";

  if (exists $self->{'hosts'}{$host} && $self->{'hosts'}{$host}{pid}) {
    $self->verbose("host $host already running");
    return;
  }

  my $options = $self->{config}->parse_options;
  $options .= " -host $host -input :44555 -nofinal";
  my $cmd = $self->callparser;

  $self->{hosts}{$host}{cmd} = "$cmd $options";

  my $pid;
##  $self->verbose("Going to start $host");
  if ($pid = fork) {
      $self->{hosts}{$host} = { pid => $pid, requests => 0 };
  } else {
      $self->verbose("starting $host");
      # launch callparser
      exec $self->{hosts}{$host}{cmd}
      or die "can't exec program: $!, stopped";
  }

}

sub close_host {
  my $self = shift;
  my $event = shift;
  $self->warning("closing child");
}

sub callparser {
  my $self = shift;
  return $self->{config}->callparser;
}

sub results {
  my $self = shift;
  return $self->{config}->results;
}

sub dag {
  my $self = shift;
  return $self->{config}->dag;
}

sub dagext {
  my $self = shift;
  return $self->{config}->dagext;
}

sub mafdocument {
  my $self = shift;
  return $self->{config}->mafdocument;
}

sub lock {
  my $self = shift;
  return $self->{config}->lock.$self->{config}->key;
}

sub verbose {
  my $self = shift;
  return unless $self->{config}->verbose;
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
  print VERBOSE "$date @_\n" if (defined $self->{log});
}

sub warning {
  my $self = shift;
  print STDERR @_,"\n";
}

# Some commands to communicate with the server of parsers 
# through telnet during corpus processing
sub user_handler {
    my $self  = shift;
    my $child = shift;
    my $info  = shift;

##  my $event = shift;
##  my $handle = $event->w->fd;
##  my $info = <$handle>;
    
    return if ($info =~ /^\s*$/);

    if ($info =~ /^quit|exit/){
	$child->print("Good bye");
	$child->close;
    } elsif ($info =~ /^hosts/) {
	$child->print("hosts ",join(" ",sort(keys %{$self->{hosts}})),"\n");
    } elsif ($info =~ /^add\s+(\w+)/) {
	$self->add_host($1);
	$child->print("done\n");
    } elsif ($info =~ /^del\s+(\w+)/) {
    } elsif ($info =~ /^corpus/) {
	foreach my $corpus (sort keys %{$self->{corpus}}) {
	    $child->print("$corpus $self->{corpus}{$corpus}{size}\n");
	}
    } else {
	$child->print("Bad user cmd: $info\n");
	$self->out("Bad user cmd: $info");
    }

}

sub exit {
  my $self=shift;
  $self->out('Good bye');
  $self->verbose("Quitting");
  exit;
}

sub out {
  my $self = shift;
  autoflush STDOUT 1;
  print join(' ',@_),"\n";
}

sub read_dag {
  my ($self, $child, $host, $corpus) = @_;
  my $handle = $self->{handle};
  my $sid;
  my @tmp;
  while (<$handle>) {
    ##    $self->verbose("Sending to $host: $_");
    unless (defined $sid) {
      ($sid) = /id="E(\d+(?:\.\d+)?)F\d+"/;
    }
    push(@tmp,$_);
    last if (/^##\s*(DAG|MAF)\s+END/);
  }

  my $date = strftime "[%F %H:%M:%S]", localtime;
  print SENTENCES "$date $corpus $sid $host\n";

  ##  $self->verbose("Trying  $self->{current} $sid");
  if ($sid && $self->{corpus}{$self->{current}}{exclude}{$sid}) {
    $self->verbose("Excluding $self->{current} $sid");
    return 0;
  } else {
      ## todo: child print collbase
      my $results = $config->results;
      $child->print("## LOG=$results/$corpus.log\n");
      $child->print("## CORPUS=$corpus COLLDIR=$results/$corpus\n");
      $child->print("## FILE=$corpus\n"); ## collbase
      $child->print($_) foreach (@tmp);
      return 1;
  }
}

############################################
# Purpose    : Process dag
# Parameters : $child, $info

sub next_dag {
  my $self  = shift;
  my $child = shift;
  my $info  = shift;

  return $self->user_handler($child,$info) unless ($info =~ /^next\s+(\S+)/);
  
  my $host = $1;
  #$self->verbose("get request from $host");

  $self->{hosts}{$host}{requests}++;

  unless (exists $self->{handle}) {
  START:
    # no open corpus
    unless (@{$self->{lcorpus}}) {
      # no remaining corpus to process
      # => close host
      delete $self->{hosts}{$host};
      $self->verbose("Closing child $host");
      ## End loop when all hosts are killed
      $self->{done} = 1;
      $self->{loop} = 0 unless (keys %{$self->{hosts}});
      $child->print("## LOGOUT\n");
      $child->close;
      return;
    }

    # new corpus to process
    my $results = $self->results;
    my $dag     = $self->dag;
    my $dagext  = $self->dagext;
    my $corpus  = shift @{$self->{lcorpus}};
    my $sid     = 0;
    if (-r "$results/status/$corpus") {
      my $status = IO::All::io("$results/status/$corpus")->[0];
      chomp $status;
      if ($status eq 'one') {
	$self->verbose("Shifting $corpus");
	goto START;
      }
      $sid = $status;
      $self->verbose("Resuming $corpus at '$sid'");
    }
    $self->verbose("Opening $corpus");
    my $file = "$dag/$corpus.$dagext";
    unless (-r $file) {
      $self->verbose("*** Dag file $file not found");
      goto START;
    }
    $self->{handle} = new IO::File "<$file"
      or die "can't open dag file $file, stopped";
    $self->{handle}->autoflush(1);
    system("mkdir -p $results/$corpus");
    $self->{current} = $corpus;
    if ($self->mafdocument) {
      my $handle = $self->{handle};
      my $preamble = <$handle>;
      chomp($preamble);
      $self->{preamble} = $preamble;
    }
    if ($sid) {
      my $stop = 0;
      my $handle = $self->{handle};
      while (<$handle>) {
	if (/^##\s*(DAG|MAF)\s+END/ && $stop) {
	  $self->verbose("Found resuming $corpus at '$sid'");
	  last;
	}
	if ($self->mafdocument && $stop && m{^\s*</fsm>}) {
	  $self->verbose("Found resuming $corpus at '$sid'");
	  last;
	}
	if (/^##\s*(DAG|MAF)\s+BEGIN/) {
	  $_ = <$handle>;
	  my ($xsid) = /E(\d+(?:\.\d+)?)F\d+/;
##	  print STDERR "Skip $xsid\n";
	  $stop=1 if  ($xsid eq $sid);
	  next;
	}
      }
    }
  }

 LOOP:
  ## current corpus finished
  if (eof($self->{handle})) {
    $self->verbose("Closing $self->{current}");
    $self->{handle}->close;
    delete $self->{handle};
    goto START;
  }

  ## Check that host has not just been restarted
  ## then need to reinitialize corpus
  my $results = $config->results;
  if (exists $self->{'restart$host'}) {
    my $corpus = $self->{hosts}{$host}{current} = $self->{current};
    my $results = $self->results;
    delete $self->{'restart$host'};
    $self->verbose("Re-Selection of corpus $corpus for $host");
    $child->print("## LOG=$results/$corpus.log\n");
    $child->print("## CORPUS=$corpus COLLDIR=$results/$corpus\n");
  }

  ## Check if change of corpus since last dag (or if first time)
  if (!defined $self->{hosts}{$host}{current} # first time for host
      || $self->{current} ne $self->{hosts}{$host}{current}
     ) {
    my $corpus = $self->{hosts}{$host}{current} = $self->{current};
    my $results = $self->results;
    $self->verbose("Selection of corpus $corpus for $host");
    $child->print("## LOG=$results/$corpus.log\n");
    $child->print("## CORPUS=$corpus COLLDIR=$results/$corpus\n");
  }

  my $corpus = $self->{hosts}{$host}{current} = $self->{current};
  ## Corpus open, ready to send next dag
  ## find another dag in another corpus if no one found in current corpus
  goto LOOP unless ($self->read_dag($child, $host, $corpus));
  
##  $self->verbose("Closing connection to $host");
  $child->close;

}


sub wait_until_active {
  my $self = shift;
  my $host = $self->{slave}{host};
  my $truehost = $host;
  if ($host =~ /(\S+):(\S+)/) {
    $truehost = $1;
  }
  my $p = Net::Ping->new();
  while (!$p->ping($truehost)) {
    sleep(10);
  }
  $p->close;
}

package MyDispatcher::Maf;
use base 'MyDispatcher';

#use Lingua::TagSet::Frmg;

sub read_dag {
  my $self     = shift;
  my $child    = shift;
  my $handle   = $self->{handle};
  my $preamble = "";
  while (<$handle>) {
    ##    $self->verbose("Sending to $host: $_");
    return 0 if (m{^\s*</maf>}); # end of corpus and no dag to return !
    unless ($preamble) {
      $preamble = <<EOF;
##MAF BEGIN
$self->{preamble}
EOF
      $child->print($preamble);
    }
    my $line = $_;
    $line = maf_normalize($line);
    $child->print($line);
    if (m{^\s*</fsm>}) {
      $child->print(<<EOF);
</maf>
##MAF END
EOF
      last;
    }
  }
  return 1;
}

sub maf_normalize {
  # to be able to reuse old-style MAF (as produced by Guillaume)
  # should be rewriten with evolution of the format
  my $line = shift;
  ## Not enough fiable at this stage
  $line =~ s/tag="(.*?)"/maf_normalize_aux($1)/e;
  ## $line =~ s/tag="(.*?)"//e;
  if ($line =~ /<wordForm/ && $line =~ /entry="talana:(.*?)"/) {
    my $lemma = $1;
    my ($lex) = ($line =~ /form="(.*?)"/);
    my $form = $lex;
    if ($lemma =~ /^_/) {
      $form = $lemma;
      $lemma =~ s/^_(Uw|uw)/$1/o;
      $line =~ s/entry="talana:(.*?)"/entry="lefff:$lemma"/o;
    } elsif ($line =~ /pos\.np/) {
      $form = $lex;
    } else {
      $form = lc($lex);
    }
    $line =~ s/form="(.*?)"/form="$form"/;
  }
  $line =~ s/entry="/entry="urn:/o;
  ## remove trailing entry index
  ## but remove entry tag if no such trailing index (risk ob not being a valid lefff entry)
  ##  $line =~ s/entry="(.*?)"//o unless ($line =~ s/entry="(.*?)_+\d+"/entry="$1"/o);
  ## Try removing entry info. Not enough fiable !
  $line =~ s/entry="(.*?)"//o;
  return $line;
}

sub maf_normalize_aux {
  my $tag = shift;
##  $tag =~ s/[@|]/./og;
##  s/cat\./pos./og;
  my $tag = Lingua::TagSet::Frmg->string2tag($tag);
  return "tag=\"$tag\"";
}

1;

__END__


=head1 NAME

dispatch.pl -- Process corpora with Alpage Linguistic Processing Chain

=head1 SYNOPSIS 	 
  	 
./dispatch.pl [options] 

where the options are

=over 4

=item --verbose             [default 1]

=item --corpus=<corpus>

=item --lock                [default 'lock']

=item --results             directory for storing results [default 'results']

=item --dag                 directory containing dags [default dag]

=item --callparser          [default '/usr/bin/callparser']

=item --parse_options       [default '-collsave -time -stats -xmldep -robust']

=item --key                 [default '']

=item --host                [default '']

=item --hostallow

=item --mafdocument         [default 0]

=item --dagext              dag extension, dag or udag [default 'udag']

=item --skel

=back

=head1 DESCRIPTION

Process corpora with Alpage Linguistic Processing Chain. Takes dag as input.
Check with dispatch.conf for example.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
