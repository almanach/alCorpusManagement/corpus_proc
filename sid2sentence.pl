#!/usr/bin/env perl

use strict;
use AppConfig qw/:argcount :expand/;

$|=1;

my $map = {};

my $config = AppConfig->new(
			    "verbose|v!" => {DEFAULT => 0},
			    "datadir=s" => {DEFAULT => "/pinotdata/Corpus/"},
			    "base=s"
			   );

$config->args();

my $premap = {
	      afp => sub { my $seg = shift; return $config->datadir."/AFP/results.$seg/" },
	      frwikipedia => sub { return $config->datadir."/CPL/results.frwiki/" },
	      wks => sub { return $config->datadir."/CPL/results.WkS/" },
	      ep => sub { return $config->datadir."/CPL/results.EP/" },
	      jrc => sub { return $config->datadir."/CPL/results.JRC/" },
	      wks => sub { return $config->datadir."/CPL/results.WkS/" },
	      estrep => sub { return $config->datadir."/CPL/results.EstRepublicain/" },
	      ftb6 => sub { return $config->datadir."/CPL/results.ftb6v6/" },
	      renault => sub { return $config->datadir."/Renault/results.renault/" },
	      biotim => sub { return $config->datadir."/Biotim/results.Cameroun/" },
	      efl => sub { return $config->datadir."/EFL/results.efl/" },
	      pau => sub { return $config->datadir."/Pau/results.Pau2/"}
	     };

my $verbose = $config->verbose;
my $sentences={};
my $xbase = $config->base;

while (<>) {
  if (/^archive\s+(\S+)\s+(\S+)/) {
    $map->{$1} = $2;		# add mapping information
  } elsif (/^quit/) {
    exit;
  } elsif (/^(\S+)[:.]E(\d+)/) {
    my $seg = $1;
    my $sid = $2;
    my ($base);
    ($seg =~ /^(\S+)_(\d+)$/ || $seg =~ /^(\S+)(\d+)$/) and  $base = $1;
    $base ||= $xbase;
    my $sentence = sid2sentence($base,$seg,$sid);
    print $sentence ? "<$seg:E$sid> $sentence\n" : "*** missing $seg:E$sid base=$base\n";
  }
}

sub sid2sentence {
  my ($base,$seg,$sid) = @_;
  unless ($sentences->{$seg}) {
    return undef unless (my $dir = automap($base));
    return undef unless (-d $dir);
    my $log = "$dir/$seg.log.bz2";
    return undef unless (-f $log);
    ##    print "[debug] log=$log\n";
    $sentences->{$seg} = {};
    open(LOG,"bunzip2 -c $log|") || die "can't read log $log: $!"; 
    while(<LOG>) {
      next unless /^(ok|fail|robust)\s+(\d+)\s+(.+)$/;
      my ($status,$sid2,$s) = ($1,$2,$3);
      $s =~ s/\s+([,:;.!?])/$1/og;
      $s =~ s/([a-zA-Z]+')\s+/$1/og;
      $sentences->{$seg}{$sid2} = $s;
    }
    close(LOG);
  }
  return $sentences->{$seg}{$sid};
}

sub automap {
  my $base = shift;
  unless ($map->{$base}) {
    my ($xbase) = $base =~ /^([a-z]+)/i;
    if (my $builder = $premap->{$xbase}) {
      $map->{$base} = $builder->($base);
    } else {
      return undef;
    }
  }
  return $map->{$base};
}

=head1 NAME

sid2sentence.pl -- given a stream of sentence identifiers, returns the sentences

=head1 SYNOPSIS

   sid2sentence.pl [options]

=head1 DESCRIPTION

B<sid2sentence.pl> is a small script used to return the sentences from
archive logs, given a stream of sentence identifier of the form
<seg>:E<id> such as frwiki_032:E12532.

A mapping should exists between the base of the segment (frwiki) and
an archive produced by B<dispatch.pl> (for instance
/pinot/data/Corpus/CPL/results.frwiki).

The command 'quit' send in the stream may be used to close the stream
from B<sid2sentence.pl>

=head1 OPTIONS

=over 4

=item --verbose|v          verbose mode [default=no]

=back

=cut
