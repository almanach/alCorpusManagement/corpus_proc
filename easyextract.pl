#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Carp;
use POSIX qw(strftime);
use POSIX ":sys_wait_h";

use IPC::Run qw(start finish run pump timeout);

use AppConfig qw/:argcount :expand/;

use File::Temp qw/tempfile/;
use File::Basename;
use File::Copy;

my $config = AppConfig->new(
			    'dest=f'          => {DEFAULT => 'frmg'},
                            "verbose!"        => {DEFAULT => 1},
                            "corpus=f@"       => {DEFAULT => []},
                            "slice=i"         => {DEFAULT => 20},
                            "run!"            => {DEFAULT => 1},
                            "lock=f"          => {DEFAULT => 'lock'},
                            "results=f"       => {DEFAULT => 'results'},
                            "dag=f"           => {DEFAULT => 'dag'},
                            "callparser=f"    => {DEFAULT => '/usr/local/bin/callparser'},
                            "parse_options=s" => {DEFAULT => '-collsave -time -stats -xmldep -robust'},
                            "key=s"           => {DEFAULT => ''},
                            "host=s@"         => {DEFAULT => []},
                            "hostallow!"      => {DEFAULT => 1},
                            "share!"          => {DEFAULT => 1},
                            "mafdocument!"    => {DEFAULT => 0},
                            "dagext=s"        => {DEFAULT => "udag"},
                            "skel=f"
			   );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;
my $dest    = $config->dest;
my $dag     = $config->dag;
my $dagext  = $config->dagext;

my %corpora = ( 'general'    =>  '.sgml',
                'litteraire' => '.txt',
                'mail'       => '',
                'medical'    => '.txt',
                'oral_delic' => '.xml',
                'oral_elda'  => '.xml',
                'questions'  => '.sgml'
              );

my @corpus = @{$config->corpus};

if (!@corpus) {
  open(INFO,"<$dag/info.txt") || die "can't open info '$dag/info.txt': $!";
  while(<INFO>) {
    my ($corpus) = split(/\s+/,$_);
    push(@corpus,$corpus)
  }
  close(INFO);
}

my $count = 0;

verbose("Extract EASY reference sentences from $results");

system("mkdir -p $dest");

foreach my $corpus (@corpus) {
  
  my ($base)  = ($corpus =~ /^(\w+)_/);
  my $dagfile = "$dag/$corpus.${dagext}";

  verbose("Missing corpus $corpus"), next unless (-d "$results/$corpus");
  system("mkdir -p $dest/$corpus");

   my $tried = 0;
   my $added = 0;

  open(DAG,"<$dagfile") || die "can't open dag for $corpus '$dagfile': $!";
  while (<DAG>) {
    next unless /^##\s*DAG\s+BEGIN/oi;

    my ($sid) = (<DAG> =~ /E(\d+(?:\.\d+)?)/);
    $added += handle_sentence($corpus,$sid);
    ++$tried;
  }
  close(DAG);
 
  my $ratio = 100 * ($added / $tried);
  my $msg = sprintf("(added %3i tried %3i ratio %6.2f%%)",$added,$tried,$ratio);
  verbose("Done $corpus $msg");
}

verbose("Done EASY conversion");

## print "FILES @easy\n";

sub handle_sentence {
  my ($corpus,$sid) = @_;
  my ($res, $run) = ($results =~ m/(\D+)(\d+)/);
  
  my $file = "$results/$corpus/$corpus.E$sid.dep.xml";
  if (!(-e $file && -s $file)) {
    while ($run != 0) {
	$run--;
	$file = "$res$run/$corpus/$corpus.E$sid.dep.xml";
	if (-s $file) {
	    verbose("*** copy dep $corpus:$sid from run $run");
	    copy($file,"$dest/$corpus/$corpus.E$sid.dep.xml");
	    last;
	}
    }
    if (run == 0) {
	verbose("*** missing dep $corpus:$sid");
	return 0;
    }
  } else {
    copy($file,"$dest/$corpus/$corpus.E$sid.dep.xml");
  }

  $file = "$results/$corpus/$corpus.E$sid.ph2.xml";
  if (!(-e $file && -s $file)) {
    while ($run != 0) {
	$run--;
	$file = "$res$run/$corpus/$corpus.E$sid.ph2.xml";
	if (-s $file) {
	    verbose("*** copy easy $corpus:$sid from run $run");
	    copy($file,"$dest/$corpus/$corpus.E$sid.ph2.xml");
	    last;
	}
    }
    if (run == 0) {
	verbose("*** missing easy $corpus:$sid");
	return 0;
    }
  } else {
    copy($file,"$dest/$corpus/$corpus.E$sid.ph2.xml");
  }

}

sub verbose {
  return unless ($config->verbose);
##  my $date = strftime "%A %d %B, %H:%M:%S", gmtime;
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
}

=head1 NAME

easyextract.pl -- Extract EASY reference sentences from results

=head1 SYNOPSIS 	 
  	 
./easyextract.pl --results <resultdir> --dest <destdir> --dag <dagdir> --dagext <dagext>

=head1 DESCRIPTION

B<easyextract.pl> extracts results which are part of the 4000
sentences reference.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
