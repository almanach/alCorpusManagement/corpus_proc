#!/usr/bin/env perl

use strict;
use AppConfig;
## use PDL;
#use Math::SparseVector;
#use Math::SparseMatrix;
use POSIX qw{strftime};
use Data::Dumper;
use Data::Serializer;

my $config = AppConfig->new(
			    'verbose|v!' => {DEFAULT => 0},
			    'min=d' => {DEFAULT => 100},
			    'ratio=d' => {DEFAULT => 0.02},
			    'cluto!' => {DEFAULT => 0},
			    'output|o=f' => {DEFAULT => 'cluto'},
			    'ctxset_min=d' => {DEFAULT => 4},
			    'min_strength=d' => {DEFAULT => 0.02},
			    "depth=d" => {DEFAULT => 8},
			    'reformat!' => {DEFAULT => 0}, # convert dependencies into a more readable format
			    'cache!' => {DEFAULT => 1}, # save to cache
			    'track=s@' => {DEFAULT => 1}, # tracking words
			    'dump_words!' => {DEFAULT => 0}, # dump
                                                             # words
                                                             # at each
                                                             # round 
			    'html!' => {DEFAULT => 1}
			   );

my $args = join(' ',@ARGV);

$config->args;

my $min = $config->min;
my $minratio = $config->ratio;
my $maxdepth = $config->depth;

my $cluto = $config->cluto;

my $ctxset_min = $config->ctxset_min;
my $min_strength = $config->min_strength;

my $track = { map {$_=>1} @{$config->track} };

my $ctx_remove = {
		  'modifieur' => { 'plus_adv' => 1,
				   'moins_adv' => 1,
				   'tr�s_adv' => 1,
				   '�tre_v' => 1,
				   'si_adv' => 1,
				   'aussi_adv' => 1,
				   'autant_adv' => 1,
				   'assez_adv' => 1,
				   'trop_adv' => 1,
				   '_NUMBER_det' => 1,
				   '_NUMBER_number' => 1,
				   '_NUMBER_adj' => 1
				 },
		  'sujet' => { '�tre_v' => 1,
			       'avoir_v' => 1,
			       'faire_v' => 1
			     },
		  'cod' => {
			    'avoir_v' => 1
			   }
		 };

my $orig_w2ctx={};		# all contexts

my $ctx2info ={};		# all contexts
my $w2ctx={};                   # mapping from words to theirs contetxs after thredholds
my $ctx2w = {};			# mapping from contexts to words after thredholds

my $ctx_sources = {};           # mapping from words to the contexts where they play the source
my $ctx_targets = {};           # mapping from words to the contexts where they play the target

my $ctx2sources = {};		# mapping from ctx to source
my $ctx2targets = {};		# mapping from ctx to target

my $ctx2ctx = {};		# mapping of ctx to abstract ctx because of cluster-based rewriting

my $clusters={};		# clusters

my $w2c = {};			# mapping from words to clusters: a word may belong to more than one cluster
my $ctx2c = {};			# mapping from ctx to clusters: a ctx may be in the definition of more than one cluster

my $l2c = {};			# labels to clusters

my $all = 0;
my $nwords = 0;
my $nwordslog = 0;

my $round = 0;

my $cluster_label=0;

my $base = $config->output;

if ($config->reformat) {
    while (<>) {
	if (my @v = @{decompose($_)}) {
	    print join("\t",@v),"\n";
	}
    }
} else {
    open(LOG,">$base.log") || die "can't open '$base.log': $!";
    collect_wctx();
    close(LOG);
}

sub decompose {
    my $s = shift;
    my @v = split(/\t/,$s);
    if (@v == 4) {
	# new format
	return \@v;
    } elsif ($s =~ m{^((?:_Uv|
			_PERSON(?:_m|_f)|
			_DATE_(?:_year|_arto|_artf)|
			_NP_WITH_INITIALS|
			_[A_Z]+|
			[^_]+?)_[a-z]+)
			 _([^_]+?)_
			 ((?:_Uv|
			   _PERSON(?:_m|_f)|
			   _DATE_(?:_year|_arto|_artf)|
			   _NP_WITH_INITIALS|
			 _[A-Z_]+|
			   [^_]+?)_[a-z]+)
			 \s+(\d+)$
		     }xo) {	
	## old format: depecated !
	return [$1,$2,$3,$4];
    } else {
	return [];
    }
}

sub verbose {
    my $date = strftime "[%F %H:%M:%S]", localtime;
    print STDERR "$date : ", @_, "\n";
    print LOG "$date : ", @_, "\n";
}

sub collect_wctx {
  verbose("starting");
  
  my $cache = "$base.cache";
  if (-f "$cache") {
      verbose("reusing cache $cache");
      my $info = Data::Serializer->new(
				       serializer => 'Storable',
				       compress   => 1,
				       )->retrieve($cache) || die "coudn't read from $cache: $!";
      $orig_w2ctx = $info->{w2ctx};
      $ctx2info = $info->{ctx2info};
      $ctx_sources = $info->{ctx_sources};
      $ctx_targets = $info->{ctx_targets};
      $all = $info->{all};
      $nwords = $info->{nwords};
      $nwordslog = log($nwords);
  } else {
      while (<>) {
	  my @v = @{decompose($_)};
	  next unless (@v);		    ## delete ill formed
	  next if ($v[3] > 100000);	    ## deleted too frequent: not informative
	  next if (!$v[0] || !$v[2]);     ## delete some non weakly lexicalized dependencies
	  next if ($v[0] =~ /_(cln|cla|cld|cll|clg|prel|aux|coo|pro|prel|csu|pri|que)$/o
		   || $v[0] =~ /^_Uv/o
		   || $v[2] =~ /_(cln|cla|cld|cll|clg|prel|aux|coo|pro|prel|csu|pri|que)$/o
		   || $v[2] =~ /^_Uv/o
		   );
	  next if ($ctx_remove->{$v[1]} && 
		   ( $ctx_remove->{$v[1]}{$v[2]} || $ctx_remove->{$v[1]}{$v[0]}));
	  ## rewrite some dependencies
	  if ($v[2] =~ /_v$/o && grep {$v[1] eq $_} qw/cod sujet/) {
	      ## the verb of a sentential argument is not really important
	      $v[2] = '*sentence*_v';
	  }
	  my $w = ($v[3] =~ /^\d+$/) ? $v[3] : 0;
	  my @samples = split(/\s+/,$v[5]);
	  unless ($v[0] =~ /_(advneg|det|csu|coo|aux)$/o) {
	      my $l = $orig_w2ctx->{$v[0]} ||= { ctx => {}, 
						 n => 0, 
						 update => 1,
						 samples => []
					       };
	      my $ca = "*_$v[1]_$v[2]";
	      $l->{ctx}{$ca} += $w;
	      $l->{n} += $w;
	      push(@{$l->{samples}},@samples);
	      $ctx2info->{$ca}{n} += $w;
	      $ctx2info->{$ca}{words}++;
	      push(@{$ctx2info->{$ca}{samples}},@samples);
	      $ctx_targets->{$v[2]}{$ca} = "*_$v[1]_";
	      $ctx2targets->{$ca} = [$v[2],$v[1]];
	  }
	  unless ($v[2] =~ /_(advneg|det|csu|coo|aux)$/o) {
	      my $r = $orig_w2ctx->{$v[2]} ||= { ctx => {}, 
						 n => 0, 
						 update => 1,
						 samples => []
					       };
	      my $cb = "$v[0]_$v[1]_*";
	      $r->{ctx}{$cb} += $w;
	      $r->{n} += $w;
	      push(@{$r->{samples}},@samples);
	      $ctx2info->{$cb}{n} += $w;
	      $ctx2info->{$cb}{words}++;
	      push(@{$ctx2info->{$cb}{samples}},@samples);
	      $ctx_sources->{$v[0]}{$cb} = "_$v[1]_*";
	      $ctx2sources->{$cb} = [$v[0],$v[1]];
	  }
	  $all += $w;
      }
      $nwords = scalar(keys %$orig_w2ctx);
      $nwordslog = log($nwords);
      if ($config->cache) {
	verbose("caching to $cache");
	Data::Serializer->new(
			      serializer => 'Storable',
			      compress   => 1,
			     )-> store( { w2ctx => $orig_w2ctx,
					  ctx2info => $ctx2info,
					  ctx_sources => $ctx_sources,
					  ctx_targets => $ctx_targets,
					  all => $all,
					  nwords => $nwords
					}, "$cache") || die "couldn't save cache in $cache: $!";
      }
    }

  ## delete low frequency words and words with non strongly specific contexts
  ## for each word, deleted non specific contexts
  ## build an index for contexts
  my $x = keys %$orig_w2ctx;
  my $y = keys %$ctx2info;
  verbose("read $all dependencies for $x words and $y contexts");

  while (my ($w,$winfo) = each %$orig_w2ctx) {
      delete $orig_w2ctx->{$w}, next if ($winfo->{n} < 10);
      $orig_w2ctx->{$w}{tracked} = 1 if ($track->{$w});
  }

  $round = 0;
  while (1) {
    $round++;
    my ($old_cluster_n,$old_word_n) = current_clustering_size();
    verbose("round $round");

    ## distribute contexts, based on existing clusters
    verbose("adding new abstract words by rewriting");
    while (my ($w,$clusters) = each %$w2c) {
      my $winfo = $orig_w2ctx->{$w};
      ## $w belongs to at least one cluster
      ##	print "Distribute $w=".Dumper($tmp);
      my $d = {};
      $winfo->{update} = 1;
      ##	  verbose("--- updating $w (cluster dispatch)");
      while (my ($c,$cluster) = each  %$clusters) {
	my $x = $d->{$c} = $orig_w2ctx->{$cluster->{xlabel}} 
	  ||= {  ctx => {}, n => 0, update => 1 };
	$x->{tracked} = 1 if (exists $winfo->{tracked});
      }
      while (my ($ctx,$v) = each %{$winfo->{ctx}}) {
	if (my $clusters2 = $ctx2c->{$ctx}) {
	  my @shared = ();
	  foreach my $c (keys %$clusters2) {
	    # $ctx is a component of cluster $c for $w
	    push(@shared,$c) if (exists $clusters->{$c});
	  }
	  my $shared = @shared;
	  next unless ($shared);
	  ## at this point, $ctx is a component of $shared clusters for $w
	  ## it is removed from $w and distributed along its clusters
	  ## we could decide to leave a small part to $w
	  my $newn = $v / $shared;
	  $winfo->{n} -= $v;
	  $winfo->{ctx}{$ctx} = 0;
	  delete $winfo->{ctx}{$ctx};	
	  ## and distribute among new pseudo clusters-words
	  foreach my $c (@shared) {
	    $d->{$c}{ctx}{$ctx} += $newn;
	    $d->{$c}{n} += $newn;
	  }
	}
      }
    
      while (my ($c,$info) = each %$d) {
	unless ($info->{n}) {
	  delete $orig_w2ctx->{$w2c->{$w}{$c}{xlabel}};
	}
      }
	  
      delete $orig_w2ctx->{$w} if ($winfo->{n} < 10);
	  
    }

    ## rewrite contexts based on clusters
    ## favor clusters that are represented by more than one context
    verbose("adding new abstract contexts by rewriting");
    while (my ($w,$winfo) = each %{$orig_w2ctx}) {
      my $rep =  {};
      ## find a repartition among all pertinent clusters
      foreach my $ctx (keys %{$winfo->{ctx}}) {
	if (my $map = $ctx2ctx->{$ctx}) {
	  foreach my $label (keys %$map) {
	    ($rep->{$label} ||= .5) *= 2;
	    ## don't grow two large !
	    $rep->{$label} = 1024 if ($rep->{$label} > 1024);
	  }
	}
      }
      if (keys %$rep) {
	$winfo->{update} = 1;
##	verbose("--- updating $w (ctx rewrite)");
	my %extra = ();
	my @remove = ();
	while (my ($ctx,$n) = each %{$winfo->{ctx}}) {
	    if (my $map = $ctx2ctx->{$ctx}) {
		my $total = 0;
		$total += $rep->{$_} foreach (keys %$map);
		while (my ($label,$newctx) = each %$map) {
		    $extra{$newctx} += $n * $rep->{$label} / $total;
		}
		$winfo->{ctx}{$ctx} = 0;
		push(@remove,$ctx);
	    }
	}
	while (my ($ctx,$n) = each %extra) {
	    $winfo->{ctx}{$ctx} = $n;
	    my $info =  $ctx2info->{$ctx} ||= { n => 0, words => 0, abstract => 1};
	    $info->{n} += $n;
	    $info->{words}++;
	}
	unless ($winfo->{n}) {
	    print "PB rewrite $w: ".Dumper($winfo);
	    exit;
	}
	delete $winfo->{ctx}{$_} foreach (@remove);
      }
    }

    ## we can now clean the current clusters
    backup_clusters($round);

    thredholds();

    initial_clustering(word_connect());

    my ($x,$y) = current_clustering_size();
    verbose("initial clustering with $x new clusters grouping $y words");

    ## aggregating clusters because either
    ##    - they share enough words
    ##    - they share enough contexts
    ## aggregating is repeated until convergence
    my $agground = 1;
##    $agground++ while (agg_step3(agg_step2(agg_step1())));

    my $max = 0;
    foreach my $cluster (values %$clusters) {
      $max = $cluster->{strength} if ($cluster->{strength} > $max);
    }

    ## doing some checks
    while (my ($c,$cluster) = each %$clusters) {
      cluster_reduce_recursive($cluster) if (exists $cluster->{recursive});
      unless ($cluster->{nw}) {
	verbose("*** void cluster $cluster->{xlabel} $c");
	cluster_remove($cluster);
      }
      foreach my $subc (keys %{$cluster->{components}}) {
	if (exists $clusters->{$subc}) {
	  verbose("*** subcluster $clusters->{$subc}{xlabel} $subc still in clusters");
	  cluster_remove_alt($clusters->{$subc});
	}
      }
    }

    ## stop when best cluster has a sufficient strength
    last unless ($max > $config->min_strength);

    collect_words_to_save();

    cluster_abstract_ctx($_) foreach (values %$clusters);

    emit_html() if ($config->html);
  }

  backup_clusters($round+1);

  verbose("done (results in file '$base.clusters')");

}


sub ctx_weight {		
    # the weight of a ctx is related to the number of words in which it occurs
    my $ctx = shift;
    my $tmp = $ctx2info->{$ctx};
    unless (exists $tmp->{w}) {
	my $words = $tmp->{words};
	if (exists $tmp->{abstract}) {
	    ## abstract context have maximal weight
	    ## should maybe be refined
	    $tmp->{w} = $nwordslog - 0.5 * log($words);
	} elsif ($words = 1) {
	    $tmp->{w} = ($tmp->{n} < 2) ? 0 : $nwordslog;
	} else {
	    $tmp->{w} = $nwordslog - log($words);
	}
    }
    return  $tmp->{w};
}

sub backup_clusters {
  my $round = shift;
#  foreach my $c (keys %$clusters) {
#    verbose("*** ADDED $c");
#  }
  $clusters = {};
  $w2c = {};
  $ctx2c = {}; 
  $ctx2ctx = {};
  return unless ($round > 1);
  ## print the clusters
  my $x = keys %$l2c;
  my $y = 0;
  my %words = ();
  my $seen = {};
  my $stats;
  $stats = sub {
    my $cluster = shift;
    my $label = $cluster->{label};
    return if $seen->{$label};
    $seen->{$label} = 1;
    $y++;
    $stats->($_) foreach (values %{$cluster->{subclusters}});
  };
  foreach my $cluster (values %$l2c) {
    $stats->($cluster);
    foreach my $w (keys %{$cluster->{cluster}}) {
      $words{$w} = 1 unless ($w =~ /^\@\d+/);
    }
  }
  my $z = keys %words;
  verbose("emitting $x root clusters, $y clusters, $z words");
  $round--;
  open(C,">$base.clusters") || die "can't open cluster file: $!";
  my $date = strftime "%F %H:%M:%S", localtime;
  print C <<EOF;
## results for '$base' at $date
##             args: $args
##            round: $round
##    root clusters: $x
##     all clusters: $y
##            words: $z

EOF
    $seen = {};
  foreach my $c (sort {$l2c->{$b}{strength} <=> $l2c->{$a}{strength}}
		 keys %$l2c) {
    cluster_print(\*C,0,$l2c->{$c},$seen);
  }
  close(C);
}

sub check_clusters {
  my $msg = shift;
  foreach my $c (keys %$clusters) {
    my $cluster = $clusters->{$c};
    die "pb '$msg' with '$c'=".Dumper($cluster) 
      unless ( $cluster 
	       && $cluster->{id}
	       && (keys %{$cluster->{components}})
	       && (keys %{$cluster->{cluster}})
	     );
    foreach my $w (keys %{$cluster->{cluster}}) {
      die "pb '$msg' with w='$w' c='$c'" unless ($w && exists $w2c->{$w}{$c});
    }
  }
  while (my ($w,$tmp) = each %$w2c) {
    foreach my $c (keys %$tmp) {
      die "pb w2c '$msg' with w='$w' c='$c'" 
	unless ( exists $clusters->{$c}
		 && exists $clusters->{$c}{cluster}{$w}
	       );
    }
  }
}

sub check_cluster {
  my $c = shift;
  my $msg = shift;
  my $cluster = $clusters->{$c};
  die "pb1 '$msg' with '$c'=".Dumper($cluster)
    unless ( $cluster 
	     && $cluster->{id}
	     && (keys %{$cluster->{components}})
	     && (keys %{$cluster->{cluster}})
	   );
  foreach my $w (keys %{$cluster->{cluster}}) {
    die "pb1 '$msg' with w='$w' c='$c'" unless ($w && exists $w2c->{$w}{$c});
  }
  while (my ($w,$tmp) = each %$w2c) {
    die "pb1 w2c '$msg' with w='$w' c='$c'\n"
      if (exists $tmp->{$c} && !exists $cluster->{cluster}{$w});
  }
}

sub cluster_print {
  my ($handle,$depth,$cluster,$seen) = @_;
  my $indent = "   " x $depth;
  my $label = $cluster->{label};
  if ($depth && 
      ($l2c->{$label} || $seen->{$label})
     ) {
    print $handle "${indent}ref_cluster \@$label\n";
    return;
  }
  $seen->{$label} = 1;
  my $words = $cluster->{cluster};
  my $comp = $cluster->{components};
  my $subs = $cluster->{subclusters};
  my $total = .01 * $cluster->{total} || 1;
  my @k = ();
  foreach my $ctx (sort {$comp->{$b} <=> $comp->{$a}} keys %$comp) {
    push(@k,sprintf("%s:%.2f",$ctx,$comp->{$ctx}/$total));
  }
  my $k = join(' ',@k);
  my $strength = sprintf("%.2f", 100*$cluster->{strength});
  print $handle "${indent}cluster \@$label strength=$strength <$k>\n";
  foreach my $w (sort {$words->{$b} <=> $words->{$a}} keys %$words) {
    my $p = sprintf("%4.1f%%",$words->{$w} / $total);
    print $handle "${indent}   $w\t$p\n";
  }
  foreach my $subc (sort {$subs->{$b}{strength} <=> $subs->{$a}{strength}}
		    keys %$subs) {
    cluster_print($handle,$depth+1,$subs->{$subc},$seen);
  }
  print $handle "${indent}end cluster \@$label\n";
  print $handle "\n" unless ($depth);
}

sub cluster_abstract_ctx {
  my $cluster = shift;
  my $label = $cluster->{label};
  my $xlabel = $cluster->{xlabel};
  my @ws = keys %{$cluster->{cluster}};
  foreach my $w (@ws) {
    if (my $sources = $ctx_sources->{$w}) {
      foreach my $ctx (keys %$sources) {
	my $body = $sources->{$ctx};
	my $new = $ctx2ctx->{$ctx}{$label} = "${xlabel}${body}";
	$ctx_sources->{$xlabel}{$new} = $body;
	$ctx2sources->{$new} = [ $xlabel, $ctx2sources->{$ctx} ];
#	delete $ctx_sources->{$w}{$ctx};
      }
    }
    if (my $targets = $ctx_targets->{$w}) {
      foreach my $ctx (keys %$targets) {
	my $body = $targets->{$ctx};
	my $new = $ctx2ctx->{$ctx}{$label} = "${body}${xlabel}";
	$ctx_targets->{$xlabel}{$new} = $body;
	$ctx2targets->{$new} = [ $xlabel, $ctx2targets->{$ctx} ];
#	delete $ctx_targets->{$w}{$ctx};
      }
    }
  }
}

sub current_clustering_size {
  return (
	  scalar(keys %$clusters), # number of clusters
	  scalar(keys %$w2c)	# numbers of words
	 );
}

sub collect_words_to_save {
    ## a low frequency word may be kept
    ## if it is source or target of sufficientely many contexts
    ## used by clusters
    ## the property is preserved over iterations
    verbose("collecting low frequency words used by enough cluster contexts");
    while (my ($w,$winfo) = each %$orig_w2ctx) {
	my $n = $winfo->{n};
##	next if ($n >= $min || $n < 10 || (exists $winfo->{keep}));
	next if ($n < 10 || $n >= $min || (exists $winfo->{keep}));
	my @ctx = ();
	push(@ctx,keys %{$ctx_sources->{$w}});
	push(@ctx,keys %{$ctx_targets->{$w}});
	my @clusters = ();
	foreach my $ctx (@ctx) {
	    push(@clusters,keys %{$ctx2c->{$ctx}});
	}
	my $score = scalar(@clusters);
##	verbose(" +++ low frequency word $w n=$n score=$score @clusters") if ($score);
	next if ($score < 3);
	$winfo->{keep} = 1;
	verbose(" --- saving low frequency word $w n=$n score=$score");
    }
}

sub w2ctx_dump {
    ## for debug
    verbose("dumping w2ctx");
    open(D,"| bzip2 -c > $base.dump.$round.bz2") || die "can't dump: $!";
    foreach my $w (sort {$w2ctx->{$b}{n} <=> $w2ctx->{$a}{n}} keys %$w2ctx) {
	my $winfo = $w2ctx->{$w};
	my @f = map {"$_:$winfo->{ctx}{$_}"} keys %{$winfo->{ctx}};
	print D "$w:$winfo->{n} @f\n";
    }
    close(D);
}

sub thredholds {
  ## remove words with low frequencies
  ## remove contexts in words with low frequencies
  $ctx2w = {};
  my $old_w2ctx = $w2ctx;
  $w2ctx={};
  verbose("thredholds at $min occ and ctx ratio > $minratio");
  while (my ($w,$winfo) = each %$orig_w2ctx) {
    my $n = $winfo->{n};
    my $update = $winfo->{update};
    delete $winfo->{update};
    next unless ($n >= $min || (exists $winfo->{keep}));
    unless ($update) {
      ## reuse from previous round
      ##	  verbose("--- reusing $w");
      $w2ctx->{$w} = $old_w2ctx->{$w};
      next;
    }
    
    my $new_winfo = { ctx => {}, n => $n };
    my $tmp = $winfo->{ctx};
    
    ## keep representative contexts, using  weighted ratio for each context
    my $Z = 0;
    my %ratio = ();
    ## first step: pseudo TD-IDF
    while (my ($ctx,$ctxinfo) = each %$tmp) {
      $Z += $ratio{$ctx} = $ctxinfo * ctx_weight($ctx) / $n;
    }
    ## second step: normalization 
    if ($Z) {
      $ratio{$_} /= $Z foreach (keys %ratio);
    }

    if (exists $winfo->{tracked}) {
      my @best = ();
      foreach my $ctx (sort {$ratio{$b} <=> $ratio{$a}} keys %ratio) {
	last if ($ratio{$ctx} < 0.1*$minratio);
	push(@best,sprintf("%s:%d:%.4f",$ctx,$tmp->{$ctx},$ratio{$ctx}));
      }
      verbose("--- tracked word '$w' n=$winfo->{n} best: @best");
    }

    foreach my $ctx (sort {$ratio{$b} <=> $ratio{$a}} keys %ratio) {
      next unless ($tmp->{$ctx} > 1); # don't keep hapax contexts
      my $ratio = $ratio{$ctx};
      last if ($ratio < $minratio);	# only keep representative contexts
      $ctx2w->{$ctx}{$w} = $ratio;
      $new_winfo->{ctx}{$ctx} = $ratio;
    }
    next if (scalar(keys %{$new_winfo->{ctx}}) < $ctxset_min);
    ## to be included in a cluster, the word must have at least $ctxset_min
    ## significative contexts
    $w2ctx->{$w} = $new_winfo;
    if (exists $winfo->{tracked}) {
      verbose("--- kept tracked word $w");
    }
  }
  my $x = keys %$w2ctx;
  my $y = keys %$ctx2w;
  verbose("cleaned, keeping $x words and $y contexts");
  w2ctx_dump() if ($config->dump_words);
}

sub word_connect {
  ## for each pair of words, identify shared contexts
  my $connect = {};		# connecting pairs of words
  my $connected=0;		# number of connected pairs
  while ( my ($ctx,$ws) = each %$ctx2w) {
    ## all words with $c as strong context
    my @w = sort keys %$ws;
    next if (@w < 3);		# need at least 3 words
    while (@w) {
      my $w1 = shift @w;
      my $tmp1 = $connect->{$w1} ||= {};
      foreach my $w2 (@w) {
	my $tmp2 = $tmp1->{$w2} ||= [];
	push(@$tmp2,$ctx);
	$connected++;
      }
    }
  }
  while ( my ($w,$winfo) = each %$w2ctx) {
    my %inverse = ();
    foreach my $ctx (keys %{$winfo->{ctx}}) {
      if (exists $ctx2sources->{$ctx}) {
	my ($source,$body) = @{$ctx2sources->{$ctx}};
	push(@{$inverse{"*_${body}_${w}"}},$source);
      } elsif (exists $ctx2targets->{$ctx}) {
	my ($target,$body) = @{$ctx2targets->{$ctx}};
	push(@{$inverse{"${w}_${body}_*"}},$target);
      }
    }
    while (my ($ctx,$ws) = each %inverse ) {
      next if (@$ws  < 3);
      my @ws = sort @$ws;
      while (@ws) {
	my $w1 = shift @ws;
	my $tmp1 = $connect->{$w1} ||= {};
	foreach my $w2 (@ws) {
	  my $tmp2 = $tmp1->{$w2} ||= [];
	  push(@$tmp2,$ctx);
	  $connected++;
	}
      }
    }
  }
  my $connected2=0;
  while ( my ($w1,$w2s) = each %$connect) {
    while ( my ($w2,$ctxset) = each %$w2s) {
      delete $w2s->{$w2} if (scalar(@$ctxset) < $ctxset_min);
    }
    $w2s = $connect->{$w1};
    my $connectivity = scalar(keys %$w2s);
    delete $connect->{$w1}, next unless ($connectivity);
    if ($w1 !~ /_np$/ && $connectivity > 200) {
      verbose("remove $w1 because of high connectivity ($connectivity)");
      delete $connect->{$w1};
      next;
    }
    $connected2 += $connectivity;
  }
  #  verbose("connected $connected2 pairs of words");
  verbose("connected $connected2 pairs of words");
  return $connect;
}

sub initial_clustering {
  my $connect = shift;
  ## only keep word connections with more than $ctxset_min shared contexts
  ## the context set defines the cluster
  while (my ($w1,$tmp1) = each %$connect) {
    my @w2s = keys %$tmp1;
    while (@w2s) {
      my $w2 = shift @w2s;
      my @ctxset = @{$tmp1->{$w2}};
      my $n = @ctxset;
##      $n++ if ($l2c->{$w1} || $l2c->{$w2}); # bonus if combining at least one pseudo-word 
      next if ($n < $ctxset_min);
      if (0) {
	my %ctxset = ( map {$_ => 1} @ctxset );
	my $w3s = {};
	foreach my $w3 (@w2s) {
	  next unless (exists $connect->{$w2}{$w3});
	  my @shared123 = grep {exists $ctxset{$_}} @{$connect->{$w2}{$w3}};
	  next if (@shared123 < $ctxset_min);
	  $w3s->{$w3} = [@shared123];
	}
	next unless (%$w3s);
	my $w3 = (sort { @{$w3s->{$b}} <=> @{$w3s->{$a}} } keys %$w3s)[0];
	@ctxset = @{$w3s->{$w3}};
      }
      my $tmpc = join(' ',sort @ctxset);
      my $tmp = $clusters->{$tmpc};
      if ($tmp) {
	  ## reuse an existing cluster
#	  cluster_add_words($tmp,$w1,$w2,$w3);
	  cluster_add_words($tmp,$w1,$w2);
      } else {
	  my $comp = {};
	  $comp->{$_} = 0 foreach (@ctxset);
#	  $tmp = cluster_find_or_register($comp,[],$w1,$w2,$w3);
	  $tmp = cluster_find_or_register($comp,[],$w1,$w2);
	  ## reduce clusters as soon as possible
	  ## by intersecting with the other existing clusters
	  ## that include either $w1 or $w2
	  my @clusters = 
	    sort {$a->{nctx} <=> $b->{nctx}}
	    map {$clusters->{$_}}
            grep { $_ ne $tmpc}
            (keys %{$w2c->{$w1}}, keys %{$w2c->{$w2}});
	  while (@clusters) {
	      my $cluster = shift @clusters;
	      next if ($cluster->{id} eq $tmpc);
	      if (my $newcluster = intersect_cluster_pairs($cluster,$tmp)) {
		  my $newc = $newcluster->{id};
		  last if ( $newc eq $cluster->{id}); # $tmp swallowed by old $cluster
		  next if ( $newc eq $tmpc); # old $cluster swallowed by $tmp
		  ## new cluster with strictly smaller support ctxset
		  ## compare the remaining clusters with this new one
		  $tmp = $newcluster;
		  $tmpc = $newc;
	      }
	  }
      }
    }
  }
}

sub intersect_cluster_pairs {
    my ($cluster1,$cluster2) = @_;
    my $comp1 = $cluster1->{components};
    my $comp2 = $cluster2->{components};
    my $comp = {};
    foreach my $ctx (keys %$comp1) {
	$comp->{$ctx} = 0 if (exists $comp2->{$ctx});
    }
    my $nctx = keys %$comp;
    return undef if ($nctx < $ctxset_min);
    if ($nctx == $cluster1->{nctx}) {
	cluster_remove($cluster2);
	return $cluster1;
    } elsif ($nctx == $cluster2->{nctx}) {
	cluster_remove($cluster1);
	return $cluster2;
    } else {
	cluster_remove($cluster2);
	cluster_remove($cluster1);
	return cluster_find_or_register($comp,[],
					keys %{$cluster1->{cluster}}, 
					keys %{$cluster2->{cluster}}
					);
    }
}

sub cluster_remove_alt {
    my $c = shift;
    return cluster_remove($clusters->{$c});
}

sub cluster_remove {
    my $cluster = shift;
    my $c = $cluster->{id};
    ## just to be sure
    unless (exists $clusters->{$c}) {
      verbose("*** removing non existing cluster $cluster->{xlabel} <$c>");
    }
    cluster_reduce_recursive($cluster) if (exists $cluster->{recursive});
    delete $w2c->{$_}{$c}  foreach (keys %{$cluster->{cluster}});
    delete $ctx2c->{$_}{$c} foreach (keys %{$cluster->{components}});
    delete $clusters->{$c};
    delete $l2c->{$cluster->{xlabel}};
    return $cluster;
}

sub cluster_reduce_recursive {
  ## reduce a recursive cluster
  my $cluster = shift;
  verbose("collapsing recursive cluster");
  my $w = $cluster->{recursive};
  my $oldcluster = $l2c->{$w};
  delete $l2c->{$w};	# remove old cluster
  ## remove the contribution of $w
  ## should also do it for each contexts, but more difficult to do
  $cluster->{total} -= $cluster->{cluster}{$w};
  delete $cluster->{cluster}{$w};
  delete $cluster->{subclusters}{$cluster->{id}};
  while (my ($subc,$subcluster) =  each %{$oldcluster->{subclusters}}) {
    $cluster->{subclusters}{$subc} = $subcluster;
  }
  cluster_add_words($cluster,keys %{$oldcluster->{cluster}});
  return $cluster;
}

sub cluster_find_or_register {
  my ($components,$subclusters,@words) = @_;
  my @ctxset = sort keys %$components;
  my $c = join(' ',@ctxset);
  my $cluster = $clusters->{$c};
  unless ($cluster) {
    $cluster = $clusters->{$c} 
      = { cluster => {},
	  components => $components,
	  subclusters => {},
	  nctx => scalar(@ctxset),
	  nw => 0,
	  id => $c,
	  label => $cluster_label++,
	  strength => 0,
	  total => 0,
	  depth => 0
	};
    $ctx2c->{$_}{$c} = $cluster foreach (@ctxset);
    my $xlabel = $cluster->{xlabel} = "\@$cluster->{label}";
    $l2c->{$xlabel} = $cluster;
  }
  cluster_add_words($cluster,@words);
  foreach my $subc (@$subclusters) {
    die "recursive subcluster $subc" if ($subc eq $c);
    my $save  =  cluster_remove_alt($subc);
    ## don't really include
    ## when the parent cluster cxtset is a subset of the included cluster ctxset
    $cluster->{subclusters}{$subc} = $save if ($save->{nctx} < $cluster->{nctx});
    cluster_add_words($cluster,keys %{$save->{cluster}});
  }
  if ($cluster->{total}) {
    $cluster->{strength} = 
      $cluster->{total} / ($cluster->{nctx} * $cluster->{nw});
  }
  return $cluster;
}

sub cluster_add_subcluster {
  my $cluster = shift;
  my $subc = shift;
  die "recursive subcluster $subc" if ($subc eq $cluster->{id});
  my $save = $clusters->{$subc};
  if (!$save) {
    verbose("*** adding non existing subcluster $subc to $cluster->{id}");
  } elsif (exists $cluster->{subclusters}{$subc}) {
    ## should nor arise
    verbose("*** re-adding subcluster $save->{xlabel} $subc to $cluster->{xlabel} $cluster->{id}");
  } else {
    cluster_remove($save);
    $cluster->{subclusters}{$subc} = $save;
    $cluster->{depth} = 1+$save->{depth} unless ($cluster->{depth} > 1+$save->{depth});
    cluster_add_words($cluster,keys %{$save->{cluster}});
  }
  return $cluster;
}

sub cluster_add_words {
  my $cluster  = shift;
  my @words = @_;
  my $c = $cluster->{id};
  my @ctxset = keys %{$cluster->{components}};
  my $tmp = $cluster->{cluster};
  foreach my $w (@words) {
    unless (exists $tmp->{$w}) {
      if (my $subcluster = $l2c->{$w}) {
	my $subc = $subcluster->{id};
	##      verbose("adding cluster=$w <$subc> as subcluster of $c");
	if ($subc eq $c) {
	  verbose("*** recursive subcluster $w $subc in $cluster->{xlabel}");
	  ## $subc is a cluster of a previous round
	  ## it gets included in a new cluster with same ctxsets
	  ## what to do ? add it !
	  $cluster->{recursive} = $w;
	}
	$cluster->{subclusters}{$subc} = $subcluster;
      }
      $w2c->{$w}{$c} = $cluster;
      $cluster->{nw}++;
      foreach my $ctx (@ctxset) {
	my $delta = $w2ctx->{$w}{ctx}{$ctx};
	$cluster->{components}{$ctx} += $delta;
	$tmp->{$w} += $delta;
	$cluster->{total} += $delta;
      }
    }
  }
  return $cluster;
}

sub agg_step1 {
  my $agg = {};
  foreach my $w1 (sort { keys %{$w2c->{$b}} <=> keys %{$w2c->{$a}}}
		  keys %$w2c) {
    my $tmp1 = $w2c->{$w1};
    my @clusters1 = 
      map {$_->{id}}
	sort {$b->{strength} <=> $a->{strength}}
	  grep {($_->{strength} > $min_strength) 
		  && ($_->{depth} < $maxdepth)
		    && ($_->{nctx} < 20)
		  }
	    map {$clusters->{$_}} 
	      keys %$tmp1;
    last unless (@clusters1 > 1);
    ## the word belongs to more than one cluster
    ## search other words sharing some pair of shared clusters with $w1
    while (@clusters1) {
      my $c1 = shift @clusters1;
      $agg->{$c1}{$_}++ foreach (@clusters1);
    }
  }
  my $x = 0;
  $x += scalar(keys %$_) foreach (values %$agg);
  verbose("\tstep1: found $x pairs of clusters sharing at least two word");
  return $agg;
}

sub agg_step2 {
  my $agg = shift;
  my $aggscore = [];
  my $counter = 0;
  while (my ($c1,$tmp1) = each %$agg) {
    my $components1 = $clusters->{$c1}{components};
    while (my ($c2,$wshared) = each %$tmp1) {
      my $components2 = $clusters->{$c2}{components};
      my $ctxshared=0;
      foreach my $ctx (keys %$components1) {
	$ctxshared++ if (exists $components2->{$ctx});
      }
      my $score =  $ctxshared + $wshared;
      unless (($ctxshared < 1) || ($score < $ctxset_min)) {
	my $comp = {};
	$comp->{$_} = 0 foreach (keys %$components1,keys %$components2);
	push(@$aggscore,
	     { score => $score,
	       c1 => $c1,
	       c2 => $c2,
	       info => $comp,
	       nctx => scalar(keys %$comp)
	     });
      }
    }
  }
  my $x = scalar(@$aggscore);
  verbose("\tstep2: kept $x clusters sharing at least $ctxset_min words or contexts");
  return $aggscore;
}

sub agg_step3 {
  my $aggscore = shift;
  my $aggregated = 0;
  foreach my $tmpagg (sort {$b->{score} <=> $a->{score}} @$aggscore) {
    my $c1 = $tmpagg->{c1};
    my $c2 = $tmpagg->{c2};
    my $cluster1 = $clusters->{$c1};
    my $cluster2 = $clusters->{$c2};
    next unless ($cluster1 && $cluster2);
    ## aggregate unless $c1 or $c2 have already been aggregated
    ## remove $c1 and $c2
    ## create $c1+$c2
    ##    verbose("aggregating c1='$c1' and c2='$c2");
    if (($tmpagg->{nctx} == $cluster1->{nctx})) {
      cluster_add_subcluster($cluster1,$c2);
    } elsif (($tmpagg->{nctx} == $cluster2->{nctx})) {
      cluster_add_subcluster($cluster2,$c1);
    } else {
      cluster_find_or_register($tmpagg->{info},[$c1,$c2]);
    }
    $aggregated++;
  }
  verbose("\tstep3: aggregated $aggregated clusters");
  if ($aggregated) {
    my ($x,$y) = current_clustering_size();
    verbose("new clustering with $x clusters grouping $y words");
  }
  return $aggregated;
}

sub emit_html {
  my $round = shift;
  ## print the clusters
  my $x = keys %$l2c;
  my $y = 0;
  my %words = ();
  my $seen = {};
  my $stats;
  verbose("emitting html version");
  open(C,">$base.clusters.html") || die "can't open cluster file: $!";
  $seen = {};
  print C <<EOF;
<html>
<head>
   <title>Clusters for $base</title>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <link rel="STYLESHEET" type="text/css" href="style.css">
</head>
<body>
<h1>Clusters for $base</h1>
<ul>
EOF
  foreach my $c (sort {$l2c->{$b}{strength} <=> $l2c->{$a}{strength}}
		 keys %$l2c) {
    cluster_html_print(\*C,0,$l2c->{$c},$seen);
  }
  print C <<EOF;
</ul>
</body>
</html>
EOF
  close(C);
}

sub cluster_html_print {
  my ($handle,$depth,$cluster,$seen) = @_;
  my $label = $cluster->{label};
  if ($depth && 
      ($l2c->{$label} || $seen->{$label})
     ) {
    print $handle <<EOF;
<li class="cluster"> cluster <a href=\"\#\@$label\">\@$label</a></li>
EOF
    return;
  }
  $seen->{$label} = 1;
  my $words = $cluster->{cluster};
  my $comp = $cluster->{components};
  my $subs = $cluster->{subclusters};
  my $total = .01 * $cluster->{total} || 1;
  my @k = ();
  foreach my $ctx (sort {$comp->{$b} <=> $comp->{$a}} keys %$comp) {
    my $label = sprintf("%s:%.2f",$ctx,$comp->{$ctx}/$total);
    $label = "<a href=\"\#$1\">$label</a>" if ($label =~ /(\@\d+)/);
    push(@k,$label);
  }
  my $k = join(' ',@k);
  my $strength = sprintf("%.2f", 100*$cluster->{strength});
  print $handle <<EOF;
<li class="cluster"> cluster <a name="\@$label">\@$label</a>
 strength=$strength \&lt;$k\&gt;
EOF
  print $handle <<EOF;
  <p class="words">
EOF
  foreach my $w (sort {$words->{$b} <=> $words->{$a}} keys %$words) {
    my $p = sprintf("%4.1f%%",$words->{$w} / $total);
    if (exists $l2c->{$w}) {
      $w = "<a href=\"\#$w\">$w</a>";
    }
    print $handle <<EOF;
       <span class="word">$w ($p)</span>
EOF
  }
  print $handle <<EOF;
  </p>
EOF
  my @subc = ();
  foreach my $subc (sort {$subs->{$b}{strength} <=> $subs->{$a}{strength}}
		    keys %$subs) {
    next if ($words->{$subs->{$subc}{xlabel}});
    push(@subc,$subs->{$subc});
  }
  if (@subc) {
    print $handle <<EOF;
<ul class="subclusters">
EOF
    cluster_html_print($handle,$depth+1,$_,$seen) foreach (@subc);
    print $handle <<EOF;
</ul>
EOF
  }
  print $handle <<EOF;
</li>
EOF
}


1;

## ~/build/cluto-2.1.2/Darwin-i386/vcluster -clmethod=graph -clabelfile=all.features -rlabelfile=all.words -sim=jacc -grmodel=al all.mtx 1000 

## then call to cluto_reader.pl
