#!/usr/bin/env perl

use strict;
use AppConfig;

my $config = AppConfig->new(
			    'verbose|v!' => {DEFAULT => 0},
			    'ratio=d' => {DEFAULT => 1},
			    'simple!' => {DEFAULT => 0},
			   );

$config->args;


my $minratio = $config->ratio;
my $simplify = $config->simple;

my $verbs={};

while(<>) {
  chomp;
  my ($f,$n,$samples) = split(/\t/,$_);
  if ($f =~ /xcomp.*xcomp/) {
    $f =~ s/\s+xcomp:\S+_coo//og;
    next if ($f =~ /xcomp.*xcomp/);
  }
  my ($v,$voice,$frame) = ($f =~ /^(.*?)_v_(active|passive)\s*(.*)\s*$/);
  ($v eq '') and print "**** STRANGE: $_\n";
  my $e = $verbs->{$v} ||= { v => $v,
			     occ => 0,
			     div => 0,
			     frames => {},
			     roles => {},
			   };
  my @samples = split(/\s+/,$samples);
  my @l = ($voice);
  my @roles=();
  while ($frame =~ /(\S+):(.+?_\S+)/og) {
    my $role = $1;
    my $lemma = $2;
    $lemma = '*sentence*_v' if ($role =~ /xcomp/);
    my $e2 = $e->{roles}{$role} ||= { role => $role,
				      occ => 0, 
				      lemma => {} };
    $e2->{occ} += $n;
    $e2->{lemma}{$lemma} += $n;
  }
  $frame =~ s/(\S+):(.+?_\S+)/$1/og if ($simplify);
  $frame =~ s/xcomp:(.+?_\S+)/xcomp:*sentence*/og;
  $frame and push(@l,$frame);
  my $xf = $e->{frames}{join(' ',@l)} ||= [0];
  $xf->[0] += $n;
  push(@$xf,@samples) unless (@$xf > 10);
  $e->{occ} += $n;
}

foreach my $e (values %$verbs) {
  $e->{div} = keys %{$e->{frames}};
}

my $i=0;
foreach my $e (sort {$b->{occ} <=> $a->{occ} || $b->{div} <=> $a->{occ}}
	       values %$verbs) {
  my $v = $e->{v};
  my $occ=$e->{occ};
  my $frames = $e->{frames};
  $i++;
  print <<EOF;
<$i> begin verb '$v' occ=$e->{occ} div=$e->{div}
EOF
  foreach my $f (sort {$frames->{$b}[0] <=> $frames->{$a}[0]}
		 keys %$frames) {
    my $o = $frames->{$f}[0];
    my $r = 100*($o / $occ);
    my @samples = @{$frames->{$f}};
    shift @samples;
    my $samples = join(' ',@samples);
    last if ($r < $minratio);
    printf("\t<%s> occ=%d r=%.2f%% {%s}\n",$f,$o,$r,$samples);
  }
  my $roles = $e->{roles};
  foreach my $r (sort {$roles->{$b}{occ} <=> $roles->{$a}{occ}}
		 keys %$roles) {
    my $o = $roles->{$r}{occ};
    my $ratio = 100*($o / $occ);
    last if ($ratio < 1);
    my $lemma = $roles->{$r}{lemma};
    my $div = scalar(keys %$lemma);
    my @lemma = 
      sort {$lemma->{$b} <=> $lemma->{$a}}
	grep {$_ =~ /_(nc|np|v|adj|adv)/}
	  keys %$lemma;
    if (@lemma > 10) {@lemma=@lemma[0..9]};
    my $lemma = join(' ',
		     map {sprintf('%s:%.2f%%',$_,100*$lemma->{$_} / $o)} 
		     @lemma
		    );
    printf("\trole '%s' occ=%d r=%.2f%% div=%d <%s>\n",
	   $r,
	   $o,
	   $ratio,
	   $div,
	   $lemma
	  );
  }
  print <<EOF;
end verb '$v'

EOF
}
