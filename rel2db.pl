#!/usr/bin/env perl

use strict;
use Carp;
use DBI;
use File::Basename;
use IO::All;


my $dbhrel = DBI->connect("dbi:SQLite:dbrel.dat", "", "",
		       {RaiseError => 1, AutoCommit => 0});

my $dbhconst = DBI->connect("dbi:SQLite:dblog.dat", "", "",
		       {RaiseError => 1, AutoCommit => 1});


# create the base

# create the base
$dbhrel->do(<<EOF );
CREATE TABLE log (corpus,id INTEGER,
                  precision REAL,recall REAL,f_measure REAL,
                  PRIMARY KEY(corpus,id),
                  UNIQUE(corpus,id)
);

EOF


$dbhrel->do(<<EOF );
CREATE TABLE ref (corpus,id INTEGER,
                  type,
                  l1, r1, 
                  l2, r2,
                  l3, r3,
                  code,
                  rid,
                  status INTEGER
);

EOF

$dbhrel->do(<<EOF );
CREATE TABLE hyp (corpus,id INTEGER,
                  type,
                  l1, r1, 
                  l2, r2,
                  l3, r3,
                  code,
                  rid,
                  status INTEGER
);

EOF

$dbhrel->commit;

my $log_sth=$dbhrel->prepare('INSERT INTO log(corpus,id,precision,recall,f_measure) VALUES (?,?,?,?,?)');
my $ref_sth=$dbhrel->prepare('INSERT INTO ref(corpus,id,type,l1,r1,l2,r2,l3,r3,code,rid,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');
my $hyp_sth=$dbhrel->prepare('INSERT INTO hyp(corpus,id,type,l1,r1,l2,r2,l3,r3,code,rid,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');

my $refconst_sth = $dbhconst->prepare(<<STH);
select id, left, right, gid from ref where corpus = ?
STH

my $hypconst_sth = $dbhconst->prepare(<<STH);
select id, left, right, gid from hyp where corpus = ?
STH

my $n=0;
foreach my $corpus (IO::All::io("$ENV{HOME}/EASy/INRIA_frmg/")->all) {
  next unless ($corpus =~ /result_relations/);
  next unless ($corpus =~ /\.(txt|sgml|xml)/ || $corpus =~ /mail_/);
  next if ($corpus =~ /oral_elda_7/); # corrupted file
  $n++;				# number of corpora
  my $sid=-1;
  my $skip=0;
  my $kind=0;
  my %info = ();
  my %precision = ();
  my %sentences = ();
  my %relations = ();
  my $base = basename($corpus,
		      qw/.txt_result_relations .xml_result_relations .sgml_result_relations _result_relations/);

  print "\tProcessing $n: corpus=$base file=$corpus\n";

  print "\t\tGetting ref and hyp groups from dblog\n";

  $refconst_sth->execute($base);
  while( my ($sid,$left,$right,$gid) = $refconst_sth->fetchrow_array ) {
    my $range = ["E$sid"."F$left","E$sid"."F$right"];
    $info{$sid}{ref}{group}{$gid} = $range;
    foreach my $k ($left .. $right) {
      $info{$sid}{ref}{form}{"E$sid"."F$k"} = $range;
    }
  }

  $hypconst_sth->execute($base);
  while( my ($sid,$left,$right,$gid) = $hypconst_sth->fetchrow_array ) {
    my $range = ["E$sid"."F$left","E$sid"."F$right"];
    $info{$sid}{hyp}{group}{$gid} = $range;
    foreach my $k ($left .. $right) {
      $info{$sid}{hyp}{form}{"E$sid"."F$k"} = $range;
    }
  }

  print "\t\tReading ref dependencies from log\n";
  open(CORPUS,"<$corpus") || die "can't open '$corpus': $!";

  while(<CORPUS>) {
##    $sid=$1 if (/<E ID="E(\d+)">/o);

    last if (m!<HYP>\s*<\\HYP>!);

    $sid=$1, $relations{$sid} = 1, $sentences{$sid}{n}=0 if (/<E id="E(\d+)".*?>/oi && $_ !~ /<HYP>/);
    
    $kind='ref', next if (m!^<\\REF>!); # strange XML
    $kind='hyp', next if (m!^<\\HYP>!); # strange XML

    if (m/^([rp-])\s+F([rp-])\s+RELATION\s+type=\s+(\S+)\s+ID=\s+(\S+)\s+arg1=\s+\((\S+) , (\S+)\)\s+arg2=\s+\((\S+) , (\S+)\)(.*)/) {
      my ($status,$fuzzy,$type,$rid,$l1,$r1,$l2,$r2,$arg3)=($1,$2,$3,$4,$5,$6,$7,$8,$9);
      my ($l3,$r3,$code) = (0,0,0);
      if ($arg3) {
	if ($arg3 =~ /\(code\)=\s+(\S+)/) {
	  $code = $1;
	} else {
	  $arg3 =~ /arg3=\s+\((\S+) , (\S+)\)/;
	  ($l3,$r3) = ($1,$2);
	}
      }
      $status = ($status eq '-') ? 0 : 1;
      $fuzzy = ($fuzzy eq '-') ? 0 : 1;
      if ($kind eq 'ref') {
	# Here, we should expand to take into account groups when needed
	# print "*** HERE $base $sid\n";
	my $g1 = [$l1,$r1];
	my $g2 = [$l2,$r2];
	my $g3;
	$g1 = $info{$sid}{ref}{form}{$l1} 
	  if ($l1 == $r1 && exists $info{$sid}{ref}{form}{$l1});
	$g2 = $info{$sid}{ref}{form}{$l2} 
	  if ($l2 == $r2 && exists $info{$sid}{ref}{form}{$l2});
	if ($l3 && $l3 == $r3 && exists $info{$sid}{ref}{form}{$l3}) {
	  $g3 = $info{$sid}{ref}{form}{$l3};
	  $l3 = $g3->[0];
	  $r3 = $g3->[1];
	}
	if ($g1->[0] ne $g2->[0] || $g1->[1] ne $g2->[1]) {
	  $l1 = $g1->[0];
	  $r1 = $g1->[1];
	  $l2 = $g2->[0];
	  $r2 = $g2->[1];
	}
	if (exists $info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}{$l3}{$r3}{$code}) {
	  print "*** Duplicate relation $base $sid $rid\n";
	} else {
	  $info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}{$l3}{$r3}{$code} = { found => 0, rid => $rid };
	  $sentences{$sid}{n}++;
	}
      }
    }
    
    if (/EVAL ENONCE precision= (\S+) rappel= (\S+) f_mesure= (\S+)/) {
      my ($p,$r,$f) = ($2,$3,$4);
      ## print "\t\tLogging $base $sid\n"; 
      ##      $sentences{$sid}->{p,r,f} = ($2,$3,$4);
    }

  }
  close(CORPUS);

  my $file = basename($corpus,qw/_result_relations/);
  $file = "$file.xml";

  print "\t\tReading dependencies from XML\n";
  open(CORPUS,"<$file") || die "can't open '$file':$!";
  my $groupe;
  while (<CORPUS>) {
##    print "Current $_";
    if (/<E id="E(\d+)".*?>/) {
      $sid=$1;
      $precision{$sid}=0;
      next if (exists $relations{$sid});
      # unannotated sentence: we skip
      while (<CORPUS>) {
	last if (m!</E>!);
      }
      next;
    }
    if (/<relation .+type="(\S+)".*>/) {
      my $type=$1;
      my $code = 0;
      my @args = ();
      my $prec = 0;
      my $keep = 1;
      my ($rid) = ($_ =~ /id="(\S+)"/);
      while (<CORPUS>) {
	last if (m!</relation>!);
	if (/href="(\S+)"/) {
	  my $k = $1;
	  if ($k =~ /E(\d+)/ && ($1 != $sid)) {
	    print "*** Extra sentence hyp relation $base $sid: $type $k\n"; 
	    $keep = 0;
	  }
	  if ($k =~ /E\d+G\d+/) {
	    if (! exists $info{$sid}{hyp}{group}{$k}) {
	      print "*** Missing hyp group $base $sid: $type $k\n"; 
	      $keep = 0;
	    }
	    push(@args,
		 $info{$sid}{hyp}{group}{$k}[0],
		 $info{$sid}{hyp}{group}{$k}[1]
		);
	  } else {
	    # the arg is a form
	    push(@args, $k,$k);
	  }
	} else {
	  ($code) = (/"(\S+)"/);
	  push(@args,0,0);
	}
      }
      next unless ($keep);
      push(@args,0,0) if (@args == 4);
      push(@args,$code);
      $precision{$sid}++;
      if (exists $info{$sid}{rel}{$type}{$args[0]}{$args[1]}{$args[2]}{$args[3]}{$args[4]}{$args[5]}{$args[6]}) {
	$prec = 1;
	$info{$sid}{rel}{$type}{$args[0]}{$args[1]}{$args[2]}{$args[3]}{$args[4]}{$args[5]}{$args[6]}{found} = 1;
      }
      $hyp_sth->execute("$base",$sid,$type,@args,$rid,$prec);
      next;
    }
  }
  close(CORPUS);

  print "\t\tComputing figures\n";

  foreach my $sid (keys %relations) {
    next unless (exists $info{$sid}{rel});
##    print "*** Handling $base $sid\n";
    my $n=0;
    my $precision=$precision{$sid} || 0;
    my $recall=0;
    foreach my $type (keys %{$info{$sid}{rel}}) {
      foreach my $l1 (keys %{$info{$sid}{rel}{$type}}) {
	foreach my $r1 (keys %{$info{$sid}{rel}{$type}{$l1}}) {
	  foreach my $l2 (keys %{$info{$sid}{rel}{$type}{$l1}{$r1}}) {
	    foreach my $r2 (keys %{$info{$sid}{rel}{$type}{$l1}{$r1}{$l2}}) {
	      foreach my $l3 (keys %{$info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}}) {
		foreach my $r3 (keys %{$info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}{$l3}}) {
		  foreach my $code (keys %{$info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}{$l3}{$r3}}) {
		    my $rec = $info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}{$l3}{$r3}{$code}{found};
		    my $rid = $info{$sid}{rel}{$type}{$l1}{$r1}{$l2}{$r2}{$l3}{$r3}{$code}{rid};
		    $n++;
		    $recall += $rec;
		    $ref_sth->execute("$base",$sid,$type,$l1,$r1,$l2,$r2,$l3,$r3,$code,$rid,$rec);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    if ($n != $sentences{$sid}{n}) {
      print "*** Mismatch #relations for $base $sid: $n vs  $sentences{$sid}{n}\n";
    }
    my $tmp = $recall;
    $recall = $tmp / $n if ($n);
    $precision = ($precision) ? $tmp / $precision : 1;
    my $f=0;
    if ($recall==0 || $precision==0) {
      $f=0;
    } else {
      $f = 2 * $recall * $precision / ($recall + $precision );
    }
    $log_sth->execute("$base",$sid,100*$precision,100*$recall,$f);
  }
  foreach my $sid (keys %relations) {
    next if (exists $info{$sid}{rel});
    ## no ref relations for these annotated sentences
    my $p = $precision{$sid} ? 0 : 100;
    my $r = 100;
    my $f = $p ? 1 : 0;
    print "*** No ref relations $base $sid\n";
    $log_sth->execute("$base",$sid,$p,$r,$f);
  }
  $dbhrel->commit;
}

$dbhrel->commit;
$dbhrel->disconnect;

$dbhconst->disconnect;

=head1 NAME

rel2db.pl -- Transfering content of EASY log files for relations to a SQLite Database

=head1 SYNOPSIS

./rel2db.pl

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
