#!/usr/bin/perl

use File::Basename;

my $dag = shift || 'dag';
my $ext = shift || 'udag';

my %info = ();

my %exclude = ();

if (-f "$dag/exclude.txt") {
  open(EXCLUDE,"<$dag/exclude.txt") or die "can't open exclude file: $!";
  while( <EXCLUDE> ) {
    my ($corpus,$i) = /^(\S+)\.E(\d+(?:\.\d+)?)\s/;
    $exclude{$corpus}{$i} = 1;
  }
  close(EXCLUDE);
}

my $all_n=0;

foreach my $file (<$dag/*.$ext>) {
  open(FILE,"<$file") or die "can't open file: $!";
  $file = basename($file,".$ext");
  $all_n += handle_file($file);
}

foreach my $file (<$dag/*.$ext.bz2>) {
  open(FILE,"bzcat $file|") or die "can't open file: $!";
  $file = basename($file,".$ext.bz2");
  $all_n += handle_file($file);
}


sub handle_file {
  my $file = shift;
  my $n = 0;
  while (<FILE>) {
    if (/^##\s*(DAG|MAF)\s+BEGIN/) {
      my ($i) = (<FILE> =~ /id="E(\d+(?:\.\d+))F\d+"/);
      if (exists $exclude{$file}{$i}) {
##	print STDERR "Exclude $file $i\n";
      } else {
	$n++;
      }
    }
  }
  $info{$file} = $n;
  close(FILE);
  return $n;
}

print "## total=$all_n\n";
foreach my $file (sort {$info{$b} <=> $info{$a}} keys %info) {
  printf "%-20s %4i\n",$file,$info{$file};
}

=head1 NAME

extract_info.pl -- create info.txt

=head1 DESCRIPTION

Return number of sentences for each corpus.
Needs to be processed before processing corpora with ATOLL linguistic 
processing chain.

=head1 SYNOPSIS 	 
  	 
./extract_info.pl [options] 

Usually:

./extract_info.pl <dir containing dags> <dag|udag> > info.txt

Example:

./extract_info.pl dagdir udag > dagdir/info.txt

Use option dag if directory contains files in .dag.

Use option udag if directory contains files in .udag.


=head1 AUTHOR

=over 4

=item Eric de la Clergerie, Eric.De_La_Clergerie@inria.fr

=back

=cut
