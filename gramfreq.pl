#!/usr/bin/perl

use strict;
use File::Basename;
use Term::Report;
use POSIX qw(strftime floor);

use AppConfig qw/:argcount :expand/;
use Date::Manip;

my $config = AppConfig->new(
			    "host|h=s@"         => {DEFAULT => []},
			    "port|p=s"          => {DEFAULT => 8999},
			    "corpus=f@"         => {DEFAULT => []},
			    "exclude=s@"        => {DEFAULT => []}, ## exclude corpus
			    "parser=s"          => {DEFAULT => "frmgtel"},
			    "rparser=s"         => {DEFAULT => "frmgtelr"},
			    "input|in|i|t=s"    => {DEFAULT => ''},
			    "verbose|v!"        => {DEFAULT => 1},
			    "stats|s!"          => {DEFAULT => 0},
			    
			    "forest!"           => {DEFAULT => 0},
			    "display|d=s"       => {DEFAULT => ''},
			    #    "dependency!"       => {DEFAULT => 0}, # display graph
			    "grammar!"          => {DEFAULT => 0},
			    "tagger!"           => {DEFAULT => 0},
			    "xmldep!"           => {DEFAULT => 0},
			    "lpdep!"            => {DEFAULT => 0},
			    "dotdep!"           => {DEFAULT => 0},
			    "easy!"             => {DEFAULT => 0},
			    "easyhtml!"         => {DEFAULT => 0},
			    
			    "errfile=s"         => {DEFAULT => "errors"},
			    "setsid!"           => {DEFAULT => 0},
			    "log_file=f"        => {DEFAULT => '/tmp/dispatch.log'},
			    "status!"           => {DEFAULT => 0},
			    "time!"             => {DEFAULT => 0},
			    "robust!"           => {DEFAULT => 0},
			    "easyinput"         => {DEFAULT => 0},
			    "timeout=i"         => {DEFAULT => 200},
			    
			    "colldir=f"         => {DEFAULT => "./results"},
			    "collsave!"         => {DEFAULT => 0},
			    "collbase=f"        => {DEFAULT => 'sentence'},,
			    
			    "allparse!"         => {DEFAULT => 0},
			    "showdag!"          => {DEFAULT => 0},
			    "run!"              => {DEFAULT => 1},
			    "final!"            => {DEFAULT => 1},
			    "disambiguate|dis!" => {DEFAULT => 0},
			    "date!"             => {DEFAULT => 0},
			    "printhost!"        => {DEFAULT => 0},
			    "results=f"         => {DEFAULT => 'results'},
			    "dagdir|dag=f"          => {DEFAULT => 'dag'},
			    "dagext=s"          => {DEFAULT => "udag"},
			    "lock=f"            => {DEFAULT => 'lock'},
			    
			    "compress!"         => {DEFAULT => 0},
			    "kill=i"            => {DEFAULT => 0},
			    "adj_max=i",
			    CREATE => 1
			   );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;
my @corpus  = @{$config->corpus};
my $dag     = $config->dagdir;
my $dagext  = $config->dagext;

my %trees   = ();
my %labels  = ();
my %kinds   = ();

my %dependencies = ();
my @dependencies = ();
my %anchors = ();
my $anchors = 0;

unless (@corpus) {
  my $info = "$dag/info.txt";
  $info    = "$results/info.txt" unless (-r $info);
  unless (-r $info) {
    die "can't find $info in $dag or $results: $!";
  }

  open(INFO,$info) or die "can't open $info: $!";

  while(<INFO>){
    chomp;
    my ($corpus,$size) = split(/\s+/,$_);
    push(@corpus,$corpus);
  }
  close(INFO);
}

foreach my $corpus (@corpus) {
  my $log = "$results/$corpus.log";
  if (-r $log) {
    open(LOG,"<$log") || die "can't open file:$!";
  } elsif (-r "$log.gz") {
    open(LOG,"-|","gunzip -c $log.gz") || die "can't open $log:$!";
  } elsif (-r "$log.bz2") {
    open(LOG,"-|","bunzip2 -c $log.bz2") || die "can't open $log:$!";
  } else {
    next;
  }
  print STDERR "Processing $log\n";
  my $id;
  while (<LOG>) {
    if (/^ok\s+(\d+)/) {
      $id = $1;
      analyze_forest($corpus,$id);
      next;
    }
  }
  close(LOG);
}

open(FREQ,">$results/trees.freq") || die "can't open file '$results/trees.freq': $!";
foreach my $key (sort {$trees{$b} <=> $trees{$a}} keys %trees) {
  print FREQ "$trees{$key}\t$key\n";
}
close(FREQ);

open(FREQ,">$results/labels.freq") || die "can't open file '$results/labels.freq': $!";
foreach my $key (sort {$labels{$b} <=> $labels{$a}} keys %labels) {
  print FREQ "$labels{$key}\t$key\n";
}
close(FREQ);

open(FREQ,">$results/kinds.freq") || die "can't open file '$results/kinds.freq': $!";
foreach my $key (sort {$kinds{$b} <=> $kinds{$a}} keys %kinds) {
  print FREQ "$kinds{$key}\t$key\n";
}
close(FREQ);

open(FREQ,">$results/deps.freq") || die "can't open file '$results/deps.freq': $!";
my @dep=();
foreach my $target (values %dependencies) {
  foreach my $label (values %$target) {
    foreach my $kind (values %$label) {
      foreach my $dir (values %$kind) {
	foreach my $dep (values %$dir) {
	  $dep->{w} = $dep->{w} / $dep->{n};
	  $dep->{o} = $dep->{w} * log($dep->{n});
	  push(@dep,$dep);
	}
      }
    }
  }
}
foreach my $dep (sort {$b->{o} <=> $a->{o}} @dep) {
  my $source = anchor2string($dep->{source});
  my $target = anchor2string($dep->{target});
  my $kind = $dep->{kind};
  my $label= $dep->{label};
  my $dir= $dep->{dir};
  my $w = sprintf("%5.2f", 100 * $dep->{w});
  print FREQ "$dep->{n}\t$w\t$source--{$label:$kind:$dir}-->$target\n";  
}
close(FREQ);

sub anchor2string {
  my $anchor = shift;
  unless ($anchor->{string}) {
    my $tree = $anchor->{tree};
    $tree = $1 if ($tree =~ /^(\d+)/);
    $anchor->{string} = "$anchor->{form}/$anchor->{lemma}/$anchor->{cat}/$tree";
  }
  return $anchor->{string};
}

sub analyze_forest {
  my $corpus = shift;
  my $eid     = shift;
  my $l1 = floor($eid / 10000);
  my $l2 = floor(($eid - ($l1 * 10000)) / 1000);
  my $forest = (-d "$results/$corpus/$l1/$l2") ?
    "$results/$corpus/$l1/$l2/$corpus.E$eid.dep.xml"
      : "$results/$corpus/$corpus.E$eid.dep.xml";
  if (-r $forest) {
    open(FOREST,"<$forest") || die "can't open forest:$!";
  } elsif (-r "$forest.gz") {
    open(FOREST,"-|","gunzip -c $forest.gz") || die "can't open forest:$!";
  } elsif (-r "$forest.bz2") {
    open(FOREST,"-|","bunzip2 -c $forest.bz2") || die "can't open forest:$!";
  } else {
    next;
  }
  my %node=();
  while (<FOREST>) {
    ++$trees{$1} if (/tree="(.+?)"/);
    if (/<edge/ && /label="(.+?)".*type="(.+?)"/) {
      ++$labels{$1};
      ++$kinds{$2};
    }
    if (/<node/) {
      my ($id) = /\bid="(.*?)"/;
      my ($cat) = /\bcat="(.*?)"/;
      my ($form) = /\bform="(.*?)"/;
      my ($lemma) = /\blemma="(.*?)"/;
      my ($tree) = /\btree="(.*?)"/;
      my ($derivs) = /\bderiv="(.*?)"/;
      my ($left,$right) = /\bcluster="c_(\d+)_(\d+)"/;
      my $skel2 =  $anchors{'SKEL2'}{'SKEL2'}{$cat}{'SKEL2'}
	||= { form => 'SKEL2',
	      lemma => 'SKEL2',
	      cat => $cat,
	      tree => 'SKEL2',
	      id => ++$anchors
	    };
      my $skel =  $anchors{'SKEL'}{'SKEL'}{$cat}{$tree}
	||= { form => 'SKEL',
	      lemma => 'SKEL',
	      cat => $cat,
	      tree => $tree,
	      id => ++$anchors,
	      skel => $skel2
	    };
      my $anchor = $anchors{$form}{$lemma}{$cat}{$tree} 
	||= { form => $form,
	      lemma => $lemma,
	      cat => $cat,
	      tree => $tree,
	      id => ++$anchors,
	      skel => $skel
	    };
      $node{$id} = { derivs => scalar(split(/\s+/,$derivs)) || 1,
		     left => $left,
		     right => $right,
		     anchor => $anchor
		   };
##      print "Register anchor $form $lemma $cat $tree $node{$id}{id}\n";
    }
    if (/<edge/) {
      my ($source) = /\bsource="(.*?)"/;
      my ($target) = /\btarget="(.*?)"/;
      my ($label) = /\blabel="(.*?)"/;
      my ($kind) = /\btype="(.*?)"/;
      my ($id) = /\bid="(.*?)"/;
      my $nsource = $node{$source};
      my $ntarget = $node{$target};
      my $dir = ($nsource->{left} >= $ntarget->{right}) ? 'left' : 'right';
      my $dep = $dependencies{$nsource->{anchor}{id}}{$ntarget->{anchor}{id}}{$label}{$kind}{$dir}
	||= { source => $nsource->{anchor},
	      target => $ntarget->{anchor},
	      kind => $kind,
	      label => $label,
	      dir => $dir,
	      n => 0,
	      w => 0
	    };
      $dep->{n}++;
      my $w=0;
      while (<FOREST>) {
	last if (m{</edge>});
	my ($derivs) = /names="(.*?)"/;
	my %h = map {$_ => 1 } split(/\s+/,$derivs);
	$w += keys %h;
      }
      $w ||= $nsource->{derivs};
      my $r = $w / $nsource->{derivs};
      if ($r > 1.0) {
	print "WARNING: r=$r for edge $id $source->$target in ${corpus}_E$eid\n";
	$r = 1;
      }
      $dep->{w} += $r;
      my $depskel = $dependencies{$nsource->{anchor}{skel}{id}}{$ntarget->{anchor}{skel}{id}}{$label}{$kind}{$dir} 
	||= { source => $nsource->{anchor}{skel},
	      target => $ntarget->{anchor}{skel},
	      kind => $kind,
	      label => $label,
	      dir => $dir,
	      n => 0,
	      w => 0
	    };
      $depskel->{n}++;
      $depskel->{w} += $r;

      my $depskel2 = $dependencies{$nsource->{anchor}{skel}{skel}{id}}{$ntarget->{anchor}{skel}{skel}{id}}{$label}{$kind}{$dir}
	||= { source => $nsource->{anchor}{skel}{skel},
	      target => $ntarget->{anchor}{skel}{skel},
	      kind => $kind,
	      label => $label,
	      dir => $dir,
	      n => 0,
	      w => 0
	    };
      $depskel2->{n}++;
      $depskel2->{w} += $r;
##      print "Register edge '$source'($node{$source}{id}) '$target'($node{$target}{id}) '$kind' '$label' n=$dep->{n}\n";
    }
  }
  close(FOREST);
}

=head1 NAME

gramfreq.pl -- Extract grammar construction frequencies from successul parses

=head1 SYNOPSIS 	 
  	 
./gramfreq.pl

=head1 DESCRIPTION

Run gramfreq.pl and check these files under ${RESULTS}/ :

=over 4

=item tree.freq

=item labels.freq

=item kinds.freq

=back

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
