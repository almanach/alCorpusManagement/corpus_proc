#!/usr/bin/env perl

use strict;

use AppConfig;
use IPC::Open2;
use EV;
use AnyEvent;
use IO::Handle;

my $config = AppConfig->new(
                            'verbose|v!' => {DEFAULT => 0},
                            'sid2sentence=f' => {DEFAULT => 'sid2sentence.pl'}
                           );

my $args = join(' ',@ARGV);

$config->args;

my $sid2sentence = $config->sid2sentence;

sub start_sid2sentence {

  my $pid = open2(\*SOUT,\*SIN,"$sid2sentence")
    || die "can't run $sid2sentence: $!";
  
  SIN->autoflush(1);
  SOUT->autoflush(1);
}

sub sid2sentence ($) {
  my $sid = shift;
  print SIN "$sid\n";
  my $s = <SOUT>;
  $s =~ /^<(S+)>\s*(.*?)/;
  return [$1,$2];
}

my $data = {};
my $tmp = {};

my $stats = {};

my $total = {};

my $count = 0;

start_sid2sentence();

while (<>) {
  next if (/^\*\*/);
  next if (/Register/);
  ##  chomp;
  my ($pat,$n,$l) = split(/\t/,$_);
  next unless ($n =~ /^\d+$/);
  my ($c1,$conj,$conjcat,$c2) = $pat =~ /c1=\{(.*)\}_(.+)_(.+)_c2=\{(.*)\}/;
  my @c1 = split(/_/,$c1);
  my @c2 = split(/_/,$c2);
  my $kind1 = $c1[0];
  my $kind2 = $c2[0];
  my $comp1 = ($c1[-1] eq 'completive') ? 'comp' : 'nocomp';
  my $comp2 = ($c2[-1] eq 'completive') ? 'comp' : 'nocomp';
  my $short = join('_',$kind1,$comp1,$kind2,$comp2);
  $pat = "c1={${kind1}_${comp1}}_${conj}_${conjcat}_c2={${kind2}_${comp2}}";
  my $sentry = $stats->{$conj} ||= { n => 0, conj => $conj };
  register_stats($sentry,$kind1,$kind2,$comp1,$comp2,$n);
  register_stats($total,$kind1,$kind2,$comp1,$comp2,$n);

  my $x = $sentry->{labels}{$pat} ||= { short => $short, n => 0 };
  $x->{n} += $n;
  my $entry = $data->{$pat} ||= {pat => $pat, n => 0, conj => $conj, sentences => []};
  $entry->{n} += $n;
  foreach my $sid (split(/\s+/,$l)) {
    next unless ($sid =~ /E\d+/);
    $count++;
    print SIN "$sid\n";
    push(@{$tmp->{$sid}},$entry);
    if (! ($count % 1000) ) {
      flush_sid2sentence();
      start_sid2sentence();
    }
  }
  print STDERR "*** pat $pat l=$l";
}

print STDERR "Done data reading\n";

flush_sid2sentence();

sub flush_sid2sentence {
  close(SIN);
  while (<SOUT>) {
    my ($sid,$sorig) = /^<(\S+)>\s*(.*)$/;
   ##  print "+++ GOT $sid\n";
    foreach my $entry (@{$tmp->{$sid}}) {
      my $conj = $entry->{conj};
      $conj =~ s/\s+/\\s+/g;
      my $s = $sorig;
      if ($conj =~ /que$/) {
	$conj =~ s/e$//;
      $s =~ s{(?:^|\s+)(${conj}e?)(\s+|')}{ <span class="hilight">$1</span>$2}ig;
      } elsif ($conj eq "si") {
	$s =~ s{(?:^|\s+)(s(?:i)?)(\s+|')}{ <span class="hilight">$1</span>$2}ig;
      } else {
	$s =~ s{(?:^|\s+)($conj)\s+}{ <span class="hilight">$1</span> }ig;
      }
      push(@{$entry->{sentences}},[$sid,$s]);
    }
  }
  close(SOUT);
}

print STDERR "Done sentence reading\n";

undef $tmp;

sub register_stats {
  my ($entry,$kind1,$kind2,$comp1,$comp2,$n) = @_;
  $entry->{n} += $n;
  ((($entry->{stats}{$kind1} ||= {})
    ->{$kind2} ||= {})
   ->{$comp1} ||= {})->{$comp2} += $n;
}

use Template;

my $tt=Template->new({});

$tt->process(\*DATA,
	     { 
	      stats => [map {$stats->{$_}} sort {$stats->{$b}{n} <=> $stats->{$a}{n}} keys %$stats],
	      data => [map {$data->{$_}} sort {$data->{$b}{n} <=> $data->{$a}{n}} keys %$data],
	      total => $total,
	      kind => [qw{je pers impers nosubj}],
	      comp => [qw{nocomp comp}],
	     }
	    );

__END__
[% USE format %]
[% pc = format('%.2f%%') %]
[% USE date %]
<html>
  <head>
   <title>Exemplier</title>
   <style type="text/css">
     <!--
       .sid { color: red; }
       .hilight { background: lightgreen; }
       thead tr.total {font-weight: bolder; background: orange; }
       tbody tr:nth-child(even) {background: #CCC}
       tbody tr:nth-child(odd) {background: #FFF}
     -->
   </style>
   <script type="text/javascript" language="javascript">
<!--
        function showhide(obj) {
           var el=document.getElementById(obj);
           if (el.style.display == "none") {
              el.style.display = "block";
           } else {
              el.style.display = "none";
           }
        }
        function show(obj) {
           var el=document.getElementById(obj);
	   el.style.display = "block";
	}
-->
  </script>
  </head>
  <h1> Statistiques </h1>
   <table rules="groups" frame="box">
     <thead>
      <tr>
       <th>conjonction</th>
       <th>n</th>
       <th>table</th>
       <th>keys</th>
      </tr>
     </thead>


    <thead>
       <tr class="total">
	 <td>Total</td>
	 <td align="right">[% total.n %]</td>

	 <td align="right">
	  <table>	    
	    <tr><th>c1|c2</th>[% FOREACH k2 IN kind %] <th>[% k2 %]</th> [% END %]</tr>
            [% FOREACH k1 IN kind %]
              <tr>
                <th>[% k1 %]</th>
               [% FOREACH k2 IN kind %]
                 <td>
                  <table>
	               [% FOREACH comp1 IN comp %]
                        <tr>
                          [% FOREACH comp2 IN comp %]
                            <td> [%  pc(100 * total.stats.$k1.$k2.$comp1.$comp2 / total.n ) %]</td>
                          [% END %]
                        <tr>
                       [% END %]
                  </table>
                 </td>
               [% END %]
              </tr>
            [% END %]
	  </table>
	  </td>

         <td></td>
       </tr>
     </thead>


     <tbody>

     [% FOREACH sentry IN stats %]
       <tr>
         <td>[% sentry.conj %]</td>
         <td align="right">[% sentry.n %]</td>
	 <td align="right">
	  <table>	    
	    <tr><th>c1|c2</th>[% FOREACH k2 IN kind %] <th>[% k2 %]</th> [% END %]</tr>
            [% FOREACH k1 IN kind %]
              <tr>
                <th>[% k1 %]</th>
               [% FOREACH k2 IN kind %]
                 <td>
                  <table>
	               [% FOREACH comp1 IN comp %]
                        <tr>
                          [% FOREACH comp2 IN comp %]
                            <td> [%  pc(100 * sentry.stats.$k1.$k2.$comp1.$comp2 / sentry.n ) %]</td>
                          [% END %]
                        <tr>
                       [% END %]
                  </table>
                 </td>
               [% END %]
              </tr>
            [% END %]
	  </table>
	  </td>
	   <td align="right">
              [% FOREACH k IN sentry.labels.keys %]
		<a href="#[%- k %]" onclick="show('[%- k %]')">[%- sentry.labels.$k.short %] ([%- sentry.labels.$k.n %]) </a>
              [% END %]
           </td>
       </tr>
     [% END %]
     </tbody>
   </table>
  <h1> Exemplier </h1>
	   <p> Cliquer sur les labels pour voir ou non les phrases exemples </p>
  <ul>
  [%- FOREACH entry IN data %]
  <li> <a name="[%- entry.pat %]" onclick="showhide('[%- entry.pat %]');">[% entry.pat %] ([% entry.n %])</a>
    <div id="[%- entry.pat %]" style="display:none;">
    <ul>
       [% FOREACH s IN entry.sentences %]
          <li> <span class="sid" title="[%- entry.pat -%]">[% s.0 %]</span> <span class="s">[% s.1 %]</span> </li>
       [% END %]
    </ul>
    </div>
  </li>
  [%- END %]
  </ul>
  <hr/>
  <p> Une production <a href="mailto:Eric.De_La_Clergerie@inria.fr">�ric de la Clergerie</a> INRIA/ALPAGE [% date.format(date.now,format='%d-%m-%y %H:%M:%S',locale = 'fr_FR') %] </p>
</html>
