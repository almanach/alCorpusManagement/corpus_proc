#!/usr/bin/perl

# to explore the differences between two runs
#  - sentences that becomes OK or not
#  - large differences in time

## example: 
## ./run_diffs -diff <spec>

use strict;
use File::Basename;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "diff=s",
			    "html=f" => {DEFAULT => "diffruns.html"}
                           );

$config->args();

my $html = $config->html;
my $diff = find_diff_file();

my $corpus;

my %corpus = ();

foreach my $log (glob("*.log.bz2")) {
  $log =~ m{(\w+)\.log\.bz2$};
  my $corpus = $1;
#  print "Dealing with '$corpus' $log\n";
  my $entry = $corpus{$corpus} = { name => $corpus,
				   log => $log,
				 };
  $entry->{current} = process_log($log);
  $entry->{old} = process_log("$diff/$log");
}

my @ok = ();
my @bad = ();
my @vars = ();
my @passage = ();

foreach my $corpus (sort keys %corpus) {
  my $data = $corpus{$corpus};
  foreach my $sid (sort {$a <=> $b} keys %{$data->{current}}) {
    my $centry = $data->{current}{$sid};
    my $oentry = $data->{old}{$sid};
#    print "handling $corpus $sid\n";
    if ($centry->{status} eq 'ok' && $oentry->{status} ne 'ok') {
      push(@ok,[$corpus,$centry,$oentry]);
    }
    if ($oentry->{status} eq 'ok' && $centry->{status} ne 'ok') {
      push(@bad,[$corpus,$oentry,$centry]);
    }

    my $ctime = $centry->{time};
    my $otime = $oentry->{time};
    my $delta = ($ctime-$otime) / ($otime || 1);
    
    if ($ctime > 1 && abs($delta) > 0.5) {
      push(@vars,[sprintf("%.2f",100*$delta),$corpus,$oentry,$centry]);
    }

    {
      my $ctime = $centry->{timepassage};
      my $otime = $oentry->{timepassage};
      my $delta = ($ctime-$otime) / ($otime || 1);
      
      if ($ctime > 1 && abs($delta) > 0.5) {
	push(@passage,[sprintf("%.2f",100*$delta),$corpus,$oentry,$centry]);
      }
      
    }

  }
}

my $ok = scalar(@ok);

print <<EOF;

1. Changed to OK (good: $ok)

EOF

foreach my $x (@ok) {
  print "$x->[0].E$x->[1]{sid}\t$x->[2]{status}\t$x->[1]{txt}\n";
}

my $bad = scalar(@bad);

print <<EOF;

2. Changed from OK (bad: $bad)

EOF

foreach my $x (@bad) {
  print "$x->[0].E$x->[1]{sid}\t$x->[2]{status}\t$x->[1]{txt}\n";
}


print <<EOF;

3. Time variations

EOF

foreach my $x (sort {$a->[0] <=> $b->[0]} @vars) {
  printf("delta=%.2f%%\told=%.3fs\tnew=%.3fs\t%s.E%s\t%s\n\n",
	 $x->[0],
	 $x->[2]{time},
	 $x->[3]{time},
	 $x->[1],
	 $x->[2]{sid},
	 $x->[2]{txt}
	);
}


print <<EOF;

4. Passage Time variations

EOF

foreach my $x (sort {$a->[0] <=> $b->[0]} @passage) {
  printf("delta=%.2f%%\told=%.3fs\tnew=%.3fs\t%s.E%s\t%s\n\n",
	 $x->[0],
	 $x->[2]{timepassage},
	 $x->[3]{timepassage},
	 $x->[1],
	 $x->[2]{sid},
	 $x->[2]{txt}
	);
}


sub process_log {
  my $log = shift;
  my $info = {};
  my $sid;
  my $sentry;
  open(LOG,"bzcat $log|") or die "can't open $log: $!";
  while(<LOG>) {
    if (/^(ok|fail|robust)\s+(\d+(?:\.\d+)?)\s+(.+)$/) {
      $sentry = $info->{$2} = { status => $1, 
				sid => $2, 
				txt => $3 
			      };
#      print "register $2 $1 $3\n";
      next;
    }
    if (/<time>\s*(\S+)/) {
      $sentry->{time} = $1;
      next;
    }
    if (/<timepassage>\s*(\S+)/) {
      $sentry->{timepassage} = $1;
      next;
    }
  }
  close(LOG);
  return $info;
}

sub perc {
  my ($a,$b) = @_;
  return '-' if ($b == 0);
  return sprintf("%.2f%%",$a / $b * 100);
}

sub find_diff_file {
  my $diff = $config->diff;
  return unless ($diff);	# we are running for a diff
  if ($diff eq 'last') {
    ## try tp find last run for a diff
    my $where = `pwd`;
    chomp $where;
    $where =~ /(\d+)$/;
    my $current = $1;
    my $prev = $current-1;
    $diff = "../results.easyref$prev/";
  } elsif ($diff =~ /^\d+$/) {
    $diff = "../results.easyref$diff/";
  } elsif (-d $diff) {
    $diff .= "/";
  }
  return $diff if (-r $diff);
}
