/*
  javascript functions to help visualizing EASy annotation
  used by easyalt2html.xsl
*/

function relation_findPosX(obj) 
{
  var curleft = 0;
  if (obj.offsetParent) 
  {
    while (obj.offsetParent) 
        {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}

function relation_findPosY(obj) 
{
    var curtop = 0;
    if (obj.offsetParent) 
    {
        while (obj.offsetParent) 
        {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
}

var saved = new Array;

function relation_hide(rid,elt) 
{
    rel = document.getElementById(rid); 
    rel.style.visibility = 'hidden';
    elt.style.backgroundColor = saved['relation'];
    var table = elt.getElementsByTagName('arg');
    for (i=0; i < table.length; i++ ) {
        var arg = document.getElementById(table[i].getAttribute('aid'));
        if (arg) {
            arg.style.backgroundColor=saved[table[i].getAttribute('aid')];
            arg.style.fontWeight = "lighter";
        }
    }
    saved = new Array;
}

function relation_show(rid,elt)
{
    rel = document.getElementById(rid);
    posX=290;
    posY=50;
    if ((rel.style.top == '' || rel.style.top == 0) 
        && (rel.style.left == '' || rel.style.left == 0))
    {
            // need to fixate default size (MSIE problem)
        rel.style.width = rel.offsetWidth + 'px';
        rel.style.height = rel.offsetHeight + 'px';

            // if tooltip is too wide, shift left to be within parent 
        if (posX + rel.offsetWidth > elt.offsetWidth) posX = elt.offsetWidth - rel.offsetWidth;
        if (posX < 0 ) posX = 0; 
        
        x = relation_findPosX(elt) + posX;
        y = relation_findPosY(elt) + posY;
        
        rel.style.top = y + 'px';
        rel.style.left = x + 'px';
        rel.style.opacity = .95;
        rel.style.backgroundColor = "#d6d6fc";
        
    }
    rel.style.visibility = 'visible';
    saved['relation'] = elt.style.backgroundColor;
    elt.style.backgroundColor = 'Gold';
    var table = elt.getElementsByTagName('arg');
    for (i=0; i < table.length; i++ ) {
        var arg = document.getElementById(table[i].getAttribute('aid'));
        if (arg) {
            saved[table[i].getAttribute('aid')] = arg.style.backgroundColor;
            arg.style.backgroundColor = 'Gold';
            arg.style.fontWeight = "bolder";
        }
    }
}

function editorOpen (corpus,sentence) {
   var Editor = document.createElement("textarea");
   Editor.id="editor"+sentence;
   Editor.value="type report for this sentence";
   var comment = document.getElementById("comment"+sentence);
   if (comment) {
        Editor.value= comment.innerHTML;
        comment.parentNode.removeChild(comment);
   }
   Editor.setAttribute("rows",10);
   Editor.setAttribute("cols",150);
   document.getElementById("comments"+sentence).appendChild(Editor);
   button = document.getElementById("editorButton"+sentence);
   button.setAttribute("title","Editor Button");
   button.innerHTML="close comment";
   button.onclick = function () { editorClose(corpus,sentence); };
   Editor.focus();
}

function editorClose (corpus,sentence) {
//    alert("Soumettre "+corpus+" "+sentence);
   var button = document.getElementById("editorButton"+sentence);
   button.innerHTML="Editer un rapport d'erreur";
   button.onclick = function () { editorOpen(corpus,sentence); };
   var editor = document.getElementById("editor"+sentence);
   var parent = editor.parentNode;
   var info = editor.value;
   parent.removeChild(editor);
   makeRequest('handlecomment.pl',"?corpus="+URLencode(corpus)+"&sentence="+escape(sentence)+"&comment="+URLencode(info),
               corpus,
               sentence
               );
}



var http_request = false;

    
function makeRequest(url, parameters, corpus, sentence) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
      http_request.onreadystatechange = function() { alertContent(corpus,sentence) };
      http_request.open('GET', url + parameters, true);
      http_request.send(null);
   }


function alertContent(corpus,sentence) {
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
            
            var comment = http_request.responseText;
//            alert(comment);
            if (comment) {
                var textNode = document.createTextNode(comment);
                commentNode = document.createElement('pre');
                commentNode.id = 'comment'+sentence;
                commentNode.appendChild(textNode);
                document.getElementById("comments"+sentence).appendChild(commentNode);
            }
        } else {
            alert('There was a problem with the request.');
        }
    }
}


function getStoredComments (corpus,sentence) {
  alert("looking for comment on "+corpus+" "+sentence);
  makeRequest('handlecomment.pl',"?corpus="+escape(corpus)+"&sentence="+escape(sentence),corpus,sentence);
}

function URLencode(sStr) {
    return escape(sStr).
             replace(/\\+/g, '%2B').
                replace(/\\"/g,'%22'). //"
                   replace(/\\'/g, '%27'). //'
                     replace(/\//g,'%2F');
  }
