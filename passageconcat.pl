#!/usr/bin/env perl

use 5.006;
use strict;
use warnings;
use Carp;
use POSIX qw(strftime floor);
use POSIX ":sys_wait_h";

## use IPC::Run qw(start finish run pump timeout);

use AppConfig qw/:argcount :expand/;

##use File::Temp qw/tempfile/;
##use File::Basename;

##use XML::Parser;

my $config = AppConfig->new(
			    { CREATE => 1 },
			    'verbose!'        => {DEFAULT => 1},
			    'results=f'       => {DEFAULT => 'results'},
			    'dest=f'          => {DEFAULT => 'concat'},
			    "corpus=f@"       => {DEFAULT => [] },
			    "dagdir=f"        => {DEFAULT => 'dag'},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "set=s"           => {DEFAULT => "frmg"},
			    "strict!"           => {DEFAULT => 0},
                            "compress!"        => {DEFAULT => 1},
                            "logdir=f",
			    "logfile=f" => { DEFAULT => "concat.log"},
			    "tagset=f",
			    "tmpdir=f", # to be used when untaring in $results is too  slow (if nfs dir)
			    "filemap=f" # a file to provide file mapping when necessary
			   );


my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;
my $dest = $config->dest;
my $dagdir = $config->dagdir;
my $dagext = $config->dagext;
my $annotset = $config->set;
my $strict = $config->strict;
my $tagset = $config->tagset;
my $log;
my $tmpdir = $config->tmpdir || $results;

my $mapping = {
	       'v' => sub { 'verb' },
	       'nc' => sub { 'commonNoun' },
	       'prep' => sub { 'preposition' },
	       'adv' => sub { 'adverb' },
	       'cln' => sub { 'personalPronoun' },
	       'cla' => sub { 'personalPronoun' },
	       'cld' => sub { 'personalPronoun' },
	       'cll' => sub { 'personalPronoun' },
	       'clg' => sub { 'personalPronoun' },
	       'clr' => sub { 'reflexivePronoun' },
	       'coo' => sub {'coordinatingConjunction' },
	       'csu' => sub { 'subordinatingConjunction' },
	       'aux' => sub { 'verb' },
	       'np' => sub { 'properNoun' },
	       'prel' => sub { 'relativePronoun' },
	       'pri' => sub { 'interrogativePronoun' }, 
	       'clneg' => sub { 'negativeParticle' },
	       'advneg' => sub { 'negativeParticle' },
	       'pro' => sub { my $tag = shift;
			      $tag && $tag =~ /person\./ && return 'personalPronoun'; 
			      return 'personalPronoun';
			    },
	       'xpro' => sub { 0 },
	       'que' => sub { 'subordinatingConjunction' },
	       'que_restr' => sub { 0 },
	       '_' => sub { 0 },
	       'ce' => sub { 'personalPronoun' },
	       'ncpred' => sub { 'commonNoun' },
	       'adjPref' => sub { 0 },
	       'advPref' => sub { 0 },
	       'pres' => sub { 'interjection' },
	       'det' => sub { my $tag = shift;
			      if ($tag) {
				$tag =~ /def\./ && return 'definiteArticle';
				$tag =~ /dem\.plus/ && return 'demonstrativeDeterminer';
				$tag =~ /numberposs\.(sg|pl)/ && return 'possessiveDeterminer';
			      }
			      return 'definiteArticle';
			    },
	       'predet' => sub { 'indefiniteDeterminer' },
	       'adj' => sub { my $tag = shift;
			      return 'qualifierAdjective';
			    },
	       'number' => sub { 'numeral' },
	       'ponctw' => sub { 'secondaryPunctuation' },
	       'poncts' => sub { 'mainPunctuation' }
	      };

my %corpora = ( 'general'     =>  '.sgml',
                'litteraire'  => '.txt',
                'mail'        => '',
                'medical'     => '.txt',
                'oral_delic'  => '.xml',
                'oral_elda'   => '.xml',
                'questions'   => '.sgml'
              );

my @corpus = @{$config->corpus};

if (!@corpus) {

  open(INFO,"<$dagdir/info.txt") || die "can't open '$dagdir/info.txt': $!";
  while(<INFO>) {
    next if /^\#/;
    my ($corpus) = split(/\s+/,$_);
    push(@corpus,$corpus)
  }
  close(INFO);
}

my $fmap={};

if ($config->filemap) {
  use YAML::Any qw{LoadFile};
  $fmap = LoadFile($config->filemap);
}

sub filemap {
  my $s = shift;
  return $fmap->{$s} || $s;
}

my $count = 0;

system("mkdir -p $dest");

if ($verbose) {
    my $logdir = $config->logdir || $dest;
    my $logfile = $config->logfile;
    $log = "$logdir/$logfile";
    open(VERBOSE,">>$log") or die "can't open log $log: $!";
    verbose("Activate log in '$log'");
}

verbose("Start PASSAGE concat from $results");

foreach my $corpus (@corpus) {
  
  my ($base) = ($corpus =~ /^(\w+)_/);
  my $dagfile = "$dagdir/$corpus.${dagext}";
##  my $destfile = "$dest/$corpus$corpora{$base}.passage.xml";
  my $destfile = "$dest/$corpus.xml";

  my $tar = 0;

  if (!-d "$results/$corpus" && (-f "$results/$corpus.tar.bz2")) {
    $tar = 1;
    verbose("untar from archive $results/$corpus in $tmpdir");
    system("cd $tmpdir && tar xjf $results/$corpus.tar.bz2");
  } elsif (!-d "$results/$corpus" && (-f "$results/$corpus.tar.gz")) {
    $tar = 1;
    verbose("untar from archive $results/$corpus in $tmpdir/");
    my $where = `pwd`;
    chomp $where;
    system("cd $tmpdir && tar xzf $where/$results/$corpus.tar.gz"); # warning: assume $results is relative
  }

  verbose("Missing corpus $corpus"), next unless (-d "$results/$corpus" || -d "$tmpdir/$corpus");

  verbose("Concatenating $corpus");

  open(DEST,">$destfile") || die "can't open destfile '$destfile': $!";

  my $reffile = filemap("$corpus" . (exists $corpora{$base} ? $corpora{$base} : ''));
  if ($strict) {
  print DEST <<EOF;
<?xml version="1.0"  encoding="UTF-8"?>
<Document dtdVersion="2.0"  file="$reffile">
EOF
  } else {
  print DEST <<EOF;
<?xml version="1.0"  encoding="ISO-8859-1"?>
<Document file="$reffile">
EOF
  }

  if ($tagset && -r $tagset) {
    open(TAGSET,"<$tagset") || die "can't open $tagset: $!";
    while(<TAGSET>) {
      print DEST $_;
    }
    close(TAGSET);
  }

   my $tried = 0;
   my $added = 0;

  if (-r $dagfile) {
    open(DAG,"<$dagfile") || die "can't open dag for $corpus '$dagfile': $!";
  } elsif (-r "${dagfile}.bz2") {
    open(DAG,"bunzip2 -c ${dagfile}.bz2|") || die "can't open dag for $corpus '$dagfile': $!";
  } else {
    die "can't find DAG file '$dagfile'";
  }
  while (<DAG>) {
    next unless /^##\s*DAG\s+BEGIN/oi;
    my $line = <DAG>;
    my ($sid) = ($line =~ /E(\d+(?:\.\d+)?)F\d+/);
    ++$tried;
    my $forms = {};
    while(1) {
      while ($line =~ m{\G.*?(<F id="E\d+(?:\.\d+)?F\d+">[^<>]+</F>)}og) {
	my $form = $1;
	##	  print STDERR "Adding $form\n";
	my ($fid) = ($form =~ /id="(E\d+(?:\.\d+)?F\d+)"/o);
	$form =~ s/>([^<>]+)</>\n\t  $1\n\t</o;
	$form =~ s/\\(["?+])/$1/og; ## remove escaping added by dag2udag
	$form = "\t$form";
	$forms->{$fid} = { form => $form, 
			   id => $fid};
      }
      if ($line =~ /^\#\#\s*OFFSET\s+(\S+)\s+(\d+)\s+(\d+)/) {
	my $fid = $1;
	my $start = $2;
	my $end = $3;
	$forms->{$fid}{start} = $start;
	$forms->{$fid}{end} = $end;
      }
      $line = <DAG>;
      unless ($line) {
	  # this case should not arise
	  verbose("*** Unfinished DAG in '$corpus' sid=$sid");
	  last;
      }
      last if ($line =~ /^##\s*DAG\s+END/);
    }
    if (handle_sentence($corpus,$sid,$forms)) {
      $added++;
    } else {
      if ($strict) {
      print DEST <<EOF;
<!-- added from dag -->
<Sentence id="E$sid" trust="0">
EOF
    } else {
      print DEST <<EOF;
<!-- added from dag -->
<Sentence id="E$sid" mode="fail">
EOF
    }
      foreach my $fid (sort {sort_fid($a,$b)} keys %$forms) {
##	verbose("handle form A");
	my $form = handle_form($forms->{$fid});
	$form =~ /&\w+;/ or $form =~ s/\&/\&amp;/og;
      print DEST <<EOF;
  $form
EOF
      }
      print DEST <<EOF;
</Sentence>
EOF
    }
  }
  close(DAG);

  print DEST <<EOF;
</Document>
EOF

  close(DEST);
  system("bzip2 $destfile") if ($config->compress);

  if ($tar) {
    system("rm -Rf $tmpdir/$corpus");
  }

  my $ratio = 100 * ($added / $tried);
  my $msg = sprintf("(added %3i tried %3i ratio %6.2f%%)",$added,$tried,$ratio);
  verbose("Done $corpus $msg");
}

verbose("Done PASSAGE concatenation");

sub sort_fid {
  my $a = shift;
  my $b = shift;
  my ($sida,$fida) = ($a =~ /E(\d+(?:\.\d+)?)F(\d+)/);
  my ($sidb,$fidb) = ($b =~ /E(\d+(?:\.\d+)?)F(\d+)/);
  return ($sida <=> $sidb) || ($fida <=> $fidb);
}

## print "FILES @easy\n";

sub handle_sentence {
  my ($corpus,$sid,$forms) = @_;
  my $l1 = floor($sid / 10000);
  my $l2 = floor(($sid - ($l1 * 10000)) / 1000);
  my $l3 = floor(($sid - ($l1 * 10000) - ($l2*1000)) / 100);
##  my $file = "$results/$corpus/$l1/$l2/$l3/$corpus.E$sid.passage.xml";
  my $file = "$results/$corpus/$l1/$l2/$l3/$corpus.E$sid.passage.xml";
  my $xfile = "$tmpdir/$corpus/$l1/$l2/$l3/$corpus.E$sid.passage.xml";
  if (-r $file && -s $file) {
    open(FILE,"<$file") || die "can't open $file: $!";
  } elsif (-r "$file.bz2" && -s "$file.bz2") {
    $file .= ".bz2";
    open(FILE,"bunzip2 -c $file|") || die "can't open $file: $!";
  } elsif (-r $xfile && -s $xfile) {
    open(FILE,"<$xfile") || die "can't open $xfile: $!";
  } elsif (-r "$xfile.bz2" && -s "$xfile.bz2") {
    $xfile .= ".bz2";
    open(FILE,"bunzip2 -c $xfile|") || die "can't open $xfile: $!";
  } else {
    verbose("*** missing $corpus:$sid");
    return 0;
  }


  my $in = 0;
  my $in_f = 0;
  my $out = '';
  my $in_group;
  my @fids = sort {sort_fid($a,$b)} keys %$forms;
##  verbose("handle sentence corpus=$corpus sid=$sid fids=@fids");
  my $rels = "";
  my $costs = "";
  my $faulty_tid;
  my $localmap={};
  while (<FILE>) {
    if (m{^\s*<Sentence .*>}) {
      $in = 1;
      $faulty_tid='';
      if ($strict) {
	s/mode="full"/trust="100"/o;
	s/mode="corrected"/trust="90"/o;
	s/mode="robust"/trust="50"/o;
	s/best="(.+?)"//o;
      }
    }
    next unless ($in);
    if (m{^\s*<G .*>}) {
      $in_group = $_;
      next;
    }

    ## bug arising in FrWiki (and maybe in other files)
    ## seems related to a pb in forest_utils ???
    if (m{^\s*<T } && /id="Obj/) {
      my $next = <FILE>;
      if ($next =~ m{(E\d+(?:\.\d+)?F\d+)\|(\S+)}) {
	my $fid=$1;
	my $form = $2;
	my $tid = $fid;
	$tid =~ s/F(\d+)/T$1/;
	$faulty_tid = $tid;
	## should factorize code with next case
	while (@fids && sort_fid($fids[0],$fid) == -1) {
	  my $tmp = shift @fids;
	  ##	verbose("handle form B");
	  my $form = handle_form($forms->{$tmp});
	  $out .= <<EOF;
    <!-- added form from dag -->
$form
EOF
      }
	shift @fids;
	my $line = "<T id=\"$tid\">";
	my $info = $forms->{$fid};
	if ($info && (exists $info->{start})) {
	  $line =~ s/<(T\s.*?)>/<$1 start="$info->{start}" end="$info->{end}">/;
	}
	##      print STDERR $line;
	chomp($line);
	$out .= $line;
	## put content and closing of T on a single line
	## <T >contente</T>
	## and do some cleaning on the content
	$line = $next;
	{ use locale;
	  $line =~ s/\B_\B/ /og;
	  $line =~ s/'_/'/og;
	  $line =~ s/^\s+//o;
	  $line =~ s/\s+$//o;
	}
	chomp($line);
	$out .= $line;
	$line = <FILE>;
	$line =~ s/^\s+//o;
	$out .= $line;
	next;
      } else {
	<FILE>;
	verbose("pb with incorrect T next=$next: removed it\n");
	next;
      }
    }

    if (m{^\s*<T } && /id="(.+?)(E\d+(?:\.\d+)?T\d+)/) {
      verbose("pb with incorrect T id: $1$2\n");
      s/id="(.+?)(E\d+(?:\.\d+)?T\d+)"/id="$2"/;
    }

    if (m{^\s*<T } && /id="(E\d+(?:\.\d+)?T\d+)"/) {
      my $fid=$1;
      $fid =~ s/T(\d+)/F$1/;
#      $in_f = 1;
      while (@fids && sort_fid($fids[0],$fid) == -1) {
	my $tmp = shift @fids;
##	verbose("handle form B");
        my $form = handle_form($forms->{$tmp});
	$out .= <<EOF;
    <!-- added form from dag -->
$form
EOF
      }
      shift @fids;
      my $line = $_;
      my $info = $forms->{$fid};
      if ($info && (exists $info->{start})) {
	$line =~ s/<(T\s.*?)>/<$1 start="$info->{start}" end="$info->{end}">/;
      }
##      print STDERR $line;
      chomp($line);
      my $header = $line;
      ## put content and closing of T on a single line
      ## <T >content</T>
      ## and do some cleaning on the content
      $line = <FILE>;
      { use locale;
	$line =~ s/\B_\B/ /og;
	$line =~ s/'_/'/og;
	$line =~ s/^\s+//o;
	$line =~ s/\s+$//o;
      }
      chomp($line);
      if ($line =~ /(E\d+(?:\.\d+)?F\d+)/) {
	## very rare case occurring when the token content was empty in the DAG
	## in that case, the content of the token after parsing is its identifier
	## should also be corrected in easyforest (and in sxpipe)
	my $newid=$1;
	$newid =~ s/F(\d+)/T$1/;
	print STDERR "STRANGE $newid $header =>";
	$header =~ s/id="(.+?)"/id="$newid"/;
	$localmap->{$1} = $newid;
	print STDERR " $header\n";
	$line = "**void**";
      }
      $out .= $header;
      $out .= $line;
      $line = <FILE>;
      $line =~ s/^\s+//o;
      $out .= $line;
      next;
##      s/E(\S+?)F(\d+)/E\1T\2/og;
##      s{ form="(\S+?)"/>}{>$1</T>}o;
    }
#    $in_f = 0 if (m{^\s*</T>});
#    if ($in_f) {
#      $_ =~ s/&/&amp;/og 
#    }
##    if (m{^\s*<W .*/>}) {
##      s/E(\S+?)F(\d+)/E\1T\2/og;
##      s/id="E(\S+)W(\d+)"/id="E$1F$2"/o;
##    }
    if (m{^\s*</Sentence>}) {
##      verbose("handle form C1 fids=@fids") if (@fids);
      while(@fids) {
	## emits pending missing forms before relations
	my $tmp = shift @fids;
##	verbose("handle form C");
	my $form = handle_form($forms->{$tmp});
	$out .= <<EOF;
    <!-- added from dag -->
$form
EOF
      }
      $out .= $rels;
      $out .= $costs;
    }
    if (m{<cost }) {
      next if ($strict);
      $costs .= $_;
      next;
    }
    if ($strict && m{<a-propager }) {
      next;
    }

    if (m{<R }) {
      my $rel = $_;
      my $ok = 1;
      while (<FILE>) {
	if ($strict && m{<s-o}) {
	  s{subject}{sujet};
	  s{object}{objet};
	}
	if ($strict && 
	    ( /ref="(E\d+n\d+)"/
	      || /ref="\d+"/
	    )
	   ) {
	  $ok = 0;
	}
	s/ref="E(\S+)W(\d+)"/ref="E$1F$2"/o;
	$rel .= $_;
	last if (m{</R>});
      }
      $rels .= $rel if ($ok);
      next;
    }

    if ($strict && m{<W }) {
      if (/tokens="Obj/ && $faulty_tid) {
	$_ =~ s/tokens="(.*?)"/tokens="$faulty_tid"/;
      }
      if (/tokens="(.*?)"/ && $1 =~ /\w+,E\d+/) {
	$_ =~ s/\w+,(E\d+)/$1/og;
      }
      $_ = normalize_w($_);
      foreach my $tid (keys %$localmap) {
	last if (s/\b\Q$tid\E\b/$localmap->{$tid}/g);
      }
    }

    if (m{<W }) {
      s/_UNDERSCORE/_/og;
      if ($strict) {
	s/ht="(.*?)"//;
      }
      if (/tokens="(.*?)"/ && $1 =~ /\w+,E\d+/) {
	$_ =~ s/\w+,(E\d+)/$1/og;
      }
      foreach my $tid (keys %$localmap) {
	last if (s/\b\Q$tid\E\b/$localmap->{$tid}/g);
      }
    }
    if ($in_group) {
      $out .= $in_group;
      undef $in_group;
    }
    $out .= $_;
    $in = 0, last if m{^\s*</Sentence>};
  }
  close(FILE);
  if ($in || $in_f) {
    verbose("*** bad XML file $corpus:$sid");
    return 0;
  } else {
    print DEST $out;
  }
  return 1;
}

sub normalize_w {
  my $line = shift;
  my ($pos) = $line =~ /pos="(.*?)"/;
  my ($tag) = $line =~ /mstag="(.*?)"/;
  my ($lemma) = $line =~ /lemma="(.*?)"/;
  if ($pos) {
    if (my $conv = $mapping->{$pos}) {
      my $new_pos = $conv->($tag,$lemma);
      if ($new_pos) {
	$line =~ s/pos="(.*?)"/pos="$new_pos"/;
      } else {
	$line =~ s/pos="(.*?)"//;
      }
    } else {
      $line =~ s/pos="(.*?)"//;
    }
  } elsif (defined $pos) {
    $line =~ s/pos="(.*?)"//;
  }
  $line =~ s/mstag=""//;	# empty mstag should be removed
  $line =~ s/mode\.minus\.adjective/mode.minus/og;
  return $line;
}

sub handle_form {
  my $info = shift;
  my $form = $info->{form};
  my $offset = "";
##verbose("Handlinf form '$form'");
  if ($strict && !(exists $info->{start})) {
    verbose("No offset info for $info->{id}");
  }
  if (exists $info->{start}) {
    $offset = " start=\"$info->{start}\" end=\"$info->{end}\""
  }
  $form =~ s{<F\s+id="(\w+)">\s*\n\s*(.*?)\s*\n\s*</F>}{<T id="$1"$offset>$2</T>}s;
  $form =~ s/"E(\S+?)F(\d+)"/"E$1T$2"/og;
  $form =~ s/>\&</>&amp;</og;
  return $form;
}

sub verbose {
  return unless ($config->verbose);
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
  print VERBOSE "$date @_\n" if (defined $log);
}

=head1 NAME

easyconcat.pl -- Concat sentences results into one big file per corpus

=head1 SYNOPSIS 	 
  	 
./easyconcat.pl

The resulting files are sent to directory F<concat> [default]. To send
the files to another directory, use option --dest.

./easyconcat.pl --dest=somewhere

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
