<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:exslt="http://exslt.org/common"
  >

<xsl:output
  method="html"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:param name="simple"/>
<xsl:param name="corpus"/>

<xsl:strip-space elements="*"/>

<xsl:variable name="colors">
  <colors>
    <color id="GN" value="#CC0000"/>
    <color id="NV" value="#009900"/>
    <color id="GP" value="#3333FF"/>
    <color id="GR" value="#CC6600"/>
    <color id="PV" value="#999999"/>
    <color id="GA" value="#993399"/>
  </colors>
</xsl:variable>

<xsl:variable name="relations">
  <relations>
    <relation type="SUJ-V" title="Sujet - Verbe" color="#ffff99">
      <arg name="sujet"/>
      <arg name="verbe"/>
    </relation>
    <relation type="AUX-V" title="Auxiliaire - Verbe" color="#88dd88">
      <arg name="auxiliaire"/>
      <arg name="verbe"/>
    </relation>
    <relation type="COD-V" title="COD - Verbe" color="#ffff99">
      <arg name="COD"/>
      <arg name="verbe"/>
    </relation>
    <relation type="CPL-V" title="Compl�ment - Verbe"  color="#88dd88">
      <arg name="compl�ment"/>
      <arg name="verbe"/>
    </relation>
    <relation type="MOD-V" title="Modifieur - Verbe" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="verbe"/>
    </relation>
    <relation type="COMP" title="Compl�menteur"  color="#88dd88">
      <arg name="compl�menteur"/>
      <arg name="NV de la proposition subordonn�e"/>
    </relation>
    <relation type="ATB-SO" title="Attribut - Sujet / Objet" color="#ffff99">
      <arg name="attribut"/>
      <arg name="verbe"/>
      <arg name="sujet / objet"/>
    </relation>
    <relation type="MOD-N" title="Modifieur - Nom"  color="#88dd88">
      <arg name="modifieur"/>
      <arg name="nom"/>
      <arg name="� propager"/>
    </relation>
    <relation type="MOD-A" title="Modifieur - Adjectif" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="adjectif"/>
    </relation>
    <relation type="MOD-R" title="Modifieur - Adverbe"  color="#88dd88">
      <arg name="modifieur"/>
      <arg name="adverbe"/>
    </relation>
    <relation type="MOD-P" title="Modifieur - Pr�position" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="pr�position"/>
    </relation>
    <relation type="COORD" title="Coordination"  color="#88dd88">
      <arg name="coordonnant"/>
      <arg name="coordonn� gauche"/>
      <arg name="coordonn� droit"/>
    </relation>
    <relation type="APPOS" title="Apposition" color="#ffff99">
      <arg name="premier �l�ment"/>
      <arg name="deuxi�me �l�ment"/>
    </relation>
    <relation type="JUXT" title="Juxtaposition"  color="#88dd88">
      <arg name="premier �l�ment"/>
      <arg name="deuxi�me �l�ment"/>
    </relation>
  </relations>
</xsl:variable>

<xsl:template match="DOCUMENT">
  <xsl:choose>
    <xsl:when test="$simple = 'yes'">
      <xsl:apply-templates/>
    </xsl:when>
    <xsl:otherwise>
      <html>
	<head>
	  <link rel="STYLESHEET" type="text/css" href="easy.css"/>
	  <script src="prototype.js" type="text/javascript"/>
	  <script src="easy.js" type="text/javascript"/>
	  <title> HTML EASy Format </title>
	</head>
	<body bgcolor="linen">
	  
	  <h1> Annotations EASY</h1>
	  
	  More information <a href="http://www.limsi.fr/Recherche/CORVAL/easy/">here</a>
	  
	  <p>
	    For the following sentences, the first group line provides the
	    reference groups and the second group line the hypothese groups.
	  </p>
	  
	  <p> The color code for relations is:
	  <ul>
	    <li> [black] : hyp. relations that are OK</li>
	    <li> [<font color="red">red</font>] : hyp. relations that are not OK (using hyp. groups)</li>
	    <li> [<font color="blue">blue</font>] : missing reference relations (using ref. groups)</li>
	  </ul>
	  </p>

	  <xsl:apply-templates/>

	</body>
      </html>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="E">

  <xsl:variable name="strip" select="1+string-length(@id)"/>
  <hr/>
  <p> <strong>No answer returned for Enonc� <xsl:value-of select="substring(@id,2)"/></strong>
  </p>

</xsl:template>

<xsl:template match="E[constituants//F]">

  <xsl:variable name="strip" select="1+string-length(@id)"/>

  <hr/>
  <a name="{@id}"/>    

  <div id="{concat('comments',@id)}">
    <div id="{concat('editorbar',@id)}">
      <a href="#" class="button" id="{concat('editorButton',@id)}">
	<xsl:attribute name="onClick">
	  <xsl:text>editorOpen('</xsl:text>
	  <xsl:value-of select="$corpus"/>
	  <xsl:text>','</xsl:text>
	  <xsl:value-of select="@id"/>
	  <xsl:text>')</xsl:text>
	</xsl:attribute>
	Edit report
      </a>
    </div>
  </div>

  <h2> Constituants </h2>
      <table class="constituants" border="1">
	<!-- Enonc� -->
	<tr>
	  <td colspan="{count(constituants//F)}" bgcolor="#99CCCC">
	    <font size="+1">
	      Enonc� <xsl:value-of select="substring(@id,2)"/>
	    </font>
	    <xsl:if test="@mode = 'full'">
	    -- full parse
	    </xsl:if>
	  </td>
	</tr>
        <!-- Groups -->
        <tr class="groups">
          <xsl:apply-templates select="constituants/*" mode="groupe">
	    <xsl:with-param name="strip" select="$strip"/>
	    <xsl:with-param name="mode">ref</xsl:with-param>
	  </xsl:apply-templates>
        </tr>
	<!-- Hyp groups -->
	<xsl:if test="hypconsts">
	  <tr class="groups">
	    <xsl:apply-templates select="hypconsts/*" mode="groupe">
	      <xsl:with-param name="strip" select="$strip"/>
	      <xsl:with-param name="mode">hyp</xsl:with-param>
	    </xsl:apply-templates>
	  </tr>
	</xsl:if>
        <!-- Words -->
        <tr class="words" bgcolor="lightBlue">
          <xsl:apply-templates select="constituants//F" mode="word">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
        <!-- F -->
        <tr class="Fs" bgcolor="#FFFFFF">
          <xsl:apply-templates select="constituants//F" mode="F">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
	<!-- lines -->
	<xsl:apply-templates select=".//lines/line">
	  <xsl:with-param name="strip" select="$strip"/>
	</xsl:apply-templates>
      </table>

      <xsl:if test="hypconsts/Groupe[not(F)]">
	<p>
	  <strong> Bogus empty groups:
	  </strong>
	  <xsl:apply-templates select="hypconsts/Groupe[not(F)]" mode="bogus">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
	</p>
      </xsl:if>

      <h2> Relations </h2>
      
      <table>

	<tr valign="top">
	  <xsl:variable name="current" select="."/>
	  <xsl:for-each select="exslt:node-set($relations)/relations/relation">
	    <xsl:variable name="type" select="@type"/>
	    <xsl:if test="$current/relations/relation[@type=$type] or $current/hyprels/relation[@type=$type] ">
	      <td align="center">
		<h3><xsl:value-of select="position()"/>. <xsl:value-of select="@title"/></h3>
		
		<table border="1" bgcolor="{@color}">
		  <tbody>
		    <tr class="header">
		      <xsl:for-each select="arg">
			<td><font size="+1"><xsl:value-of select="@name"/></font></td>
		      </xsl:for-each>
		    </tr>
		    <xsl:apply-templates select="$current/relations/relation[@type=$type and @map]">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		    <xsl:apply-templates select="$current/relations/relation[@type=$type and not(@map)]">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		    <xsl:apply-templates select="$current/hyprels/relation[@type=$type]">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		  </tbody>
		</table>
		
	      </td>
	    </xsl:if>
	  </xsl:for-each>

	</tr>
      </table>

      <xsl:apply-templates select=".//relation" mode="tooltip">
	<xsl:with-param name="strip" select="$strip"/>
      </xsl:apply-templates>

</xsl:template>

<xsl:template match="hypconsts/Groupe[F]" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:param name="mode"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./F)}" class="{$type}" bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}"   id="{concat($mode,@id)}">
    <font color="white">
      <xsl:value-of select="@type"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="substring(@id,$strip+1)"/>
    </font>
  </td>
</xsl:template>

<xsl:template match="Groupe[@map]" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:param name="mode"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./F)}" class="{$type}" 
      bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}"
      id="{concat($mode,@id)}"
  >
    <xsl:value-of select="@type"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="hypconsts/Groupe" mode="bogus">
  <xsl:param name="strip"/>
  <xsl:variable name="type" select="@type"/>
  <span style="background: {exslt:node-set($colors)/colors/color[@id=$type]/@value};">
    <xsl:value-of select="@type"/>
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </span>
  <xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="F" mode="groupe">
  <xsl:param name="strip"/>
  <td colspan="1"/>
</xsl:template>

<xsl:template match="Groupe" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:param name="mode"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./F)}" class="{$type}" bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}"  id="{concat($mode,@id)}">
      <xsl:value-of select="@type"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="F" mode="word">
  <xsl:param name="strip"/>
  <td>
    <xsl:value-of select="."/>
  </td>
</xsl:template>

<xsl:template match="F" mode="F">
  <xsl:param name="strip"/>
  <td class="F" id="{@id}">
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<!-- extra hyp relations -->
<xsl:template match="hyprels/relation[not(@map)]">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<font color="red">
	  <xsl:apply-templates select="." mode="relarg"> 
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
	</font>
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<!-- relations that are both hyp and rel -->
<xsl:template match="relations/relation[@map]">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<xsl:apply-templates select="." mode="relarg"> 
	  <xsl:with-param name="strip" select="$strip"/>
	</xsl:apply-templates>	
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<!-- missing ref relations -->
<xsl:template match="relations/relation[not(@map)]">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<font color="blue">
	  <xsl:apply-templates select="." mode="relarg"> 
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
	</font>
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template match="*" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:value-of select="substring(./@xlink:href,$strip)"/>
</xsl:template>

<xsl:template match="*[@overlap]" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:variable name="map" select="../@map"/>
  <xsl:variable name="role" select="name(.)"/>
  <xsl:value-of select="substring(./@xlink:href,$strip)"/>
  <xsl:variable name="rmap" select="ancestor::E/hyprels/relation[@id=$map]/*[name(.)=$role]/@xlink:href"/>
  <xsl:if test="@xlink:href != $rmap or @overlap &lt; 100">
    <xsl:text> </xsl:text>
    <font color="red">
      <xsl:value-of select="substring($rmap,$strip)"/>
    </font>
  </xsl:if>
  <xsl:if test="@overlap &lt; 100">
    <xsl:text> </xsl:text>
    <xsl:value-of select="@overlap"/>
    <xsl:text>%</xsl:text>
  </xsl:if>
</xsl:template>


<xsl:template match="s-o" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:value-of select="@valeur"/>
</xsl:template>

<xsl:template match="�-propager[@booleen='vrai']" mode="relarg">
  <xsl:param name="strip"/>
  X
</xsl:template>

<xsl:template match="a-propager[@booleen='vrai']" mode="relarg">
  <xsl:param name="strip"/>
  X
</xsl:template>


<xsl:template match="line">
  <xsl:param name="strip"/>
  <tr class="line">
    <xsl:apply-templates select="edge">
      <xsl:with-param name="strip" select="$strip"/>
    </xsl:apply-templates>
  </tr>
</xsl:template>

<xsl:template match="edge">
  <xsl:param name="strip"/>
  <xsl:variable name="current" select="."/>
  <xsl:variable name="rid" select="@rid"/>
  <xsl:variable name="rel">
    <xsl:choose>
      <xsl:when test="@status = 'hyp'">
	<xsl:copy-of select="ancestor::E/hyprels/relation[@id=$rid]"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:copy-of select="ancestor::E/relations/relation[@id=$rid]"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
<!--
  <xsl:message> TOTO: <xsl:value-of select="name(exslt:node-set($rel)/*)"/>
  </xsl:message>
-->
  <xsl:variable name="prev">
    <xsl:choose>
      <xsl:when test="preceding-sibling::edge">
	<xsl:value-of select="1+number(preceding-sibling::edge[1]/@right)"/>
      </xsl:when>
      <xsl:otherwise>1</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:if test="number(@left) > number($prev)">
    <td colspan="{number(@left) - number($prev)}" class="void"/>
  </xsl:if>
  <td colspan="{1+number(@right)-number(@left)}" class="{concat('edge_',@type)}">
<!--
    <xsl:attribute name="title">
      (<xsl:value-of select="@status"/>)
      <xsl:value-of select="substring($rid,$strip)"/>
      <xsl:text>:</xsl:text>
      <xsl:for-each select="exslt:node-set($rel)/*/*[@xlink:href]">
	<xsl:text> </xsl:text>
	<xsl:value-of select="name(.)"/>
	<xsl:text>=</xsl:text>
	<xsl:value-of select="substring(@xlink:href,$strip)"/>
      </xsl:for-each>
    </xsl:attribute>
-->
    <xsl:attribute name="onMouseOver">
      <xsl:text>relation_show('</xsl:text>
      <xsl:value-of select="@status"/>
      <xsl:value-of select="@rid"/>
      <xsl:text>',this)</xsl:text>
    </xsl:attribute>
    <xsl:attribute name="onMouseOut">
      <xsl:text>relation_hide('</xsl:text>
      <xsl:value-of select="@status"/>
      <xsl:value-of select="@rid"/>
      <xsl:text>',this)</xsl:text>
    </xsl:attribute>
    <span class="{@status}"> <xsl:value-of select="@type"/> </span>
    <xsl:for-each select="exslt:node-set($rel)/*/*[@xlink:href]">
      <xsl:variable name="aid" select="@xlink:href"/> 
      <xsl:choose>
	<xsl:when test="contains($aid,'F')">
	  <arg aid="{$aid}"/>
	</xsl:when>
	<xsl:when test="$current/@status = 'hyp'">
	  <arg aid="{concat('hyp',$aid)}"/>
	</xsl:when>
	<xsl:otherwise>
	  <arg aid="{concat('ref',$aid)}"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </td>
</xsl:template>

<!-- extra hyp relations -->
<xsl:template match="hyprels/relation[not(@map)]" mode="tooltip">
  <xsl:param name="strip"/>
  <div class="tooltip" id="{concat('hyp',@id)}">
    <table>
      <tr>
	<td colspan="2"> 
	   <xsl:value-of select="@type"/>
	   <xsl:text> (hyp </xsl:text>
	   <xsl:value-of select="substring(@id,$strip+1)"/>
	   <xsl:text>)</xsl:text>
	</td>
      </tr>
      <xsl:for-each select="*">
	<tr> 
	  <td> <xsl:value-of select="name(.)"/></td>
	  <td>
	    <xsl:apply-templates select="." mode="relarg"> 
	      <xsl:with-param name="strip" select="$strip"/>
	    </xsl:apply-templates>	
	  </td>
	</tr>
      </xsl:for-each>
    </table>
  </div>
</xsl:template>

<!-- relations that are both hyp and rel -->
<xsl:template match="relations/relation[@map]" mode="tooltip">
  <xsl:param name="strip"/>
  <div class="tooltip" id="{concat('both',@id)}">
    <table>
      <tr>
	<td colspan="2"> <xsl:value-of select="@type"/>
	   <xsl:text> (ref</xsl:text>
	   <xsl:value-of select="substring(@id,$strip+1)"/>
	   <xsl:text>  hyp</xsl:text>
	   <xsl:value-of select="substring(@map,$strip+1)"/>
	   <xsl:text>)</xsl:text>
	</td>
      </tr>
      <xsl:for-each select="*">
	<tr> 
	  <td> <xsl:value-of select="name(.)"/></td>
	  <td>
	    <xsl:apply-templates select="." mode="relarg"> 
	      <xsl:with-param name="strip" select="$strip"/>
	    </xsl:apply-templates>	
	  </td>
	</tr>
      </xsl:for-each>
    </table>
  </div>
</xsl:template>

<!-- missing ref relations -->
<xsl:template match="relations/relation[not(@map)]" mode="tooltip">
  <xsl:param name="strip"/>
  <div class="tooltip" id="{concat('ref',@id)}">
    <table>
      <tr>
	<td colspan="2"> 
	  <xsl:value-of select="@type"/>
	  <xsl:text> (ref</xsl:text>
	  <xsl:value-of select="substring(@id,$strip+1)"/>
	  <xsl:text>)</xsl:text>
	</td>
      </tr>
      <xsl:for-each select="*">
	<tr> 
	  <td> <xsl:value-of select="name(.)"/></td>
	  <td>
	    <xsl:apply-templates select="." mode="relarg"> 
	      <xsl:with-param name="strip" select="$strip"/>
	    </xsl:apply-templates>	
	  </td>
	</tr>
      </xsl:for-each>
    </table>
  </div>
</xsl:template>



</xsl:stylesheet>
