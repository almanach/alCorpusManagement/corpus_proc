#!/usr/bin/perl

## to be used on rsel.sorted.log
## to get restriction selection for easyforest

use strict;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "limit=i" => {DEFAULT => 100}
                           );

$config->args();

my $limit = $config->limit;

while(<>) {
  s/^\s*//;
##  my @v = split(/\t+/,$_);
  my @v = /^(\d+)\s+(.+?)\t+(\S+?)\t+(.+?)\t+(.+?)\t+(.+?)$/;
  last if ($v[0] < 100);
  next if ($v[2] =~ /Conjunction/ || $v[5] =~ /Conjunction/);
  next if ($v[2] =~ /relative/ || $v[5] =~ /relative/);
  next if ($v[4] =~ /relativePronoun/);
##  print "\%\% $v[0]: $v[1] -- $v[3] --> $v[4]\n";
  my $w = $v[0];
  $w = int(sqrt($w));
##  $w = int($w/10);
  my ($s,$p,$t) = map {quote($_)} ($v[1],$v[3],$v[4]);
  print <<EOF;
rpref('$s','$p','$t',$w). %% $v[0]
EOF
}

sub quote {
  my $s = shift;
  $s =~ s/'/''/og;
  return $s;
}
