<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:exslt="http://exslt.org/common"
  >

<!--
     Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr"

     To  extract sentences from

     - removed <nodes></nodes>
     - removed <constraints></constraints>
     - <fs></fs> are no longer nested inside <val></val>
     - relations (father, precedes, ...) are rewritten

  -->

<xsl:output
  encoding="ISO-8859-1"
  method="text"
  indent="yes"/>

<xsl:template match="@*|text()"/>

<xsl:template match="hypconsts/Groupe[@map]/F">
  <xsl:text>
ok	</xsl:text>
  <xsl:value-of select="../@type"/>
  <xsl:text>/</xsl:text>
  <xsl:value-of select="../@type"/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="@cat"/>
</xsl:template>

<xsl:template match="hypconsts/Groupe[not(@map)]/F">
  <xsl:variable name="fid" select="@id"/>
  <xsl:variable name="reftype">
    <xsl:choose>
      <xsl:when test="ancestor::E/constituants/Groupe/F[@id=$fid]">
	<xsl:value-of select="ancestor::E/constituants/Groupe[F[@id=$fid]]/@type"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>void</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:text>
fail	</xsl:text>
  <xsl:value-of select="../@type"/>
  <xsl:text>/</xsl:text>
  <xsl:value-of select="$reftype"/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="@cat"/>
</xsl:template>


<xsl:template match="hypconsts/F">
  <xsl:variable name="fid" select="@id"/>
  <xsl:variable name="reftype">
    <xsl:choose>
      <xsl:when test="ancestor::E/constituants/Groupe/F[@id=$fid]">
	<xsl:value-of select="ancestor::E/constituants/Groupe[F[@id=$fid]]/@type"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>void</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="$reftype='void'">
      <xsl:text>
ok</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>
fail</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>	</xsl:text>
  <xsl:text>void</xsl:text>
  <xsl:text>/</xsl:text>
  <xsl:value-of select="$reftype"/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="@cat"/>
</xsl:template>


<xsl:template match="hyprels/relation[@map]/*[@xlink:href]">
  <xsl:variable name="cid" select="@xlink:href"/>
  <xsl:apply-templates select="ancestor::E//constituants//*[@id=$cid]" mode="rel">
    <xsl:with-param name="type" select="../@type"/>
    <xsl:with-param name="anchor" select="name(.)"/>
    <xsl:with-param name="status"><xsl:text>ok</xsl:text></xsl:with-param>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="hyprels/relation[not(@map)]/*[@xlink:href]">
  <xsl:variable name="cid" select="@xlink:href"/>
  <xsl:apply-templates select="ancestor::E//constituants//*[@id=$cid]" mode="rel">
    <xsl:with-param name="type" select="../@type"/>
    <xsl:with-param name="anchor" select="name(.)"/>
    <xsl:with-param name="status"><xsl:text>hyp</xsl:text></xsl:with-param>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="relations/relation[not(@map)]/*[@xlink:href]">
  <xsl:variable name="cid" select="@xlink:href"/>
  <xsl:apply-templates select="ancestor::E//hypconst//*[@id=$cid]" mode="rel">
    <xsl:with-param name="type" select="../@type"/>
    <xsl:with-param name="anchor" select="name(.)"/>
    <xsl:with-param name="status"><xsl:text>ref</xsl:text></xsl:with-param>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="F" mode="rel">
  <xsl:param name="type"/>
  <xsl:param name="anchor"/>
  <xsl:param name="status"/>
  <xsl:text>
</xsl:text>
  <xsl:value-of select="$status"/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="$type"/>
  <xsl:text>:</xsl:text>
  <xsl:value-of select="$anchor"/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>	</xsl:text>
  <xsl:value-of select="@cat"/>
</xsl:template>

<xsl:template match="Groupe" mode="rel">
  <xsl:param name="type"/>
  <xsl:param name="anchor"/>
  <xsl:param name="status"/>
  <xsl:apply-templates select="F" mode="rel">
    <xsl:with-param name="type" select="$type"/>
    <xsl:with-param name="anchor" select="$anchor"/>
    <xsl:with-param name="status" select="$status"/>
  </xsl:apply-templates>
</xsl:template>


</xsl:stylesheet>
