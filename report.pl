#!/usr/bin/perl

use strict;
use File::Basename;
use Term::Report;
use POSIX qw(strftime);

use AppConfig qw/:argcount :expand/;
use Date::Manip;
use IO::Uncompress::Bunzip2 qw/bunzip2 $Bunzip2Error/;

my $config = AppConfig->new(
			    { CREATE => 1 },
			    "verbose!" => {DEFAULT => 1},
			    "results=f" => {DEFAULT => 'results'},
			    "key=s" => {DEFAULT => '' },
			    "lock=f" => {DEFAULT => 'lock'},
			    "corpus=f@" => {DEFAULT => []},
			    "dag=f" => {DEFAULT => 'dag'},
			    "slice=i" => {DEFAULT => 20},
			    "run!" => {DEFAULT => 1},
			    "lock=f" => {DEFAULT => 'lock'},
			    "results=f" => {DEFAULT => 'results'},
			    "callparser=f" => {DEFAULT => '/usr/bin/callparser'},
			    "parse_options=s" => {DEFAULT => '-collsave -time -stats -xmldep -robust'},
			    "key=s" => {DEFAULT => ''},
			    "host=s@" => {DEFAULT => []},
			    "hostallow!" => {DEFAULT => 1},
			    "share!" => {DEFAULT => 1},
			    "mafdocument!" => {DEFAULT => 0},
			    "dagext=s" => {DEFAULT => "udag"},



			    "stats|s!"          => {DEFAULT => 0},
			    
			    "forest!"           => {DEFAULT => 0},
			    "display|d=s"       => {DEFAULT => ''},
			    #    "dependency!"       => {DEFAULT => 0}, # display graph
			    "grammar!"          => {DEFAULT => 0},
			    "tagger!"           => {DEFAULT => 0},
			    "xmldep!"           => {DEFAULT => 0},
			    "lpdep!"            => {DEFAULT => 0},
			    "dotdep!"           => {DEFAULT => 0},
			    "txtdep!" => { DEFAULT => 0},
			    "easy!"             => {DEFAULT => 0},
			    "easyhtml!"         => {DEFAULT => 0},
			    "passage!"             => {DEFAULT => 0},
			    "passagehtml!"         => {DEFAULT => 0},
			    
			        "time!"             => {DEFAULT => 0},
    "robust!"           => {DEFAULT => 0},
    "easyinput"         => {DEFAULT => 0},
    "timeout=i"         => {DEFAULT => 200},

    "colldir=f"         => {DEFAULT => "./results"},
    "collsave!"         => {DEFAULT => 0},
    "collbase=f"        => {DEFAULT => 'sentence'},,

    "allparse!"         => {DEFAULT => 0},
    "showdag!"          => {DEFAULT => 0},
    "run!"              => {DEFAULT => 1},
    "final!"            => {DEFAULT => 1},
    "disambiguate|dis!" => {DEFAULT => 0},
    "date!"             => {DEFAULT => 0},
    "printhost!"        => {DEFAULT => 0},
    "results=f"         => {DEFAULT => 'results'},
    "dagdir=f"          => {DEFAULT => 'dag'},
    "dagext=s"          => {DEFAULT => "udag"},
    "lock=f"            => {DEFAULT => 'lock'},
			    "tar!" => {DEFAULT => 0},
			    
     "compress!" => {DEFAULT => 0},
     "kill!" => {DEFAULT => 0},
			    "adj_max=i",
			    "backdir=s",
			    "dispatch!" => {DEFAULT => 1} # use dispatch.log
			   );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
} else {
  my $running_dispatch = `ps aux | grep "dispatch[.]pl"`;
  if ($running_dispatch and $running_dispatch =~ /--config\s+(\S+)/) {
    $conffile = $1;
  } else {
    my $lastconf = `ls -t dispatch.*.conf | head -1`;
    if ($lastconf && $lastconf =~ /(dispatch\..*\.conf)/) {
      $conffile = $1;
    }
  }
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;
my $lock = $config->lock.$config->key;
my $share = $config->share;
my $dag = $config->dagdir;
my %info = ();
my @hosts =();
my $dagext = $config->dagext;


foreach my $host (map {host_expand($_)} @{$config->host}) {
  $host =~ s/:\d+$//og;
  $host =~ s/\@\S+$//og;
  push(@hosts,$host);
  print "ALL HOST @hosts\n";
}


sub host_expand {
  my $s = shift;
  if ($s =~ /^(\S+)\^([a-z])=(\S+?)$/) {
    my $base = $1;
    my $var = $2;
    return map {host_expand2($base,$var,$_)} index_expand($3);
  } else {
    return $s;
  }
}

sub host_expand2 {
  my ($s,$i,$v) = @_;
  $s =~ s/\%$i/$v/g;
  return host_expand($s);
}

sub index_expand {
  my $s = shift;
  if ($s =~ /^\d+$/) {
    return $s;
  } elsif ($s =~ /^(\d+)..(\d+)$/) {
    return $1..$2;
  } elsif ($s =~ /^(\S+?),(\S+)$/) {
    return (index_expand($1),index_expand($2));
  } else {
    return ();
  }
}



my $nhosts=scalar(@hosts);

my $info = "$dag/info.txt";
$info = "$results/info.txt" unless (-r $info);
$info = "info.txt" unless (-r $info);
if (!-r $info) {
  die "can't find info.txt in $dag or locally: $!";
}

open(INFO,"<$info") or die "can't open $dag/$info: $!";

my $all_n = 0;
my $remain_n=0;

my @corpus = @{$config->corpus};

my %tmp = ();

$tmp{$_}||=1 foreach (@corpus);

while(<INFO>) {
  next if /^\#/;
  chomp;
  my ($file,$n) = split(/\s+/,$_);
  $info{$file} = { size =>  $n };
  @corpus or $config->corpus($file);
  (!@corpus || exists $tmp{$file}) and $all_n += $n;
}

$remain_n = $all_n;

close(INFO);

Date::Manip::Date_Init("TZ=GMT");

my $start;

my $suspended=0;
my $ds1;
my $ds2;
my $suspensions = 0;


if ($config->dispatch) {
  open(LOG,"<$results/dispatch.log") || die "can't open file: $!";
  while (<LOG>) {
    if (/^\[(.+)\]/ && !$start)  {
      $start = $1;
    }
    ## last stop is either explicit Quitting or last 'Done processing'
    if (/^\[(.+)\]\s+Quitting/) {
      my $stop = $1;
      $ds1 = Date::Manip::ParseDate($stop);
    } 
    if (/^\[(.+)\]\s+Done postprocessing\s+(\S+)/) {
      my $stop = $1;
      my $corpus = $2;
      $info{$corpus}{processed} = 1;
      $ds1 = Date::Manip::ParseDate($stop);
    }
    if (/^\[(.+)\]\s+Resuming processing/) {
      my $restart = $1;
      $suspensions++;
      $ds2 = Date::Manip::ParseDate($restart);
      $suspended += Date::Manip::Delta_Format(Date::Manip::DateCalc($ds1,$ds2),1,"%st");
      $ds1=undef;
    }
  }
  close(LOG);
}

@corpus = sort @{$config->corpus()};

my $ncorpus = @corpus;

my $items = 100;

my $report = Term::Report->new( startRow => 0, 
				## numFormat => 1
			      );
##my $status = $report->{statusBar};
##$status->setItems($items);
##$status->start();

my %log = ();

my $iter = -1;

#keep trace of last info (time,tried)
my @last = ();

##$start =~ s|-|/|og;
##$start =~ s/2005/2006/;

my $d1 = Date::Manip::ParseDate(ParseDate($start));
my $d3;

my @buff = ();
sub next_line {
  if (@buff) {
    my $line = shift @buff;
    return $line;
  } else {
    return <LOG>;
  }
}

while (1) {
  process();
  if (-e "$results/status/done") {
    process();
    exit;
  }
  sleep(30);
}

sub process {

  $iter++;

  my @results = ();

  my $all_success = 0;
  my $all_robust = 0;
  my $all_fail = 0;
  my $ncorp = 0;
  my $ncorp_processed = 0;
  my $retried = 0;
  my $retried_ok=0;
  my $retried_time=0;

  my %current = ();

  open(DISPATCH,"$results/dispatch.log".$config->key) || die "can't open dispatch: $!";
  while(<DISPATCH>) {
    if (/start\s+(\w+)\s+on\s+(\S+)/) {
      $current{$2} = $1;
      next;
    }
    if (/done\s+(\w+)\s+on\s+(\S+)/) {
      delete $current{$2};
      next;
    }
    next;
  }
  close(DISPATCH);

  my %hosts = ();
  my %success_hosts = ();
  my %timeout_hosts = ();
  my %time_hosts = ();
  my $timeout = 0;
  my $time = 0;
  my $time2 = 0;
  my %times = ();

  foreach my $corpus (@corpus) {
    my $log = "$results/$corpus.log";

    $log .= ".bz2" if (-r "$log.bz2");
    unless (-r $log && -s $log) {
      my $x = $info{$corpus};
      if ($x->{processed}) {
	$ncorp++;
	$ncorp_processed++;
	if (!$x->{seen}) {
	  $remain_n -= $x->{size};
	  $x->{seen} = 1;
	}
      }
      next;
    }

    my $mtime = (stat($log))[9];
    my $x = $info{$corpus};
    
    if ($mtime == $info{$corpus}{mtime}) {
      $x->{modified} = 0;
      $ncorp++;
    } else {
      $x->{mtime} = $mtime;
      $x->{modified} = 1;
      $x->{success} = 0;
      $x->{fail} = 0;
      $x->{robust} = 0;
      $x->{time} = 0;
      $x->{time2} = 0;
      $x->{timeout} = 0;
      $x->{host} = {};
      $x->{others} = {};
      $x->{retried} = 0;
      $x->{retried_ok}=0;
      $x->{retried_time}=0;

      my $h;

      if ($log =~ /\.bz2$/) {
#	open($h,"bunzip2 -c $log |") || die "can't open logfile $log: $!";
	$h = new IO::Uncompress::Bunzip2 $log
	  or die "can't open logfile $log: $!";
      } else {
	open($h,"$log") || die "can't open logfile $log: $1";
      }
      $ncorp++;
      
      my $tmpstatus=0;
      my $tmptimeout=0;
      my $host;
      my $haspb=0;

    LOOP: while( <$h> ) {
	$tmpstatus=1, $haspb=0, $x->{success}++, next if (/^ok\s+(\d+)/);
	$tmpstatus=0,$haspb=0, $x->{robust}++, next if (/^robust\s+/);
	$tmpstatus=0, $haspb=1, $x->{fail}++, next if (/^fail\s+/);
	$haspb=1 if (/\*\*\s+(timeout|null\s+output)/);
	$x->{time} += $1, $x->{host}{$host}{time} += $1 if (/<time>\s+(\S+)/);
	$x->{time2} += $1, $x->{timeout}++, $x->{host}{$host}{timeout}++, next if (/<time>\s+(\S+)\s+\*\*\s*timeout/);
	$x->{others}{$1} += $2, next if (/<time(.+?)>\s+(\S+)/);
	if (/<host>\s+(\S+)/) {
	  $host = $1;
	  $x->{host}{$host}{success} += $tmpstatus;
	  $x->{host}{$host}{tried}++;
	  next;
	} 
	if (/<retried>\s+(\S+)/) {
	  $x->{retried}++;
	  $x->{retried_time} += $1;
	  $haspb or $x->{retried_ok}++;
	  next ;
	}
      }
    
      close($h);
    }
        
    $all_success += $x->{success};
    $all_robust += $x->{robust};
    $all_fail += $x->{fail};
    $time += $x->{time};
    $time2 += $x->{time2};
    $timeout += $x->{timeout};
    $retried += $x->{retried};
    $retried_ok += $x->{retried_ok};
    $retried_time += $x->{retried_time};

    foreach my $k (keys %{$x->{others}}) {
      $times{$k} += $x->{others}{$k};
    }
    
    foreach my $host (keys %{$x->{host}}) {
      my $y = $x->{host}{$host};
      $hosts{$host} += $y->{tried};
      $success_hosts{$host} += $y->{success};
      $timeout_hosts{$host} += $y->{timeout};
      $time_hosts{$host} += $y->{time};
    }

    my $success = $x->{success};
    my $robust = $x->{robust};
    my $tried = $x->{success}+$x->{fail}+$x->{robust} || 0.001;
    my $failed = $x->{fail};
    my $done = 0;
    
    $done = 100*($tried/$x->{size}) if ($x->{size});

    $log{$corpus} = { tried => $tried,
		      success => 100*$success / ($tried),
		      failed => 100*$failed / ($tried),
		      done => $done
		    };
    
    push(@results,
	 sprintf("%-20s Tried: %7i Success: %6.2f%% -- %5.2f%% Done: %6.2f%% %s",
		 substr($corpus,0,20), 
		 $tried,
		 100*$success / ($tried),
		 100*$failed/ ($tried),
		 $done,
		 $x->{modified} ? "*" : ""
		)
	);
    
  }

  my $all_tried = $all_success+$all_fail+$all_robust;
  my $all_done = 0;
    
  $time2 = (($time - $time2)/$all_tried) if ($all_tried);
  $all_done = 100*($all_tried/$remain_n) if ($remain_n);
  $timeout = 100*($timeout/$all_tried) if ($all_tried);
  my $alltime = $time;
  $time = $time / $all_tried if ($all_tried);

  my $date = strftime "%H:%M:%S", localtime;
  ## use last stop time or current time otherwise
  my $d2 = $ds1;
  $ds1=0;
##  $d2 = Date::Manip::DateCalc("today","+0hours") unless($d2);
  $d2 = Date::Manip::ParseDate("now") unless($d2);
  my $xelapsed = Date::Manip::DateCalc($d1,$d2);

  ## $elapsed = Date::Manip::Delta_Format($elapsed,1,"%st");
  my $elapsed = Date::Manip::Delta_Format($xelapsed,'approx',1,"%st");
  ##$elapsed -= $suspended;

  my $d3 = $ds2 || $d1;
  my $xelapsed_since_last_resume = Date::Manip::DateCalc($d3,$d2);
  my $elapsed_since_last_resume = Date::Manip::Delta_Format($xelapsed_since_last_resume,'approx',1,"%st");

  my $estimated = 0;
  $estimated = int( ($elapsed_since_last_resume / $all_tried) * $remain_n) if ($all_tried);
  my $remain = 0;
  $remain = int(($elapsed_since_last_resume / $all_tried) * ($remain_n - $all_tried)) if ($all_tried);
  my $end = Date::Manip::UnixDate(Date::Manip::DateCalc($d2,"+ $remain seconds"),"%Y-%m-%d %H:%M:%S");

  my $parse_time_ratio = 100 * $alltime / ($elapsed || .01);

  my $speed = 0;
  if (@last) {
    my $old = $last[0];
    my $delta = ($all_tried - $old->{tried});
    $speed = $nhosts*($elapsed-$old->{elapsed}) / $delta  if ($delta > 0);
  }
  push(@last,{ elapsed => $elapsed, tried => $all_tried });
  shift(@last) if ($#last > 12);

  my $full_speed = $elapsed / ($all_tried || 1);
  my $sent_per_sec = $all_tried / ($elapsed || 1);

  if (my $timeproc =  $times{timeproc}) {
    $sent_per_sec = $nhosts * $all_tried / ($timeproc || 1);
  }

  my $sent_per_hour = $sent_per_sec * 3.6;
  my $sent_per_day = $sent_per_hour * 24;

  my $checktime = $time;

  my $othertimes = 'Other times:';
  if ($all_tried) {
    while (my ($k,$v) = each %times) {
      $othertimes .= sprintf(" %s=%.2f",$k,$v/$all_tried);
      $checktime += $v/$all_tried unless (grep {$k eq $_} qw{1 1yesno 1amb 1passage 1conll proc x});
    }
    $othertimes .= sprintf(" %s=%.2f",'control',$checktime);
  }

  @results = (
	      "Report for $results at $date\nElapsed ${elapsed}sec, started at $start, est. end at $end [${estimated}sec]",
	      sprintf("missing processed corpus: %d",$ncorp_processed),
	      sprintf("Suspensions: %2d t=%ds",$suspensions,$suspended),
	      sprintf("Retried sentences: %d (%.2f%%) ok=%d (%.2f%%) pb=%d (%.2f%%) time=%.2fs\n",$retried,100*$retried / ($all_tried || 1),
		      $retried_ok, 100 * $retried_ok / ($retried || 1),
		      $retried-$retried_ok, 100*($retried-$retried_ok) / ($all_tried || 1),
		      $retried_time
		     ),
	      sprintf("Time: %5.2fs/%5.2fs Timeout: %5.2f%% ParseTime: %5.2f%% Speed: %5.2fs",$time,$time2,$timeout,$parse_time_ratio/$nhosts,$speed),
	      $othertimes,
	      sprintf("Speeds [%3d hosts]: %5.2fsec/s %5.2fs/sec %4.1fKs/hour %5.1fKs/day",
		      $nhosts,$full_speed,$sent_per_sec,$sent_per_hour,$sent_per_day),
	      sprintf("Speeds [%3d hosts]: %5.2fsec/s %5.2fs/sec %4.1fKs/hour %5.1fKs/day\n",
		      1,$full_speed*$nhosts,$sent_per_sec/$nhosts,$sent_per_hour/$nhosts,$sent_per_day/$nhosts),
	      sprintf("%-20s Tried: %7i Success: %6.2f%% -- %5.2f%% Done: %6.2f%% %2i/$ncorpus", 
		      'Total', $all_tried,100*$all_success / ($all_tried || 1),  100*$all_fail  / ($all_tried || 1), $all_done,$ncorp),
	      '-'x70,
	      @results
	     );

  my @hostinfo = ();
  foreach my $host (keys %hosts) {
    my $tried = $hosts{$host};
    my $success = $success_hosts{$host};
    my $rate = 0;
    $rate = 100 * ($success/$tried) if ($tried);
    my $timout = 0;
    $timeout = 100 * ($timeout_hosts{$host} / $tried) if ($tried);
    my $time = 0;
    $time = $time_hosts{$host} / $tried if ($tried);
    my $rel = 0;
    $rel = 100 * ($tried / $all_n) if ($all_n);
    push(@hostinfo,
	 sprintf("%-10s %8i %8i Rel: %6.2f%% Success: %6.2f%% Time: %5.2fs Timeout: %6.2f%%",
		 $host,$tried,$success,$rel,$rate,$time,$timeout));
  }
       
  my $msg = join("\n",@hostinfo,'-'x70,@results,"\n");
  $report->finePrint(0,0,$msg);

  if (($iter % 12)
      ||  (-e "$results/status/done")
     ) {
    if (-d "$ENV{HOME}/public_html") {
    open(STATS,">$ENV{HOME}/public_html/$results.txt") || die "can't open stats: $!";
    print STATS $msg;
    close(STATS);
  }

    open(STATS,">$results/results.txt") || die "can't open stats: $!";
    print STATS $msg;
    close(STATS);
    
  }

}



__END__


=head1 NAME

report.pl -- print a report from dispatch.pl logs

=head1 SYNOPSIS 	 
  	 
./report.pl [--config mydispatch.conf]

=head1 DESCRIPTION

report.pl builds a report from dispatch.pl logs. 

After typing ./report.pl:

=over 4

=item The report is displayed in the terminal. It is refreshed
automatically.

=item Results are sent to {results}/{results}.txt and are regularly
updated. You need to refresh the view to watch the results.

=back

The report is divided in two parts:

* For each host: 

=over 4

=item Tried: number of sentences tried

=item Number of sentences successfully parsed

=item Rel: rate tried/all sentences

=item Success: rate success/tried

=item Time: time/tried

=item Timeout: timeout/tried

=back

* For each corpus:

=over 4

=item Tried: total number of sentences

=item Success: rate success/tried

=item Done: tried/number of sentences specified in info.txt

=back

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, 2008, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
