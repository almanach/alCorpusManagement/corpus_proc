#!/usr/bin/perl

## Loading a log file inside a database to compute more easily statistics

use strict;
use Carp;

use DBI;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    { CREATE => 1 },
                            "verbose!"        => {DEFAULT => 1},
                            "corpus=f@"       => {DEFAULT => []},
                            "run!"            => {DEFAULT => 1},
                            "lock=f"          => {DEFAULT => 'lock'},
                            "results=f"       => {DEFAULT => 'results'},
                            "dest=f"          => {DEFAULT => 'results'},
                            "callparser=f"    => {DEFAULT => '/usr/bin/callparser'},
                            "parse_options=s" => {DEFAULT => '-collsave -time -stats -xmldep -robust'},
                            "key=s"           => {DEFAULT => ''},
                            "host=s@"         => {DEFAULT => []},
                            "hostallow!"      => {DEFAULT => 1},
			    "dag|dagdir=f"           => {DEFAULT => 'dag'},
			    "mafdocument!"    => {DEFAULT => 0},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "maxiter=i"       => {DEFAULT => 40},
			    "dump!"           => {DEFAULT => 0},
			    "beta=i"          => {DEFAULT => 0.1},
			    'distrib=f'
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my %status = ( unparsed => -1,
	       ok => 1,
	       fail => 0,
	       robust => 0
	     );

my $results = $config->results;
my $dest    = $config->dest;
my $verbose = $config->verbose;
my @corpus  = @{$config->corpus};
my $dagdir  = $config->dag;
my $dagext  = $config->dagext;

(-d $dest) or mkdir $dest
  || die "can't create destination dir $dest: $!";

my %cinfo = ();			# corpus info

if (@ARGV) {			
  # all remaining args are dispatch config files
  foreach my $conffile (@ARGV) {
    $config->file("$conffile")
      || die "can't open or process configuration file $conffile";
    $results = $config->results;
    $dagdir  = $config->dag;
    $dagext  = $config->dagext;
    print STDERR "Adding corpus $results\n";
    handle_corpus();
  }
} elsif (!@corpus) {		# single corpus
  handle_corpus();
}

sub handle_corpus {
  my $info = "$dagdir/info.txt";
  $info = "info.txt" unless (-r $info);
  if (!-r $info) {
    die "can't find info.txt in $dagdir or locally: $!";
  }

  open(INFO,"$dagdir/info.txt") or die "can't open $dagdir/info.txt: $!";

  while(<INFO>){
    chomp;
    next if (/^##/);
    print "INFO $_\n";

    my ($corpus,$size) = split(/\s+/,$_);
    push(@corpus,$corpus);
    $cinfo{$corpus} = {
		       dag => "$dagdir/$corpus.$dagext",
		       log => "$results/$corpus.log",
		      }
  }
  close(INFO);
}


my $rate=0;

my %words;
my %sentences;

my @words;
my @sentences;

my $wcount=0;
my $scount=0;
my $socc=0;
my $success=0;
my $avg=0;
my $var=0;

my $maxiter=$config->maxiter;
my $beta = $config->beta;


loader();
if ($config->dump) {
  maxent_dump();
  exit;
}
for (my $iter=1; $iter <= $maxiter; $iter++) {
  process($iter);
}

sub loader {

  print STDERR "Loading error database ...\n";

  open(LINE,">$dest/lines.dat") || die "can't open lines.dat";

  foreach my $corpus (@corpus) {
    print "Corpus $corpus\n";
    my $log = $cinfo{$corpus}{'log'};
    if (-r $log) {
      open(LOG,"<$log") or die "can't open log file: $!";
    } elsif (-r "$log.gz") {
      open(LOG,"-|","gunzip -dc $log.gz") or die "can't open log file: $!";
    } elsif (-r "$log.bz2") {
      open(LOG,"-|","bunzip2 -dc $log.bz2") or die "can't open log file: $!";
    } else {
      next;
    }



    print STDERR "\tprocessing log $corpus\n";
    my $id;
    my $status;
    my $time;
    my $ambiguity;
    my $host;
    my $timeout=0;
    while (<LOG>) {
      if (/^(fail|ok|robust)\s+(\d+)/) {
	$id = $2;
	next unless ($id);
	$status = $status{$1};
	$time = undef;
	$ambiguity = undef;
	$host = undef;
	$timeout=0;
	my $sid = $sentences{$corpus}{$id} = $scount++;
	my $sentry = { status => $status, 
		       corpus => $corpus, 
		       id => $id,
		       occ => [],
		       n => 0,
		       line => $.,
		       sid => $sid
		     };
	push(@sentences,$sentry);
	print LINE "$corpus $id $.\n";
	$success++ if ($status);
	next;
      }
    }
    close(LOG);
  }

  close(LINE);
  print STDERR "\tLoaded $scount sentences including $success successes\n";

  foreach my $corpus (@corpus) {
    my $file = $cinfo{$corpus}{dag};
    if (-r "$file") {
      open(DAG,"<$file") or die "can't open file: $!";
    } elsif (-r "$file.gz") {
      open(DAG,"-|","gunzip -dc $file.gz") or die "can't open file: $!";
    } elsif (-r "$file.bz2") {
      open(DAG,"-|","bunzip2 -dc $file.bz2") or die "can't open file: $!";
    } else {
      print STDERR "missing DAG file: $corpus ($file)\n";
      next;
    }
    print STDERR "\tprocessing dag $corpus\n";
    my $sid;
    my $n=0;
    my $edges = 0;
    my $lastsid;
    my $sentry;
    while (my $line = <DAG>) {
      if ($line =~ /^##\s*DAG\s+END/io){
	if ((defined $sentry) && !@{$sentry->{occ}}) {
	  print STDERR "*** VOID SENTENCE $corpus $sid\n";
	  exit 1;
	}
	next;
      }
      if ($line =~ /^##\s*DAG\s+BEGIN/io) {
	$line = <DAG>;
	$line =~ /id="E(\d+)F\d+"/;
	$lastsid = $sid;
	$sid = $1;
	if (defined $lastsid && $sid == $lastsid) {
	  print STDERR "Warning: duplicate $corpus $sid\n";
	  $sid++;
	}
	$n = 0;
	$edges = 0;
	undef $sentry;
	my $key = $sentences{$corpus}{$sid};
	$sentry = $sentences[$key] if (defined $key);
      }
      if ($line =~ /^(\d+)\s+\{(.+)\}\s+(.+?)\s+(\d+)/o) {
	## print "DAG $line";
	next unless (defined $sentry);
	my ($left,$content,$lex,$right) = ($1,$2,$3,$4);
	my @content = ($content =~ m{<F\s+.*?>\s*(.*?)\s*</F>}g);
	my $v = join("_",@content).'/'.$lex;
	# occurence to process
	my $wid = $words{$v} ||= $wcount++;
	## wentry: 0=rate 1=tmprate 2=occ 3=$lex 4=confidence 5=focc 6=fsentences 7=spring 8=history
	## 9=barycentre
	my $wentry = $words[$wid] ||= [1,0,0,$v,0,0,[],0,"",1];
	push(@{$sentry->{occ}},$wentry);
	$sentry->{n}++;
	$wentry->[2]++;
	$wentry->[5]++ unless ($sentry->{status});
	$socc++;
      }
      my ($tmp) = ($line =~ /id="E\d+F(\d+)"/o);
      $edges++;
      $n = $tmp if ($tmp > $n);
    }
    close(DAG);
  }

  print STDERR "\tLoaded $wcount words and $socc occurrences\n";

  %words = {};
  %sentences = {};

  $avg = ($scount-$success) / $socc;

  foreach my $wentry (@words) {
    my $lambda = $wentry->[7] = (1-exp(-$beta*$wentry->[2]));
    $wentry->[9] = $avg + $lambda * (1-$avg);
  }

  if ($config->distrib()) {
    my $db = "$dest/".$config->distrib();
    unlink $db if (-r $db);
    my $dbh = DBI->connect("dbi:SQLite:$db", "", "",
			   {RaiseError => 1, AutoCommit => 0});
    $dbh->do(<<EOF);
CREATE TABLE form (key,occ,focc);
EOF
    my $sth = $dbh->prepare('INSERT INTO form(key,occ,focc) VALUES (?,?,?)');
    foreach my $wentry (@words) {
      $sth->execute($wentry->[3],$wentry->[2],$wentry->[5]);
    }
    $dbh->commit;
    $dbh->disconnect;
  }

}

sub process {
  my $iter = shift;

  print STDERR "Processing round $iter...\n";

  foreach my $wentry (@words) {
    $wentry->[6] = [];
  }

  print STDERR "\tUpdating occurrences in sentences\n";
  foreach my $sentry (@sentences) {
    next if ($sentry->{status});
    my $n = 0;
    my $max=0;
    my $maxw=0;
##    $n += $avg+$_->[7]*($_->[0]-$avg) foreach (@{$sentry->{occ}});
    $n += $_->[9] foreach (@{$sentry->{occ}});
    foreach my $wentry (@{$sentry->{occ}}) {
##      my $delta = (($avg+$wentry->[7]*($wentry->[0]-$avg)) / $n);
      my $delta = $wentry->[9] / $n;
      $wentry->[1] += $delta;
      if ($delta > $max) {
	$max=$delta;
	$maxw=$wentry;
      }
    }
    $sentry->{max} = $max;
    if ($iter == $maxiter ) {
      if ($max == 0) {
	print STDERR "*** max=0 $sentry->{corpus} $sentry->{id}\n";
      } else {
	my $thresdhold=0.6*$max;
	foreach my $wentry (@{$sentry->{occ}}) {
	  my $delta = (($avg+$wentry->[7]*($wentry->[0]-$avg)) / $n);
	  next unless ($delta > $thresdhold);
	  my @l = ([$delta,$sentry],@{$wentry->[6]});
	  @l = sort {$b->[0] <=> $a->[0]} @l;
	  @l = @l[0..20] if (@l > 20);
	  $wentry->[6] = [@l];
	}
      }
    }
  }

  print STDERR "\tUpdating words\n";

  foreach my $wentry (@words) {
    my $tmp = $wentry->[0] = $wentry->[1] / $wentry->[2];
    $wentry->[1] = 0;
    $wentry->[9] = $avg + $wentry->[7]*($tmp - $avg);
    ##    $wentry->[4] = $wentry->[9] * $wentry->[2];
  }
  
  ## sorting @words
  unless (($iter % 5) && ($iter < ($maxiter -1))) {
    @words = sort {$b->[9] <=> $a->[9]} @words;
    my $rank=0;
    # handling history on words
    foreach my $wentry (@words) {
      next unless ($wentry->[2] > 5);
      last unless ($wentry->[9] > 1.5*$avg);
      $rank++;
      $wentry->[8] = "$wentry->[8] $iter:$rank:$wentry->[9]";
    }
  }

  if (0) {
    foreach my $wentry (@words) {
      my $n = $wentry->[2];
      next unless ($n > 10);
      my $nu = $n * $avg;
      my $s = 2*$nu * (1-$avg);
      $wentry->[4] = exp(-($wentry->[0]-$nu)**2/$s)/sqrt($s*3.14);
    }
  }

  mydump($iter) if ($iter == $maxiter);

}

sub mydump {
  my $iter = shift;
  ##  return if ($iter % 5);
  print STDERR "Dumping round $iter ...\n";
  print STDERR "\tsorting\n";
  ## my @words = sort {$b->[9] <=> $a->[9]} @words;
  print STDERR "\twriting\n";
  open(DUMP,">$dest/mine$iter".".csv") || die "can't dump iter $iter";
  print "#AVERAGE FAILURE RATE=$avg\n";
  print DUMP "#AVERAGE FAILURE RATE=$avg\n";
  my $rank=0;
  foreach my $wentry (@words) {
    next unless ($wentry->[2] > 5);
    last unless ($wentry->[9] > 1.5*$avg);
 ##   next unless ($iter != $maxiter || @{$wentry->[6]});
    $rank++;
    print DUMP "$wentry->[3]\t$wentry->[9]\t$wentry->[2]\t$wentry->[5]\t$wentry->[4]\t$wentry->[0]\n";
    print DUMP "\thistory $wentry->[8]\n";
    foreach my $info (@{$wentry->[6]}) {
      my $sentry = $info->[1];
      print DUMP "\t$sentry->{corpus}#$sentry->{id}\t$sentry->{line}\n";
    }
  }
  close(DUMP);
}


sub maxent_dump {
  open(DUMP,">$dest/me.txt") || die "can't open file:$!";
  foreach my $sentry (@sentences) {
    my $status = $sentry->{status};
    next if ($status == -1);
    $status = 1-$status;
    my %f = ();
    foreach my $occ (@{$sentry->{occ}}) {
      $f{$occ->[3]}++;
    }
    print DUMP "$status";
    foreach my $lex (sort keys %f) {
      my $v = $f{$lex} / $sentry->{n};
      print DUMP "\t$lex $v";
    }
    print DUMP "\n";
  }
  close(DUMP);
}

1;

__END__

=head1 NAME

errors2stats.pl -- To do error mining

=head1 SYNOPSIS

./errors2stats.pl --results <resultsdir> --dest <destdir>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
