dagdir  = /home/yquem/atoll/passage/dageasy.20071219
dagext  = udag
refdir  = /home/yquem/atoll/passage/new_ref
run     = 16
results = /home/pomerol/atoll/passage/results${run}
convert = ./convert${run}
extract = ./extract${run}
concat  = ./concat${run}
glue    = ./glue${run}
merge   = merge${run}
exportbuild = /home/pomerol/atoll/exportbuild

.PHONY: extract convert concat glue

extract:
	./easyextract.pl --results ${results} --dest ${extract} --dag ${dagdir} --dagext ${dagext}

convert:
	./easyconvert.pl --config ${results}/dispatch.conf --dag ${dagdir}  --results ${extract} --dest ${convert} --easyforest_options="-weights" -host pomerol

concat:
	./easyconcat.pl --results ${results} --dest ${concat} --dag ${dagdir} --dagext ${dagext}

${glue}/%.xml : ${concat}/%.xml
	-mkdir -p ${glue}
	cat $< | ./reglue.pl > $@ 2> $@.log

glue: ${patsubst ${concat}/%.xml,${glue}/%.xml,${wildcard ${concat}/*.xml}}

## Path to original files
new=${concat}

ORIGDIR=./${new}

WWW=${HOME}/atoll-www/frmg4/

package=${new}_patch

files= ${notdir ${wildcard ${glue}/*.xml}}

%.xml: correct_frmg.pl

%.xml: ${glue}/%.xml
	perl ./correct_frmg.pl $< > $@

%.log: %.xml
	xsh -I $< 'ls //Groupe[not(*)]' > $@

.PHONY: all clean dist new patch log

all: patch

all: ${files}

log: ${files:%.xml=%.log}

clean:
	-rm *.ph2.xml

dist: 
	-rm ${package}.tar.gz
	make ${package}.tar.gz

${package}.tar.gz: 
	-rm -R ${package}
	mkdir ${package}
	cp ${wildcard *.patch} Makefile README ${package}/
	tar -czf $@ ${package}
	-rm -R ${package}

new: 
	-rm ${new}.tgz
	make ${new}.tgz

${new}.tgz: all 
	-rm -R ${new}
	mkdir ${new}
	cp ${files} ${wildcard *.patch} Makefile README ${new}/
	tar -czf $@ ${new}
	-rm -R ${new}

patch:
	./check_frmg.pl --dagdir=${dagdir} --segdir=${refdir} ${wildcard ${glue}/*.xml}

dblog.dat:
	perl ./log2db.pl

dbrel.dat:
	perl ./rel2db.pl

%.png: %.pls
	 ploticus -png -o $@ $<

corpus_stats.png: corpus_stats.dat
const_stats.png: const_stats.dat
kind_stats.png: kind_stats.dat

corpus_rel_stats.png: corpus_rel_stats.dat
rel_stats.png: rel_stats.dat
kind_rel_stats.png: kind_rel_stats.dat

stats.html corpus_rel_stats.dat rel_stats.dat kind_rel_stats.dat:  dbrel.dat

stats.html corpus_stats.dat const_stats.dat kind_stats.dat: dblog.dat logdb2stats.pl
	perl logdb2stats.pl

.PHONY: stats install make_stats html newhtml merge ref

stats: corpus_rel_stats.png rel_stats.png kind_rel_stats.png
stats: stats.html corpus_stats.png const_stats.png kind_stats.png

install: stats html
	cp -u *_stats.png  *.html *.new.html style.css ${WWW}/

make_stats: 
	-rm make_stats.tgz
	make make_stats.tgz

make_stats.tgz: all 
	-rm -R make_stats
	mkdir make_stats
	cp log2db.pl logdb2stats.pl ${wildcard *.pls} Makefile make_stats/
	tar -czf $@ make_stats
	-rm -R make_stats

%.html: %.xml easy2html.xsl
	xsltproc -o $@  easy2html.xsl $<

%.merged.html: %.merged.xml easy2html.xsl
	xsltproc --novalid -o $@ --stringparam corpus $* easyalt2html.xsl $<

%.ref.html: %.ref.xml easy2html.xsl
	xsltproc --novalid -o $@ --stringparam corpus $* easyalt2html.xsl $<

html: ${files:.xml=.html}

mergedfiles = ${wildcard *.merged.xml}
reffiles = ${wildcard *.ref.xml}

mergedhtml: ${mergedfiles:.merged.xml=.merged.html}

refhtml: ${reffiles:.ref.xml=.ref.html}

merge: ${wildcard ${glue}/*.xml}
	./easymerge.pl -html ${merge}.html -ref ${refdir} -hyp ${glue} > ${merge}.log

ref: ${wildcard *.xml}
	./easymerge.pl -html ref.html -nohyp -ref ${refdir}  > ref.log

## Error mining

mine200.csv:
	./errors2stats.pl --config ${results}/dispatch.conf --results ${results} --dest ./ --maxiter 200 --dag ${dagdir}

dbdir/mine.dat:
	-mkdir -p dbdir
	./mine2db.pl --logdir ${results} --lexed ${exportbuild}/bin/lexed --lefff_frmg ${exportbuild}/lib/lefff-frmg

www= /home/pomerol/atoll/exportbuild/var/www/perl/results${run}

webinstall: dbdir/mine.dat
	-mkdir -p ${www}
	cp errorscgi.pl handlecomment.db.pl lefff.pl style.css ${www}/
