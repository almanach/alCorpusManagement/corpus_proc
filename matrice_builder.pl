#!/usr/bin/perl

# to build a confusion matrix from a filtered token.log

## example: 
## matrice_builder.pl -p ' form:^que$ '

use strict;
use File::Basename;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "pattern|p=s@" => {DEFAULT => []},
			    "diff=s",
			    "html=f" => {DEFAULT => "filtered.html"}
                           );

$config->args();

my $file = shift || 'tokens.log';
my $html = $config->html;
my $diff = find_diff_file();

my %map = (
	   'mode' => '$mode',
	   'cat' => '$cat',
	   'form' => '$form',
	   'hyp' => '$thyp',
	   'ref' => '$tref',
	   'id' => '$id',
);

my @gtypes = qw/GN GA GR GP NV PV/;
my @allgtypes = ('?','missing','void');

foreach my $gt (@gtypes) {
  push(@allgtypes,"B_$gt",$gt,"E_$gt","BE_$gt");
}

my @patterns = map {compile_pattern($_)} @{$config->pattern};

## print "PATTERN @patterns\n";
print "diff file=$diff\n" if ($diff);

if ($diff) {
  open(DIFF,">difftokens.log") || die "can't open difftokens.log:$!";
  my $current = filter($file);
  my $tmp = {};
  foreach my $entry (@$current) {
    chomp $entry;
    $entry =~ /^(\S+)\s/;
    $tmp->{$1} = $entry;
  }
  my $old = filter($diff,$tmp);
  foreach my $entry (@$old) {
    chomp $entry;
    $entry =~ /^(\S+)\s/;
    my $key = $1;
##    print "-- key='$key' entry=$entry\n\ttmp=$tmp->{$key}\n";
#    print "Try $key\n";
    unless (exists $tmp->{$key}) {
      my $xnew = $entry;
      $xnew =~ s/ (\S+)$/ missing/;
      $tmp->{$key} = $xnew;
#      print "\tRegister missing $key\n";
    }
    my $new = $tmp->{$key};
    my $ok_old = ok($entry);
    my $ok_new = ok($new);
    if ($ok_old eq $ok_new) {
      delete $tmp->{$key};
#      print "\tDiscard $key\n";
#      print "\tNot same $key:\n\t\t$new\n\t\t$entry\n" unless ($new eq $entry);
      next;
    }
#    print "\tKeep $key";
##    my ($old_type) = $entry =~ /\s(\S+)$/;
    print DIFF "$new --- $entry\n";
  }
  close(DIFF);
  emit_table([ values %$tmp ]);
} else {
  emit_table(filter($file));
}

sub ok {
  my $line = shift;
  return ($line =~ /\s+(\S+)\s+\1$/) ? 'ok' : 'no';
}

sub filter {
  my $file = shift;
  my $keep = shift || {};
  my @pending = ();
  my @tmp = @patterns;
  my @results = ();
  open(FILE,"<$file") || die "can't open $file: $!";
  while (my $line = <FILE>) {
    next if ($line =~ /^\#\#/);
    ## some lines are bogus
    next unless ($line =~ /(full|robust)\s+\S+\s+\S+$/);
  TEST: 
    if (@tmp) {
      if ($tmp[0]->($line)) {
	shift @tmp;
	push(@pending,$line);
	unless (@tmp) {
	  push(@results,@pending);
	  @pending = ();
	  @tmp = @patterns;
	}
      } else {
	if ($keep 
	    && $line =~ /^(\S+)\s/
	    && $keep->{$1}
	   ) {
	  push(@results,$line);
	}
	if (@pending) {
	  @pending = ();
	  @tmp = @patterns;
	  goto TEST;
	}
      }
    }
  }
  return \@results;
}

sub compile_pattern {
  my $s = shift;
  $s =~ s/^\s+//o;
  $s =~ s/\s+$//o;
  my $pattern = <<EOF;
sub {
  my \$line = shift;
  my (\$id,\$form,\$cat,\$mode,\$tref,\$thyp) = \$line =~ /^(\\S+F\\d+)\\s+(.+?)\\s+(\\S+)\\s+(robust|full)\\s+(\\S+)\\s+(\\S+)/o;
EOF
  my @tests = split(/\s+/,$s);
  foreach my $test (@tests) {
    my ($f,$t) = split(/[:=]/,$test);
##    print "test f='$f' t='$t'\n";
    if ($f eq 'form') {
      $t =~ s/_/ /og;
      $pattern .= "\n\treturn 0 unless (\$form =~ /$t/);";
    } elsif (($f eq 'hyp') && $t eq 'mismatch') {
      $pattern .= "\n\treturn 0 unless (\$tref ne \$thyp);";
    } elsif (($f eq 'hyp') && $t eq 'match') {
      $pattern .= "\n\treturn 0 unless (\$tref eq \$thyp);";
    } else {
      $pattern .= "\n\treturn 0 unless ($map{$f} =~ /$t/);";
    }
  }
  $pattern .= "\n\treturn 1; }";
  my $fun = eval($pattern);
##  print "pat=$pattern fun=$fun\n";
  return $fun;
}

sub emit_table {
  ## build the matrix
  my $lines = shift;
  my $table = {};
  my $col = {};
  my $row = {};
  my $keep = {};
  foreach my $line (@$lines) {
    my ($id,$form,$cat,$mode,$tref,$thyp) = 
      $line =~ /^(\S+F\d+)\s+(.+?)\s+(\S+)\s+(robust|full)\s+(\S+)\s+(\S+)/o;
    $table->{$tref}{$thyp}++;
    $col->{$thyp}++;
    $row->{$tref}++;
    $keep->{$thyp}++;
    $keep->{$tref}++;
  }
  ## emit the matrix
  my @pat = @{$config->pattern};
  open(HTML,">$html") || die "can't open $html: $!";
  print HTML header,
    start_html('EASy filtered confusion table'),
      h1("Filtered confusion table for '@pat'");

  if ($diff) {
    print HTML  p("differential bewteen with $diff");
  }

  my @gtypes = ();
  foreach my $r (@allgtypes) {
    next unless ($keep->{$r});
    push(@gtypes,$r);
  }

  my $tb = [th(['classified as =>', @gtypes,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  foreach my $t (@allgtypes) {
    next unless ($keep->{$t});
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    foreach my $r (@allgtypes) {
      next unless ($keep->{$r});
      $total += $info->{$r} || 0;
      $sum->{$r} += $info->{$r} || 0;
    }
    foreach my $r (@allgtypes) {
      next unless ($keep->{$r});
      my $n = $info->{$r} || 0;
      my $x = perc($n,$total);
      if ($r eq $t) {
	if ($diff && $total && ($n/$total > 0.5)) {
	  $n = "<span style='color: green;'>$n</span>" ;
	} else {
	  $n = "<span style='color: red;'>$n</span>" ;
	}
      }
      $n = "<span style='background-color: blue;'>$n</span>" if ($n > 150 && $r ne $t);
      if ($n == 0 && $r ne $t) {
	$n = "" ;
	$x = "";
      }
      push(@x,"<span title='$t $r'>$n<br/>$x</span>");
    }
    push(@$tb,td([$t,@x,$total,$t]));
    $ttotal += $total;
  }
  my @x = ('total');
  my $rtotal = 0;

  foreach my $r (@allgtypes) {
    next unless ($keep->{$r});
    push(@x,$sum->{$r});
    $rtotal += $sum->{$r};
  }
  push(@x,"$rtotal/$ttotal");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['classified as =>', @gtypes,'total','']));
  print HTML table({-border => 1}, Tr($tb));

  print HTML end_html;
  close(HTML);
}

sub perc {
  my ($a,$b) = @_;
  return '-' if ($b == 0);
  return sprintf("%.2f%%",$a / $b * 100);
}

sub find_diff_file {
  my $diff = $config->diff;
  return unless ($diff);	# we are running for a diff
  if ($diff eq 'last') {
    ## try tp find last run for a diff
    my $where = `pwd`;
    chomp $where;
    $where =~ /(\d+)$/;
    my $current = $1;
    my $prev = $current-1;
    $diff = "../results.easyref$prev/tokens.log";
  } elsif ($diff =~ /^\d+$/) {
    $diff = "../results.easyref$diff/tokens.log";
  } elsif (-d $diff) {
    $diff .= "/tokens.log";
  }
  return $diff if (-r $diff);
}
