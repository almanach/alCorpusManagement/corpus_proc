#!/usr/bin/env perl

use strict;
use AppConfig;
##use POSIX qw{strftime};
use POSIX;
use Data::Dumper;
use Data::Serializer;
use Data::Serializer::JSON;

sub Data::Serializer::JSON::deserialize {
  return JSON->VERSION < 2 ? JSON->new->jsonToObj($_[1]) : JSON->new->utf8->decode($_[1]);
}

use Text::LevenshteinXS qw(distance);
use String::LCSS_XS qw(lcss);
use Text::Undiacritic qw(undiacritic);
use Scalar::Util qw{weaken};
use XML::Writer;
use IO::File;
use Regexp::Optimizer;
use Regexp::List;
use List::Util::XS;
use List::AllUtils qw( :all );
## use Math::Random;
use Storable qw{dclone};

##use Sort::Maker;

use Set::Object;

my $config = AppConfig->new(
			    'verbose|v!' => {DEFAULT => 0},
			    'min=d' => {DEFAULT => 10},
			    'ratio=d' => {DEFAULT => 0.01},
			    'output|o=f' => {DEFAULT => 'cluto'},
			    'ctxset_min=d' => {DEFAULT => 2},
			    'min_strength=d' => {DEFAULT => 0.01},
			    "depth=d" => {DEFAULT => 8},
			    'reformat!' => {DEFAULT => 0}, # convert dependencies into a more readable format
			    'cache!' => {DEFAULT => 1}, # save to cache
			    'track=s@' => {DEFAULT => 1}, # tracking words
			    'dump_words!' => {DEFAULT => 0}, # dump
                                                             # words
                                                             # at each
                                                             # round 
			    'html!' => {DEFAULT => 1},
			    'round=d' => {DEFAULT => 10},
			    'mcl!' => {DEFAULT => 0}, # markov cluster algorithm
			    'inflate=d' => {DEFAULT => 2},
			    'seeds=f',
			    'wordnet=f',
			    'ctx_keep=d' => {DEFAULT => 50},
			    'beta=d' => {DEFAULT => 0.2 },
			    'xml!' => {DEFAULT => 1},
			    'comment=s',
			    'terms!' => {DEFAULT => 0},
			   );

my @alloptions=@ARGV;

my $args = join(' ',@ARGV);

$config->args;

my $min = $config->min;
my $minratio = $config->ratio;

my $minratio2 = 10 * $minratio * $minratio;

my $maxdepth = $config->depth;
my $maxround = $config->round;

my $ctxset_min = $config->ctxset_min;
my $min_strength = $config->min_strength;

my $inflate = $config->inflate;

my $extra_pos_inflate = 
  { adv => 5,
    adj => 3,
    nc => -1,
  };

my $beta = $config->beta;

my $ctx_keep = $config->ctx_keep;

my $track = { map {$_=>1} @{$config->track} };

my $ctx_remove = {
		  'modifieur' => { 'plus_adv' => 1,
				   'moins_adv' => 1,
				   'tr�s_adv' => 1,
				   '�tre_v' => 1,
				   'si_adv' => 1,
				   'aussi_adv' => 1,
				   'autant_adv' => 1,
				   'assez_adv' => 1,
				   'trop_adv' => 1,
				   '_NUMBER_det' => 1,
				   '_NUMBER_number' => 1,
				   '_NUMBER_adj' => 1
				 },
		  'antemodifieur' => { 'plus_adv' => 1,
				       'moins_adv' => 1,
				       'tr�s_adv' => 1,
				       '�tre_v' => 1,
				       'si_adv' => 1,
				       'aussi_adv' => 1,
				       'autant_adv' => 1,
				       'assez_adv' => 1,
				       'trop_adv' => 1,
				       '_NUMBER_det' => 1,
				       '_NUMBER_number' => 1,
				       '_NUMBER_adj' => 1
				     },
		  'sujet' => { '�tre_v' => 1,
			       'avoir_v' => 1,
			       'faire_v' => 1
			     },
		  'cod' => {
			    'avoir_v' => 1
			   },
		  'attribut' => {
				 'ce_cln' => 1 # clive: "c'est ..."
				}
		 };

## for wordnet
my %wn_pos = ( n => 'nc',
	       v => 'v',
	       a => 'adj',
	       b => 'adv'
	     );

## for RI
my $maxri = 666**2;

sub wn_attach_pos {
  my ($w,$pos) = @_;
  my $xpos = $wn_pos{$pos} || $pos;
  return "${w}_${xpos}";
}

## inspired by
## http://cm1cm2.ceyreste.free.fr/paulbert/prefix.html

my $prefixes = Regexp::List->new->list2re(
					 qw{se_ pro anti an ana arch archi
					    cata di dia dys ecto en �pi exo
					     hemi hyper hypo m�ta par para p�ri pro
					     syn sym kilo hecto deci milli centi ultra
					 }
					);

my $suffixes = Regexp::List->new->list2re(
					  qw{
					      atre
					      aire
					      algie
					      archie
					      arque
					      �tre
					      bare
					      bole
					      carpe
					      c�phale
					      cosmo
					      crate
					      cratie
					      cycle
					      dactyle
					      doxe
					      drome
					      �dre
					      game
					      gamie
					      g�ne
					      gone
					      gramme
					      graphe
					      graphie
					      hydre
					      l�trie
					      lithe
					      lite
					      logie
					      logue
					      mancie
					      mane
					      manie
					      m�tre
					      nome
					      nomie
					      o�de
					      onyme
					      pathe
					      p�die
					      phage
					      phagie
					      phane
					      phile
					      philie
					      phobe
					      phone
					      phonie
					      phore
					      pith�que
					      pode
					      pole
					      pt�re
					      scope
					      scopie
					      sph�re
					      taphe
					      technie
					      technique
					      th�que
					      th�rapie
					      therme
					      tomie
					      type
					      typie
					      cide
					      cole
					      culteur
					      culture
					      f�re
					      fique
					      forme
					      fuge
					      pare
					      p�de
					      vore
					      ie
					      eur
					      trice
					      ent
					      able
					      ion
					      isme
					      ique
					  }
					 );

my $pos_try = pos_try(
		      qw{
			  nc np
			  adj v
			  adj nc
			  adv nc
			  nc v
		      }
		     );

sub pos_try {
  my @l = @_;
  my $x = {};
  while (@l) {
    my $a = shift @l;
    my $b = shift @l;
    $x->{$a}{$b} = 1;
    $x->{$b}{$a} = 1;
  }
  return $x;
}

my $extra_sim = {};

my $potential_prefixes = {};
my $potential_suffixes = {};


my $ctx2info = {};		# all contexts
my $w2ctx = {};                   # mapping from words to theirs contexts after thredholds

my $ctx2w = {};			# mapping from contexts to words after thredholds

# my $ctx_sources = {};           # mapping from words to the contexts where they play the source
# my $ctx_targets = {};           # mapping from words to the contexts where they play the target

my $ctx2sources = {};		# mapping from ctx to source
my $ctx2targets = {};		# mapping from ctx to target

my $ctx2ctx = {};		# mapping of ctx to abstract ctx because of cluster-based rewriting

my $body2ctx = {};

my $clusters={};		# clusters

my $w2c = {};			# mapping from words to clusters: a word may belong to more than one cluster
my $ctx2c = {};			# mapping from ctx to clusters: a ctx may be in the definition of more than one cluster

my $l2c = {};			# labels to clusters

my $all = 0;
my $nwords = 0;
my $nwordslog = 0;

my $nctx = 0;
my $nctxlog = 0;

my $round = 0;

my $cluster_label=0;

my $base = $config->output;

my $w_ctr=0;
my $ctx_ctr=0;

my $chaos = 1;
my $who;
my $who2;
my $howmuch=0;
my $xchaos=0;
my $xn=0;

my $ychaos = 0;

if ($config->reformat) {
    while (<>) {
      ##      chomp;
      if (my @v = @{decompose($_)}) {
	print join("\t",@v),"\n";
      }
    }
} else {
    open(LOG,">$base.log") || die "can't open '$base.log': $!";
    collect_wctx();
    close(LOG);
}

BEGIN {
  ## to avoid duplicating corpus names in samples
  my %corpus = ();
  sub corpus {
    my ($corpus,$sid) = split(/:E/,$_);
    return [($corpus{$corpus} ||= \$corpus),$sid];
  }
}

sub decompose {
    my $s = shift;
    my @v = split(/\t/,$s);
    if (@v > 3) {
      # new format (4 or 5 fields)
      $v[0] =~ s/_\{(\w+)\}/_$1/o; # bug in a version of task_dispatcher.pl
      @v == 6 and $v[5] = [map(&corpus,split(/\s+/,$v[5]))];
      return \@v;
    } elsif ($s =~ m{^((?:_Uv|
			_PERSON(?:_m|_f)|
			_DATE_(?:_year|_arto|_artf)|
			_NP_WITH_INITIALS|
			_[A_Z]+|
			[^_]+?)_[a-z]+)
			 _([^_]+?)_
			 ((?:_Uv|
			   _PERSON(?:_m|_f)|
			   _DATE_(?:_year|_arto|_artf)|
			   _NP_WITH_INITIALS|
			 _[A-Z_]+|
			   [^_]+?)_[a-z]+)
			 \s+(\d+(?:\.\d+)?)$
		  }xo) {	
	## old format: depecated !
	return [$1,$2,$3,$4,$4,[]];
    } else {
	return [];
    }
}

sub verbose {
    my $date = strftime "[%F %H:%M:%S]", localtime;
    print STDERR "$date : ", @_, "\n";
    print LOG "$date : ", @_, "\n";
}

sub collect_wctx {
  verbose("starting: using '$0 @alloptions'");

  my $ra = Regexp::Optimizer->new;
  my $re1 = $ra->optimize(qr/_(cla|cld|cll|clg|prel|aux|coo|pro|prel|csu|pri|que|advneg)$/);
  my $re2 = $ra->optimize(qr/_(advneg|det|csu|coo|aux)$/);

  my $np = {};

  my $cache = "$base.cache";
  if (-f "$cache") {
      verbose("reusing cache $cache");
      my $info = Data::Serializer->new(
#				       serializer => 'Storable',
#				       serializer => 'YAML::XS',
#				       compress   => 1,
				       )->retrieve($cache) || die "coudn't read from $cache: $!";
      $w2ctx = $info->{w2ctx};
      $ctx2info = $info->{ctx2info};
      $ctx2sources = $info->{ctx2sources};
      $ctx2targets = $info->{ctx2targets};
      $all = $info->{all};
      $nwords = $info->{nwords};
      $nwordslog = log($nwords);
      verbose("cache loaded");
  } else {
      while (<>) {
	chomp;
	my @v = @{decompose($_)};
	next unless (@v);		    ## delete ill formed
	my ($v0,$v1,$v2,$v3) = @v;
	next unless ( $v3 > 1 || $v0 =~ /_np$/ || $v2 =~ /_np/);
	next if ( $v3 > 100000 ## deleted too frequent: not informative 
		  || any {!$_ || $_ eq '_' || /^(?:_Uv|cln_cln)/o || /$re1/o} ($v0,$v2)
		);
	my $rem1 = $ctx_remove->{$v1};
	($rem1 && ( $rem1->{$v2} || $rem1->{$v0})) and next;
	$v1 eq 'objet' and $v1 =  'cod';
	$v0 =~ /_np$/ and $v0 =~ s/^[ld]'//ig;
	$v2 =~ /_np$/ and $v2 =~ s/^[ld]'//ig;
	## rewrite some dependencies
	if (($v1 eq 'cod' || $v1 eq 'sujet') && $v2 =~ /_(v|aux)$/o) {
	  ## the verb of a sentential argument is not really important
	  ##	  print "*** Rewrite $v0 $v1 $v2 => sentence\n"; 
	  $v2 = '*sentence*_v';
	}
	if ($v1 eq 'attribut') {
	  next if ($v0 =~ /_cln/);
	  $v1 = 'modifieur';
	}
	if ($v0 =~ /_(v|aux)$/o && $v1 =~ /^(de|�|pour|par)$/o) {
	  $v1 = "cpl_$v1";
	}
	## $w is the number of occurrences
	## $w2 is the weight (less weight for dep in robust parses)
	my $w = ($v3 =~ /^\d+(\.\d+)?$/) ? ceil($v3) : 0;
	my $w2 = defined $v[4] ? ceil($v[4]) : $w;
	##	my $w2 = $w;
	my $samples = $v[5];
	unless ($v0 =~ /$re2/o) {
	  my $word = $v0;
	  my $l = $w2ctx->{$word} ||= { ctx => {}, 
					n => 0, 
					update => 1,
					id => $w_ctr++,
					nctx => 0
				      };
##	  my $ca = "*_${v1}_${v2}";
	  my $xv2 = $v2;
	  $xv2 =~ /_np$/ and $xv2 =~ s/^(?:.+:)//o;
	  my $ca = "L${v1}_${xv2}";
	  $l->{ctx}{$ca} += $w2;
	  $l->{n} += $w;
##	  $l->{samples}{$_} = $ca foreach (@$samples);
	  my $cai = $ctx2info->{$ca} 
	    ||= { n => 0, 
		  words => {},
		  id => $ctx_ctr++,
		  samples => {},
		};
	  $cai->{n} += $w;
	  $cai->{words}{$word} ||= 1;
	  $cai->{samples}{$word}=$samples;
#	  $ctx_targets->{$v2}{$ca} = "*_${v1}_";
	  $ctx2targets->{$ca} = [$v1,$xv2];
	  $v0 =~ /_np$/ and $np->{$v0}++;
	}
	unless ($v2 =~ /$re2/o) {
	  my $word = $v2;
	  my $r = $w2ctx->{$word} ||= { ctx => {}, 
					n => 0, 
					update => 1,
					id => $w_ctr++,
					nctx => 0
				      };
#	  my $cb = "${v0}_${v1}_*";

	  my $xv0 = $v0;
	  $xv0 =~ /_np$/ and $xv0 =~ s/^(?:.+:)//o;

	  my $cb = "R${xv0}_${v1}";
	  $r->{ctx}{$cb} += $w2;
	  $r->{n} += $w;
	  my $cbi = $ctx2info->{$cb} 
	    ||= { n => 0, 
		  words => {},
		  id => $ctx_ctr++,
		  samples => {}
		};
	  $cbi->{n} += $w;
	  $cbi->{words}{$word} ||= 1;
	  $cbi->{samples}{$word} = $samples;
# 	  $ctx_sources->{$v0}{$cb} = "_${v1}_*";
	  $ctx2sources->{$cb} = [$v1,$xv0];
	  $v2 =~ /_np$/ and $np->{$v2}++;
	}
	$all += $w;
      }
      $nwords = scalar(keys %$w2ctx);
      $nwordslog = log($nwords);
      verbose("removing hapax words and ctx; rewrite low NPs");
      my $wdel = 0;
      my $nprewrite=0;
      ## dealing with NPs
      ## abstract low-frequency NPs into their type
      ## only keep the 2% best NPs
      my $nps = int(0.01 * (keys %$np));
      my $i = 0;
      foreach my $w (sort {$np->{$b} <=> $np->{$a}} keys %$np) {
	next if ($i++ < $nps);
	##	next if ($np->{$w} > 20);
	my ($type) = ($w =~ /:(_.+?)$/);
	next unless ($type);
	my $w2i = $w2ctx->{$w};
	my $type2i = $w2ctx->{$type} ||= { ctx => {},
					   n => 0,
					   update => 1,
					   id => $w_ctr++,
					   nctx => 0
					 };
	$type2i->{n} += $w2i->{n};
	foreach my $ctx (keys %{$w2i->{ctx}}) {
	  $type2i->{ctx}{$ctx} += $w2i->{ctx}{$ctx}; 
	  my $ci = $ctx2info->{$ctx};
	  delete $ci->{words}{$w};
	  $ci->{words}{$type} ||= 1;
	  my $cisamples = $ci->{samples} ||= {};
	  push(@{$cisamples->{$type}},@{$cisamples->{$w}});
	  delete $cisamples->{$w};
	}
	delete $w2ctx->{$w};
	$nprewrite++;
      }
      undef $np;
      foreach my $w (keys %$w2ctx) {
	my $w2i = $w2ctx->{$w};
	if ($w2i->{n} > 1) {
	  $w2i->{nctx} = scalar(keys %{$w2i->{ctx}});
	} else {
	  delete $w2ctx->{$w};
	  $wdel++;
	}
      }
      my $cdel = 0;
      foreach my $c (keys %$ctx2info) {
	my $c2i = $ctx2info->{$c};
	if ($c2i->{n} > 1) {
	  $c2i->{words} = scalar(keys %{$c2i->{words}});
	} else {
	  foreach my $w (keys %{$c2i->{words}}) {
	    delete $w2ctx->{$w}{ctx}{$c};
	  }
	  delete $ctx2info->{$c};
	  if (exists $ctx2sources->{$c}) {
	    my ($wa,$r) = @{$ctx2sources->{$c}};
	    delete $ctx2sources->{$c};
# 	    delete $ctx_sources->{$wa}{$c};
	  } else {
	    my ($wb,$r) = @{$ctx2targets->{$c}};
	    delete $ctx2targets->{$c};
#	    delete $ctx_targets->{$wb}{$c};
	  }
	  $cdel++;
	}
      }
      verbose("removed $wdel words and $cdel ctx; rewriten $nprewrite NPs");

      if ($config->cache) {
	verbose("caching to $cache");
	my $pid = fork();
	defined $pid or die "couldn't fork for caching: $!";
	if ($pid == 0) {
	  Data::Serializer->new(
				## serializer => 'Storable',
##				serializer => 'YAML::Syck',
				serializer => 'JSON',
				compress   => 1,
			       )-> store( { w2ctx => $w2ctx,
					    ctx2info => $ctx2info,
#					    ctx_sources => $ctx_sources,
#					    ctx_targets => $ctx_targets,
					    ctx2sources => $ctx2sources,
					    ctx2targets => $ctx2targets,
					    all => $all,
					    nwords => $nwords
					  }, "$cache") || die "couldn't save cache in $cache: $!";
	  exit;
	}
      }
    }

  $nctx = keys %$ctx2info;
  $nctxlog = log($nctx);

  ## delete low frequency words and words with non strongly specific contexts
  ## for each word, deleted non specific contexts
  ## build an index for contexts
  my $x = keys %$w2ctx;
  my $y = keys %$ctx2info;
  verbose("read $all dependencies for $x words and $y contexts");

  $x = 0;
  $y = 0;
  ## my $cmin = floor(0.1*$min);
  ## $cmin < 2 and $cmin=2;
  while (my ($ctx,$info) = each %$ctx2info ) {
    delete $ctx2info->{$ctx}, next if ($info->{n} < $min);
    $y++;
    $info->{ws} ||= {};		# word distribution
    my $u = $info->{ctx}{$ctx} ||= [$info,1,0,{}]; # ctx self similarity
    weaken($u->[0]);
    #    $info->{ctx}{$ctx} ||= [$info,1,0,{}]; # ctx self similarity
    $info->{logn} = log($info->{n});
    if (my $struct = $ctx2sources->{$ctx}) {
      $info->{struct} = ['sources',$struct->[0],$struct->[1]];
      $body2ctx->{sources}{$struct->[1]}{$struct->[0]} = $ctx;
    } elsif (my $struct = $ctx2targets->{$ctx}) {
      $info->{struct} = ['targets',$struct->[0],$struct->[1]];
      $body2ctx->{targets}{$struct->[1]}{$struct->[0]} = $ctx;
    }
  }

  undef $ctx2sources;
  undef $ctx2targets;

  if ($config->terms) {
    term_extraction();
    return;
  }

  while (my ($w,$winfo) = each %$w2ctx) {
    ##    verbose("+++ check $w $winfo->{n}");
    delete $w2ctx->{$w}, next if ($winfo->{n} < $min);
    my $tmp = $winfo->{ctx};
    my $v = {};
    $winfo->{ws} ||= {};
    unless (exists $winfo->{ws}{$w}) {
      my $u = $winfo->{ws}{$w} = [$winfo,1,0,$v,{},0,1];	# word self similarity
      weaken($u->[0]);
    }
    $winfo->{logn} = log($winfo->{n});
    $winfo->{struct} ||= [ $w =~ /^(.+)_(\w+?)$/ ];
    $winfo->{similar} = {};
    my $n = $winfo->{n};
    my $Z = 0;
    my $cat1 = $winfo->{struct}[1];
    ## handling coordinations
    ## they will act as a kind of loop or quasi-fixed border for similarity
    foreach my $ctx (keys %$tmp) {
      ##      next unless ($ctx =~ /^(.+)_(et|ou|et-ou|,)_(.+)/o);
      next unless ( $ctx =~ /^(L)(et|ou|et-ou|,)_(.+)$/o
		    || $ctx =~ /^(R)(.+)_(et|ou|et-ou|,)$/o
		  );
      ##      print "Handling coord $ctx\n";
      my $w2 = $1 eq 'L' ? $3 : $2;
      my $v = $tmp->{$ctx};
      ##      my $struct = $ctx2info->{$ctx}{struct};
      next if ($w eq $w2);
      my $w2i = $w2ctx->{$w2};
      next unless ($w2i);
      my $cat2 = ($w2i->{struct} ||= [ $w2 =~ /^(.+)_(\w+?)$/ ])->[1];
      next unless (compatible_pos($cat1,$cat2));
      delete $tmp->{$ctx};	# delete coordinations
      (($winfo->{similar}{$w2} += $v / $n) > 1) and $winfo->{similar}{$w2} = 1;
      (($w2i->{similar}{$w} += $v/($w2i->{n} || 1)) > 1) and $w2i->{similar}{$w} = 1;
    }
    ## Random Index vectors attached to each word
    ## as the sum of the random vector attached to each context
    my $ri = undef;
    foreach my $ctx (keys %$tmp) {
      delete $tmp->{$ctx}, next unless ( exists $ctx2info->{$ctx} );
      my $ci = $ctx2info->{$ctx};
      $ci->{ws}{$w} = [$winfo,$tmp->{$ctx},0,0,0];
      $Z += $tmp->{$ctx} = log($tmp->{$ctx}+0.1)*ctx_weight($ctx);
      $ri = ri_add( $ri, 
		    $ci->{ri} ||= ri_new(),
		    $tmp->{$ctx}
		  );
    }
    $winfo->{ri} = ri_normalize($ri);
    unless (keys %$tmp) {
      verbose("*** potential deletion of high-frequency word $w: $n") if ($n > 100);
    }
    $Z ||= 1;
    my $ratio = 0;
    my $delete = 0;
    my $kept = 0;
    ##    $winfo->{origctx} = Set::Object->new(keys %$tmp);
    my $local_ctx_keep = ($cat1 eq 'nc') ? 4 * $ctx_keep : $ctx_keep;
    foreach my $c (
		   map {$_->[0]}
		   sort {$b->[1] <=> $a->[1] || $b->[2] <=> $a->[2] }
		   map {[$_,$tmp->{$_},$ctx2info->{$_}{n}]}
		   keys %$tmp
		  ) {
      verbose("STRANGE: r=0 for $w $c") unless ($tmp->{$c});
      my $r = $tmp->{$c} / $Z;
      ## $ratio ||= 0.2 * $r;
      $ratio ||= 0.1 * $r;
      delete $tmp->{$c}, next if ($delete ||= ($r < $ratio || $kept > $local_ctx_keep));
      $kept++;
      my $cinfo = $ctx2info->{$c};
      $cinfo->{ws}{$w}[3] = $r;
      $tmp->{$c} = [$cinfo,$r,0];
    }
    $winfo->{shared} = max(3,floor(sqrt($kept)));
    unless ($kept) {
      ($n > 100) and verbose("*** deleted high-frequency word $w: n=$n shared=$winfo->{shared} kept=$kept orig=$winfo->{nctx} ratio=$ratio");
      ## delete $w2ctx->{$w};
      $winfo->{delete} = 1;
      next;
    }
    $track->{$w} and $w2ctx->{$w}{tracked} = 1;
    $v->{$_} = $tmp->{$_}[1] foreach (keys %$tmp);
    $winfo->{sources} = {};	# source support
    $winfo->{targets} = {};	# target support
    $x++;
  }

  foreach my $c (keys %$ctx2info) {
    my $c2i = $ctx2info->{$c};
    my $Z = 0;
    my $cws = $c2i->{ws};
    $Z += $cws->{$_}[1] = log(0.1+$cws->{$_}[1])*w_weight($_) foreach (keys %$cws);
    my $n = $c2i->{n};
    my $ratio = 0;
    my $kept=0;
    my $delete = 0;
    if ($Z) {
      foreach my $w (
		     map {$_->[0]}
		     sort {$b->[1] <=> $a->[1] || $b->[2] <=> $a->[2] }
		     map {[$_,$cws->{$_}[1],$w2ctx->{$_}{n}]}
		     keys %$cws
		    ) {
	my $r = $cws->{$w}[1] /= $Z;
	##	$ratio ||= 0.2 * $r;
	$ratio ||= 0.1 * $r;
	if ($delete ||= ($r < $ratio || $kept > $ctx_keep)) {
	  delete $cws->{$w};
	  verbose("in ctx $c deleted word $w: n=$n kept=$kept ratio=$ratio r=$r") if ($n > 200 && $kept < 10 && ($c2i->{words}-$kept) > 10);
	  next;
	}
	$kept++;
	my $wcs = $cws->{$w}[0]{ctx};
	$wcs->{$c}[2] = $r if (exists $wcs->{$c});
      }
    } else {
      ##      verbose("*** Z=0 for ctx $c");
      delete $ctx2info->{$c};
      next;
    }
    $c2i->{shared} = max(3,floor(sqrt($kept)));
    unless ($kept) {
      ($n > 100) and verbose("*** deleted high-frequency ctx $c: n=$n shared=$c2i->{shared} kept=$kept orig=$c2i->{words}");
      delete $ctx2info->{$c};
      next;
    }
    unless (exists $c2i->{ctx}{$c}) {
      my $u = $c2i->{ctx}{$c} = [$c2i,1,0,{}]; # ctx self similarity
      weaken($u->[0]);
      
    }
  }

  foreach my $w (grep {exists $w2ctx->{$_}{delete}}  keys %$w2ctx) {
    delete $w2ctx->{$w};
  }

  load_seeds();			# if any
  load_wordnet();		# if any

  {
    open(W,"| bzip2 -c > $base.words.bz2") || die "can't open $base.words: $!";
    foreach my $w ( 
		   map { $_->[0] }
		   sort { $b->[1] <=> $a->[1] }
		   map { [$_,$w2ctx->{$_}{n}] }
		   keys %$w2ctx
		  ) {
      my $w2i = $w2ctx->{$w};
      my $c = scalar(keys %{$w2i->{ctx}});
      printf W "$w n=$w2i->{n}\tcorig:$w2i->{nctx}\tckept:$c\tcshared:$w2i->{shared}\n";
    }
    close(W);
  }

  {
    open(W,"| bzip2 -c > $base.ctx.bz2") || die "can't open $base.ctx: $!";
    foreach my $c (
		   map { $_->[0] }
		   sort { $b->[1] <=> $a->[1] }
		   map { [$_,$ctx2info->{$_}{n}] }
		   keys %$ctx2info
		  ) {
      my $c2i = $ctx2info->{$c};
      my $w = scalar(keys %{$c2i->{ws}});
      printf W "$c n=$c2i->{n}\tworig:$c2i->{words}\twkept:$w\twshared:$c2i->{shared}\n";
    }
    close(W);
  }


  verbose("kept $x words and $y contexts after applying thredholds\n");
  
  $round = 0;

  while (($chaos > 0.001) && ($round++ < $maxround)) {

    $chaos=0;
    $xchaos=0; $xn =0;
    $who = '';
    $who2 = '';

    $ychaos=0;

    verbose("round $round: updating context distributions");
    update_ctx_similarities($round);

    verbose("round $round: updating word distributions");
    update_word_similarities($round);

    $xn ||= 1;
    $xchaos /= $xn;

    verbose("done round $round: xchaos=$xchaos ychaos=$ychaos chaos=$chaos who=$who who2=$who2 old=$howmuch");
    word_dump();
    word_xml_dump() if ($config->xml);
##    dot_dump();
##    sif_dump();
    dump_potential_affixes();

    if ($config->mcl) {
      dump_mcl_edges();
      exit;
    }

  }

  verbose("done (results in file '$base.clusters')");

}

sub compatible_pos {
  my ($pos1,$pos2) = sort @_;
  return ($pos1 eq $pos2 ) 
    || ($pos1 eq 'adj' && $pos2 eq 'v')
      || ($pos1 eq 'nc' && $pos2 eq 'np');
}

sub word_extra_similarity {
  ## examine other parameters to increase the similarity between two words
  my ($w1i,$w2i) = @_;
  $w1i == $w2i and return 1;
  my $bonus = 1;
  my ($xw1,$pos1) = @{$w1i->{struct}};
  my ($xw2,$pos2) = @{$w2i->{struct}};
  compatible_pos($pos1,$pos2) and $bonus += 0.05;
  if ($xw1 eq $xw2) {
    $bonus += 0.3;
  } elsif (exists $extra_sim->{$xw1}{$xw2}) {
    $bonus += $extra_sim->{$xw1}{$xw2};
  } else {
    my $xbonus = 0;
    my $l1 = length($xw1);
    my $l2 = length($xw2);
    if ($l1 > 4 
	&& $l2 > 4
	&& distance($xw1,$xw2) < 2
       ) {
      ## this one should capture small typo diffs
      my $yw1 = undiacritic($xw1);
      my $yw2 = undiacritic($xw2);
      $yw1 =~ s/-//og;
      $yw1 =~ s/[exsr]$//o;
      $yw2 =~ s/-//og;
      $yw2 =~ s/[exsr]$//o;
      $xbonus += 
	($yw1 eq $yw2) ? 0.3 :
	  ($l1 > 6 && $l2 > 6) ? 0.05 : 0;
    } elsif ($pos1 ne 'np' && $pos2 ne 'np') {
      ## should be the place to try some derivationnal morphology
      my ($lcss,$start1,$start2) = lcss($xw1,$xw2,4);
      if ($lcss) {
	my $prefa = substr($xw1,0,$start1);
	my $prefb = substr($xw2,0,$start2);
	my $suffa = substr($xw1,$start1+length($lcss));
	my $suffb = substr($xw2,$start2+length($lcss));
	my $isprefa = !$suffa && (length($prefa) > 3 || ($prefa =~ /^$prefixes$/o));
	my $isprefb = !$suffb && (length($prefb) > 3 || ($prefb =~ /^$prefixes$/o));
	my $issuffa = !$prefa &&  (length($suffa) > 3 || ($suffa =~ /^$suffixes$/o));
	my $issuffb = !$prefb && (length($suffb) > 3 || ($suffb =~ /^$suffixes$/o));
	$suffa and $potential_suffixes->{$suffa}{$xw1} = 1;
	$suffb and $potential_suffixes->{$suffb}{$xw2} = 1;
	$prefa and $potential_prefixes->{$prefa}{$xw1} = 1;
	$prefb and $potential_prefixes->{$prefb}{$xw2} = 1;
	$xbonus += 0.1 if (($isprefa && $isprefb)
			   || ($isprefa && !$prefb)
			   || ($isprefb && !$prefa) 
			  );
	$xbonus += 0.1 if ( ($issuffa && $issuffb)
			    || ($issuffa && !$suffb)
			    || ($issuffb && !$suffa) 
			  );
      }
    }
    $bonus += $extra_sim->{$xw1}{$xw2} = $xbonus;
  }
  ##      $xbonus and verbose("extra sim ${xw1} ${xw2} xbonus=$xbonus");
  return $bonus;
}

sub new_ws_entry {
  my ($w1i,$w2i,$r) = @_;
  my $u = [ $w2i,        # w2 word info #0
	    $r || 0,     # old rate     #1
	    0,           # new rate     #2
	    {},          # old distrib  #3
	    {},          # new distrib  #4
	    0,           # score        #5
	    # bonus                     #6
	    word_extra_similarity($w1i,$w2i)
	  ];
  weaken($u->[0]);
  return $u;
}

sub dump_potential_affixes {
  open(A,">$base.affixes") || die "can't open $base.affixes: $!";
  my $potential = {};
  my $map = {};
  foreach my $pref (
		    map {$_->[0]}
		    sort {$b->[1] <=> $a->[1]}
		    map {[$_,scalar(keys %{$potential_prefixes->{$_}})]}
		    keys %$potential_prefixes
		   ) {
    next unless (length($pref) > 1);
    my @n = keys %{$potential_prefixes->{$pref}};
    my $n = @n;
    next unless ($n > 8);
    my $l = length($pref);
    $map->{"$pref-"} = [0,$pref];
    foreach my $x (@n) {
      my $base = substr($x,$l);
      $potential->{$x}{"$pref-"} = $x;
    }
    ## print A "${pref}-\t$n\t@n\n";
  }
  foreach my $suff (
		    map {$_->[0]}
		    sort {$b->[1] <=> $a->[1]}
		    map {[$_,scalar(keys %{$potential_suffixes->{$_}})]}
		    keys %$potential_suffixes
		   ) {
    my @n = keys %{$potential_suffixes->{$suff}};
    ## print "potential: $suff => @n\n";
    next unless (length($suff) > 1);
    my $n = @n;
    next unless ($n > 8);
    my $l = length($suff);
    $map->{"-$suff"} = [1,$suff];
    foreach my $x (@n) {
      my $base = substr($x,0,-$l);
      $potential->{$base}{"-$suff"} = $x;
    }
    ##    print "-${suff}\t$n\t@n\n";
  }
  my $affixes = {};
  foreach my $base (keys %$potential) {
    my $laffixes = $potential->{$base};
    my @l = 
      map {$_->[1]}
	sort {$a->[0] <=> $b->[0]}
	  map {[length($_),$_]}
	    keys %$laffixes;
    ##    print "Testing potential '$base' affixes=@l\n";
    while (@l) {
      my $aff1 = shift @l;
      my ($code1,$xaff1) = @{$map->{$aff1}};
      delete $laffixes->{$aff1} 
	if (grep { 
	  my ($code2,$xaff2) = @{$map->{$_}};
	  my $i=index($xaff2,$xaff1);
	  ($code1 eq $code2)
	    && 
	      ($code1 == 0 && $i == 0)
		|| ($code1 == 1 && $i == length($xaff2)-length($xaff1))
	      } @l);
    }
    next unless (keys %$laffixes > 1);
    foreach my $affix (keys %$laffixes) {
      push(@{$affixes->{$affix}},[$base,$potential->{$base}{$affix}]);
    }
  }
  foreach my $affix (
		     map {$_->[0]}
		     sort {$b->[1] <=> $a->[1]}
		     grep {$_->[1] > 3}
		     map {[$_,scalar(@{$affixes->{$_}})]}
		     keys %$affixes
		    ) {
    #    print "Affix $affix\n";
    my @n = 
      map {"$_->[0]:$_->[1]"}
	sort {$b->[1] <=> $a->[1]}
	  map {[$_->[1],scalar(keys %{$potential->{$_->[0]}})]}
	    @{$affixes->{$affix}};
    my $n = scalar(@n);
    print A "$affix\t$n\t@n\n";
  }
  ##  $potential_prefixes={};
  ##  $potential_suffixes={};
  close(A);
}

sub rank_correlation {
  my ($v1,$v2) = @_;
  my $n = 0;
  my $r1 = 0;
  my $c = 0;

  my %tmp2 = ();
  my $r2=0;

  $tmp2{$_->[1]} = [++$r2,$_->[0]] foreach ( sort {$b->[0] <=> $a->[0]}
					     map {[$v2->{$_},$_]}
					     keys %$v2 );

  foreach my $vk1 ( sort {$b->[0] <=> $a->[0]}
		    map {[$v1->{$_},$_]}
		    keys %$v1
		  ) {
    $r1++;
    my $r2i = $tmp2{$vk1->[1]};
    next unless ($r2i);
    my $x1 = $vk1->[0];
    my $u = 1 / (1+(abs($r2i->[1]-$x1) / $x1));
    $c +=  $u / (1+(abs($r2i->[0]-$r1) / $r1));
    $n++;
  }
  ##  return $n ? ($c / $n) : 0;
  return $c;
}

sub ctx_weight {		
    # the weight of a ctx is related to the number of words in which it occurs
    my $ctx = shift;
    my $cinfo = $ctx2info->{$ctx};
    $cinfo or return 0;
    unless (exists $cinfo->{w}) {
	my $words = $cinfo->{words};
	my $v;
	if (exists $cinfo->{abstract}) {
	  ## abstract context have maximal weight
	  ## should maybe be refined
	  $v=$cinfo->{w} = $nwordslog - 0.5 * log($words);
	} elsif ($words == 0) {
	  $v = $cinfo->{w} = 0;
	  verbose("*** ctx_weight $ctx  nwords=$nwords words=$words weight=$v");
	} elsif ($words == 1) {
	  $v=$cinfo->{w} = ($cinfo->{n} < 2) ? 0.1 : $nwordslog;
	} else {
	  $v=$cinfo->{w} = $nwordslog - log($words);
	  ## need to decrease the weight of abstracted ctx 
	  $ctx =~ /__.*_np/ and $v=$cinfo->{w} *= 0.5; 
	  $ctx =~ /^R_.*_np/ and $v=$cinfo->{w} *= 0.5; 
	  $ctx =~ /cod_\*sentence\*_v/ and $v=$cinfo->{w} *= 0.5; 
	}
##	verbose("ctx_weight $ctx  id=$cinfo->{id} words=$words weight=$v");
	if ($v < 0) {
	  verbose("*** ctx_weight $ctx  nwords=$nwords words=$words weight=$v");
	  exit;
	}
    }
    return  $cinfo->{w};
}

sub w_weight {		
  # the weight of a w is related to the number of ctx in which it occurs
  my $w = shift;
  my $wi = $w2ctx->{$w};
  return 0 unless ($wi);
  unless (exists $wi->{weight}) {
    my $n = $wi->{nctx};
    my $v;
    if ($n == 0) {
      $v = $wi->{weight} = 0;
      verbose("w_weight $w  weight=$v");
    } elsif ($n == 1) {
      $v=$wi->{weight} = ($wi->{n} < 2) ? 0.1 : $nctxlog;
    } else {
      $v=$wi->{weight} = $nctxlog - log($n);
    }
    if ($v < 0) {
      verbose("w_weight $w  weight=$v");
      exit;
    }
  }
  return  $wi->{weight};
}

sub word_dump {
  verbose("dumping to $base.dump.$round.bz2");
  my $n = 0;
  open(D,"|bzip2 -c > $base.dump.$round.bz2") || die "can't dump: $!";
  foreach my $qw1 ( sort {$b->[0] <=> $a->[0]}
		    map {[$w2ctx->{$_}{score},$_,$w2ctx->{$_}]}
		    keys %$w2ctx
		  ) {
    my ($score1,$w1,$winfo) = @$qw1;
##    last unless ($winfo->{score});
##    last unless ($winfo->{score} > -1000);
    my $tmp1 = $winfo->{ws}{$w1};
    my $w1score = $tmp1->[1] || 1;
    last if ($score1 < $minratio);
    my $v0 = $winfo->{ctx};
    my $v1 = $tmp1->[3];
    unless (scalar(keys %$v0)) {
      verbose("*** $w1 has a void ctx list");
      delete $w2ctx->{$w1};
      next;
    }
    my $ws = $winfo->{ws};
##    my @closest = (sprintf("%s:%.2f%%",$w1,100*$w1score));

    my $freq1 = $winfo->{n};
    my $div1 = $winfo->{nctx};
    my $xw1 = $winfo->{struct}[0];
    print D sprintf("\n<%d> %s (%.2f%% s=%.2f self=%.2f n=%d c=%d:%d:%d) <%s> <<%s>>\n",
		    ++$n,
		    $w1,
#		    100*$w1score,
		    100,
		    100*$score1,
		    100*$w1score,
		    $freq1,
		    scalar(keys %$v0),
		    scalar(keys %$v1),
		    $winfo->{shared},
		    join(' ',
			 map { sprintf("%s:%.2f",$_->[1],100*$_->[0]) } 
			 sort {$b->[0] <=> $a->[0]} 
			 map {[$v1->{$_},$_]}
			 keys %$v1
			),
		    join(' ',
			 map { sprintf("%s:%.2f",$_->[1],100*$_->[0]) } 
			 sort {$b->[0] <=> $a->[0]} 
			 map {[$v0->{$_}[1],$_]}
			 keys %$v0
			)
		   );
    my $n = 0;
    foreach my $w2 ( map {$_->[1]}
		     sort {$b->[0] <=> $a->[0]}
		     map {[$ws->{$_}[5],$_]}
		     keys %$ws
		   ) {
      next if ($w1 eq $w2);
      my $tmp2 = $ws->{$w2};
      my $w2score = $tmp2->[1] / $w1score;
      my $v2 = $tmp2->[3];
      my $w2i = $tmp2->[0];
      my $freq2 = $w2i->{n};
      my $div2 = $w2i->{nctx};
      my $xw2 = $w2i->{struct}[0];
      my $super = ($freq2 > 4*$freq1 && $div2 > 2*$div1) ? '+'
	: ($freq1 > 4*$freq2 && $div1 > 2*$div2) ? '-'
	  : '~';
      $super .= '*' if $winfo->{similar}{$w2};
      $super .= '#'  if ( exists $extra_sim->{$xw1}{$xw2}
			 && $extra_sim->{$xw1}{$xw2}
			);
      print D 
	"\t$w2 [$super] ",
	  sprintf('%.2f%% %.2f %2.f%% n=%d c=%d:%d:%d ',
		  100*$w2score, 
		  100*$tmp2->[5],
		  100*$tmp2->[1],
		  $freq2,
		  scalar(keys %{$w2i->{ctx}}),
		  scalar(keys %$v2),
		  $w2i->{shared}
		 ),
		   "<", 
		     join(' ', 
			  map { my $s = sprintf("%s:%.2f%%",
						$_,
						100*$v2->{$_},
						##						(exists $v1->{$_} ? '+' : '')
					       );
##				$s = "\e[1m$s\e[0m" if exists $v1->{$_};
				if (exists $v1->{$_} && exists $v0->{$_}) {
				  $s = "\e[0;31m$s\e[0m";
				} elsif (exists $v1->{$_}) {
				  $s = "\e[0;32m$s\e[0m";
				} elsif (exists $v0->{$_}) {
				  $s = "\e[0;36m$s\e[0m";
				}
				$s;
			      }
			  sort {$v2->{$b} <=> $v2->{$a}}
			  keys %$v2),
			    ">\n";
      last if ($n++ > 10);
    }
  }
  close(D);
}


sub word_xml_dump {
  verbose("dumping to $base.dump.$round.xml.bz2");
  my $n = 0;
  my $output = IO::File->new("| bzip2 -c > $base.dump.$round.xml.bz2") 
    or die "can't dump: $!";
  my $gen = XML::Writer->new(
			     OUTPUT => $output,
##			     NEWLINES => 1,
##			     ENCODING => 'ISO-8859-1',
			     DATA_MODE => 1,
			     DATA_INDENT => 1,
			    );
  my $date = strftime "%F %H:%M:%S", localtime;
  $gen->xmlDecl('ISO-8859-1');
  $gen->startTag('network',
		 round => $round,
		 base => $base,
		 date => $date,
		);
  $gen->dataElement('params',
		    $config->comment,
		    min => $min,
		    minratio => $minratio,
		    inflate => $inflate,
		    beta => $beta,
		    ctx_keep => $ctx_keep
		   );

  foreach my $qw1 (  sort {$b->[0] <=> $a->[0]}
		     map {[$w2ctx->{$_}{score},$_,$w2ctx->{$_}]}
		     keys %$w2ctx
		  ) {
    my ($score1,$w1,$winfo) = @$qw1;
##    last unless ($winfo->{score});
##    last unless ($winfo->{score} > -1000);
    my $tmp1 = $winfo->{ws}{$w1};
    my $w1score = $tmp1->[1] || 1;
    last if ($score1 < $minratio);
    my $v0 = $winfo->{ctx};
    my $v1 = $tmp1->[3];
    unless (scalar(keys %$v0)) {
      verbose("*** $w1 has a void ctx list");
      delete $w2ctx->{$w1};
      next;
    }
    my $ws = $winfo->{ws};
    my $freq1 = $winfo->{n};
    my $div1 = $winfo->{nctx};
    my $xw1 = $winfo->{struct}[0];

    $gen->startTag('w',
		   rank => ++$n,
		   lemma => $winfo->{struct}[0],
		   pos => $winfo->{struct}[1],
		   nctx => $winfo->{nctx},
		   shared => $winfo->{shared},
		   id => $w1,
		   freq => $freq1
		  );
    $gen->emptyTag('explanation',
		   weight1 => 1,
		   weight2 => $score1,
		   weight3 => $w1score,
		  );
    $gen->startTag('ctxset',
		   name => 'v0',
		   n => scalar(keys %$v0)
		  );
    $gen->emptyTag( 'ctx', 
		    name => $_->[1], 
		    w => $_->[0])
      foreach ( sort {$b->[0] <=> $a->[0]}
		map {[$v0->{$_}[1],$_]}
		keys %$v0 );
    $gen->endTag('ctxset');

    $gen->startTag('ctxset',
		   name => 'v1',
		   n => scalar(keys %$v1)
		  );
    $gen->emptyTag('ctx', 
		   name => $_->[1],
		   w => $_->[0] )
      foreach ( sort {$b->[0] <=> $a->[0]}
		map {[$v1->{$_},$_]}
		keys %$v1
	      );
    $gen->endTag('ctxset');

    my $n = 0;
    foreach my $w2 ( map {$_->[1]}
		     sort {$b->[0] <=> $a->[0]}
		     map {[$ws->{$_}[5],$_]}
		     keys %$ws
		   ) {
      next if ($w1 eq $w2);
      my $tmp2 = $ws->{$w2};
      my $w2score = $tmp2->[1] / $w1score;
      my $v2 = $tmp2->[3];
      my $w2i = $tmp2->[0];
      my $freq2 = $w2i->{n};
      my $div2 = $w2i->{nctx};
      my $xw2 = $w2i->{struct}[0];
      $gen->startTag('w',
		     lemma => $w2i->{struct}[0],
		     pos => $w2i->{struct}[1],
		     freq => $freq2,
		     nctx => $w2i->{nctx},
		     shared => $w2i->{shared},
		     rel => 
		     ($freq2 > 4*$freq1 && $div2 > 2*$div1) ? '+'
		     : ($freq1 > 4*$freq2 && $div1 > 2*$div2) ? '-'
		     : '~'
		     );
      my @attr2 = (
		   weight1 => $w2score,
		   weight2 => $tmp2->[5],
		   weight3 => $tmp2->[1],
		  );
      $winfo->{similar}{$w2} and 
	push(@attr2, similar =>  $winfo->{similar}{$w2});
      $extra_sim->{$xw1}{$xw2} and 
	push(@attr2, bonus => $extra_sim->{$xw1}{$xw2});
      $gen->emptyTag('explanation',@attr2);
      $gen->startTag('ctxset',
		     name => 'v2',
		     n => scalar(keys %$v2)
		    );
      $gen->emptyTag('ctx', name => $_->[1], w => $_->[0])
	foreach ( sort {$b->[0] <=> $a->[0]}
		  map {[$v2->{$_},$_]}
		  keys %$v2
		);
      $gen->endTag('ctxset');
      $gen->startTag('samples');
      foreach my $ctx ( map {$_->[1]}
			sort {$b->[0] <=> $a->[0]}
			map {[$v2->{$_},$_]}
			keys %$v2) {
	my $ci = $ctx2info->{$ctx};
	my $samples1 = $ci->{samples}{$w1};
	my $samples2 = $ci->{samples}{$w2};
	next unless ($samples1 && $samples2);
	$gen->emptyTag('sample',
		       w=> "1", 
		       ctx => $ctx, 
		       sid=>  join(' ',map {"${$_->[0]}:E$_->[1]"} @$samples1)
		      );
	$gen->emptyTag('sample',
		       w=> "2", 
		       ctx => $ctx,
		       sid => join(' ',map {"${$_->[0]}:E$_->[1]"} @$samples2) 
		      );
      }
      $gen->endTag('samples');
      $gen->endTag('w');
      last if ($n++ > 10);
    }
    $gen->endTag('w');
  }
  $gen->endTag('network');
  $gen->end();
  $output->close();
}

sub dot_dump {
  verbose("dumping to $base.dot");
  open(D,"> $base.dot") || die "can't dump: $!";
  print D <<EOF;
digraph G {
EOF
  my $seen = {};
  foreach my $w1 (sort {$w2ctx->{$b}{score} <=> $w2ctx->{$a}{score}}
		  keys %$w2ctx) {
    my $winfo = $w2ctx->{$w1};
    my $tmp1 = $winfo->{ws}{$w1};
    my $w1score = $tmp1->[1];
    last if ($winfo->{score} < $minratio);
    my $ws = $winfo->{ws};
    my $v1 = $tmp1->[3];
    my $n = 0;
    my $freq1 = $winfo->{n};
    my $div1 = $winfo->{nctx};
    foreach my $w2 (
		    sort {$ws->{$b}[5] <=> $ws->{$a}[5]}
		    keys %$ws) {
      next if ($w1 eq $w2);
      my $tmp2 = $ws->{$w2};
      my $v2 = $tmp2->[3];
      ##      my $label = sprintf("%.2f",100*$tmp2->[1] / $w1score);
      next if ($tmp2->[1] < 0.3);
      next if (exists $seen->{$w2}{$w1});
      my $w2i = $tmp2->[0];
      my $freq2 = $w2i->{n};
      my $div2 = $w2i->{nctx};
      my $super = ($freq2 > $freq1 || $div2 > $div1);
      if (exists $w2i->{ws}{$w1}) {
	next if ($super || $w2i->{ws}{$w1}[1] > $tmp2->[1]);
      }
      $seen->{$w1}{$w2} = 1;
      print D <<EOF;
"$w1" -> "$w2" [usedMetric1="$tmp2->[1]"];
EOF
      ##      last if ($n++ > 3);
      ##      last if (++$n > 0);
    }
  }
print D <<EOF;
}
EOF
  close(D);
}

sub sif_dump {
  verbose("dumping to $base.sif");
  open(D,"> $base.sif") || die "can't dump: $!";
  foreach my $w1 (sort {$w2ctx->{$b}{score} <=> $w2ctx->{$a}{score}}
		  keys %$w2ctx) {
    my $winfo = $w2ctx->{$w1};
    my $tmp1 = $winfo->{ws}{$w1};
    my $w1score = $tmp1->[1];
    last if ($winfo->{score} < $minratio);
    my $ws = $winfo->{ws};
    my $v1 = $tmp1->[3];
    my $n = 0;
    foreach my $w2 ( ## sort {$ws->{$b}[1] <=> $ws->{$a}[1]}
		    sort {$ws->{$b}[5] <=> $ws->{$a}[5]}
		    keys %$ws) {
      next if ($w1 eq $w2);
      my $tmp2 = $ws->{$w2};
      my $v2 = $tmp2->[3];
      ##      my $label = sprintf("%.2f",100*$tmp2->[1] / $w1score);
      print D "$w1\txx\t$w2\n";
      last if ($n++ > 10);
    }
  }
  close(D);
}


sub dump_mcl_edges {
  verbose("dumping mcl edges to $base.mcl");
  open(MCL,">$base.mcl") || die "can't dump to $base.mcl:$!";
  while (my ($w1,$w1i) = each %$w2ctx) {
    my $tmp1 = $w1i->{ws};
    $w1 =~ s/\s/_/og;
    while (my ($w2,$w2i) = each %$tmp1) {
      $w2 =~ s/\s/_/og;
      my $v = $w2i->[1];
      print MCL "$w1 $w2 $v\n";
    }
  }
  close(MCL);
}

sub dump_mcl {
  verbose("dumping an mcl matrix to $base.mcl");
  my $xdim = scalar(keys %$w2ctx);
  my $ydim = scalar(keys %$ctx2info);
  open(MCL,">$base.mcl") || die "can't dump to $base.mcl:$!";
  print MCL <<EOF;
(mclheader 
mcltype matrix 
dimensions ${ydim}x${xdim}
) 
(mclmatrix 
begin 
EOF
  foreach my $w (keys %$w2ctx) {
    my $winfo=$w2ctx->{$w};
    my @row = ($winfo->{id});
    while (my ($ctx,$f) = each %{$winfo->{ctx}}) {
      my $id = $ctx2info->{$ctx}{id};
      push(@row,"$id:$f");
    }
    push(@row,'$');
    print MCL "@row\n";
  }
  print MCL <<EOF;
)
EOF
  close(MCL);
}

## we use
##    F=(w.c) impl. through   w2ctx->{$w}{ctx}{$c} = [$ctxinfo,w.c]
##    F^t=(c.w) impl. through  ctx2info->{$c}{ws}{$w} = [$winfo,w.c]
##    G=(w1.w2) impl. through w2ctx->{$w1}{ws}{$w2} = [$w2info,w1.w2,tmp,{},{}]
##    H=(c1.c2) impl. through ctx2info->{$c1}{ctx}{$c2} = [$ctx2info,c1.c2,tmp,{},{}]
##    K=(lw1.lw2) impl. through K_1 and K_2
##         K_1:   c -> (kind, w, body) impl as ctx2info->{c}{struct}
##         K_2:  (kind,w,body) -> c    impl as body2ctx->{kind}{body}{w} 

##  we solve fix-points by iteration for
##        G = inflate(FHF^t+K^tHK+Loop)   : update word similarities
##        H = inflate(F^tGF+KGK^t+Loop)   : update ctx similarities

sub update_word_similarities {
  my $round = shift;

  my $transfer=0;

  my $op=0;

  ## loop1 on words
  foreach my $w1 (grep {keys %{$w2ctx->{$_}{ctx}}} keys %$w2ctx) {
    ##    verbose("\tword $w1");
    my $w1i = $w2ctx->{$w1};
    my ($w1cs,$w1ws,$logn1) = @{$w1i}{qw{ctx ws logn}};
    my $w1w1 = $w1ws->{$w1} 
      ||= [ $w1i,
	    0,
	    0,
	    {},
	    {},
	    0
	  ];
    my $v1 = $w1w1->[3];
    my $pos1 = $w1i->{struct}[1];
    my $pos1_accept = $pos_try->{$pos1} || {};

    ##    my $maxd = ($pos1 eq 'nc') ? 10 : 5;

    ## transfer (may be applied first)
    ## computing K^tHk^t
    my %transfer = ();
    if ($beta) {
      foreach my $kind1 (keys %$body2ctx) {
	my $kw1 = $body2ctx->{$kind1}{$w1};
	$kw1 or next;
	foreach my $body1 (keys %$kw1) {
	  my $c1 = $kw1->{$body1};
	  my $c1i = $ctx2info->{$c1};
	  $c1i or next;
	  my $c1cs = $c1i->{ctx};
	  foreach my $c2 ( grep {$c1 cmp $_ < 0} 
			   keys %$c1cs 
			 ) {
	    my $c2i = $c1cs->{$c2}[0];
	    ($c2i && $c2i->{struct}) or next;
	    my ($kind2,$body2,$w2) = @{$c2i->{struct}};
	    next unless (($kind2 eq $kind1) && ($body1 eq $body2));
	    next if ($w1 eq $w2);
	      if ((my $w2i = $w2ctx->{$w2}) 
		  && (my $delta = $beta * $c1cs->{$c2}[1])) {
		$transfer{$w2} += $delta;
		$transfer++;
	      }
	  }
	}
      }
    }

    ## collect potential similars
    my $seen = $w1i->{potential}
      ||= do { my %seen = ();
	       foreach (values  %$w1cs) {
		 $seen{$_}++ foreach (
				      grep { my $pos2 = $w2ctx->{$_}{struct}[1]; 
					     (($pos1 eq $pos2) 
					      || exists $pos1_accept->{$pos2})
					       && (abs($logn1-$w2ctx->{$_}{logn}) < log(20));
					   }
				      keys %{$_->[0]{ws}}
				     );
	       }
	       Set::Object->new( $w1,
				 keys %{$w1i->{similar}},
				 grep {$seen{$_} > 2} keys %seen
			       );
	     };
##    if (0 && $round > 2) {
    if (1) {
      my @stack = ([$w1,0]);
      my %seen2 = ($w1 => 0);
      while (@stack) {
	my ($w,$d) = @{shift @stack};
	($d++ > 2) and next;
	push( @stack,
	      map {[$_,$seen2{$_}=$d]}
	      grep { !exists $seen2{$_} }
	      keys %{$w2ctx->{$w}{ws}} 
	    );
      }
      $seen = $seen + Set::Object->new( ## keys %transfer,
					keys %seen2
				      );
    }

    ## loop2 on ctx
    foreach my $ca (map {$_->[0]}
		    sort {$b->[1] <=> $a->[0]}
		    map {[$_,$w1cs->{$_}[1]]}
		    keys %$w1cs
		   ) {
      my ($cai,$w1ca) = @{$w1cs->{$ca}}[0,1];
      ## $w1ca or verbose("*** $w1 void ctx $ca"), next;
      my $cacs = $cai->{ctx};
      ## keys %$cacs or verbose("*** $w1 $ca: void cs set");

      ## loop3 on ctx
      foreach my $cb (keys %$cacs) {
	my ($cbi,$cacb) = @{$cacs->{$cb}}[0,1];
	my $samec = ($ca eq $cb);
	## ($samec && !$cacb) and verbose("*** $w1 $ca void similar ctx $cb"), next;
	my $delta1 = $w1ca;

	## aggregation if both $ca and $cb are ctx for $w1 (and are similar)
	my $is_cb_in_w1=$w1cs->{$cb};
	if ($is_cb_in_w1) {
	  $delta1 += $is_cb_in_w1->[1];
	} elsif (0 && $w1i->{origctx}->member($cb)) {
	  $is_cb_in_w1 = 1;
	  $delta1 += 0.1;
	}

	$delta1 *= $cacb;

	my $cbws = $cbi->{ws};
	if ($samec && !exists $cbws->{$w1}) {
	  ## verbose("*** $w1 $ca $cb: missing self correl");
	  my $delta = $delta1 * $w1ca;
	  $w1w1->[2] += $delta;
	  $w1w1->[4]{$ca} += $delta;
	}

	## loop4 on words
	foreach my $w2 (grep {$seen->member($_)} keys %$cbws) {
	  my ($w2i,$delta) = @{$cbws->{$w2}}[0,3];
	  my $same = $samec && ($w1 eq $w2);
	  unless ($delta) {
	    $same and verbose("$w1 $ca: void self similarity");
	    next;
	  }
	  if ($same && !exists $w1ws->{$w2}) {
	    verbose("*** pb $w1 $ca");
	    exit;
	  }
	  ## $cb missing ctx for $w1 is penalized
	  ## inversely to the strength of $cb for $w2
	  ($same || $is_cb_in_w1) or $delta *= (1-0.98*$delta);
	  $delta *= $delta1;
	  my $w1w2 = $w1ws->{$w2} ||= new_ws_entry($w1i,$w2i);
	  $w1w2->[2] += $delta;
	  ##	  $w1w2->[4]{$cb} += $delta;
##	  $delta /= 2;
	  $w1w2->[4]{$ca} += $delta;
##	  $w1w2->[4]{$cb} += $delta;
	  $op++;
	}

      }
    }

    verbose("*** $w1: void ctx set") unless (keys %{$w1w1->[4]});

    ## inflating
    ##    verbose("\t\tinflating max=$max");
    my $Z = 0;
    my $max = 0;
    my $n = 0;
    my %rank1 = ();
    my $shared1 = $w1i->{shared};
    @rank1{keys %$v1} = (1 .. (keys %$v1));

    my $ri_check = ri_check_gen($w1,$w1i);

    my $inflate1 =  $inflate + ($extra_pos_inflate->{$pos1} || 0);

    foreach my $w2 (keys %$w1ws) {
      my $notw1 = ($w1 ne $w2);
      my $w1w2 = $w1ws->{$w2};
      my $cs = $w1w2->[4];
      my $w2i = $w1w2->[0];

      my $shared2 = min($shared1,$w2i->{shared});
      ## my $xshared2 = $shared2+2;
      delete $w1ws->{$w2}, next 
	if ($notw1 
	    && (! exists $w1i->{similar}{$w2})
	    && (scalar(keys %$cs) < $shared2));
      my $u = $w1w2->[2];
      ## print "PRIMER $w1 $w2 u=$u\n";
      my $ZZ = $u || 1;
      $u **= $inflate1;
      my $r2 = 0;
      my $ratio = 0;
      my $delete = 0;
      my $cshared = 0;

      foreach my $c (sort {$cs->{$b} <=> $cs->{$a}}
		     keys %$cs
		    ) {
	my $r = $cs->{$c} /= $ZZ;
	$ratio ||= $minratio * $r;
	##	verbose("*** ratio=0 w1=$w1 w2=$w2 c=$c"), next unless ($ratio);
	delete $cs->{$c}, next
	  if ($delete ||= (($r2++ > $ctx_keep) || ($r < $ratio)));
	my $r1 = $rank1{$c};
	my $v1c = $v1->{$c};
	if ($notw1 
	    && $r1 
	    ##   && $cs->{$c}
	    ##	 && $v1c
	    && (my $d=abs(log(2)+log($cs->{$c})-log($w1cs->{$c}[1]+$v1c))) < log(30)
	   ) {
	  $cshared++;
	  $u *= (1+$v1c+0.5/(1+$d));
	}
	$r1 ||= $ctx_keep;
	$r1 += abs($r2-$r1);
##	$u /= log(1+$r1 / 3);
      }

      delete $w1ws->{$w2}, next 
	if ( $notw1 && 
	     ($cshared < $shared2
	      || scalar(keys %$cs) < (2+$shared2)
	     )
	   );

      # use bonus
      $u *= $w1w2->[6];		
      if ($notw1) {
	# use RI bonus
	$u *= $ri_check->($w2,$w1w2->[0]);
	# use symmetry bonus (to be checked)
	exists $w2i->{ws}{$w1} and $u *= 1+0.5*$w2i->{ws}{$w1}[1];
	# use transfer
	exists $transfer{$w2} and $u *= 1+$transfer{$w2};
      }

      $Z += $w1w2->[2] = $u;
      $max = max($max,$u);
    }

    ## loop
    $max ||= 1;
    if ($max > $w1w1->[2]) {
      $Z = $Z-$w1w1->[2];
      $Z += $w1w1->[2] = $max;
    }

    while (my ($w2,$r) = each %{$w1i->{similar}}) {
      next unless (exists $w2ctx->{$w2});
      my $w1w2 = $w1ws->{$w2} ||= new_ws_entry($w1i,$w2ctx->{$w2});
      $r *= $max;
      if ($r > $w1w2->[2]) {
	$Z = $Z - $w1w2->[2];
	$Z += $w1w2->[2] = $r*$max;
      }
    }

    my $ratio1 = $minratio * min(1,$max / $Z);
    my $score = 0;
    ##    verbose("STRANGE w1=$w1 max=$max Z=$Z"), exit if ($max > $Z);
    my $ymax=0;
    my $ysumsq=0;
    foreach my $w2 (keys %$w1ws) {
      my $notw1 = ($w1 ne $w2);
      my $w1w2 = $w1ws->{$w2};
      ##      ($tmp->[2] > $max || $tmp->[2] < 0 ) and
      ##	verbose("STRANGE: $w1 $w2 $tmp->[2] > $max");
      my $r = $w1w2->[2] / $Z;
      if ($notw1) {
	my $delta = abs($r-$w1w2->[1]); 
	$xchaos += $delta; 
	$xn++;
	if ($w1w2->[1] && $delta > $chaos) {
	  $chaos = $delta; 
	  $who=$w1; 
	  $who2=$w2;
	  $howmuch=$w1w2->[1];
	}
	( $r < $ratio1 ) and delete $w1ws->{$w2}, next;
      }
      ($r > $ymax) and $ymax = $r;
      $ysumsq += $r ** 2;
      $w1w2->[1] = $r;
      $w1w2->[2] = 0;
      my $v12 = $w1w2->[3] = $w1w2->[4];
      $w1w2->[4] = {};
      my $corr = $w1w2->[5]
	= 0.5 * $r * ( rank_correlation($v1,$v12)
		       + rank_correlation($w1w2->[0]{ws}{$w2}[3],$v12) 
		     ); 
      delete $w1ws->{$w2}, next 
	if ($notw1 && $corr < 0.01);
      ($notw1 && ($score < $corr)) and ($score = $corr);
    }
    $w1i->{score} = $score;
    ($ymax - $ysumsq > $ychaos) and $ychaos = $ymax - $ysumsq;
  }
  
  verbose("round $round: done $op w-w updates and $transfer c-w transfers");

}

sub update_ctx_similarities {
  my $round = shift;

  my $transfer = 0;

  my $op = 0;

  ## loop1 on ctx
  foreach my $c1 (keys %$ctx2info) {
    ## verbose("\tctx $c1");
    my $c1i = $ctx2info->{$c1};
    my ($c1ws,$c1cs,$logn1) = @{$c1i}{qw{ws ctx logn}};

    
    ## transfer (may be computed first)
    ## computing KGK^t
    my %transfer=();
    if ($beta && exists $c1i->{struct}) {
      my ($kind1,$body1,$w1) = @{$c1i->{struct}};
      if (my $w1i = $w2ctx->{$w1}) {
	my $kws = $body2ctx->{$kind1};
	foreach my $w2 (grep {$w1 ne $_} keys %$kws) {
	  my $c2 = $kws->{$w2}{$body1};
	  next unless ($c2);
	  next if ($c2 eq $c1);
	  my $w1w2 = $w1i->{ws}{$w2};
	  next unless ($w1w2);
	  my $delta = $beta * $w1w2->[1];
	  next unless ($delta);
	  my $c2i = $ctx2info->{$c2};
	  next unless ($c2i);
	  $transfer{$c2} += $delta;
	  $transfer++;
	}
      }
    }

    ## collect potential similars
    my $seen = $c1i->{potential} 
      ||= do { my %seen = ();
	       foreach (values  %$c1ws) {
		 $seen{$_}++ foreach (keys %{$_->[0]{ctx}});
	       }
	       Set::Object->new( $c1,
				 grep {$seen{$_} > 2} keys %seen
			       );
	     };
    if (1) {
      my @stack = ([$c1,0]);
      my %seen2 = ($c1 => 0);
      while (@stack) {
	my ($c,$d) = @{shift @stack};
	($d++ > 2) and next;
	push( @stack,
	      map {[$_,$seen2{$_}=$d]}
	      grep { !exists $seen2{$_} }
	      keys %{$ctx2info->{$c}{ctx}} 
	    );
      }
      $seen = $seen + Set::Object->new( ## keys %transfer,
					keys %seen2
				      );
    }

    ## loop2 on words
    ## computing F^tGF
    ##    verbose("\t\tcombining through words");
    foreach my $wa ( map {$_->[0]}
		     sort {$b->[1] <=> $a->[1]} 
		     map {[$_,$c1ws->{$_}[1]]}
		     keys %$c1ws
		   ) {
      my ($c1wa,$wai) = @{$c1ws->{$wa}}[1,0];
      next unless ($c1wa);
      my $waws = $wai->{ws};

      ## loop3 on words
      foreach my $wb (keys %$waws) {
	my ($wbi,$wawb) = @{$waws->{$wb}}[0,1];
	next unless ($wawb);
	my $wbcs = $wbi->{ctx};

	my $delta1 = $c1wa;

	## aggregation is both $wa and $wb are words for $c1 (and are similar)
	my $is_wb_in_c1 = $c1ws->{$wb};
	$is_wb_in_c1 and $delta1 += $is_wb_in_c1->[1];
	
	$delta1 *= $wawb;

	my $samew = ($wa eq $wb);
	if ($samew && !exists $wbcs->{$c1}) {
	  ##	  verbose("*** $c1 $wa $wb: missing self correl");
	  my $delta = $delta1 * $c1wa;
	  my $c1c1 = $c1cs->{$c1} ||= [$c1i,0,0,{}];
	  $c1c1->[2] += $delta;
	  $c1c1->[3]{$wa} += $delta;
	}

	## loop4 on ctx
	foreach my $c2 (grep {$seen->member($_)} keys %$wbcs) {
	  my ($c2i,$wbc2) = @{$wbcs->{$c2}}[0,2]; # equals to $c2tmp->[0]{ws}{$wb}[1]
	  my $same = $samew && ($c1 eq $c2);
	  unless ($wbc2) {
	    $same and verbose("$c1 $wa: void self correl");
	    next;
	  }
	  ## next if (abs($c2i->{logn}-$logn1) > log(20));
	  verbose("*** pb ctx $c1 $wa")	if ($same && !exists $c1cs->{$c2});
	  my $c1c2 = $c1cs->{$c2} ||= [$c2i,0,0,{}];
	  my $delta = $wbc2;
	  ## $wb missing word for $c1 is penalized
	  ## inversely to the strength of $wb for $c2
	  ($same || $is_wb_in_c1) or $delta *= (1-$delta);
	  $delta *= $delta1;
	  $c1c2->[2] += $delta;
	  ##	  $c1c2->[3]{$wb} += $delta;
	  $c1c2->[3]{$wa} += $delta;
	  $op++;
	}
      }
    }

    ##    verbose("\t\tinflating max=$max");
    ## inflating

    my $Z = 0;
    my $max = 0;
    my $shared1 = $c1i->{shared};
    foreach my $c2 (keys %$c1cs) {
      my $c1c2 = $c1cs->{$c2};
      my ($c2i,$u,$ws) = @{$c1c2}[0,2,3];
      my $shared2 = min($shared1,$c2i->{shared});
      ##      my $xshared2 = $shared2+2;
      my $notc1 = ($c1 ne $c2);
      ($notc1 && (keys %$ws < $shared2)) and delete $c1cs->{$c2}, next;
      my $ZZ=$u || 1;
      $u **= $inflate;
      my $r2=0;
      my $ratio = 0;
      my $delete = 0;
      my $wshared=0;
      foreach my $w ( map {$_->[1]}
		      sort {$b->[0] <=> $a->[0]}
		      map {[$ws->{$_},$_]}
		      keys %$ws 
		    ) {
	my $r = $ws->{$w} /= $ZZ;
	$ratio ||= $minratio * $r;
	delete $ws->{$w}, next
	  if ($delete ||= (($r2++ > $ctx_keep) || ($r < $ratio)));
	if (exists $c1ws->{$w}) {
	  $u *= 1+$r;
	  $wshared++;
	}
      }
      delete $c1cs->{$c2}, next 
	if ($notc1 && 
	    ($wshared < $shared2 
	     || scalar(keys %$ws) < (2+$shared2)
	    )
	   );

      ## use bonus (transfer)
      ($notc1 && exists $transfer{$c2}) and $u *= 1+$transfer{$c2};

      $Z += $c1c2->[2] = $u;
      $max = max($max,$u);
    }

    ## loop
    $max ||= 1;
    if (1 || $round == 1) {
      my $c1c1=$c1cs->{$c1};
      $Z = $Z - $c1c1->[2];
      $Z += $c1c1->[2] = $max;
    } else {
      $Z ||= 1;
    }

    my $ratio1 = $minratio * ($max / $Z);
    $minratio < $ratio1 and $ratio1 = $minratio;

    my $ymax=0;
    my $ysumsq=0;

    foreach my $c2 (keys %$c1cs) {
      my $notc1 = ($c1 ne $c2);
      my $tmp = $c1cs->{$c2};
      ##      verbose("STRANGE $c1 $c2 $tmp->[2] Z=$Z"),exit if ($tmp->[2] > $Z);
      my $r = $tmp->[2] / $Z;
      verbose("*** $c1 $c2 r=0") unless ($r);
      if ($notc1) {
	delete $c1cs->{$c2}, next if ($r < $ratio1);
	my $delta = abs($r-$tmp->[1]);
	$xchaos += $delta; $xn++;
	($delta > $chaos) and $chaos = $delta, $who=$c1, $who2=$c2, $howmuch=$tmp->[1];
      }
      @{$tmp}[1,2,3] = ($r,0,{});
      ($r > $ymax) and $ymax = $r;
      $ysumsq += $r ** 2;
    }

    ($ymax-$ysumsq > $ychaos) and $ychaos = $ymax-$ysumsq;

  }

  verbose("round $round: done $op c-c updates and $transfer w-c transfers");

}

sub ri_new {
  my $v = {};
  $v->{int(rand(100000))} = int(rand(2)) || -1 ;
  ##  $v->{int(rand(10000))} = int(rand(2)) || -1 ;
  return $v;
}

sub ri_add {
  my ($v1,$v2,$lambda) = @_;
  $lambda ||= 1;
  if (defined $v1) {
    $v1->{$_} += $lambda * $v2->{$_} foreach (keys %$v2);
    return $v1;
  } else {
    ##    my $v = dclone $v2;
    my %v = ();
    @v{keys %$v2} = map {$lambda * $_} values %$v2;
    return \%v;
  }
}

sub ri_normalize {
  my $v = shift;
  my $Z = 0;
  $Z += $_ ** 2 foreach (values %$v);
  $Z ||= 1;
  $Z = sqrt($Z);
  $v->{$_} /= $Z foreach (keys %$v);
  return $v;
}

sub ri_mult {
  my ($v1,$v2) = @_;
  my $x = 0;
  $x += $v1->{$_} * $v2->{$_} foreach (keys %$v1);
  ##  verbose("ri mult x=$x\n\t@$v1\n\t@$v2") if ($x < 0);
  return $x;
}

sub ri_check_gen {
  my ($w1,$w1i) = @_;
  my $ri1 = $w1i->{ri};
  return sub { my ($w2,$w2i) = @_;
	       if (!exists $w1i->{ric}{$w2}) {
		 my $ri2 = $w2i->{ri};
#		 my $match = 0.5+0.5*ri_mult($ri1,$ri2);
		 my $match = ri_mult($ri1,$ri2);
		 return $w1i->{ric}{$w2}=
		   $w2i->{ric}{$w1} = $match;
	       } else {
		 return $w1i->{ric}{$w2};
	       }
	     };
}

sub vector_cosine {
  my ($w1,$w2) = @_;
  my $w1i = $w2ctx->{$w1};
  my $w2i = $w2ctx->{$w2};
  my $v1 = $w1i->{ctx};
  my $v2 = $w2i->{ctx};
  my $x1 = $w1i->{vnorm} ||= vector_norm($v1);
  my $x2 = $w2i->{vnorm} ||= vector_norm($v2);
  my $x = 0;
  $x += $v1->{$_}[1] * $v2->{$_}[1]
    foreach (grep {exists $v2->{$_}}
	     keys %$v1
	    );
  return $x / ($x1 * $x2);
}

sub vector_norm {
  my $v = shift;
  my $x = 0;
  $x += $_->[1] * $_->[1] foreach (values %$v);
  return sqrt($x);
}

sub ri_and_vector_check_gen {
  my ($w1,$w1i) = @_;
  my $ri1 = $w1i->{ri};
  return sub { my ($w2,$w2i) = @_;
	       if (!exists $w1i->{cosine}{$w2}) {
		 my $ri2 = $w2i->{ri};
		 my $match = 0.5 + 0.25 * (vector_cosine($w1,$w2)+ri_mult($ri1,$ri2));
		 return $w1i->{cosine}{$w2}=
		   $w2i->{cosine}{$w1} = $match;
	       } else {
		 return $w1i->{cosine}{$w2};
	       }
	     };
}

sub vector_check_gen {
  my ($w1,$w1i) = @_;
  return sub { my ($w2,$w2i) = @_;
	       if (!exists $w1i->{cosine}{$w2}) {
		 my $match = 0.5+0.5*vector_cosine($w1,$w2);
		 return $w1i->{cosine}{$w2}=
		   $w2i->{cosine}{$w1} = $match;
	       } else {
		 return $w1i->{cosine}{$w2};
	       }
	     };
}

sub load_seeds {
  my $seeds = $config->seeds;
  return unless ($seeds);
  (-f $seeds) or verbose("*** missing seed file $seeds"), return;
  if ($seeds =~ /\.gz$/) {
    $seeds = "unzip -c $seeds|" ;
  } elsif ($seeds =~ /\.bz2$/) {
    $seeds = "bunzip2 -c $seeds|";
  } else {
    $seeds = "<$seeds";
  }
  open(S,"$seeds") || die "can't open seed file: $!";
  while(<S>) {
    chomp;
    my ($w1,$w2,$s) = split(/\t/,$_);
    $s ||= 1;
    if (exists $w2ctx->{$w1} && exists $w2ctx->{$w2}) {
      verbose("adding seed similarity $s between $w1 and $w2");
      (($w2ctx->{$w1}{similar}{$w2} += $s) > 1) and $w2ctx->{$w1}{similar}{$w2} =1;
      (($w2ctx->{$w2}{similar}{$w1} += $s) > 1) and $w2ctx->{$w2}{similar}{$w1} =1;
    }
  }
  close(S);
}

sub load_wordnet {
  my $file = $config->wordnet;
  return unless ($file && -r $file);
  verbose("loading wordnet '$file'");
  if ($file =~ /\.bz2/) {
    open(W,"bzcat $file|") || die "can't open $file: $!";
  } else {
    open(W,"<$file") || die "can't open $file: $!";
  }

  my $wn = [];
  my $synset;
  my $entry;
  my $pos;

  while (<W>) {
    if (/^0\s+\@(\d+)\@\s+WORD_MEANING/) {
      ## emit_synset() if ($synset);
      $synset = $1;
      $entry = [];
      push(@$wn,$entry);
      next;
    }
    if (/^\s+1\s+PART_OF_SPEECH\s+"(.+?)"/) {
      push(@$entry,$1);
      $pos = $1;
      next;
    }
    if (/^\s+2\s+LITERAL\s+"(.+?)"/) {
      my $w = wn_attach_pos($1,$pos);
      push(@$entry,$w);
      next;
    }
    
    ## add hyperonyms in synset
    if (##/^\s+2\s+RELATION\s+"(has_hyperonym|antonym|has_meronym|is_subevent_of)"/
	0 &&
	/^\s+2\s+RELATION/
       ) {
      my $pos2;
      while(<W>) {
	$pos2=$1, next if (/^\s+4\s+PART_OF_SPEECH\s+"(.+?)"/);
	if (/^\s+4\s+LITERAL\s+"(.+?)"/) {
	  my $w = wn_attach_pos($1,$pos2);
	  push(@$entry,$w);
	  last;
	}
      }
    }
    
    ## reading my own format
    if (/^begin\s+synset\s+(\S+?)\s+pos=(\w)/) {
      $synset = $1;
      $entry = [$2];
      $pos = $2;
      push(@$wn,$entry);
      while (<W>) {
	last if /^end\s+synset/;
	chomp;
	s/\s+$//og;
	my $w = wn_attach_pos($_,$pos);
	push(@$entry,$w);
      }
      next;
    }

    ## reading dot
    if (/"(.+?)"\s+->\s+"(.+?)"\s+.*;/) {
      push(@$wn,['toto',$1,$2]);
      next;
    }

  }
  close(W);

  foreach my $synset (@$wn) {
    my @entry = @$synset;
    shift @entry;
    while (@entry) {
      my $w1 = shift @entry;
      foreach my $w2 (@entry) {
	next if ($w1 eq $w2);
	my $s = 1;
	if (exists $w2ctx->{$w1} && exists $w2ctx->{$w2}) {
	  verbose("adding seed similarity $s between $w1 and $w2");
	  (($w2ctx->{$w1}{similar}{$w2} += $s) > 1) and $w2ctx->{$w1}{similar}{$w2} =1;
	  (($w2ctx->{$w2}{similar}{$w1} += $s) > 1) and $w2ctx->{$w2}{similar}{$w1} =1;
	}
      }
    }
  }
}

sub term_extraction {
  verbose("starting term extraction");
  my $terms = {};
  foreach my $w1 (keys %$w2ctx) {
    my $w1i = $w2ctx->{$w1};
    my $n1 = $w1i->{n};
    next unless ($n1 > 0);	# should not arise but !
    my $m1 = ($nctxlog-log($n1));
    my $ctx1 = $w1i->{nctx};
    my $v1 = $w1i->{ctx};
    my ($xw1,$pos1) = ($w1 =~ /^(.+)_(\w+?)$/);
    next unless ($pos1 eq 'nc');
    next if ($xw1 =~ /^_/);
    my @w = 
      map {$v1->{$_}}
	sort {$v1->{$b} <=> $v1->{$a}} 
	  keys %$v1;
    my $median = $w[ceil(scalar(@w)/2)];
    foreach my $c ( sort {$v1->{$b} <=> $v1->{$a}} 
		    keys %$v1) {
      next unless  ($c =~ /^\*_/);
      next if ($c =~ /^(.+)_(et|ou|et-ou|,|car|mais|plus|moins)_(.+)/o);
      next if ($c =~ /_(sujet|cod|attribut|complement)_/o);
      my $struct = $ctx2targets->{$c};
      next unless ($struct);
      my ($w2,$relation) = @$struct;
      my ($xw2,$pos2) = ($w2 =~ /^(.+)_(\w+?)$/);
##      next unless ($pos2 eq 'nc' || $pos eq 'adj');
      next unless ($pos2 eq 'nc');
      next if ($xw2 =~ /^_/);
      my $w2i = $w2ctx->{$w2};
      my $n2 = $w2i->{n};
      my $f12 = $v1->{$c};
      last unless ($f12 > 10 * $median);
      my $bonus = 1;
      $bonus *= 1.2 if ($relation =~ /=$/);
      $bonus *= 1.2 if ($relation =~ /^de(=)?/);
      my $key = "$w1 $c";
      $terms->{$key} = { freq => $bonus,
			 tfidf => $bonus,
			 roche => $bonus
		       };
      if (1) {
	## simple frequency
	my $r = $f12 / $all;
	$terms->{$key}{freq} *= $r;
      }
      if (1) {
	## a kind of tf-idf
	my $m2 = ($nctxlog-log($n2));
	my $tfidf = $f12 * $m1  * $m2;
	$terms->{$key}{tfidf} *= $tfidf;
      }
      ## the following measure mentioned in a paper from Roche
      ## reusing from Daille
      if (1) {
	my $rv = auxf($f12)
	  + auxf($n1-$f12)
	    + auxf($n2-$f12)
	      + auxf($all-$n1-$n2)
		- auxf($n1)
		  - auxf($n2)
		    - auxf($all-$n2-$f12)
		      - auxf($all-$n1-$f12)
		      + auxf($all)
			;
	$terms->{$key}{roche} *= $rv;
      }
    }
  }
  verbose("dumping to $base.terms.bz2");
  open(D,"| bzip2 -c > $base.terms.bz2") || die "can't dump: $!";
  foreach my $term (sort { $terms->{$b}{freq} <=> $terms->{$a}{freq} 
			     || $terms->{$b}{tfidf} <=> $terms->{$a}{tfidf}
			       || $terms->{$b}{roche} <=> $terms->{$a}{roche}
			     } 
		    keys %$terms) {
    my $v = $terms->{$term};
    print D "$term\t$v->{freq}\t$v->{tfidf}\t$v->{roche}\n";
  }
  close(D);
}

sub auxf {
  my $n = shift;
  return ($n+0.001) * log($n+0.001);
}

package Data::Serializer::YAML::XS;
BEGIN { @Data::Serializer::YAML::XS::ISA = qw(Data::Serializer) }

use warnings;
use strict;
use YAML::XS;
use vars qw($VERSION @ISA);

$VERSION = '0.02';

sub serialize {
    return YAML::XS::Dump($_[1]);
}

sub deserialize {
    return YAML::XS::Load($_[1]);
}

1;

=head1 NAME

dep2cluster.pl -- from synctactic dependencies to semantic clusters

=head1 SYNOPSIS

   dep2clusters.pl -o words --inflate=2 --round=10 --min=20 data.rsel

=head1 DESCRIPTION

B<dep2cluster.pl> is a Perl script that takes syntactic dependencies
as input and dumps information about semantic word similarities.

=head1 OPTIONS

=over 4

=item --output|o=F<base>   basename for generated output files

=item --verbose|v          verbose mode [default=no]

=item --verbose|v          verbose mode [default=no]

=item --min=<number>       

minimum frequency for words and contexts [default=10]

=item --ratio=<number>     

minimum weight ratio for surviving contexts [default=0.01]

=item --cache              

dump loaded dependencies in F<base.cache> for faster reload [default=on]

=item --round=<number>     

maximal number of iterations [default=10]

=item --ctx_keep=<number>     

maximal number of kept contexts per words [default=50]

=back

=head1 INPUT FORMAT

The input files follow a very simple format with each line specifying
a dependency triple and a count

    <source> <relation> <target> <count>

with each field separated by tabulations

The obsolete following format is also accepted

    <source>_<relation>_<target> <count>

A triple may be repeated, its resulting count being the sum of all its
counts.

=head1 OUTPUT FILES

At the end of each iteration B<I>, a dump file F<base.dump.I.bz2> is
emmitted.

=cut

1;
