#!/usr/bin/env perl

## a kind of poor MapReduce algorithm, 
##     dispatching tasks on several clients
##     aggregating the results on the master
##     displaying the results

## Synopsis:
##   ./task_dispatcher.pl 
##            --map <map_name> 
##            --reduce <reduce_name> 
##            --display <display_name> 
##             -v 
##            --results <path_to_results> 
##            > toto.results

## for instance, getting all dependency triples for AFP/afp02022010 (last run)
##   ./task_dispatcher.pl 
##            --map rsel_collect
##            --reduce rsel_reduce
##            --display rsel_display
##             -v 
##            --results /pinot/data/Corpus/AFP/results.afp02022010/
##            > afp02022010.rsel

## alternate form: getting all dependency triples for the whole AFP
##   ./task_dispatcher.pl 
##            --map rsel_collect
##            --reduce rsel_reduce
##            --display rsel_display
##             -v 
##            /pinot/data/Corpus/AFP/results.*
##            > AFP.rsel

## NEW: one may now use more compact options with --task
##   ./task_dispatcher.pl 
##            --task rsel
##             -v 
##            /pinot/data/Corpus/AFP/results.*
##            > AFP.rsel

## NEW: one may now define the map, reduce, and display function in an external file
##   ./task_dispatcher.pl --script mytask.pl -v /pinot/data/Corpus/AFP/results.*
## the script file should define the following functions:
## sub td_collect {}
## sub td_reduce {}
## sub td_display {}

use strict;

use AppConfig;
use EV;
use AnyEvent;
use POSIX qw(strftime setsid floor);
use YAML::Any qw{LoadFile};

my $hostname = `hostname`;
chomp $hostname;

my %hosts = (
	     'mp-41080.local' => generate_localhost(2),
	     'mp-41080.rocq.inria.fr' => generate_localhost(2),
	     'mp-41186.local' => generate_localhost(2),
	     'mp-41186.rocq.inria.fr' => generate_localhost(2),
	     'mp-41213.local' => generate_localhost(2),
	     'mp-41294.local' => generate_localhost(2),
	     'mp-41294.rocq.inria.fr' => generate_localhost(2),
	     'mp-41215.rocq.inria.fr' => generate_localhost(2),
#	     'mblogerl20.mbloger.comp.univ-paris-diderot.fr' => generate_localhost(2),
	     'pinot.inria.fr' => generate_localhost(4),
	     'pinot' => generate_localhost(4),
	     'pinot.paris.inria.fr' => generate_localhost(4),
	     'mikedelta.inria.fr' => generate_localhost(13),
	     'mikedelta' => generate_localhost(13),
	     'rioa' => generate_rioa(),
	     'rioc' => generate_rioc(),
	     'cognac' => generate_localhost(13),
	     'cognac.inria.fr' => generate_localhost(13),
	     'cognac.paris.inria.fr' => generate_localhost(13),
	     'pomerol' => generate_localhost(4),
	     'pomerol.inria.fr' => generate_localhost(4),
	     'pomerol.paris.inria.fr' => generate_localhost(4),
	     'serpentes' => generate_localhost(13),
	     'serpentes.inria.fr' => generate_localhost(13),
	     'serpentes.paris.inria.fr' => generate_localhost(13),
	     'bearn' => generate_localhost(10),
	     'bearn.inria.fr' => generate_localhost(10),
	     'bearn.paris.inria.fr' => generate_localhost(10),
	     'guest-rocq-135162.inria.fr' => generate_localhost(2),
	    );

my $oar = undef;
my $ssh = 'ssh';
if (defined $ENV{OAR_NODE_FILE} || -f $ENV{OAR_NODE_FILE}) {
  $oar = $ENV{OAR_NODE_FILE};
  $ssh = 'oarsh';
}

my $config = AppConfig->new(
			    'master=s' => {DEFAULT => '127.0.0.1'},
			    'port=i' => {DEFAULT => 10100},
			    'verbose|v!' => {DEFAULT => 0},
			    'corpus=f@',
			    'results=f' => {DEFAULT => './'},
			    'client=s',
			    'map=s',
			    'reduce=s',
			    'display=s',
			    'clients=s@' => {DEFAULT => $oar ? generate_cluster($oar) : $hosts{$hostname} },
			    'task=s',
			    'script=f',
			    'simple!' => {DEFAULT => 0},
			    'schema=s',
			    'task_data=f', # should be a YAML file
			   );

$config->args;

if (my $script=$config->script) {
  unless (my $return = do $script) {
    die "couldn't parse $script: $@" if $@;
    die "couldn't do $script: $!"    unless defined $return;
    die "couldn't run $script"       unless $return;
  }
  print STDERR "here\n";
  ## the script file should define at least the following three functions
  ##  td_map
  ##  td_reduce
  ##  td_display
  $config->set(map => \&td_collect);
  $config->set(reduce => \&td_reduce);
  $config->set(display => \&td_display);
}

if (my $task=$config->task) {
  $config->map or $config->set(map => "${task}_collect");
  $config->reduce or $config->set(reduce => "${task}_reduce");
  $config->display or $config->set(display => "${task}_display");
}

my %tasks = (
	     time_collect => \&time_collect,
	     time_reduce => \&time_reduce,
	     time_stats => \&time_stats,

	     form_collect => \&form_collect,
	     form_reduce => \&form_reduce,
	     form_display => \&form_display,

	     lemma_collect => \&lemma_collect,
	     lemma_reduce => \&lemma_reduce,
	     lemma_display => \&lemma_display,

	     distrib_collect => \&distrib_collect,
	     distrib_reduce => \&distrib_reduce,
	     distrib_display => \&distrib_display,

	     rsel_collect => \&rsel_collect,
	     rsel_reduce => \&rsel_reduce,
	     rsel_display => \&rsel_display,

	     index_collect => \&index_collect,
	     index_reduce => \&index_reduce,
	     index_display => \&index_display,

	     path_collect => \&path_collect,
	     path_reduce => \&path_reduce,
	     path_display => \&path_display,

	     term_collect => \&term_collect,
	     term_reduce => \&term_reduce,
	     term_display => \&term_display,

	     frame_collect => \&frame_collect,
	     frame_reduce => \&frame_reduce,
	     frame_display => \&frame_display,

	     rule_collect => \&rule_collect,
	     rule_reduce => \&rule_reduce,
	     rule_display => \&rule_display,

	     danlos_collect => \&danlos_collect,
	     danlos_reduce => \&danlos_reduce,
	     danlos_display => \&danlos_display,

	     verbs_collect => \&verbs_collect,
	     verbs_reduce => \&verbs_reduce,
	     verbs_display => \&verbs_display,

	     audience_collect => \&audience_collect,
	     audience_reduce => \&audience_reduce,
	     audience_display => \&audience_display,

	     ngram_collect => \&ngram_collect,
	     ngram_reduce => \&ngram_reduce,
	     ngram_display => \&ngram_display

	    );

sub get_fun {
  my $task = shift;
  return $tasks{$task} || eval($task) || $task;
}


sub verbose {
  if ($config->verbose) {
    my $date = strftime "[%F %H:%M:%S]", localtime;
    print SDTERR "$date @_\n";
  }
}

my $map = $config->get('map');
my $mapfun = get_fun($map);

my $task_data = {};

if (my $data = $config->task_data) {
  -f $data or die "missing data file $data";
  $task_data = LoadFile($data);
}

if ($config->simple) {
  my $data = {};
  my $server = {};
  get_fun($config->map)->($data,$config);
  get_fun($config->reduce)->($data,$server);
  get_fun($config->display)->($server->{data},'simple');
  exit;
} elsif ($config->client) {
  # start as client
  Corpus::Task->new($config->client,$config,$mapfun,$task_data);
  exit;
} else {
  # start as dispatcher
  my $master = $config->master;
  my $port = $config->port;
  my $results = $config->results;
  my $map = $config->get('map');

  my @results = ();
  if (@ARGV) {
    @results = @ARGV;
  } else {
    @results = ($results);
  }

##  print STDERR "resdir @results\n";
  
  my $cinfo = {};

  unless (@{$config->corpus}) {
    foreach my $results (@results) {
      if (-f "$results/info.txt") {
	open(INFO,"<","$results/info.txt") 
	  || die "can't open info file '$results/info.txt': $!";
	while(<INFO>) {
	  chomp;
	  /\#\#/ and next;
	  if (my ($corpus) = /^(\S+)/) {
	    if (-f "$results/$corpus.log.bz2" && -f "$results/$corpus.tar.gz") {
	      $config->corpus($corpus);
	      $cinfo->{$corpus} = $results;
	    }
	  }
	}
	close(INFO);
      } else {
	foreach my $file (glob("$results/*_*.log.bz2")) {
	  my ($corpus) = ($file =~ /(\w+_\w+)\.log\.bz2$/);
	  next unless (-f "$results/$corpus.tar.gz");
	  $config->corpus($corpus);
	  $cinfo->{$corpus} = $results;
	}
      }
    }
  }

  my @corpus = @{$config->corpus};
  verbose("All corpus: @corpus");

##  print STDERR "Corpus @corpus\n";
  
  my $server = Corpus::Dispatcher->new($config,get_fun($config->reduce),$cinfo);
  
  foreach my $client (@{$config->clients}) {
    my $label = $client;
    if ($client =~ /(\w+)\@(\w+)/) {
      $label = $1;
      $client = $2;
    }
    my $task_data= $config->task_data ? " -task_data ".$config->task_data : "";
##    my $cmd = "$0 -client '$label' -master '$master' -port '$port' -map '$map' -results '$results'"; 
    my @xres = join(' ',map {"'$_'"} @results);
    my @options = ();
    if ($config->schema) {
      push(@options,'--schema='.$config->schema);
    }
    my $options = join(' ',@options);
    my $cmd = "$0 -client '$label' -master '$master' -port '$port' -map '$map' $options $task_data @xres"; 
    if (my $script = $config->script) {
      $cmd = "$0 -client '$label' -master '$master' -port '$port' --script='$script' $options $task_data @xres"; 
    }
    ($client eq 'localhost') or $cmd = "$ssh $client $cmd";
    if (fork == 0) {
      verbose("Run '$cmd'");
      exec($cmd) || die "can't run on $client: $!";
    }
  }

  ## wait until everything is done
  $server->{done}->recv;

  my $displayfun = get_fun($config->get('display'));
  $displayfun->($server->{data});
  
}

sub time_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $logs = Corpus::Logs->new($where,$corpus);
  while (my $log=$logs->next) {
    $data->{n}++;
    foreach my $key (qw/time timepassage timedepxml timeproc timeamb time1 timefread/) {
      $data->{$key} += $log->{$key};
    }
  }
##  print "$data->{host}: $data->{n} records\n";
}

sub time_reduce {
  my ($data,$server) = @_;
  $server->{data}{n} += $data->{n};
  foreach my $key (qw/time timepassage timedepxml timeproc timeamb time1 timefread/) {
    $server->{data}{$key} += $data->{$key};
  }
}

sub time_stats {
  my $data = shift;
  my $n=$data->{n};
  foreach my $key (qw/time timepassage timedepxml timeamb time1 timefread timeproc /) {
    my $time = $data->{$key};
    if ($time) {
      printf("%15s:  n=%d total=%d avg=%.2f\n",$key,$n,$time,$time/($n || 1));
    }
  }
}

sub form_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $logs = Corpus::Logs->new($where,$corpus);
  while (my $log=$logs->next) {
    foreach my $w (split(/\s+/,$log->{content})){
      $data->{words}{$w}++;
    }
  }
}

sub form_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{words}}) {
    $server->{data}{words}{$w} += $data->{words}{$w};
  }
}

sub form_display {
  my $data = shift;
  foreach my $w (sort {$data->{words}{$b} <=> $data->{words}{$a}} keys %{$data->{words}}) {
    print "$w\t$data->{words}{$w}\n";
  }
}

sub ngram_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $logs = Corpus::Logs->new($where,$corpus);
  while (my $log=$logs->next) {
    my $prev='$start';
    foreach my $w (split(/\s+/,$log->{content})){
      $data->{ngrams}{"$prev $w"}++;
      $prev = $w;
    }
    $data->{ngrams}{"$prev \$end"}++;
  }
}

sub ngram_reduce {
  my ($data,$server) = @_;
  foreach my $ngram (keys %{$data->{ngrams}}) {
    $server->{data}{ngrams}{$ngram} += $data->{ngrams}{$ngram};
  }
}

sub ngram_display {
  my $data = shift;
  foreach my $ngram (sort {$data->{ngrams}{$b} <=> $data->{ngrams}{$a}} 
		     grep {$data->{ngrams}{$_} > 2} 
		     keys %{$data->{ngrams}}) {
    print "$ngram\t$data->{ngrams}{$ngram}\n";
  }
}

sub lemma_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o=$archive->next) {
    next unless ($o->name =~ /\.passage\.xml/);
    Corpus::Passage::lemma($o,$data);
  }
}

sub lemma_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{lemma}}) {
    $server->{data}{lemma}{$w} += $data->{lemma}{$w};
  }
}

sub lemma_display {
  my $data = shift;
  foreach my $w (sort {$data->{lemma}{$b} <=> $data->{lemma}{$a}} keys %{$data->{lemma}}) {
    print "$w\t$data->{lemma}{$w}\n";
  }
}


sub distrib_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o=$archive->next) {
    next unless ($o->name =~ /\.passage\.xml/);
    Corpus::Passage::distrib($o,$data);
  }
}

sub distrib_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{lemma}}) {
    $server->{data}{lemma}{$w} += $data->{lemma}{$w};
  }
}

sub distrib_display {
  my $data = shift;
  foreach my $w (sort {$data->{lemma}{$b} <=> $data->{lemma}{$a}} keys %{$data->{lemma}}) {
    print "$w\t$data->{lemma}{$w}\n";
  }
}


sub rsel_collect {
  use Text::Scan;
  my ($data,$config,$task_data) = @_;
  if (exists $task_data->{scanner} && !ref($task_data->{scanner})) {
    my $file = $task_data->{scanner};
    my $scan = $task_data->{scanner} = new Text::Scan;
    $scan->restore($file);
  }
  my $schema = 'passage';
  (defined $config->schema) and $schema = $config->schema;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  if ($config->simple) {
    if ($schema eq 'depxml') {
      Corpus::DepXML::rsel(\*STDIN,$data,$task_data,'dummy','1');
    } else {
      Corpus::Passage::rsel(\*STDIN,$data,$task_data,'dummy','1');
    }
  } elsif ($schema eq 'depxml') {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      next unless ($name =~ /\.dep\.xml/);
      my $cref = $file->get_content_by_ref;
      my $io = IO::String->new($$cref);
      my ($seg,$sid) = ($name =~ m{([\w.-]+)[:.]E(\d+)});
##      print "Processing $name\n";
      Corpus::DepXML::rsel($io,$data,$task_data,$seg,$sid);
    }
  } else {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      next unless ($name =~ /\.passage\.xml/);
      my $cref = $file->get_content_by_ref;
      my $io = IO::String->new($$cref);
      my ($seg,$sid) = ($name =~ m{([\w.-]+)[:.]E(\d+)});
      Corpus::Passage::rsel($io,$data,$task_data,$seg,$sid);
    }
  }
}

sub rsel_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{rsel}}) {
    my $u = $server->{data}{rsel}{$w} ||= [0,0];
    my $v = $data->{rsel}{$w};
    my $n = $u->[0] += $v->[0];
    $u->[1] += $v->[1];
    my $nu = scalar(@$u);
    my $nv = scalar(@$v);
    my $xn = int(sqrt($n));
    for(my $i=2; ($i < $nv); $nu++, $i++) {
      if ($nu > 8) {
	next if (int(rand($xn)));
	my $pos = 2+int(rand(6)); # 11+2=13
	$u->[$pos] = $v->[$i];
      } else {
	push(@$u,$v->[$i]);
      }
    }
  }
}

sub rsel_display {
  my $data = shift;
  foreach my $w (sort {$data->{rsel}{$b}[0] <=> $data->{rsel}{$a}[0]} keys %{$data->{rsel}}) {
    my @u = @{$data->{rsel}{$w}};
    my @l = map {"$_->[0]:E$_->[1]"} @u[2 .. $#u];
    print "$w\t$u[0]\t$u[1]\t@l\n";
  }
}

sub index_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o=$archive->next) {
    next unless ($o->name =~ /\.passage\.xml/);
    Corpus::Passage::index($o,$data);
  }
}

sub index_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{index}}) {
    foreach my $seg (keys %{$data->{index}{$w}}) {
      push(@{$server->{data}{index}{$w}{$seg}},@{$data->{index}{$w}{$seg}});
    }
  }
}

sub index_display {
  my $data = shift;
  foreach my $w (sort keys %{$data->{index}}) {
    my @seg;
    foreach my $seg (keys %{$data->{index}{$w}}) {
      push(@seg,
	   join(':',$seg,@{$data->{index}{$w}{$seg}}));
    }
    print "$w\t@seg\n";
  }
}

sub path_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o=$archive->next) {
    next unless ($o->name =~ /\.dep\.xml/);
    Corpus::DepXML::path($o,$data);
##    path_display($data);
  }
}

sub path_reduce {
  my ($data,$server) = @_;
  foreach my $path (keys %{$data->{path}}) {
    $server->{data}{path}{$path} += $data->{path}{$path};
  }
}

sub path_display {
  my $data = shift;
  foreach my $path (sort {$data->{path}{$b} <=> $data->{path}{$a}} keys %{$data->{path}}) {
    print "$path\t$data->{path}{$path}\n";
  }
}


sub term_collect {
  my ($data,$config) = @_;
  if ($config->simple) {
    Corpus::Passage::terms(\*STDIN,$data,'dummy','E1');
  } else {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      next unless ($name =~ /\.passage\.xml/);
      my $cref = $file->get_content_by_ref;
      my $io = IO::String->new($$cref);
      my ($seg,$sid) = ($name =~ m{([\w.-]+)[:.]E(\d+)});
      Corpus::Passage::terms($io,$data,$seg,$sid);
    }
  }
}

sub term_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{terms}}) {
    my $delta = $data->{terms}{$w};
    # a potential term should appear at least twice in any segment to be counted
    if (1 || $delta->[0] > 1) {
      my $u = $server->{data}{terms}{$w} ||= [0,0,0,0];
      my $n = $u->[0] += $delta->[0];	# number of occurrences
      $u->[1] += $delta->[1];	# is np ?
      $u->[2] += $delta->[2];	# is a governor ?
      $u->[3] += $delta->[3];	# has a role ?
      my $nu = scalar(@$u);
      my $ndelta = scalar(@$delta);
      my $xn = int(sqrt($n));
      for(my $i=4; ($i < $ndelta); $nu++,$i++) {
	if ($nu > 20) {
	  next if (int(rand($xn)));
	  my $pos = 4+int(rand(16));
	  $u->[$pos] = $delta->[$i];
	} else {
	  push(@$u,$delta->[$i]);
	}
      }
    }
  }
}

sub term_display {
  my $data = shift;
  my $is_simple = shift || '';
  foreach my $w (sort { $data->{terms}{$b}[0] <=> $data->{terms}{$a}[0]
			  || $data->{terms}{$b}[1] <=> $data->{terms}{$a}[1]
			    || $data->{terms}{$b}[2] <=> $data->{terms}{$a}[2]
			      || $data->{terms}{$b}[3] <=> $data->{terms}{$a}[3]
			  } keys %{$data->{terms}}) {
    my @u = @{$data->{terms}{$w}};
    if ( $is_simple 
	||
	 ( $u[0] > 10 # we keep if #occ > 10
	   && $u[1]  # and has np instances
	   && $u[3] # and play some role for some occs
	 )
       ) {
      my @l = map {"$_->[0]:E$_->[1]"} @u[4 .. $#u];
      print "$w\t$u[0]\t$u[1]\t$u[2]\t$u[3]\t@l\n";
    }
  }
}

sub frame_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o=$archive->next) {
    next unless ($o->name =~ /\.dep\.xml/);
    Corpus::DepXML::frame($o,$data);
  }
}

sub frame_reduce {
  my ($data,$server) = @_;
  foreach my $frame (keys %{$data->{frame}}) {
    my $dentry = $data->{frame}{$frame};
    my $entry = $server->{data}{frame}{$frame} ||= [0];
    $entry->[0] += $dentry->[0];
    my @l = @$dentry;
    shift @l;
    my $i = scalar(@$entry);
    while (@l && ($i < 11)) {
      my $x = shift @l;
      push(@$entry,$x);
      $i++;
    }
  }
}

sub frame_display {
  my $data = shift;
  foreach my $frame (sort { $data->{frame}{$b}[0] <=> $data->{frame}{$a}[0] } 
		     keys %{$data->{frame}}) {
    my $entry = $data->{frame}{$frame};
    my $u = $entry->[0];
    my @l = @$entry;
    shift @l;
    @l = map {"$_->[0]:E$_->[1]"} @l;
    print "$frame\t$u\t@l\n";
  }
}

sub rule_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  ##  print "$data->{host}: Collecting from where=$where corpus=$corpus\n";
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o=$archive->next) {
    next unless ($o->name =~ /\.passage\.xml/);
    Corpus::Passage::rules($o,$data);
  }
}

sub rule_reduce {
  my ($data,$server) = @_;
  foreach my $r (keys %{$data->{rules}}) {
    my $delta = $data->{rules}{$r};
    if ($delta->[0] > 1) {
      my $u = $server->{data}{rules}{$r} ||= [0,0,0,0];
      $u->[0] += $delta->[0];
      $u->[1] += $delta->[1];
      $u->[2] += $delta->[2];
      $u->[3] += $delta->[3];
      my $nu = scalar(@$u);
      my $ndelta = scalar(@$delta);
      for(my $i=4; ($i < $ndelta) && ($nu < 9); $nu++) {
	push(@$u,$delta->[$i++]);
      }
    }
  }
}

sub rule_display {
  my $data = shift;
  foreach my $rule (sort { $data->{rules}{$b}[0] <=> $data->{rules}{$a}[0]
			     || $data->{rules}{$b}[2] <=> $data->{rules}{$a}[2] }
		    keys %{$data->{rules}}
		   ) {
    my @u = @{$data->{rules}{$rule}};
    my @l = map {"$_->[0]:E$_->[1]"} @u[4 .. $#u];
    my $wyes = int($u[1] / $u[0]);
    my $wno = int($u[3] / $u[2]);
    print "$rule\t$u[0]\t$wyes\t$u[2]\t$wno\t@l\n";
  }
}


sub danlos_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o = $archive->next) {
    next unless ($o->name =~ /\.dep\.xml/);
    Corpus::DepXML::danlos($o,$data);
  }
}

sub danlos_reduce {
  my ($data,$server) = @_;
  foreach my $pattern (keys %{$data->{patterns}}) {
    my $dentry = $data->{patterns}{$pattern};
    my $entry = $server->{data}{patterns}{$pattern} ||= [0];
    $entry->[0] += $dentry->[0];
    my @l = @$dentry;
    shift @l;
    my $n = @l;
#    ($n > 100) and $n = 100;
    ($n > 20) and $n = 20;
    push(@$entry,@l[0..$n]);
  }
}

sub danlos_display {
  my $data = shift;
  foreach my $pattern ( map {$_->[1]}
			sort {$b->[0] <=> $a->[0]}
			map  {[ $data->{patterns}{$_}[0],$_ ]}
			keys %{$data->{patterns}} 
		      ) {
    my $entry = $data->{patterns}{$pattern};
    my $u = $entry->[0];
    my @l = @$entry;
    shift @l;
    @l = map {"$_->[0]:E$_->[1]"} @l;
    print "$pattern\t$u\t@l\n";
  }
}


sub verbs_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o = $archive->next) {
    next unless ($o->name =~ /\.passage\.xml/);
    Corpus::Passage::verbs($o,$data);
  }
}

sub verbs_reduce {
  my ($data,$server) = @_;
  foreach my $verb (keys %{$data->{verbs}}) {
    my $dentry = $data->{verbs}{$verb};
    my $entry = $server->{data}{verbs}{$verb} ||= [0];
    $entry->[0] += $dentry->[0];
    my @l = @$dentry;
    shift @l;
    push(@$entry,@l);
  }
}

sub verbs_display {
  my $data = shift;
  foreach my $verb ( map {$_->[1]}
			sort {$b->[0] <=> $a->[0]}
			map  {[ $data->{verbs}{$_}[0],$_ ]}
			keys %{$data->{verbs}} 
		      ) {
    my $entry = $data->{verbs}{$verb};
    my $u = $entry->[0];
    my @l = @$entry;
    shift @l;
    @l = map {"$_->[0]:E$_->[1]"} @l;
    print "$verb\t$u\t@l\n";
  }
}

sub audience_collect {
  my ($data,$config) = @_;
  my $corpus = $data->{corpus};
  my $where = $data->{where} || $config->results;
  my $archive = Corpus::Archive->new($where,$corpus);
  while (my $o = $archive->next) {
    next unless ($o->name =~ /\.dep\.xml/);
    Corpus::DepXML::audience($o,$data);
  }
}

sub audience_reduce {
  my ($data,$server) = @_;
  foreach my $pattern (keys %{$data->{audience}}) {
    my $dentry = $data->{audience}{$pattern};
    my $entry = $server->{data}{audience}{$pattern} ||= [0];
    $entry->[0] += $dentry->[0];
    my @l = @$dentry;
    shift @l;
    my $n = @l;
#    ($n > 100) and $n = 100;
    ($n > 20) and $n = 20;
    push(@$entry,@l[0..$n]);
  }
}

sub audience_display {
  my $data = shift;
  foreach my $pattern ( map {$_->[1]}
			sort {$b->[0] <=> $a->[0]}
			map  {[ $data->{audience}{$_}[0],$_ ]}
			keys %{$data->{audience}} 
		      ) {
    my $entry = $data->{audience}{$pattern};
    my $u = $entry->[0];
    my @l = @$entry;
    shift @l;
    @l = map {"$_->[0]:E$_->[1]"} @l;
    print "$pattern\t$u\t@l\n";
  }
}

sub generate_localhost {
  my $n = shift;
  my @h = ();
  foreach my $l (1..$n) {
    push(@h,"l${l}\@localhost");
  }
  return [@h];
}

sub generate_rioa {
  my @h = ();
  foreach my $l ("001".."012") {
    push(@h,"node${l}_1\@node${l}","node${l}_2\@node${l}");
  }
  return [@h];
}

sub generate_rioc {
  my @h = ();
  foreach my $l ("001".."016","031".."042") {
    push( @h,
	  map {"node${l}_$_\@node${l}"} 1..6
	);
  }
  return [@h];
}

sub generate_cluster {
  my $nodefile = shift;
  my @h = ();
  my %nodes=();
  open(NODES,'<',$nodefile)
    || die "can't read nodefile '$nodefile':$!";
  while(<NODES>){
    chomp;
    my $node = $_;
    my $i = $nodes{$node}++;
    push(@h,"${node}_${i}\@${node}");
  }
  close(NODES);
  return \@h;
}
			   
package Corpus::Logs;

sub new {
  my ($class,$where,$corpus) = @_;
  $where ||= "./";
  my $base="$where/$corpus";
  my $file;
  foreach my $x ("${base}.log","${base}.log.bz2") {
    $file=$x, last if (-f $x);
  }
  return unless ($file);
  my $cmd = ($file =~ /\.bz2$/) ? "bunzip2 -c $file|" : "<$file";
  open(my $fh,"$cmd") || die "can't open $file: $!";
  return bless { file => $file,
		 corpus => $corpus,
		 where => $where,
		 handle => $fh,
	       }, $class;
}

sub next {
 my $self = shift;
 my $fh = $self->{handle};
 my $sentence = {};
 while (<$fh>) {
   if (/^(ok|robust|fail)\s+(\d+)\s+(.*)/) {
     $sentence->{status} = $1;
     $sentence->{sid} = $2;
     $sentence->{content} = $3;
     $sentence->{size} =  scalar(split(/\s+/,$3));
     next;
   }
   if (/<ambiguity>\s+(-?\d+(?:\.\d+)?)/) {
     $sentence->{amb}=$1;
     /(\d+(?:\.\d+)?)\s+avgderivs/ and $sentence->{ambderiv} = $1;
     /(\d+)\s+derivs/ and $sentence->{amball} = $1;
     next;
   }
   if (/^\s*\<(.+?)\>\s*(.*)/) {
     my $f = $1;
     $sentence->{$f} = $2;
     ##     print "f=$f v=$sentence->{$f}\n";
     return $sentence if ($f eq 'timeproc');
   }
 }
 return;
}

package Corpus::Archive;
use Archive::Tar;

sub new {
  my ($class,$where,$corpus) = @_;
  $where ||= "./";
  my $base="$where/$corpus";
  my $file;
  foreach my $x ("${base}.tar","${base}.tar.gz","${base}.tgz") {
    $file=$x, last if (-f $x);
  }
  if (!$file && -f "$base" && -r "$base") {
    $file = "/tmp/ta$$.tgz";
    my $tar = Archive::Tar->new;
    $tar->add_files($base);
    $tar->write($file,COMPRESS_GZIP);
  }
  return unless ($file);
  return bless { file => $file,
		 corpus => $corpus,
		 where => $where,
		 archive => Archive::Tar->iter($file,1)
	       }, $class;
}

sub next {
 my $self = shift;
 while (my ($file) = $self->{archive}->()) {
   return $file if ($file->is_file);
 }
}

package Corpus::DepXML;
use Forest::Dependency::Reader;
use Forest::Tagger::Writer;
use XML::Twig;
use DepXML;
use DepXML::Query::Compile;

sub emit_tags {
  my $file = shift;
  my $cref = $file->get_content_by_ref;
  my $twig = XML::Twig->new();
  $twig->parse($$cref)->print;
}

my $hypomarks = hypomark();

sub hypomark {
  return {map {$_ => 1} 
	  qw{
	      fondation exp�dition
	      fa�on genre mani�re sorte esp�ce type groupe ensemble
	  }};
}

sub rsel {
  my ($file,$data,$task_data,$seg,$sid) = @_;
#  my $cref = $file->get_content_by_ref;
  my $cas = DepXML->new();
#  my $name = $file->name;
  eval {
    $cas->parse($file);
  };
  if ($@) {
    ## warn "bad xml file $name: $@";
    $cas->reset;
    return;
    ##    die;
  }
  $seg ||= 'dummy';
  $sid ||= 1;

  my $propagate = {};
  
  my $scanner = $task_data->{scanner};
  my $rdata = $data->{rsel} ||= {};
  my $info = [$seg,$sid];
  
  my $incr = sub {
    my ($label,$w,$gid,$did) = @_;
    my $u = $rdata->{$label} ||= [0,0];
    $u->[0]++;
    $u->[1] += $w;
    push(@$u,$info) unless (@$u > 12);

    if (@$u < 13) {
      push(@$u,$info);
    } elsif (!int(rand(20))) {
      # we may decide to keep the sentence and remove another one !
      my $pos = 2+int(rand(12));
      $u->[$pos][0] = $seg; 
      $u->[$pos][1] = $sid; 
    }

    (defined $gid) and $propagate->{$gid}{$did} = [$label,$w];
  };


  foreach my $edge ($cas->find_edges( dpath source {!$_->is_empty} )) {
    my $label = $edge->label;
    my $type = $edge->type;
    my $source = $edge->source;
    my $slemma = $source->xlemma;
    my $scat = $source->cat;
    my $target = $edge->target;
    my @tmp=();
    if ($target->cat eq 'prep'
	&& ($label eq 'preparg' || $label eq 'N2')
       ) {
      my $olabel = $label;
      if (my $tmp = ($target->apply(dpath out is_subst target))[0]) {
	$label = $target->lemma;
	$target = $tmp;
      }
      if ($olabel eq 'N2') {
	my $tlemma = $target->xlemma;
	my $tcat = $target->cat;
	my @stack = $source->apply(dpath in is_subst is_N2 source is_prep in is_adj is_N2 source );
	while (@stack) {
	  my $source2 = shift @stack;
	  my $slemma2 = $source2->xlemma;
	  my $scat2 = $source2->cat;
	  $incr->("${slemma2}_${scat2}\t${label}*\t${tlemma}_${tcat}",0.5);
	  push(@stack,$source2->apply(dpath in is_subst is_N2 source is_prep in is_adj is_N2 source ));
	}
      }
    }
    if ($target && $target->cat eq 'prel' && $scat eq 'v') {
      @tmp = $edge->source->apply(dpath in is_subst is_SRel source is_N2 in is_adj source);
      @tmp and $target = $tmp[0];
    }
    if ($target && $target->is_empty) {
      $target = ($target->apply(dpath out is_subst target {!$_->is_empty}))[0]
	|| ($target->apply(dpath out is_lexical target {!$_->is_empty}))[0]
      }
    if ($target
	&& $target->cat eq 'prep'
	&& $type eq 'adj'
	&& ($label eq 'vmod' || $label eq 'S')) {
      $label = $target->lemma;
      $target = ($target->apply(dpath out is_subst target))[0];
    }
    ($target && !$target->is_empty) or next;
    if ($edge->source->is_passive) {
      if ($label eq 'subject') {
	$label = 'object';
      } elsif ($label eq 'par' && $type eq 'subst') {
	$label = 'subject';
      }
    }
    my $tlemma = $target->xlemma;
    my $tcat = $target->cat;
    $incr->("${slemma}_${scat}\t$label.$type\t${tlemma}_${tcat}",1,$source->id,$target->id);

    foreach my $target ($edge->apply(dpath target out is_adj target is_coo out is_subst is_coord3 target {!$_->is_empty})) {
      my $tlemma = $target->xlemma;
      my $tcat = $target->cat;
      $incr->("${slemma}_${scat}\t$label.$type\t${tlemma}_${tcat}",1,$source->id,$target->id);
    }

  }

  foreach my $id1 (keys %$propagate) {
    my @ids2 = keys %{$propagate->{$id1}};
    while (@ids2) {
      my $id2 = shift @ids2;
      my $info1 = $propagate->{$id1}{$id2};
      my ($label1,$w1) = @$info1;
      my ($g1,$l1,$d1) = split(/\t/,$label1);
      $l1 =~ /\.adj/ and next;
      $l1 eq "det.subst" and next;
      foreach my $id3 (@ids2) {
	my $info2 = $propagate->{$id1}{$id3};
	my ($label2,$w2) = @$info2;
	my ($g2,$l2,$d2) = split(/\t/,$label2);
	$l1 eq $l2 and next;
	$l2 =~ /\.adj/ and next;
	$l2 eq "det.subst" and next;
	my $w = ($w1 + $w2) / 4;
	my $label = ($l1 lt $l2) ? join("\t",$d1,"$l1+$g1+$l2",$d2) : join("\t",$d2,"$l2+$g1+$l1",$d1);
	$incr->($label,$w);
      }
      exists $propagate->{$id2} or next;
      $l1 =~ /^(?:subject|object|xcomp|comp|�|de)\.subst/o or next;
      foreach my $id3 (keys %{$propagate->{$id2}}) {
	my $info2 = $propagate->{$id2}{$id3};
	my ($label2,$w2) = @$info2;
	my ($g2,$l2,$d2) = split(/\t/,$label2);
	#	$l2 =~ /\.adj/ and next;
	$l2 eq 'de.adj' or next;
	# $l2 eq "det.subst" and next;
	my $w = ($w1 + $w2) / 4;
	my $label = join("\t",$g1,"$l1/$l2",$d2);
	$incr->($label,$w);
      }
    }
  }

  
}

sub DepXML::Node::xlemma {
  my $self = shift;
  exists $self->{xlemma} and return $self->{xlemma};
  my $lemma = $self->lemma;
  if ($self->cat eq 'np'
      && $lemma =~ /^_[A-Z]/
      && $self->cluster
      && exists $self->{cas}{clusters}{$self->cluster}
     ) {
    return $self->{xlemma} = $self->{cas}{clusters}{$self->cluster}->token.":$lemma";
  } else {
    return $lemma;
  }
}

sub path {
  my ($file,$data) = @_;
  my $cref = $file->get_content_by_ref;
  my $cas = DepXML->new();
  my $name = $file->name;
  $hypomarks ||= hypomark();
  print "processing $name\n";
  ##  return;
  eval {
    $cas->parse($$cref);
  };
  if ($@) {
    ## warn "bad xml file $name: $@";
    $cas->reset;
    return;
    ##    die;
  }

  if (0) {
    my @nodes = keys %{$cas->{nodes}};
    print "nodes: @nodes\n";
    return;
  }

  if (0) {
    foreach my $v ($cas->find_nodes( dpath is_v )) {
      my $v_lemma = $v->{xml}->att('lemma');
      foreach my $subj ($v->apply(dpath all_out is_subject target)) {
	my $s_lemma = $subj->xml->att('lemma');
	my $s_cat = $subj->xml->att('cat');
	$data->{path}{"${v_lemma}_v\t${s_lemma}_${s_cat}"}++;
	print "${v_lemma}_v\t${s_lemma}_${s_cat}\n";
      }
    }
  }

  if (0) {
    foreach my $edge ($cas->find_edges( dpath is_subst )) {
      my $slemma = $edge->source->lemma;
      my $scat = $edge->source->cat;
      my $tlemma = $edge->target->lemma;
      my $tcat = $edge->target->cat;
      my $label = $edge->label;
      my $type = $edge->type;
      $data->{path}{"${slemma}_${scat}\t${label}_${type}\t${tlemma}_${tcat}"}++;
    }
  }

  if (1) {
    
    return unless( grep {$cas->has_lemma($_)} keys %$hypomarks );
    
    ## $cas->activate();
  foreach my $hypo ($cas->find_nodes( 
				  dpath is_nc {exists $hypomarks->{$_->lemma}}
				 )) {
    my $hypolemma = $hypo->lemma;
    print "hypopred: $hypolemma\n";
    ## next unless (exists $hypomarks->{$vlemma});
    foreach my $t ($hypo->apply(dpath 
				all_out is_adj is_N2 
				target is_prep lemma_in_de
				all_out is_subst
				target {!$_->is_empty} )
		  ) {
      my $tlemma = $t->lemma;
      my $tcat = $t->cat;
      foreach my $n1 ($hypo->apply(dpath
				   in is_object
				   source is_v
				   in is_subject
				   source )) {
	my $n1lemma = $n1->lemma;
	my $n1cat = $n1->cat;
	my $u = "${n1lemma}_${n1cat}\t${hypolemma}_nc\t${tlemma}_${tcat}";
	$data->{path}{$u}++;
	print "$u\n";
      }
    }
  }
}
  undef $cas;
}


my $sat_prio;

sub sat_priority {
  my $x = shift;
  $sat_prio ||= {
		 ncpred => 1,
                 impsubj => 2,
                 subject => 3,
                 clr => 4,
                 comp => 5,
                 object => 6,
                 xcomp => 7,
                 '�_xcomp' => 8,
                 'de_xcomp' => 9,
                 '�_arg' => 10,
                 'de_arg' => 11,
                 preparg => 12
                };
  my $prio = $sat_prio->{$x};
  $prio and return $prio;
  $x =~ /_xcomp/ and return $sat_prio->{$x} = 13;
  $x =~ /_arg/ and return $sat_prio->{$x} = 14;
  $x =~ /_vmod/ and return $sat_prio->{$x} = 15;
  return $sat_prio->{$x} = 16;
}

sub frame {
  my ($file,$data) = @_;
  my $cref = $file->get_content_by_ref;
  my $cas = DepXML->new();
  my $name = $file->name;
  my ($seg,$sid) = ($name =~ m{([\w.-]+)[.:]E(\d+)});
  $seg ||= 'dummy';
  $sid ||= 1;
  ## print "processing $name\n";
  ##  return;
  eval {
    $cas->parse($$cref);
  };
  foreach my $pred ($cas->find_nodes(dpath is_v)) {
    my $lemma = $pred->lemma;
    my %sat=();
#    foreach my $e ($pred->apply(dpath all_out (true union is_Infl target all_out))) {
    foreach my $e ($pred->apply(dpath all_out (is_Infl target all_out)* )) {
      my $rel = $e->label;
      my $type = $e->type;
      my $target = $e->target;
      my $tcat = $target->cat;
      my $tlemma = $target->lemma;
      next if (grep {$tcat eq $_} qw{adv advneg clneg aux});
      next if (grep {$rel eq $_} qw{skip Infl S csu S2 incise strace V prep reltrace advneg causative_prep CleftQue v});
      next if ($type eq 'adj' && grep {$rel eq $_} qw{xcomp});
      if ($rel eq 'ncpred') {
	$lemma .= "_$tlemma";
	next;
      }
      if ($rel eq 'preparg') {
	if ($tcat eq 'prep') {
	  $target = ($target->apply(dpath all_out is_subst is_N2 target))[0];
	  if ($target) {
	    $rel = "${tlemma}_arg";
	    $tcat = $target->cat;
	    $tlemma = $target->lemma;
	  }
	} elsif ($tcat eq 'cld') {
	  $rel="�_arg";
	} elsif ($tcat eq 'clg') {
	  $rel="de_arg";
	}
      }
      if ($rel eq 'vmod' && $tcat eq 'VMod') {
	if ($target = ($target->apply(dpath all_out (is_subst union is_lexical) target))[0]) {
	  $tcat = $target->cat;
	  $tlemma = $target->lemma;
	  my $xlabel = ($target->in)[0]->label;
	  $rel = $xlabel if (grep {$xlabel eq $_} qw{time_mod audience});
	  if ($tcat eq 'prep') {
	    $target = ($target->apply(dpath all_out (is_subst union is_lexical) target))[0];
	    if ($target) {
	      $rel = "${tlemma}_vmod";
	      $tcat = $target->cat;
	      $tlemma = $target->lemma;
	    }
	  }
	}
      }
      if ($rel eq 'xcomp') {
	my $prep = ($pred->apply(dpath all_out is_prep target))[0];
	if ($prep) {
	  my $preplemma = $prep->lemma;
	  $rel = "${preplemma}_xcomp";
	}
      }
      if ($rel eq 'comp' && $tcat eq 'comp') {
	if ($target = ($target->apply(dpath all_out is_subst target))[0]){
	  $tcat = $target->cat;
	  $tlemma = $target->lemma;
	}
      }
      push(@{$sat{$rel}},"$rel:${tlemma}_${tcat}");
    }
    my $cat=$pred->is_passive ? "v_passive" : "v_active";
    unless ($sat{subject} || $sat{impsubj}) {
      my $subj = $pred->get_subject;
      if ($subj) {
	my $tcat = $subj->cat;
	my $tlemma = $subj->lemma;
	push(@{$sat{subject}},"subject:${tlemma}_${tcat}");
      } elsif (my $pred2 = ($pred->apply(dpath in is_xcomp source))[0]) {
	if ($subj = ($pred2->apply(dpath all_out 
				  (is_preparg union is_object union is_subject) 
				   target))[0]) {
	  my $tcat = $subj->cat;
	  my $tlemma = $subj->lemma;
	  push(@{$sat{subject}},"subject:${tlemma}_${tcat}");
	}
      } elsif (my $target = ($pred->apply(dpath in is_SubS source in is_adj source))[0]) {
	my $tcat = $target->cat;
	my $tlemma = $target->lemma; 
	if ($tcat eq 'v') {
	  if ($target = $target->get_subject) {
	    $tcat = $target->cat;
	    $tlemma = $target->lemma; 
	  }
	}
	push(@{$sat{subject}},"subject:${tlemma}_${tcat}");
      }
    }
    my @args=("${lemma}_${cat}");
    foreach my $rel (
                     map {$_->[0]}
                     sort {$a->[1] <=> $b->[1] || $a->[0] <=> $b->[0] }
                     map {[$_,sat_priority($_)]}
                     keys %sat
                    ) {
      push(@args,@{$sat{$rel}});
    }
    my $label=join(' ',@args);
    my $entry = $data->{frame}{$label} ||= [0];
    $entry->[0]++;
    push(@$entry,[$seg,$sid]) unless (@$entry > 10);
  }
  undef $cas;
}

sub danlos_old {
  my ($file,$data) = @_;
  my $cref = $file->get_content_by_ref;
  my $cas = DepXML->new();
  my $name = $file->name;
  my ($seg,$sid) = ($name =~ m{([\w.-]+)[.:]E(\d+)});
  $seg ||= 'dummy';
  $sid ||= 1;
  eval {
    $cas->parse($$cref);
  };
  
  my %citation_verbs = ( map {$_=>1} qw{dire croire} );
  my %imp_verbs = ( map {$_=>1} qw{sembler});
  my %conj_cat = ( map {$_=>1} qw{coo csu});
  
  ##    print "*** Processing $seg.E$sid\n";
  
  ## case 1: conj introduce a citation predicate with completive
  ## find verb nodes with lemma in dire or croire
  ## such that in out edges, one can find an xcomp edge, a csu edge and a subject edge
  ## and such that the in edge is a substtitution one 
  ##                      such that the cat of the source node is a coo or a csu !
  foreach my $pred ($cas->find_nodes( dpath is_v
				      .( all_out .x(is_xcomp) .x(is_csu) .x(is_subject))
				      .( in is_subst source cat_in_coo_csu .n(lemma_in_et) parent is_v)
				    )) {
    my $conj = ($pred->apply(dpath in source))[0];
    my $main = ($conj->apply(dpath in source is_v))[0];
    my $subj = ($pred->apply(dpath all_out is_subject target))[0];

    my $principal = $main->apply(dpath in) ? 'embbeded' : 'principal';
    my $pattern = join('_',$conj->lemma,$conj->cat,'pers',($subj->form =~ /^j[e\']$/io) ? 'je' : 'other',$principal,$main->lemma,$pred->lemma);
    print "** Register pattern $pattern $seg.E$sid\n";
    my $entry = $data->{patterns}{$pattern} ||=[0];
    $entry->[0]++;
    push(@$entry,[$seg,$sid]);
  }

  ## case2: conj introduce an impersonal with completive
  foreach my $pred ($cas->find_nodes(dpath cat_in_v_adj
				     .( all_out .x(is_impsubj) .x(is_subject target is_csu) )
				     .( in is_subst source cat_in_coo_csu .n(lemma_in_et) parent is_v)
				    )) {
    my $conj = ($pred->apply(dpath in source))[0];
    my $main = ($conj->apply(dpath in source is_v))[0];

    my $principal = $main->apply(dpath in) ? 'embbeded' : 'principal';
    my $pattern = join('_',$conj->lemma,$conj->cat,'impers',$principal,$main->lemma,$pred->lemma);
    print "** Register pattern $pattern $seg.E$sid\n";
    my $entry = $data->{patterns}{$pattern} ||=[0];
    $entry->[0]++;
    push(@$entry,[$seg,$sid]);
  }
  undef $cas;
}

sub danlos {
  my ($file,$data) = @_;
  my $cref = $file->get_content_by_ref;
  my $cas = DepXML->new();
  my $name = $file->name;
  my ($seg,$sid) = ($name =~ m{([\w.-]+)[.:]E(\d+)});
  $seg ||= 'dummy';
  $sid ||= 1;
  eval {
    $cas->parse($$cref);
  };
  
  
  ##    print "*** Processing $seg.E$sid\n";
  
  ## case 1: conj introduce a citation predicate with completive
  ## find verb nodes with lemma in dire or croire
  ## such that in out edges, one can find an xcomp edge, a csu edge and a subject edge
  ## and such that the in edge is a substtitution one 
  ##                      such that the cat of the source node is a coo or a csu !
  foreach my $pred ($cas->find_nodes( dpath is_v
				      .( in is_subst source cat_in_coo_csu .n(lemma_in_et) parent is_v )
				    )) {
    my $conj = ($pred->apply(dpath in source))[0];
    my $main = ($conj->apply(dpath in source is_v))[0];

    my @pat = ();
    foreach my $n ($main,$pred) {
      my @c = ($n->lemma);
      if ($n->apply(dpath .(all_out .x(is_impsubj) .x(is_subject target is_csu)))) {
	unshift(@c,'impers');
      } elsif (my $subj = ($n->apply(dpath all_out is_subject target))[0]) {
	unshift(@c,($subj->form =~ /^j[e\']$/io) ? 'je' : 'pers');
	$n->apply(dpath all_out .x(is_xcomp) .x(is_csu)) and push(@c,'completive');
      } else {
	unshift(@c,'nosubj');
	$n->apply(dpath all_out .x(is_xcomp) .x(is_csu)) and push(@c,'completive');
      }
      push(@pat,"{".join('_',@c)."}");
    }
    my $pattern = join('_',"c1=$pat[0]",$conj->lemma,$conj->cat,"c2=$pat[1]");
##    print "** Register pattern $pattern $seg.E$sid\n";
    my $entry = $data->{patterns}{$pattern} ||=[0];
    $entry->[0]++;
    push(@$entry,[$seg,$sid]);
  }
  undef $cas;
}


sub audience {
  my ($file,$data) = @_;
  my $cref = $file->get_content_by_ref;
  my $cas = DepXML->new();
  my $name = $file->name;
  my ($seg,$sid) = ($name =~ m{([\w.-]+)[.:]E(\d+)});
  $seg ||= 'dummy';
  $sid ||= 1;
  ## print "processing $name\n";
  ##  return;
  eval {
    $cas->parse($$cref);
  };
  foreach my $edge ($cas->find_edges(dpath is_audience)) {
    my $entry = $data->{audience}{$edge->target->lemma} ||=[0];
    $entry->[0]++;
    push(@$entry,[$seg,$sid]);
  }
}

package Corpus::Passage;
use XML::Twig;
use IO::String;

sub lemma {

  my ($file,$data) = @_;

  my $cref = $file->get_content_by_ref;
  my $io = IO::String->new($$cref);

  my $refl=0;
  my $tokens={};

  my $ldata = $data->{lemma} ||= {};

  while (my $line = <$io>) {


    if ($line =~ m{<T }) {
      my ($id) = $line =~ m{id="(.+?)"};
      my $content;
      if ($line =~ m{>(.*?)</T>}) {
	$content = $1;
      } else {
	$line = <$io>;
	chomp($line);
	$line =~ s/^\s+//og;
	$line =~ s/\s+$//og;
	$content = $line;
      }
      $tokens->{$id} = $content;
      next;
    }

    if ($line =~ m{<G } ) {
      $refl = 0;
      next;
    }

    if ($line =~ m{<W\s}) {
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($form) = $line =~ /form="(.*?)"/;
      my ($toks) = $line =~ /tokens="(.*?)"/;
      $refl ||=  ($pos eq 'clr' && ($form eq 'se' || $form eq "s'"));
      $lemma = "se_$lemma" if ($pos eq 'v' && $refl);
      if ($pos eq 'np' && $lemma =~ /^_[A-Z]+/) {
	my $content =
	  join(' ',
	       map {$tokens->{$_}}
	       split(/\s+/,$toks));
	$lemma = "$content:$lemma";
      }
      $lemma .= "_$pos";
      $ldata->{"$lemma\t$form"}++;
    }

  }

}

sub distrib {

  my ($file,$data) = @_;

  my $cref = $file->get_content_by_ref;
  my $io = IO::String->new($$cref);

  my $refl=0;
  my $tokens={};

  my $ldata = $data->{lemma} ||= {};

  while (my $line = <$io>) {


    if ($line =~ m{<T }) {
      my ($id) = $line =~ m{id="(.+?)"};
      my $content;
      if ($line =~ m{>(.*?)</T>}) {
	$content = $1;
      } else {
	$line = <$io>;
	chomp($line);
	$line =~ s/^\s+//og;
	$line =~ s/\s+$//og;
	$content = $line;
      }
      $tokens->{$id} = $content;
      next;
    }

    if ($line =~ m{<G } ) {
      $refl = 0;
      next;
    }

    if ($line =~ m{<W\s}) {
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($form) = $line =~ /form="(.*?)"/;
      my ($toks) = $line =~ /tokens="(.*?)"/;
      $refl ||=  ($pos eq 'clr' && ($form eq 'se' || $form eq "s'"));
      $lemma = "se_$lemma" if ($pos eq 'v' && $refl);
      if ($pos eq 'np' && $lemma =~ /^_[A-Z]+/) {
	my $content =
	  join(' ',
	       map {$tokens->{$_}}
	       split(/\s+/,$toks));
	$lemma = "$content:$lemma";
      }
      $lemma .= "_$pos";
      $ldata->{"$lemma"}++;
    }

  }

}

sub rsel {
  my ($io,$data,$task_data,$seg,$sid) = @_;

  $seg ||= 'dummy';
  $sid ||= 1;

  my $propagate = {};
  
  my $scanner = $task_data->{scanner};

  my $incr = sub {
    my ($label,$w,$gid,$did) = @_;
    my $u = $data->{rsel}{$label} ||= [0,0];
    $u->[0]++;
    $u->[1] += $w;
    push(@$u,[$seg,$sid]) unless (@$u > 12);

    if (@$u < 13) {
      push(@$u,[$seg,$sid]);
    } elsif (!int(rand(20))) {
      # we may decide to keep the sentence and remove another one !
      my $pos = 2+int(rand(12));
      $u->[$pos][0] = $seg; 
      $u->[$pos][1] = $sid; 
    }

    (defined $gid) and $propagate->{$gid}{$did} = [$label,$w];
  };

  # list of potential targets (within GP and PV, keep the prep)
  # the list is reset for each sentence
  my $ws = {};	
  my $preps = {};			# to reach GP or PV, keep prep
  my $current ;				# current group
  my $rel;				# current relation id
  my $reltype;				# current relation type
  my $delay = {};
  my $coords = {};
  my $refl = 0;
  my $tokens = {};
  my $has_det = 0;
  my $weight = 1;
  my $modn = {};		# PP modn may have more than one antecedent (ambiguities)
  my $rmodn = {};		# PP modn may have more than one antecedent (ambiguities)
  my $txt = undef;
  my $txtpos2w = {};
  my $termocc = undef;
  my $vinf = {}; 		# to propagate info on vinf argument (modal, control, ...)
  while (my $line=<$io>) {
    ##    print $line;
    if ($line =~ m{<Sentence\b}io) {
      $weight = 1;
      ##      print STDERR "start sentence\n";
      $weight=0.3 unless ($line =~ /mode="full"/);
      $txt = "";
      $txtpos2w = {};
      $termocc = undef;
    }
    if ($line =~ /pos="preposition"/ 
	|| $line =~ /pos="prep"/ 
       ) {
      $line =~ /lemma="(.+?)"/;
      $preps->{$current} = $1;
      next;
    }
##    if ($line =~ m{<G.*type="(PV|GP)"} ) {
    if ($line =~ m{<G } ) {
      $line =~ m{id="(.+?)"};
      $current = $1;
      $refl = 0;
      $has_det = 0;
      next;
    }
    if ($current && $line =~ m{</G>}) {
      $current = undef;
      next;
    }
    if ($line =~ m{<T }) {
      my ($id) = $line =~ m{id="(.+?)"};
      my $content;
      if ($line =~ m{>(.*?)</T>}) {
	$content = $1;
      } else {
	$line = <$io>;
	chomp($line);
	$line =~ s/^\s+//og;
	$line =~ s/\s+$//og;
	$content = $line;
      }
      $tokens->{$id} = $content;
      next;
    }
    if ($line =~ m{<W.*id="(.+?)"}) {
      my $wid = $1;
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($form) = $line =~ /form="(.*?)"/;
      my ($ht) = $line =~ /ht="(.*?)"/;
      my ($toks) = $line =~ /tokens="(.*?)"/;
      $ht = { map {/^(\w+?)\.(.+)/ && ($1 => $2)} split(/\s+/,$ht) };
      my ($mstag) = $line =~ /mstag="(.*?)"/;
      $mstag = { map {/^(\w+?)\.(.+)/ && ($1 => $2)} split(/\s+/,$mstag) };
      $refl ||=  ($pos eq 'clr' && ($form eq 'se' || $form eq "s'"));
      $has_det ||= ($pos eq 'det');
      $lemma = "se_$lemma" if ($pos eq 'v' && $refl);
      if ($pos eq 'np' && $lemma =~ /^_[A-Z]+/) {
	my $content =
	  join(' ',
	       map {$tokens->{$_}}
	       split(/\s+/,$toks));
	$lemma = "$content:$lemma";
      }
      my $wentry = $ws->{$wid} = {
				  lemma => $lemma,
				  pos => $pos,
				  group => $current,
				  ht => $ht,
				  mstag => $mstag,
				  has_det => $has_det,
				  terms => []
				  ##		     content => $content
				 };
      if ($scanner) {
	$txtpos2w->{$wentry->{txtpos} = length($txt)} = $wid;
	$txt .= "$tokens->{$_} " foreach (split(/\s+/,$toks));
      }
      next;
    }
    if ($line =~ m{<R.*id="(.+?)"}) {
      if ($scanner && !defined($termocc)) {
	$termocc=1;
	foreach my $occ ($scanner->multiscan($txt)) {
	  my ($variant,$occpos,$term) = @$occ;
	  my $tinfo = [$term,$occpos,$occpos+length($variant)];
	  my $xpos = $occpos;
	  foreach my $comp (split(/\s+/,$term)) {
	    my $wid = $txtpos2w->{$xpos};
	    if (my $wentry=$ws->{$wid}) {
	      push(@{$wentry->{terms}},$tinfo);
	    }
	    $xpos += length($comp)+1;
	  }
	}
      }
      $rel = $1;
      $line =~ /type="(\S+?)"/ and $reltype = $1;
      next;
    }
    if ($line =~ m{</R>}) {
      $rel = undef;
      $reltype = undef;
      next;
    }
    if ($rel 
	&& $line =~ m{<(complement|modifieur|sujet|cod).*ref="(.+?)"} 
##	&& $ws->{$2}{group}
       ) {
      my $tid = $2;
      my $label = $1;
      my $is_prep = 0;
      if ( $label !~ /sujet|cod/ 
	   && $preps->{$ws->{$tid}{group}}) {
	$label = $preps->{$ws->{$tid}{group}};
	$is_prep = 1;
      }
      $line = <$io>;
      $line =~ /(\S+)\s+ref="(.+?)"/;	# governor
      my $srole = $1;
      my $sid = $2;
      my $tlemma = $ws->{$tid}{lemma};
      my $tpos = $ws->{$tid}{pos};
      my $slemma = $ws->{$sid}{lemma};
      my $spos = $ws->{$sid}{pos};
      if ($spos eq 'v' && $tpos eq 'v' && ($label eq 'cod' || $label eq 'complement')) {
	my $entry = $vinf->{$sid} ||= [];
	push(@$entry,[$tid,$tlemma,$tpos]);
      }
      if ($label eq "sujet"  || $label eq 'cod') {
	## wait to get real verb or attribute
	$delay->{$sid} ||= {};
#	print "register delay sid=$sid label=$label pos=$tpos lemma=$tlemma\n";
	$delay->{$sid}{$label} = { pos => $tpos, 
				   lemma => $tlemma ,
				   wid => $tid
				 };
	next if ($spos eq 'aux');
      }
      if ($label eq "complement" && $tpos eq 'prel' && $tlemma eq 'dont') {
	## wait to get real verb or attribute
	$delay->{$sid} ||= {};
	$delay->{$sid}{'de'} = { pos => $tpos, 
				   lemma => $tlemma ,
				   wid => $tid
				 };
      }
      if ($is_prep && $tpos eq 'prel' && $tlemma ne 'dont') {
	$delay->{$sid} ||= {};
	$delay->{$sid}{$label} = { pos => $tpos, 
				   lemma => $tlemma ,
				   wid => $tid
				 };
      }
      if (($label eq 'par') && ($spos eq 'v')) {
	$is_prep = 0;
	if (my $ht = $ws->{$sid}{ht}) {
	  $label = 'sujet' if ($ht->{diathesis} eq 'passive');
	}
      }
      if ($is_prep && $tpos eq 'nc' && !$ws->{$tid}{has_det}) {
	$label = "$label=";
      }
      if ($tpos eq 'coo') {
	$coords->{target}{$tid}{$label} = {pos => $spos, lemma => $slemma};
      }
      if ($spos eq 'coo') {
	$coords->{source}{$sid}{$label} = {pos => $tpos, lemma => $tlemma};
      }
      if ($tpos eq 'adj' 
	  && $label eq 'modifieur'
	  && fid_compare($sid,$tid) == 1
	 ) {
	$label = 'antemodifieur';
      }
      if ($label eq 'modifieur'
	  || $srole eq 'nom'
	  || $tpos eq 'v'
	 ) {
#	print "try prel tid=$tid sid=$sid\n";
	# maybe we are in a relative construction
	my @totest = ([$tid,$tlemma,$tpos]);
	while (@totest) {
	  my ($tid,$tlemma,$tpos) = @{shift @totest};
	  (exists $vinf->{$tid}) and push(@totest,@{$vinf->{$tid}});
	  foreach my $prel_role ( grep {$delay->{$tid}{$_}{pos} eq 'prel'} 
				  keys %{$delay->{$tid}} ) {
	    my $x = "${tlemma}_${tpos}\t$prel_role\t${slemma}_${spos}";
#	    print "found relative $prel_role: <$x>\n";
	    $incr->($x,$weight,$tid,$sid);
	  }
	}
      }
      next if ($tpos eq 'coo' || $spos eq 'coo');
      if ($is_prep) {
	($spos eq 'nc') 
	  and push(@{$modn->{$sid}},{ label => "$label*", tlemma => $tlemma, tpos => $tpos, w => $weight });
	($tpos eq 'nc')
	  and push(@{$rmodn->{$tid}}, { label => "$label*", slemma => $slemma, spos => $spos, w => $weight });
      }
      $incr->("${slemma}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);
      if ($is_prep) {
	## we propagate PP-attachement to all potential NPs (not just the one chosen by the desambiguisation)
	if (($spos eq 'nc') && exists $modn->{$tid}) {
	  foreach my $x (@{$modn->{$tid}}) {
	    $incr->("${slemma}_${spos}\t$x->{label}\t$x->{tlemma}_$x->{tpos}",$x->{w},$sid,$tid);
	    push(@{$modn->{$sid}},$x);
	  }
	}
	if (($tpos eq 'nc') and exists $rmodn->{$sid}) {
	  foreach my $x (@{$rmodn->{$sid}}) {
	    $incr->("$x->{slemma}_$x->{spos}\t$x->{label}\t${tlemma}_${tpos}",$x->{w},$sid,$tid);
	    push(@{$rmodn->{$tid}},$x);
	  }
	}
      }
      if ($scanner) {		# relation attached to terms, either on source or target or both
	my $xtpos = $ws->{$tid}{txtpos};
	my $tterms = $ws->{$tid}{terms};
	my $xspos = $ws->{$sid}{txtpos};
	my $sterms = $ws->{$sid}{terms};
	foreach my $sinfo (@$sterms) {
	  my ($sterm,$sstart,$send) = @$sinfo;
	  ($xtpos < $sstart || $xtpos > $send) or next;
	  $incr->("${sterm}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);
	  foreach my $tinfo (@$tterms) {
	    my ($tterm,$tstart,$tend) = @$tinfo;
	    ($xspos < $tstart || $xspos > $tend) or next;
	    ($tend < $sstart || $send < $tstart) or next;
	    $incr->("${sterm}_${spos}\t${label}\t${tterm}_${tpos}",$weight,$sid,$tid);
	  }
	}
	foreach my $tinfo (@$tterms) {
	  my ($tterm,$tstart,$tend) = @$tinfo;
	  ($xspos < $tstart || $xspos > $tend) or next;
	  $incr->("${slemma}_${spos}\t${label}\t${tterm}_${tpos}",$weight,$sid,$tid);
	}
      }
      next;
    }
    if ($rel 
	&& $line =~ m{<(auxiliaire).*ref="(.+?)"} 
##	&& $ws->{$2}{group}
       ) {
      my $tid = $2;
      my $label = $1;
      $line = <$io>;
      $line =~ /ref="(.+?)"/;	# governor
      my $sid = $1;
      my $tmp = $delay->{$sid}{aux};
      my $slemma = $tmp ? $tmp->{lemma} : $ws->{$sid}{lemma};
      my $spos = $tmp ? $tmp->{pos} : $ws->{$sid}{pos};
      my $ht = $tmp ? $tmp->{ht} : $ws->{$sid}{ht};
      my $xlabel = ($ht && ($ht->{diathesis} eq "passive")) ? 'cod' : 'sujet';
      if ($delay->{$tid}{sujet}) {
	my $tlemma = $delay->{$tid}{sujet}{lemma};
	my $tpos = $delay->{$tid}{sujet}{pos};
	if ($spos eq 'aux') {
	  ## further delay !
	  $delay->{$sid} ||= {};
	  $delay->{$sid}{sujet} = { pos => $tpos, lemma => $tlemma, wid => $delay->{$tid}{sujet}{wid} };
	} else {
	  $incr->("${slemma}_${spos}\t${xlabel}\t${tlemma}_${tpos}",$weight,$sid,$tid);

	  if ($scanner) {		# relation attached to terms, either on source or target or both
	    my $xtpos = $ws->{$tid}{txtpos};
	    my $tterms = $ws->{$tid}{terms};
	    my $xspos = $ws->{$sid}{txtpos};
	    my $sterms = $ws->{$sid}{terms};
	    foreach my $sinfo (@$sterms) {
	      my ($sterm,$sstart,$send) = @$sinfo;
	      ($xtpos < $sstart || $xtpos > $send) or next;
	      $incr->("${sterm}_${spos}\t${xlabel}\t${tlemma}_${tpos}",$weight,$sid,$tid);
	      foreach my $tinfo (@$tterms) {
		my ($tterm,$tstart,$tend) = @$tinfo;
		($xspos < $tstart || $xspos > $tend) or next;
		($tend < $sstart || $send < $tstart) or next;
		$incr->("${sterm}_${spos}\t${xlabel}\t${tterm}_${tpos}",$weight,$sid,$tid);
	      }
	    }
	    foreach my $tinfo (@$tterms) {
	      my ($tterm,$tstart,$tend) = @$tinfo;
	      ($xspos < $tstart || $xspos > $tend) or next;
	      $incr->("${slemma}_${spos}\t${xlabel}\t${tterm}_${tpos}",$weight,$sid,$tid);
	    }
	  }

	}
      } else {
	$delay->{$sid} ||= {};
	$delay->{$tid}{aux} = { pos => $spos, 
				lemma => $slemma,
				ht => $ws->{$sid}{ht},
				wid => $sid
			      }; 
      }
      next;
    }
    if ($rel 
	&& $line =~ m{<(attribut).*ref="(.+?)"} 
##	&& $ws->{$2}{group}
       ) {
      my $tid = $2;
      my $label = $1;
      $line = <$io>;
      $line =~ /ref="(.+?)"/;	# governor
      my $sid = $1;

      $line = <$io>;		# s-o info
      my ($controlled) = $line =~ /valeur="(\S+?)"/;
#      print "ATB control=$controlled\n";

      my $tlemma = $ws->{$tid}{lemma};
      my $tpos = $ws->{$tid}{pos};
      if (my $tmp = $delay->{$sid}{$controlled}) {
	my $slemma = $tmp->{lemma};
	my $spos = $tmp->{pos};
	my $sid = $tmp->{wid};
	$incr->("${slemma}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);

	if ($scanner) {		# relation attached to terms, either on source or target or both
	  my $xtpos = $ws->{$tid}{txtpos};
	  my $tterms = $ws->{$tid}{terms};
	  my $xspos = $ws->{$sid}{txtpos};
	  my $sterms = $ws->{$sid}{terms};
	  foreach my $sinfo (@$sterms) {
	    my ($sterm,$sstart,$send) = @$sinfo;
	    ($xtpos < $sstart || $xtpos > $send) or next;
	    $incr->("${sterm}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);
	    foreach my $tinfo (@$tterms) {
	      my ($tterm,$tstart,$tend) = @$tinfo;
	      ($xspos < $tstart || $xspos > $tend) or next;
	      ($tend < $sstart || $send < $tstart) or next;
	      $incr->("${sterm}_${spos}\t${label}\t${tterm}_${tpos}",$weight,$sid,$tid);
	    }
	  }
	  foreach my $tinfo (@$tterms) {
	    my ($tterm,$tstart,$tend) = @$tinfo;
	    ($xspos < $tstart || $xspos > $tend) or next;
	    $incr->("${slemma}_${spos}\t${label}\t${tterm}_${tpos}",$weight,$sid,$tid);
	  }
	}


      }
      #next;
    }

#    print "HERE test coord $rel\n";
    if ($rel
	&& $line =~ m{<(coordonnant).*ref="(.+?)"}
       ) {
      my $cid=$2;
      my $label = $ws->{$cid}{lemma};
      $line = <$io>;
      next unless ($line =~ /<coord-g.*ref="(.+?)"/);
      my $tid = $1;
      my $tlemma = $ws->{$tid}{lemma};
      my $tpos = $ws->{$tid}{pos};
      $line = <$io>;
      next unless ($line =~ /<coord-d.*ref="(.+?)"/);
      my $sid = $1;
      my $slemma = $ws->{$sid}{lemma};
      my $spos = $ws->{$sid}{pos};
      $incr->("${slemma}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);

      if ($scanner) {		# relation attached to terms, either on source or target or both
	my $xtpos = $ws->{$tid}{txtpos};
	my $tterms = $ws->{$tid}{terms};
	my $xspos = $ws->{$sid}{txtpos};
	my $sterms = $ws->{$sid}{terms};
	foreach my $sinfo (@$sterms) {
	  my ($sterm,$sstart,$send) = @$sinfo;
	  ($xtpos < $sstart || $xtpos > $send) or next;
	  $incr->("${sterm}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);
	  foreach my $tinfo (@$tterms) {
	    my ($tterm,$tstart,$tend) = @$tinfo;
	    ($xspos < $tstart || $xspos > $tend) or next;
	    ($tend < $sstart || $send < $tstart) or next;
	    $incr->("${sterm}_${spos}\t${label}\t${tterm}_${tpos}",$weight,$sid,$tid);
	  }
	}
	foreach my $tinfo (@$tterms) {
	  my ($tterm,$tstart,$tend) = @$tinfo;
	  ($xspos < $tstart || $xspos > $tend) or next;
	  $incr->("${slemma}_${spos}\t${label}\t${tterm}_${tpos}",$weight,$sid,$tid);
	}
      }

      if (my $tmp = $coords->{target}{$cid}) {
	foreach my $xlabel (keys %$tmp) {
	  my $xpos = $tmp->{$xlabel}{pos};
	  my $xlemma = $tmp->{$xlabel}{lemma};
	  $incr->("${xlemma}_${xpos}\t${xlabel}\t${tlemma}_${tpos}",$weight,$cid,$tid);
	  $incr->("${xlemma}_${xpos}\t${xlabel}\t${slemma}_${spos}",$weight,$cid,$sid);
	}
      }
      if (my $tmp = $coords->{source}{$cid}) {
	foreach my $xlabel (keys %$tmp) {
	  my $xpos = $tmp->{$xlabel}{pos};
	  my $xlemma = $tmp->{$xlabel}{lemma};
	  $incr->("${tlemma}_${tpos}\t${xlabel}\t${xlemma}_${xpos}",$weight,$tid,$cid);
	  $incr->("${tlemma}_${tpos}\t${xlabel}\t${xlemma}_${xpos}",$weight,$sid,$cid);
	}
      }
    }
#    print "HERE test APPOS id=$rel type=$reltype line=<$line>\n";
      if ($rel
	  && $reltype eq 'APPOS'
	  && $line =~ m{<(premier).+ref="(.+?)"}
       ){
#      print "HERE APPOS\n";
      my $tid = $2;
      my $label = 'appos';
      $line = <$io>;
      $line =~ /ref="(.+?)"/;
      my $sid = $1;
      my $tlemma = $ws->{$tid}{lemma};
      my $tpos = $ws->{$tid}{pos};
      my $slemma = $ws->{$sid}{lemma};
      my $spos = $ws->{$sid}{pos};
      if ($spos eq 'np' && $tpos ne 'np') {
	## switch roles
	my $tmp = [$sid,$spos,$slemma];
	$sid = $tid;
	$spos = $tpos;
	$slemma = $tlemma;
	($tid,$tpos,$tlemma) = @$tmp;
	$label = 'rappos';
      }
      $incr->("${slemma}_${spos}\t${label}\t${tlemma}_${tpos}",$weight,$sid,$tid);
    }
      
    if ($line =~ m{</Sentence>}) {
      foreach my $id1 (keys %$propagate) {
	my @ids2 = keys %{$propagate->{$id1}};
	while (@ids2) {
	  my $id2 = shift @ids2;
	  my $info1 = $propagate->{$id1}{$id2};
	  my ($label1,$w1) = @$info1;
	  my ($g1,$l1,$d1) = split(/\t/,$label1);
	  $l1 =~ /modifieur/ and next;
	  foreach my $id3 (@ids2) {
	    my $info2 = $propagate->{$id1}{$id3};
	    my ($label2,$w2) = @$info2;
	    my ($g2,$l2,$d2) = split(/\t/,$label2);
	    $l2 =~ /modifieur/ and next;
	    my $w = ($w1 + $w2) / 4;
	    my $label = ($l1 lt $l2) ? join("\t",$d1,"$l1+$l2",$d2) : join("\t",$d2,"$l2+$l1",$d1);
	    $incr->($label,$w);
	  }
	  exists $propagate->{$id2} or next;
	  foreach my $id3 (keys %{$propagate->{$id2}}) {
	    my $info2 = $propagate->{$id2}{$id3};
	    my ($label2,$w2) = @$info2;
	    my ($g2,$l2,$d2) = split(/\t/,$label2);
	    $l2 =~ /modifieur/ and next;
	    my $w = ($w1 + $w2) / 4;
	    my $label = join("\t",$g1,"$l1/$l2",$d2);
	    $incr->($label,$w);
	  }
	}
      }
      $ws = {};
      $preps = {};
      $current = undef;
      $rel = undef;
      $delay = {};
      $coords = {};
      $tokens = {};
      $vinf = {};
      $propagate = {};
    }
  }
}

sub fid_compare {
  my ($fid1,$fid2) = @_;
  my ($x1) = $fid1 =~ /F(\d+)/;
  my ($x2) = $fid2 =~ /F(\d+)/;
  return $x1 <=> $x2;
}

sub index {
  my ($file,$data) = @_;
  my $cref = $file->get_content_by_ref;
  my $io = IO::String->new($$cref);
  my $keep = { map {$_ => 1} qw/v nc np adj adv/};
  my ($base) = ($file->name =~ m{/([^/]+)\.passage\.xml$});
  my ($seg,$sid) = split(/\./,$base);
  while (my $line=<$io>) {
    if ($line =~ m{<W }) {
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      next unless ($keep->{$pos});
      push(@{$data->{index}{"${lemma}_${pos}"}{$seg}},$sid);
    }
  }
}

sub terms {
  my ($io,$data,$seg,$sid) = @_;

  $seg ||= 'dummy';
  $sid ||= 1;

  my $incr = sub {
    my ($label,$np,$nodep,$role) = @_;
    my $v = $data->{terms}{$label} ||= [0,0,0,0];
    $v->[0]++;
    $v->[1] += $np;
    $v->[2] += $nodep;
    $v->[3] += $role;
    if (@$v < 13) {
      push(@$v,[$seg,$sid]);
    } elsif (($np || $nodep || $role) && !int(rand(20-$np-$nodep-$role))) {
      # we may decide to keep the sentence and remove another one !
      my $pos = 4+int(rand(12));
      $v->[$pos][0] = $seg; 
      $v->[$pos][1] = $sid; 
    }
  };
  ##  print "processing ".$file->name."\

  my $groups = {};
  my $tokens = {};
  my $ws={};
  my $current;
  my $rel;
  my $chains = {};
  my $inchain = {};
  my $has_role = {};

  while (my $line=<$io>) {

    if ($line =~ m{<G } ) {
      $line =~ m{id="(.+?)"};
      $current = $1;
      my ($type) = $line =~ m{type="(.+?)"};
      $groups->{$current} = { type => $type,
			      ws => [],
			      ctx => {}
			    };
      next;
    }

    if ($current && $line =~ m{</G>}) {
      $current = undef;
      next;
    }

    if ($line =~ m{<T }) {
      my ($id) = $line =~ m{id="(.+?)"};
      my $content;
      if ($line =~ m{>(.*?)</T>}) {
	$content = $1;
      } else {
	$line = <$io>;
	chomp($line);
	$line =~ s/^\s+//og;
	$line =~ s/\s+$//og;
	$content = $line;
      }
      $tokens->{$id} = $content;
      next;
    }

    if ($current && $line =~ m{<W.*id="(.+?)"}) {
      my $wid = $1;
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($form) = $line =~ /form="(.*?)"/;
      my ($toks) = $line =~ /tokens="(.*?)"/;
      my $content =
	join(' ',
	     map {$tokens->{$_}}
	     split(/\s+/,$toks));
      if ($pos eq 'np' && $lemma =~ /^_[A-Z]/) {
	$lemma = "$content:$lemma";
      }
      my $xw = $ws->{$wid} = {
			      fid => $wid,
			      lemma => $lemma,
			      pos => $pos,
			      group => $current,
			      content => $content
		    };
      if ($pos eq 'v') {
	my ($mstag) = $line =~ /mstag="(.*?)"/;
##	$mstag = { map {/^(\w+?)\.(.+)/ && ($1 => $2)} split(/\s+/,$mstag) };
	my ($mood) = ($mstag =~ /mode\.(\S+)/);
	$xw->{mood} = $mood if ($mood =~ /(infinitive|participle|gerundive)/);
      }
      if ($current) {
	push(@{$groups->{$current}{ws}},$xw);
      };
      next;
  }

  if ($line =~ m{<R.*id="(.+?)"}) {
      my ($type) = $line =~ /type="(.*?)"/;
      next unless ( grep {$type eq $_} 
		    qw{MOD-N MOD-A MOD-R CPL-V} ##-
		  );
      $rel = $1;
      next;
    }
  
    if ($line =~ m{</R>}) {
      $rel = undef;
      next;
    }

    if ($rel 
	&& $line =~ m{<(modifieur|complement).*ref="(.+?)"}
       ) {
      my $tid = $2;
      $line = <$io>;
      $line =~ /ref="(.+?)"/;	# governor
      my $sid = $1;
      my $xtid = $ws->{$tid};
      my $xsid = $ws->{$sid};
      next unless ($xtid && $xsid && $xtid->{group} && $xsid->{group});
      next if ($xtid->{group} eq $xsid->{group});
      my $spos = $xsid->{pos};
      my $tpos = $xtid->{pos};
      next unless (grep {$spos eq $_} qw/nc np adj adv advneg/);
      next unless (grep {$tpos eq $_} qw/nc np adj v adv advneg/);
      next if ($tpos eq 'v' && !exists $xtid->{mood});
      push(@{$chains->{$xsid->{group}}},$xtid->{group});
##      print "add chain $sid/$xsid->{group} $tid/$xtid->{group}\n";
      $inchain->{$xtid->{group}} = 1;
    }

    if ($line =~ m{<(sujet|cod|attribut).*ref="(.+?)"/>}) {
      my $role = $1;
      my $tid = $2;
      my $xtid = $ws->{$tid};
      next unless ($xtid && $xtid->{group});
      my $tpos = $xtid->{pos};
      next unless (grep {$tpos eq $_} qw/nc np adj v adv advneg/);
      if (1) {
	$line = <$io>;		# the verb
	$line =~ m{<verbe .*ref="(.+?)"/>};
	my $sid = $1;
	my $xsid = $ws->{$sid};
	if ($xsid && exists  $xsid->{mood}) {
	  if ($role eq 'sujet') {
	    $xsid->{mood} =~ /gerundive/ and next;
	    next 
	  } else {
	    push(@{$chains->{$xsid->{group}}},$xtid->{group});
	  }
	}
      }
      $has_role->{$xtid->{group}} = $role;
      next;
    }

    if ($line =~ m{</Sentence>}) {
      CHAIN:
      foreach my $start (keys %$chains) {
	##	next if ($inchain->{$start});
	my $type = $groups->{$start}{type};
##	print "START $groups->{$start}{ws}[0]{content}\n";
	next unless ($type eq 'GN' || $type eq 'GP');
	my $np = ($type eq 'GN') ? 1 : 0;
	my $flat = [];
	flat_chain($chains,$start,$flat);
	$flat = [ sort {gid_compare($a,$b)} @$flat ];
	my $lastfid;
	my $lastform;
	## should have a chain of adjacent groups
	foreach my $gid (@$flat) {
	  my $g = $groups->{$gid};
	  if (defined $lastfid) {
	    my ($first) = ($g->{ws}[0]{fid} =~ /F(\d+)$/);
	    next CHAIN if ($first > (1+$lastfid));
	  }
	  ($lastfid) = ($g->{ws}[-1]{fid} =~ /F(\d+)$/);
	}
	my $l = scalar(@$flat);
	foreach my $i (1 .. 3) {
	  last if ($i == $l);
	  my $sflat = [ @$flat[0..$i] ];
	  my @flat = @$sflat;
	  shift @flat;
	  last if grep {$groups->{$_}{type} eq 'GN'} @flat;
	  next if $groups->{$flat[-1]}{type} eq 'GR';
	  ##	  next if (scalar(@flat) > 3);
	  my @forms = (); 
	  my @labels = ();
	  foreach my $gid (@$sflat) {
	    my $g = $groups->{$gid};
	    my @ws = @{$g->{ws}};
	    my $type = $g->{type};
	    my @content = ();
	    foreach my $w (@ws) {
	      my $pos = $w->{pos};
	      ## remove starting det and prep
	      next if (($gid eq $start) and grep {$pos eq $_} qw{det prep});
	      ## remove EPSILON
	      next if ($w->{lemma} eq '_EPSILON');
	      push(@content,"$w->{lemma}/$pos");
	      my $form = $w->{content};
	      $form = lc($form) unless ($pos eq 'np');
	      push(@forms,$form);
	    }
	    $type = 'GN' if ($gid eq $start);
	    push(@labels,join(' ',"{:$type",
			      ##			    "$g->{ws}[0]{fid}",	# debug
			      @content,
			      ##			    "$g->{ws}[-1]{fid}", # debug
			      "${type}:}"));
	  }
	  my $label = join('__',@labels);
	  my @xforms = ();
	  my $last;
	  while (@forms) {
            my $f = shift @forms;
	    next if (@forms && lc($forms[0]) eq $f);
	    push(@xforms,$f) unless (##$f =~ /'/ &&
                                     (
				      ## for contractions
				      ($last && $last eq $f)
				      ## following is for np with contraction as in "d'Air France"
				      ## || (@forms && lc($forms[0]) eq $f)
                                     )
				    );
	    $last=$f;
	  }
	  my $forms = join(' ',@xforms);
	  $forms =~ s/^[ld]'//io;	# remove contracted det
	  ##	print "add chain $label\n";
	  # approximation of no dependencies
	  # should test the case where the last component is a verb with arguments or complements
	  my $nodep = (($i+1) == $l) ? 1 : 0; 
	  my $role = $has_role->{$sflat->[0]} ? 1 : 0;
	  $incr->("$forms\t$label",$np,$nodep,$role);
	}
      }
    }
  }
}

sub gid_compare {
  my ($gid1,$gid2) = @_;
  my ($x1) = $gid1 =~ /G(\d+)/;
  my ($x2) = $gid2 =~ /G(\d+)/;
  return $x1 <=> $x2;
}

sub flat_chain {
  my $chains = shift;
  my $start = shift;
  my $flat = shift;
  my @agenda=($start);
  my %seen = ();
  while (@agenda) {
    my $current = shift @agenda;
    next if ($seen{$current});
    $seen{$current} = 1;
    push(@$flat,$current);
    if (exists $chains->{$current}) {
      push(@agenda,@{$chains->{$current}})
    }
  }
}

sub rules {
  my ($file,$data) = @_;
  my $name = $file->name;
  my ($seg,$sid) = ($name =~ m{(\w+)\.E(\d+)});
  $seg ||= 'dummy';
  $sid ||= 1;
  my $mode='full';

  my $incr = sub {
    my ($rule,$pat,$status,$weight) = @_;
    my $v = $data->{rules}{"$rule\t$pat\t$mode"} ||= [0,0,0,0];
    if ($status eq 'yes') {
      $v->[0]++;
      $v->[1] += $weight;
    } else {
      $v->[2]++;
      $v->[3] += $weight;
    }
    push(@$v,[$seg,$sid]) unless (@$v > 8);
  };

  my $cref = $file->get_content_by_ref;
  my $io = IO::String->new($$cref);
  ##  print "processing ".$file->name."\


  while (<$io>) {
    if (m{<Sentence\b}io) {
      /mode="(\S+?)"/ and $mode=$1;
      next;
    }
    if (m{<cost }) {
      my ($status) = /kept="(.+?)"/;
      my ($info) = /info="\[(.+?)\]"/;
      my ($rules) = /ws="\[(.+?)\]"/;
      $info = join('_',split(/,/,$info));
      $info =~ s/\[\]/void/og;
      foreach my $xrule ( map {[split(/\s+:\s+/,$_)]} 
			  split(/,/,$rules)) {
	my ($rule,$w) = @$xrule;
	$incr->($rule,$info,$status,$w);
      }
    }
  }

}

sub verbs {

  my ($file,$data) = @_;
  my $name = $file->name;
  my ($seg,$sid) = ($name =~ m{(\w+)\.E(\d+)});
  $seg ||= 'dummy';
  $sid ||= 1;

  my $cref = $file->get_content_by_ref;
  my $io = IO::String->new($$cref);

  my $refl=0;
  my $tokens={};

  my $incr = sub {
    my ($lemma,$np,$nodep) = @_;
    my $v = $data->{verbs}{$lemma} ||= [0];
    $v->[0]++;
    push(@$v,[$seg,$sid]);
  };

  while (my $line = <$io>) {

    if ($line =~ m{<W\s}) {
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      next unless ($pos eq 'v');
      $incr->($lemma);
    }
    
  }
}

package Corpus::Dispatcher;
use AnyEvent::JSONRPC::Lite;
use POSIX qw(strftime setsid floor);

## dispatcher waits for corpus request from task clients
## and send them the name of a corpus

## then wait for an answer from the client
## and reduce the data sent 

## if no corpus to dispath, send a special signal

sub new {
  my ($class,$config,$reduce,$cinfo) = @_;
  my $server = AnyEvent::JSONRPC::Lite::Server->new(
						    address => '127.0.0.1',
						    port => $config->port
						   );
  my $self =  bless { server => $server,
		      hosts => {},
		      tasks => [],
		      corpus => [@{$config->corpus}],
		      done =>  AnyEvent->condvar,
		      data => {}
		    }, $class;

  my $ngot = 0;
  
  $server->reg_cb(

		  'next' => sub {
		    my ($res_cv,$host) = @_;
		    ## send the next corpus if any or 'done'
		    if ($config->verbose) {
		      my $date = strftime "[%F %H:%M:%S]", localtime;
		      print STDERR "$date $host: Next request\n";
		    }
		    if (my $corpus = shift @{$self->{corpus}}) {
		      my $task = $self->{host}{$host} = 
			{ corpus => $corpus,
			  where => $cinfo->{$corpus},
			  host => $host };
		      $res_cv->result($task);
		    } else {
		      $res_cv->result('done');
		      delete $self->{host}{$host};
		      unless (keys %{$self->{host}}) {
			$self->{done}->send;
		      }
		    }
		  },

		  'result' => sub {
		    my ($res_cv,$data) = @_;
		    ## reduce the received information
		    if ($config->verbose) {
		      my $date = strftime "[%F %H:%M:%S]", localtime;
		      $ngot++;
		      print STDERR "$date $data->{host}: Got result <$ngot> for corpus=$data->{corpus}\n";
		    }
		    $res_cv->result('ok');
		    $reduce->($data,$self);
		  },
		  
		 );
  return $self;
}

package Corpus::Task;
use AnyEvent::JSONRPC::Lite;

sub new {
  my ($class,$host,$config,$task,$task_data) = @_;
  ## task_data provide access to external ressources when needed for a task
  ## it is provided in YAML form
  my $client = AnyEvent::JSONRPC::Lite::Client->new(
						    host => $config->master,
						    port => $config->port
						   );
  while (1) {
    my $data = $client->call( next => "$host" )->recv;
    if ($data eq 'done') {
##      print "$host: Quitting\n";
      return;
    }
##    print "$host: processing $data->{corpus}";
    $task->($data,$config,$task_data);
    $client->call( result => $data )->recv;
  }
}

## A task client sends requests to get a corpus
## process it and send back an answer to the dispatcher

## A task client should close when getting the end signal

=pod

=head1 NAME

task_dispatcher

=head1 SYNOPSIS

./task_dispatcher --task rsel -v results.AFP > AFP.rsel

=head1 DESCRIPTION

B<task_dispatcher.pl> is a kind of poor B<MapReduce> algorithm,
dispatching collecting tasks on several clients, aggregating
(counting) the results on the master host and displaying the results.
It is used to collect and count information extracted from parsing
archives built by the corpus processing tool B<dispatch.pl>. More
specifically, information may be extracted from the log, Passage or
DepXML files in these archives.

The available collecting tasks are:

=over 4

=item * I<rsel> to collect local syntactic dependency contexts from
Passage files

=item * I<frame> to collect verbal "frame" from DepXML files. The frames
group arguments and modifiers, listing the syntactic functions and the
filling lemma.

=item * I<term> to collect potential multi-words terms from Passage
files. The terms are sequences of chunks, respecting some patterns.

=item * I<form> to collect all forms from log files

=item * I<index> to build a form index (using Passage files)

=back

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2000-2010, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 SEE ALSO

=over 4

=item * ALPAGE website L<http://alpage.inria.fr/catalogue.en.html>

=item * Inria GForge project page L<http://lingwb.gforge.inria.fr>

=back

=cut

1;
