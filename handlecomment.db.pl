#!/usr/bin/perl -w

use CGI::Pretty qw/-compile -nosticky :standard *pre *table *div map :html3 -private_tempfiles -oldstyle_urls/;
use CGI::Carp qw(fatalsToBrowser);

use Text::Query;

use strict;

##use File::Temp qw/tempfile tempdir/ ;
##use File::Basename;

use DBI;

##$ENV{PATH} = '/bin:/usr/bin/:/usr/local/bin';

local our $query = new CGI::Pretty;

######################################################################
## Configuration

our $PACKAGE = 'results14.biotim';
our $htmldir = '/var/www/perl/$PACKAGE';
our $basedir = "/home/bearn/clerger/ParserRuns/$PACKAGE";
our $commentdb = "$basedir/dbdir/comments.dat";


######################################################################
## Reading parameters

our ($key) = $query->param(-name=>'key');
our ($comment) = $query->param(-name=>'comment');
our ($search) = $query->param(-name=>'search');

##print STDERR "PARAM key='$key' comment='$comment' cached\n";

if ((defined $key) && $key && (defined $comment) && $comment) {
  my $ip = remote_host();
  if ($ip eq '128.93.11.23' || ($ip !~ /^128\.93\./ && $ip !~ /^127\.0\.0\.1$/)) {
    $comment = "*** Sorry but you are not allowed to edit comments";
  } elsif (length($comment) > 2000 ) {
    $comment = "*** Sorry but your comment is too long";
  } else {
    my $dbh = DBI->connect_cached("dbi:SQLite:$commentdb", "", "",
				  {RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
    my $update = $dbh->prepare_cached('REPLACE INTO comment(key,comment) VALUES(?,?)');
    $update->execute($key,$comment);
    ##  $dbh->disconnect;
  }
  emit();
} elsif (defined $key) {
  my $dbh = DBI->connect_cached("dbi:SQLite:$commentdb", "", "",
				{RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
  my $fetch = $dbh->prepare_cached('SELECT comment FROM comment WHERE key=?');
  $fetch->execute($key);
  ($comment) = $fetch->fetchrow_array;
  $fetch->fetchrow_array if ($comment);
  emit();
} else {
  emit_form();
}

undef $comment;

######################################################################
## Emitting Data

sub emit {
  my $emit = (defined $comment) ? $comment : '';
  print
    header({-type => "text/plain",-expires => 'now'}),$emit;
}

sub emit_form {
  print 
    header,
      start_html( 
		 -title => 'Browsing comments',
		 -style => { -src => 'style.css' }
		),
		  h1('Browsing comments errors for ',$PACKAGE);
  print  start_form;
  print  p('Search in comments: (please use the notation of Alta Vista simple queries) ',
	   br,
	   $query->textfield(-name=>'search',
			     -size=>70,
			     -default=> ''
			    ));
  print p($query->reset,$query->submit('Action','Submit'));
  print end_form;
  print hr;
  if (defined $search) {
    my $q=Text::Query->new($search);
    my $dbh = DBI->connect_cached("dbi:SQLite:$commentdb", "", "",
				  {RaiseError => 1, AutoCommit => 1}) or die $DBI::errstr;
    $dbh->func( "mycheck", 1,  sub { my $comment=shift; return $q->match($comment) }, "create_function" );
    my $sth = $dbh->prepare_cached('SELECT key,comment FROM comment WHERE mycheck(comment)');
    $sth->execute();
    while (my ($key,$comment) = $sth->fetchrow_array()) {
      $comment =~ s/\n/br/mgeo;
      print p(a({href=>"errorscgi.pl?key=::$key"},$key),":",$comment);
    }
    print hr;
  }
  print end_html;
}

=head1 NAME

handlecomment.db.pl --  Browsing comments errors

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
