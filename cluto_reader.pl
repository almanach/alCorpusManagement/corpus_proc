#!/usr/bin/env perl


## readable view of cluto clusters

use strict;

my $words = shift;
my $clusters = shift;

my @words = ();
my %cluster = ();

open(W,"<$words") || die "can't open $words: $!";
while(<W>) {
  chomp;
  push(@words,$_);
}
close(W);

open(C,"<$clusters") || die "can't open $clusters: $!";
while(<C>) {
  chomp;
  my $c=$_;
  my $w = shift @words;
  next if ($c == -1);
  $cluster{$c} ||= [];
  push(@{$cluster{$c}},$w)
}
close(C);

foreach my $c (sort {$a <=> $b} keys %cluster) {
  print "cluster $c: @{$cluster{$c}}\n\n";
}
