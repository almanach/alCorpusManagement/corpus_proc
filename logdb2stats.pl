#!/usr/bin/env perl

use strict;
use Carp;
use DBI;

use GD::Graph::Data;
use GD::Graph::bars;
use GD::Graph::lines;
use GD::Graph::mixed;

use CGI::Pretty qw/:standard/;

use AppConfig qw/:argcount :expand/;

use PDL;
#use PDL::AutoLoader;
#use PDL::Fit::Polynomial;

use Data::Dumper;

my $config = AppConfig->new(
			    { CREATE => 1},
                            "verbose!"        => {DEFAULT => 1},
                            "corpus=f@"       => {DEFAULT => []},
                            "run!"            => {DEFAULT => 1},
                            "lock=f"          => {DEFAULT => 'lock'},
                            "results=f"       => {DEFAULT => 'results'},
                            "callparser=f"    => {DEFAULT => ''},
                            "parse_options=s" => {DEFAULT => ''},
                            "key=s"           => {DEFAULT => ''},
                            "host=s@"         => {DEFAULT => []},
                            "hostallow!"      => {DEFAULT => 1},
			    "mafdocument!"    => {DEFAULT => 0},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "dagdir=f"           => {DEFAULT => 'dag'},
			    "edges=d" => {DEFAULT => 100},
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my %status = ( unparsed => -1,
	       ok       => 1,
	       fail     => 0
	     );

my $results = $config->results;
my $verbose = $config->verbose;
my @corpus  = @{$config->corpus};

my $host;

foreach my $h (@{$config->{host}}) {
  $h =~ /^[a-z]+/ and $host ||= $1;
}

my $dbh = DBI->connect("dbi:SQLite:$results/dblog.dat", "", "",
		       {RaiseError => 1, AutoCommit => 1});

$dbh->func( "median", 1, 'median', "create_aggregate" );


## my $sth=$dbh->prepare('select edges, avg(time), count(*) from log where host= ? and edges < ? group by edges');

my $edges = $config->edges;

build_ambiguity($host,$edges);
build_time        ($host,$edges);
build_success_time        ($host,$edges);
build_robust_time        ($host,$edges);
build_acc_time        ($host,$edges);
build_timepassage ($host,$edges);
build_timefread   ($host,$edges);
build_timeamb     ($host,$edges);
build_timexmldep  ($host,$edges);
build_timeout     ($host,$edges);
build_success     ($host,$edges);
#build_success_rate($host,$edges);
build_length      ($host,$edges);
build_time_vs_amb($host,$edges);

html_emit();

sub emit_graph {
  my $name    = shift;
  my $data    = shift;
  my $options = shift;
  my @legend  = @_;
  my $chart   = GD::Graph::mixed->new(1400,500);
  $chart->set_legend(@legend);
  
  my $title = $name;
  $title =~ s/_/ /og;

##  print Data::Dumper->new([$data],['data'])->Dump;

  $chart->set(
	      legend_placement => 'BC',
	      x_label_skip     => 10,
	      x_tick_offset    => -1,
	      title            => $title,
	      x_label          => '#edges',
	      %$options
	     );

##  $chart->set_legend_font(['verdana', 'arial', 'gdMediumBoldFont'], 18);


  my $gd = $chart->plot($data);

  open(IMG,">$results/$name.png") or die $!;
  binmode IMG;
  print IMG $gd->png;
  close IMG;

  $data->export("$results/$name.dat");

}

sub build_ambiguity {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth   = $dbh->prepare(<<STH);
select edges, avg(max(0,ambiguity)), min(max(0,ambiguity)), max(max(0,ambiguity))
from log 
where status=1 and edges <= ?  and typeof(ambiguity) IN ("integer","real")
group by edges
STH
  $sth->execute($edges);

  my $distrib = $sth->fetchall_arrayref();
##  my ($y) = fitpoly1d(pdl([map($_->[0],@$distrib)]),pdl([map($_->[1],@$distrib)]),2);

  ## print $y;

  my $i = 0;

  foreach my $row (@$distrib) {
##    $data->add_point(@$row,$y->at($i++));
    $data->add_point(@$row);
  }

  emit_graph('distrib_amb',
	     $data,
	     {},
	     qw/avg_ambiguity/
	    );
}

sub scale ($) {
  return $_[0];
}


sub build_time {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(time)
from log
where status > -1 
   and time <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
#    $data->add_point(log($row[0] || 1) => log($row[1] || 1));
    $data->add_point($row[0] => ($row[1] / (0.0005 * ($row[0] * $row[0]) || 1)));
  }

  emit_graph('distrib_time',$data,{},qw/avg_time(s)/);

}


sub build_success_time {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(time)
from log
where status = 1
   and time <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
#    $data->add_point(log($row[0] || 1) => log($row[1] || 1));
    $data->add_point(@row);
  }

  emit_graph('distrib_timesuccess',$data,{},qw/avg_time(s)/);

}

sub build_robust_time {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(time)
from log
where status = 2
   and time <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
#    $data->add_point(log($row[0] || 1) => log($row[1] || 1));
    $data->add_point(@row);
  }

  emit_graph('distrib_timerobust',$data,{},qw/avg_time(s)/);

}



sub build_acc_time {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, sum(time)
from log
where status > -1 
   and time <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
#    $data->add_point(log($row[0] || 1) => log($row[1] || 1));
    $data->add_point(@row);
  }

  emit_graph('distrib_acctime',$data,{},qw/acc_time(s)/);

}



sub build_time_alt {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(time), min(time), max(time)
from log
where status > -1 
   and edges <= ? 
   and time <> "NULL"
   and timeout = 0
group by edges
STH


  $sth->execute($edges);
  my $distrib = $sth->fetchall_arrayref();
##  my ($y,$c) = fitpoly1d(pdl([map($_->[0],@$distrib)]),pdl([map($_->[1],@$distrib)]),4);

##  print "Correl: $c\n";

  print "build time\n";

  ## print scalar(map($_->[1],@$distrib));



  my $i    = 0; 
  my %info = ();
  my $x    = 1;
  foreach my $row (@$distrib) {
    print "$x @$row\n";
    while ($x < $row->[0]) {
      $info{$x} ||= [$x,0,0,0,0,0,0];
      $x++;
    }
    $x++;
##    $info{$row->[0]} = [@$row,$y->at($i++)];
    $info{$row->[0]} = [@$row,0,0,0];
  }

  print "Doing row2\n";

  my $sth2 = $dbh->prepare(<<STH);
select edges,avg(time)
from log
where status = 1
   and edges <= ? 
   and time <> "NULL"
group by edges
STH
  $sth2->execute($edges);
  $x=1;
  while( my @row2 = $sth2->fetchrow_array) {
    my $e = shift @row2;
    $info{$e}[4]=$row2[0];
  }

  print "Doing row3\n";

  my $sth3 = $dbh->prepare(<<STH);
select edges,avg(time), median(time)
from log
where status > - 1 
   and edges <= ? 
   and time <> "NULL"
group by edges
STH
  $sth3->execute($edges);
  $x = 1;
  while( my @row3 = $sth3->fetchrow_array) {
    my $e = shift @row3;
    next unless $e;		#  this case should not occur but there seem to be some bug related to median ?
    @{$info{$e}}[5,6] = @row3;
  }

  print "Add points\n";
  foreach my $key (sort {$a <=> $b} %info) {
    my ($x,@v) = @{$info{$key} || []};
    @v = map {log($_||1)} @v;
    $data->add_point($x => @v);
  }
  
  emit_graph('distrib_time',$data,{},qw/avg_time(s) avg_time_success(s) avg_time_all median_time_all/);
}

sub build_timepassage {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(timepassage)
from log
where status > -1 
   and timepassage <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(scale($row[0] || 1) => scale($row[1] || 1));
  }

  emit_graph('distrib_timepassage',$data,{},qw/avg_time(s)/);

}


sub build_timefread {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(timefread)
from log
where status > -1 
   and timefread <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(scale($row[0] || 1) => scale($row[1] || 1));
  }

  emit_graph('distrib_timefread',$data,{},qw/avg_time(s)/);

}


sub build_timeamb {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(timeamb)
from log
where status > -1 
   and timeamb <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(scale($row[0] || 1) => scale($row[1] || 1));
  }

  emit_graph('distrib_timeamb',$data,{},qw/avg_time(s)/);

}

sub build_timexmldep {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth=$dbh->prepare(<<STH);
select edges, avg(timexmldep)
from log
where status > -1 
   and timexmldep <> "NULL"
   and edges < ?
   and timeout = "0"
group by edges
STH


  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(scale($row[0] || 1) => scale($row[1] || 1));
  }

  emit_graph('distrib_timexmldep',$data,{},qw/avg_time(s)/);

}


sub build_time_vs_amb {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth = $dbh->prepare(<<STH);
select edges, avg(time / ((1+max(0,ambiguity))*edges))
from log
where status = 1 
   and edges <= ? 
   and timeout = "0"
group by edges
STH

  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(@row);
  }
  
  emit_graph('distrib_time_amb',$data,{},qw/avg_time_amb(s)/);
}

sub build_timeout {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth = $dbh->prepare('select edges, 100 * sum(timeout) / max(1,count(*)) as avg_timeout from log where status > -1 and edges <= ? group by edges');

  $sth->execute($edges);

  my $distrib = $sth->fetchall_arrayref();
##  my ($y) = fitpoly1d(pdl([map($_->[0],@$distrib)]),pdl([map($_->[1],@$distrib)]),2);

##  print $y;

##  print Data::Dumper->new([$distrib],['distrib'])->Dump;

  my $i = 0;
  foreach my $row (@$distrib) {
##    $data->add_point(@$row,$y->at($i++));
    $data->add_point(@$row);
  }
  
#  emit_graph('distrib_timeout',$data,{},qw/%timeout lin_interp_timeout/);
  emit_graph('distrib_timeout',$data,{},qw/%timeout/);
}


sub build_success {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth = $dbh->prepare(<<STH);
select edges, count(*), sum(1-timeout),sum(status)
  from log 
    where status <> -1 
      and edges <= ? 
   group by edges
STH
  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(@row);
  }
  
#  emit_graph('distrib_success',$data,{default_type => 'area'},qw/#sentences #notimeout #success/);
  emit_graph('distrib_success',$data,{},qw/#sentences #notimeout #success/);
}


sub build_success_rate {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth = $dbh->prepare(<<STH);
select edges, 100 * avg(status), count(*), sum(status)
  from log 
    where status <> -1 and edges <= ? 
      group by edges
STH
  $sth->execute($edges);

  my $count   = 0;
  my $success = 0;
  while( my @row = $sth->fetchrow_array) {
    $count += $row[2];
    $success += $row[3];
    $data->add_point(@row[0,1],
		     $row[1] ? 100 * exp(log(.01 * $row[1]) / $row[0]) : 0,
		     $count ? 100 * ($success / $count) : 0
		    );
  }
  
  emit_graph('distrib_success_rate',$data,{two_axes => 1},qw/%success %success_per_edge/);
}


sub build_length {
  my $host  = shift;
  my $edges = shift;

  my $data  = GD::Graph::Data->new();

  my $sth = $dbh->prepare(<<STH);
select edges, avg(length)
  from log 
    where status <> -1 and edges <= ? 
      group by edges
STH
  $sth->execute($edges);

  while( my @row = $sth->fetchrow_array) {
    $data->add_point(@row);
  }
  
  emit_graph('distrib_length',$data,{},qw/avg_length/);
}

sub html_emit {
  open(HTML,">$results/distrib.html") or die $!;
  print HTML
##    header('text/html'),
      start_html('Distributions'),
	h1('Distributions wrt number of edges in input word lattice'),
	  ( h2('Sentence Length vs edges'),
	    img({src => ploticus_call('length')}),
	    h2('Success'),
	    img({src => ploticus_call('success')}),
	    h2('Success Rate'),
	    img({src => ploticus_call('success_rate')}),
	    h2('Execution time'),
	    img({src => ploticus_call('time')}),

	    h2('Success Execution time'),
	    img({src => ploticus_call('timesuccess')}),

	    h2('Robust Execution time'),
	    img({src => ploticus_call('timerobust')}),

	    h2('Accumulated time'),
	    img({src => ploticus_call('acctime')}),
	    h2('Passage time'),
	    img({src => ploticus_call('timepassage')}),
	    h2('Dep XML time'),
	    img({src => ploticus_call('timexmldep')}),
	    h2('Forest time'),
	    img({src => ploticus_call('timefread')}),
	    h2('Ambiguity time'),
	    img({src => ploticus_call('timeamb')}),
	    h2('Execution times vrt ambiguity rate and #edges'),
	    img({src => ploticus_call('time_amb')}),
	    h2('Timeout'),
	    img({src => ploticus_call('timeout')}),
	    h2('Ambiguity Rate'),
	    img({src =>  ploticus_call('amb')}),
	  ),
	    end_html;
  close(HTML);
}

sub ploticus_call_old {
  my $name = shift;
  return "http://localhost/cgi-bin/pl?cgi=1&distrib_$name.pls";
}

sub ploticus_call {
  my $name = shift;
  return "distrib_$name.png";
}

package GD::Graph::Data;

sub export {
  my $self     = shift;
  my $file     = shift;
  my $num_sets = @{$self->[0]};
  my $num_dim  = @{$self};
##  print "DATA sets=$num_sets dim=$num_dim\n";
  open(FILE,">$file") or die $!;
  for my $np (0 .. $num_sets-1) {
    my @p = ();
    for my $nd (0 .. $num_dim-1) {
##      print "DATA export $np $nd $self->[$np][$nd]\n";
      push(@p,$self->[$nd][$np]);
    }
    print FILE "@p\n";
  }
  close(FILE);
}

package median;

sub new { bless [], shift; }

sub step {
  my ( $self, $value ) = @_;
  push @$self, $value;
}

sub finalize {
  my $self = $_[0];
  my $n    = @$self;
  @$self   = sort {$a <=> $b} @$self;

  return $self->[int($n/2)];
}


1;

__END__


=head1 NAME

logdb2stats.pl -- Generate stats in html format from log database

=head1 SYNOPSIS 	 
  	 
./logdb2stats.pl

=head1 DESCRIPTION

Run logdb2stats.pl after log2db.pl. Stats are sent to ${RESULTS}/distrib.html.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
