#!/usr/bin/perl

## Generate an MSTAG (ie a TagSet) from a set of annotations

my $mstags = {};

while (<>) {
  if (/<W / && /mstag="(.+?)"/) {
    foreach my $tag (split(/\s+/,$1)) {
      tag_process($tag);
    }
  }
}

foreach my $tag (sort {tag_sort($a,$b)} keys %$mstags) {
  print $mstags->{$tag}{xml};
}

sub tag_sort {
  my ($a,$b) =@_;
  my $ma = $mstags->{$a};
  my $mb = $mstags->{$b};
  return ($ma->{f} cmp $mb->{f})
    || (@{$ma->{v}} <=> @{$ma->{v}})
      || ($ma->{v}[0] cmp $mv->{v}[0]);
}


sub tag_process {
  my $tag = shift;
  return if (exists $mstags->{$tag});
  my ($f,@v) = split(/\./,$tag);
  my $info = $mstags->{$tag} = { feature => $f,
				 v => [@v],
			       };
  my $xml;
  if (@v > 1) {
    foreach my $v (@v) {
      $xml .= <<EOF;
             <symbol value="$v[0]"/>
EOF
    }
    chomp($xml);
    $xml = <<EOF;
         <vAlt>
$xml
         </vAlt>
EOF
  } else {
    $xml = <<EOF;
         <symbol value="$v[0]"/>
EOF
    chomp($xml);
  }
  $info->{xml} = <<EOF;
<MSTAG id="$tag">
   <fs>
      <f name="$f">
$xml
      </f>
   </fs>
</MSTAG>
EOF
}
