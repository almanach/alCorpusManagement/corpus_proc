#!/usr/bin/env perl

use strict;
use AppConfig;
##use POSIX qw{strftime};
use POSIX;

my $config = AppConfig->new(
			    'verbose|v!' => {DEFAULT => 0},
			    'tokens=f' => {DEFAULT => 'reltokens.log'},
			    'passage=f' => {DEFAULT => 'glue1'}
			   );

$config->args;

my $token_file = $config->tokens;

my $passage_dir = $config->passage;

my $data = {};

sub verbose ($) {
  my $msg = shift;
  print "$msg\n";
}

open(TOKENS,"<$token_file")
  || die "can't open $token_file: $!";

## read info about correct and incorrect relations
verbose "Reading relations";
while(<TOKENS>) {
  if (/^(\S+?)\.(E\d+)(F\d+)\s###\sfull.*(match|mismatch|onlyref|onlyhyp|samehead|sametype)\s+(\S+)\s+(\S+)/) {
    my $corpus = $1;
    my $sid = $2;
    my $fid = $3;
    my $type = $4;
    my $expected = $5;
    my $got = $6;
    if ($type eq 'onlyhyp') {
      #      $expected .= $got;
      $expected = $got;
    }
    my $info = $data->{$corpus}{$sid} ||= {};
    push(@{$info->{types}{$type}{${expected}}},$fid);
  }
}

close(TOKENS);

-d $passage_dir
  || die "missing directory $passage_dir";

my $distribution = {};
my $recap = [];

verbose "Reading costs in $passage_dir";
foreach my $file (glob("$passage_dir/*.xml.bz2")) {
  open(FILE,"bzcat $file|") 
    || die "can't open $file:$!";
  my $sid;
  my $mode;
  my $info;
  my ($corpus) = ($file =~ m{/([^/]+)\.xml.bz2});
  verbose "\tprocessing $corpus";
  ## read costs in each result files
  while(<FILE>) {
    if (m{<Sentence}) {
      ($sid) = /id="(E\d+)"/;
      ($mode) = /mode="(\S+?)"/;
      $info = $data->{$corpus}{$sid};
      next;
    }
    if ($info && ($mode eq 'full') && m{<cost}) {
      my ($kept) = /kept="(\S+?)"/;
      next unless ($kept eq 'yes');
      my ($ws) = /ws="\[(.*?)\]"/; 
      my @ws = map {[split(/:/,$_)]} split(/,/,$ws);
      my ($tmp) = /info="\[(.+?)\]"/;
      my ($dir,$spos,$label,$tpos,$swid,$twid) = split(/,/,$tmp);
      my ($target) = /target="E\S+?c_(\d+)_\d+"/;
      if ($twid =~ /F(\d+)/) {
	$target = $twid;
      }
      foreach my $ws (@ws) {
	push(@{$info->{rules}{"$ws->[0] ${spos},${tpos},$label,$dir"}},$target);
	push(@{$info->{rules}{"$ws->[0] ${spos},${tpos},$label"}},$target);
	push(@{$info->{rules}{"$ws->[0] ${spos},${tpos}"}},$target);
	push(@{$info->{rules}{"$ws->[0]"}},$target);
	push(@{$info->{rules}{"$spos $label $tpos $dir"}},$target);
	push(@{$info->{rules}{"$spos $label $tpos"}},$target);
#	push(@{$info->{rules}{"$ws->[0]"}},$target);
      }
      next;
    }
    if ($info && m{</Sentence>} && ($mode eq 'full')) {
      foreach my $type (keys %{$info->{types}}) {
	foreach my $expected (keys %{$info->{types}{$type}}) {
	  my $n = @{$info->{types}{$type}{$expected}};
	  my $dist = $distribution->{$expected} 
	    ||= { total => 0,
		  rules => {}
		};
	  $dist->{total} += $n;
	  foreach my $rule (keys %{$info->{rules}}) {
	    my $v = @{$info->{rules}{$rule}};
	    my $w = $v * $n;
	    my $dist2 = $dist->{rules}{$rule} 
	      ||= { ok => 0,
		    bad => 0,
		    total => 0,
		    total2 => 0,
		    total3 => 0,
		    samples => {}
		  };
	    foreach my $sample (@{$info->{types}{$type}{$expected}}) {
	      my $where1 = ($sample =~ /F(\d+)/);
	      foreach my $where2 (@{$info->{rules}{$rule}}) {
		next unless (abs($where1-$where2) < 3);
		if ($expected =~ /^missing/) {
		  next unless ($where1 == $where2);
		}
		$dist2->{total}++;
		$dist2->{($type eq 'match') ? 'ok' : 'bad'}++;
		unless ($type eq 'match') {
		  $dist2->{samples}{"$corpus.$sid$_"} = 1 foreach (@{$info->{types}{$type}{$expected}});
		}
	      }
	    }
	  }
	}
      }
      next;
    }
  }
  close(FILE);
}

foreach my $type (keys %$distribution) {
  my $dist = $distribution->{$type};
  my $xtotal = $dist->{total};
  foreach my $rule (keys %{$dist->{rules}}) {
    my $dist2 = $dist->{rules}{$rule};
    $dist2->{rok} = $dist2->{ok} / ($dist2->{total} || 1);
    $dist2->{rbad} = $dist2->{bad} / ($dist2->{total} || 1);
    $dist2->{delta} = $dist2->{rbad}-$dist2->{rok};
    $dist2->{w} = $dist2->{delta} * log($dist2->{total} || 1);
    $dist
  }
  print <<EOF;
------------------
$type
EOF
  my $rules = $dist->{rules};
  foreach my $rule (sort {$rules->{$b}{w} <=> $rules->{$a}{w} } 
		    keys %$rules) {
    my $dist2 = $rules->{$rule};
    next if ($dist2->{total} < 10);
    next if (abs($dist2->{delta}) < 0.02);
    my $s = sprintf("%s bad=%.2f%% ok=%.2f%% delta=%.2f%% total=%d xtotal=%d %s",
		    $rule,
		    100*$dist2->{rbad},
		    100*$dist2->{rok},
		    100*$dist2->{delta},
		    $dist2->{total},
		    $xtotal,
		    $type
		   );
    if ($dist2->{delta} > 0.05) {
      if ($dist2->{total} > 30) {
	$s = "\e[0;31m$s\e[0m";
	push(@$recap,[$type,$rule,$s,$dist2->{samples}]);
      } else {
	$s = "\e[0;36m$s\e[0m";
      }
    } elsif ($dist2->{delta} < -0.2) {
      $s = "\e[0;32m$s\e[0m";
      if ($dist2->{total} > 100) {
	## push(@$recap,[$type => $s]);
      }
    }
    print "\t$s\n";
  }
}


print <<EOF;
======================================================================
RECAP
EOF

foreach my $x (@$recap) {
  next if (grep {$x->[0] eq $_} qw{missing JUXT APPOS});
  my $r = $x->[1];
  next if (grep {is_more_specific($r,$_)} map {$_->[1]} @$recap);
  print "*** $x->[0]\t$x->[2]\n";
  my $samples = $x->[3];
  my @samples = (keys %$samples)[0..20];
  print "\t@samples\n";
}

sub is_more_specific {
  ## is $r2 more specific than $r1
  my ($r1,$r2) = @_;
  ($r1 eq $r2) and return 0;
  ##  print "test spec r1=$r1 r2=$r2\n";
  return (index($r2,$r1,0) == 0);
}


=head1 NAME

rule_analyze.pl -- Analyze distorsions in the desamb rules

=head1 SYNOPSIS

   rule_analyze.pl

=head1 DESCRIPTION

B<rule_analyze.pl> is a Perl script that analyzes the distributions of
the disamb rules to identify problematic rules

=head1 OPTIONS

=cut

1;
