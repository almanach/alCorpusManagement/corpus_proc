#!/usr/bin/env perl

use strict;

use AppConfig;
use EV;
use AnyEvent;
use POSIX qw(strftime setsid floor);
use Text::LevenshteinXS qw(distance);
use String::LCSS_XS qw(lcss);
use Text::Undiacritic qw(undiacritic);
use Scalar::Util qw(weaken);
use List::Util qw(min max);

## Variants are grouped by identical models

## models with too many variants and/or presence of some NEs such as _NUM, _DATE, ...
## are deleted

## models whose variants are not very close using a distance editions
## are deleted

## a kind of mutual infornation are computed for each surviving model

## Things to be tried:

##  - remove models X__Y such that there exists a model X__Z__Y  
##    => the model is internally modified => not a good term

##  - remove models X__Y with weak standalone such that there exists a larger right extension
##   ie X__Y__Z

##  - remove models X__Y with weak np such that there exists a larger left extension
##   ie Z__X__Y



my $config = AppConfig->new(
			    'verbose|v!' => {DEFAULT => 0},
			    'output|o=f' => {DEFAULT => 'toto'},
			    'distrib=f',
			    'sid2sentence=f',
			    'sentences=f',
			    'min_score=d' => {DEFAULT => 10},
			    'min_mi=d' => {DEFAULT => 0.1},
			    'min_div2=d' => {DEFAULT => 0.25},
			   );

my $args = join(' ',@ARGV);

$config->args;

my $base = $config->output;
my $distrib = $config->distrib || "$base.distrib";

my $min_score = $config->min_score;
my $min_mi = $config->min_mi;
my $min_div2 = $config->min_div2;

sub verbose {
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print STDERR "$date : ", @_, "\n";
##  print LOG "$date : ", @_, "\n";
}

my $all;

my $sentences = {};

my $sid2sentence = $config->sid2sentence;

use IPC::Open2;
sub start_sid2sentence {
  my $pid = open2(\*SOUT,\*SIN,"$sid2sentence")
    || die "can't run $sid2sentence: $!";
  SIN->autoflush(1);
  SOUT->autoflush(1);
}

{ 
  no strict 'refs';
  if ($sid2sentence) {
    start_sid2sentence();
    *{'sid2sentence'} = sub {
      my $sid = shift;
      print SIN "$sid\n";
      my $s = <SOUT>;
      $s =~ /^<(S+)>\s*(.*?)/;
#      return [$1,$2];
      return $2;
    };
  } elsif ($config->sentences && -f $config->sentences) {
    open(SENT,"<",$config->sentences) || die "can't read sentences";
    while(<SENT>){
      my ($sid,$s) = /^<(\S+)>\s*(.*)$\s*/;
      $sentences->{$sid} = $s;
    }
    close(SENT);
    *{'sid2sentence'} = sub { my $sid=shift; return $sentences->{$sid}};
  } else {
    *{'sid2sentence'} = sub { return undef };
  }
}

my $words = {};			# (approximation of) word distribution computed on triples

open(W,"<$distrib") || die "can't open distrib file '$distrib': $!";
while(<W>) {
  chomp;
  my ($w,$f) = split(/\t/,$_);
  next if $w =~ /_(prep|det|predet|coo|csu|_)/;
  $words->{$w} = $f;
  $all += $f;
}
close(W);

my $logall = log($all);


my $terms = {};			# potential terms as provided by task_dispatcher.pl for terms
my $models = {};

my $simplified_models = {};

verbose("Reading potential terms");
while (<>) {
  chomp;
  my ($e,$m,$f,$np,$standalone,$role,$samples) = split(/\t/,$_);
  ($m =~ m{\sun/nc}) and next;
  my $entry = $terms->{$e} ||= { form => $e,
				 model => $m,
				 freq => 0,
				 np => 0,
				 standalone => 0,
				 role => 0,
				 samples => [],
			       };
  $entry->{freq} += $f;
  $entry->{np} += $np;
  $entry->{standalone} += $standalone;
  $entry->{role} += $role;
  my @chunks = ();
  my @lemma = ();
  my @types = ();
  foreach (split(/__/,$m)) {
    my ($type,$w) = /\{:(\S+)\s+(.+?)\s+\1:\}/o;
    my $cw = [];
    push(@chunks,[$type,$cw]);
    while ($w =~ m{\G\s*(.+?)/(np|adv|adj|det|prep|nc|number|predet|advPref|title|_|coo|v|ncpred|adjPref)}og) {
      my $cat = $2;
      ## $cat eq 'det' and next;
      my $lemma = "${1}_${cat}";
      push(@lemma,$lemma);
      push(@$cw,$lemma);
      push(@types,$cat);
    }
  }
  if ($samples) {
    push(@{$entry->{samples}}, grep {/\S+:E\d+/} split(/\s+/,$samples));
  }
  my $model = $models->{$m} ||= { model => $m,
				  freq => 0,
				  np => 0,
				  standalone =>0,
				  role => 0,
				  variants => {},
				  ws => [],
				  bonus => 0,
				  chunks => \@chunks,
				  lemma => \@lemma,
				  type => \@types,
				  entity => 0
				};
  unless (%{$model->{variants}}) {
    foreach my $c (@chunks) {
      my ($type,$lemma) = @$c;
      if ($type eq 'GP') {
	if (my @det = grep {$_ =~ /_det|predet/} @$lemma) {
	  if (grep {$_ =~ /un_det/} @det) {
	    verbose("malus det $m");
	    $model->{bonus} += -0.8;
	  }
	} else {
	  $model->{bonus} += 0.2;
	}
	(grep {$_ =~ /_v/} @$lemma) and $model->{bonus} += -0.1;
	(grep {$_ =~ /ant_v/} @$lemma) and $model->{bonus} += -0.2;
      } elsif ($type eq 'NV') {
	$model->{bonus} += -0.1;
      }
    }
    if ($chunks[-1][0] eq 'NV' && $e =~ /ant$/) {
      verbose("malus gerundive $e $m");
      $model->{bonus} += -0.2;
    }
    $model->{entity} = scalar(grep {$_ eq 'np'} @types) / (scalar(grep {$_ !~ /det|prep/} @types) || 1);
  }
  $model->{freq} += $f;
  $model->{np} += $np;
  $model->{standalone} += $standalone;
  $model->{role} += $role;
  $model->{variants}{$e} = $entry;
  weaken($model->{variants}{$e});
  unless (exists $model->{smodel}) {
    my $m2 = $m;
    $m2 =~ s{\S+/det}{}og;
    $m2 =~ s{/\S+}{}og;
    $m2 =~ s/\s+/ /og;
    $m2 = lc(undiacritic($m2));
    my $smodel = $simplified_models->{$m2} ||= [];
    push(@$smodel,$m);
    $model->{smodel} = $m2;
  }
  #  verbose("register simplify $m => $m2");
}

verbose("Merging close potential models");
foreach my $sm (grep {@$_ > 1} values %$simplified_models) {
  my @m = sort {$models->{$b} <=> $models->{$a}} @$sm;
  # reroot toward the most frequent one
  my $id1 = shift @m;
  my $m1 = $models->{$id1};
  foreach my $id2 (@m) {
    verbose("merging $id2 with $id1");
    my $m2 = $models->{$id2};
    $m1->{$_} += $m2->{$_} foreach (qw{freq np standalone role});
    $m1->{variants}{$_} = $m2->{variants}{$_} foreach (keys %{$m2->{variants}});
    delete $models->{$id2};
  }
}
$simplified_models = {};

verbose("Filtering variants");
foreach my $m (map {$_->[0]}
	       sort {$b->[1] <=> $a->[1] || $b->[2] <=> $a->[2]}
	       grep {$_->[1] > 1}
	       map {[$_,scalar( keys %{$models->{$_}{variants}} ),$models->{$_}{freq}]} 
	       keys %$models) {
  my $model = $models->{$m};
  my @variants = keys %{$model->{variants}};
  my $mark = "";
  if (@variants > 6
      || $m =~ /_(DATE|NUM|TEL|HEURE)/
     ) {
    delete_model($m,'entity');
    next;
  } else {
    ## search if the variant are close 
    my @v1 = @variants;
    my $d = 0;
    my $n = 0;
    while (@v1) {
      my $w1 = shift @v1;
      $w1 =undiacritic($w1);
      foreach my $w2 (@v1) {
	$w2 = undiacritic($w2);
	$d += distance($w1,$w2);
	$n++;
      }
    }
    $d = $d / $n;
    if ($d > 3) {
      delete_model($m,"too many variants $d: @variants");
      next;
    }
  }
}

my $c2m = {};			# mapping from chunks to models

my $trie = {};

sub add_model {
  my $m = shift;
  my $model = $models->{$m};
  my $where = $trie;
  foreach my $chunk (map {join(" ",$_->[0],grep {!/_det$/} @{$_->[1]})} @{$model->{chunks}}) {
    $where = $where->{next}{$chunk} ||= {};
    (exists $where->{model}) and verbose("potential extensions $m: $where->{model}");
  }
  (exists $where->{next}) and verbose("potential extension $m");
  $where->{model} = $m;
  $model->{trie} = $where;
  weaken($model->{trie});
}

sub collect_extensions {
  my $where = shift;
  my @extensions = ();
  if (exists $where->{model} && exists $models->{$where->{model}}) {
    push(@extensions,$where->{model});
  } elsif (exists $where->{next}) { 
    push(@extensions,map {collect_extensions($_)} values %{$where->{next}})
  };
  return @extensions;
}

sub check_subpart {
  my $model = shift;
  my @chunks = @{$model->{chunks}};
  shift @chunks;
  return unless (@chunks && $chunks[0][0] eq 'GP');
  my $first = shift @chunks;
  my $type = 'GN';
  my @lemma = @{$first->[1]};
  shift @lemma;
  return unless @lemma;
  $lemma[0] =~ /_det$/ and shift @lemma;
  return unless @lemma;
  $lemma[-1] =~ /_(adj|v)/ and $type = 'GA';
  unshift @chunks, [$type,\@lemma];
  my $where = $trie;
  foreach my $chunk (map {"$_->[0] @{$_->[1]}"} @chunks) {
    $where = $where->{next}{$chunk};
    $where or return;
  }
  return unless ($where && exists $where->{model});
  $model->{subpart} = $where->{model};
  push(@{$models->{$where->{model}}{embedded}},$model->{model});
}

foreach my $m (keys %$models) {
  add_model($m);
}

my @todelete = ();
foreach my $model (grep {$_->{trie}{next}} values %$models) {
  $model->{extended} = [ map {collect_extensions($_)} values %{$model->{trie}{next}} ];
  check_subpart($model);
  if (scalar(@{$model->{extended}}) > 10) {
    ## many direct extensions => STRANGE: we delete the extensions
    foreach my $m (@{$model->{extended}}) {
      push(@todelete,[$m,$model->{model}]);
    }
  }
  if (@{$model->{extended}}==1) {
    my $m2 = $model->{extended}[0];
    ($model->{standalone} < 0.1) and delete_model($model->{model},"weak-autonomy extendable term by $m2");
  }
}
delete_model($_->[0],"one of the many extension of $_->[1]") foreach (@todelete);
@todelete=();

# foreach my $m (keys %$models) {
#   my $model = $models->{$m};
#   my $key = join('+',map {$_->[0]} @{$model->{chunks}});
#   $c2m->{$key}{$m} = 1;
#   $model->{key} = $key;
# }

verbose("Trying to extend weak terms");
foreach my $m (keys %$models) {
  my $model = $models->{$m};
  $model->{r_standalone} = $model->{standalone} / $model->{freq};
  $model->{r_np} = $model->{np} / $model->{freq};
  $model->{r_role} = $model->{role} / $model->{freq};

  if ($model->{r_standalone} < 0.1) {
    $model->{weak_standalone} = 1;
  #   my $k1 = $model->{key};
  # EXT1:
  #   foreach my $k2 (keys %$c2m) {
  #     next unless (index($k2,$k1,0) == 0);
  #     foreach my $m2 (keys %{$c2m->{$k2}}) {
  # 	next if ($m2 eq $m);
  # 	if (index($m2,$m,0) == 0) {
  # 	  ##	delete_model($m);
  # 	  push(@{$model->{extended}},$m2);
  # 	  last EXT1;
  # 	}
  #     }
  #   }
    #    exists $model->{trie}{next} and push(@{$model->{extended}},collect_extensions($model->{trie}{next}));
  }
  if ($model->{r_np} < 0.1) {
    $model->{weak_np} = 1;
    if (0) {			# should modify first :NP of $m into :GP
      foreach my $m2 (keys %$models) {
	next if ($m2 eq $m);
	if (index($m2,$m,0) > 0) {
	delete_model($m,'weak np');
	last;
      }
      }
    }
  }
  if ($model->{r_role} < 0.05) {
    delete_model($m,'too few roles');
  }
}

sub delete_model {
  my $m = shift;
  my $reason = shift || 'no reason';
  my $model = $models->{$m};
  delete $models->{$m};
  foreach my $w (keys %{$model->{variants}}) {
    delete $terms->{$w};
  }
  if (exists $model->{trie}) {
    delete $model->{trie}{model};
    delete $model->{trie};
  }
  $model->{deleted} = 1;
  verbose("deleted $m: $reason");
}


verbose("Computing mutual information");
foreach my $m (keys %$models) {
  my $model = $models->{$m};
  my @w = @{$model->{lemma}};
  my $mi=0;
  my $x=0;
  my $logf=log($model->{freq});
  foreach my $w (grep {!/_(prep|det|predet|coo|csu|_)/} @w) {
    $x++;
    if (exists $words->{$w}) {
      # should keep only important words
      my $u = log($words->{$w}); 
      $mi += $u;
      push(@{$model->{ws}},[$w,$words->{$w},$u]);
    } else {
      ## penalize missing words
      $mi += $logf;
      print STDERR "missing word in $m: $w\n";
    }
  }
  ##  $x and $mi = $mi / $x;
  
  ## PMI
  ##  $mi = $model->{mi} = max(0,$logf-$mi+($x-1)*$logall);

  ## NPMI: normalized PMI
  $mi = $model->{mi} = max(0,($logf-$mi+($x-1)*$logall) / - ($logf-$logall));

  delete_model($m,"weak mi $mi"), next if ($mi < $min_mi);

  ##  $model->{r} = $logf *  $mi;
  $model->{r} = sqrt($model->{freq}) *  $mi;
  #  $model->{weak_standalone} and $model->{r} *= 0.5;
  #  $model->{weak_np} and $model->{r} *= 0.5;
  $model->{r} *= (1 + 0.5 * $model->{r_standalone}) 
    * (1+0.5*$model->{r_np}) 
      * (1+$model->{r_role}) 
	* (1-$model->{entity});
  $model->{bonus} and $model->{r} *= 1+$model->{bonus};
  
  if (1) {
    my $sn=0;
    my %tmp = ();
    my %words = ();
    my $wn = 0;
    foreach (values %{$model->{variants}}) {
      ##    verbose("processing $m $_");
      foreach my $s (@{$_->{samples}}) {
	if (my $info = sid2sentence($s)) {
	  ##	  verbose("got $s: $info");
	  $tmp{$info} = 1;
	  ++$sn;
	  foreach my $w (split(/\s+/,$info)) {
	    $wn++;
	    $words{$w}++;
	  }
	}
      }
    }
    my $diversity = scalar(keys %tmp);
    $model->{diversity} = $diversity / ($sn || 1);
    my $diversity2 = scalar(keys %words);
    $model->{diversity2} = $diversity2 / ($wn || 1);
    $model->{r} *= (1+$diversity);
    delete_model($m,"weak word diversity $model->{diversity2}"), next if ($model->{diversity2} < $min_div2);


    if (exists $model->{embedded}) {
      my $n = scalar(@{$model->{embedded}});
      ($n > 2) and $model->{r} *= (1+0.2*$n);
    }

    delete_model($m,"weak score $model->{r}"), next if ($model->{r} < $min_score);
  }

}

verbose("Display terms");
my $i=0;

foreach my $m (sort {$models->{$b}{r} <=> $models->{$a}{r}}
	       keys %$models) {
  my $model = $models->{$m};
  $model->{id} = ++$i;
}

foreach my $m (sort {$models->{$b}{r} <=> $models->{$a}{r}}
	       keys %$models) {
  my $model = $models->{$m};
  my $id = $model->{id};
  print "BEGIN TERM <$id>\n";
  printf("\tmodel: %s\ts=%f\tmi=%f\tf=%d\ta1=%d\ta2=%d\trole=%d\tent=%.2f\tdiv=%.2f\tdiv2=%.2f\n",
	 $m,
	 $model->{r},
	 $model->{mi},
	 $model->{freq},
	 $model->{np},
	 $model->{standalone},
	 $model->{role},
	 $model->{entity},
	 $model->{diversity},
	 $model->{diversity2}
	);
  my @variants = sort {$terms->{$b}{freq} <=> $terms->{$a}{freq}}
		 keys %{$model->{variants}};

  foreach my $w (@variants) {
    my $entry = $terms->{$w};
    my @samples = @{$entry->{samples} || []};
    printf("\tvariant: %s\t%d\t%d\t%d\t%d\t%s\n",
	   $w,
	   $entry->{freq},
	   $entry->{np},
	   $entry->{standalone},
	   $entry->{role},
	   "@samples"
	  );
  }
  if ($model->{weak_standalone}){
    print "\t*** weak standalone\n";
  }
  if ($model->{weak_np}) {
    print "\t*** weak np\n";
  }
  if (my $m2 = $model->{extended}) {
    foreach my $xm (sort {$models->{$a}{id} <=> $models->{$b}{id}} 
		    grep {exists $models->{$_}} @$m2) {
      my $model2 = $models->{$xm};
      my @variants = sort {$terms->{$b}{freq} <=> $terms->{$a}{freq}}
	keys %{$model2->{variants}};
      print "\t*** possible extension <$model2->{id}> $variants[0]\n";
    }
  }
  if (my $m2 = $model->{embedded}) {
    foreach my $xm (sort {$models->{$a}{id} <=> $models->{$b}{id}} 
		    grep {exists $models->{$_}} @$m2) {
      my $model2 = $models->{$xm};
      my @variants = sort {$terms->{$b}{freq} <=> $terms->{$a}{freq}}
	keys %{$model2->{variants}};
      print "\t*** possible embedding <$model2->{id}> $variants[0]\n";
    }
  }
  if (exists $model->{subpart} && exists $models->{$model->{subpart}}) {
    my $xm = $model->{subpart};
    my $model2 = $models->{$xm};
    my @variants = sort {$terms->{$b}{freq} <=> $terms->{$a}{freq}}
      keys %{$model2->{variants}};
    print "\t*** possible subterm <$model2->{id}> $variants[0]\n";
  }
  print "END TERM <$id>\n\n";
}
