#!/usr/bin/env perl

## test a dispatcher task for providing data about robust sentences

use strict;
use List::Util qw{min max first};

sub td_collect {
  my ($data,$config) = @_;
  if ($config->simple) {
    Corpus::DepXML::collect(\*STDIN,$data,'dummy','E1');
  } else {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      next unless ($name =~ /\.dep\.xml/);
      my $cref = $file->get_content_by_ref;
      my $io = IO::String->new($$cref);
      my ($seg,$sid) = ($name =~ m{([\w-]+)\.E(\d+)});
      Corpus::DepXML::collect($io,$data,$seg,$sid);
    }
  }
}

sub td_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{ngrams}}) {
    my $u = $server->{data}{ngrams}{$w} ||= [0];
    my $v = $data->{ngrams}{$w};
    my $n = $u->[0] += $v->[0];
    my $nu = scalar(@$u);
    my $nv = scalar(@$v);
    my $xn = int(sqrt($n));
    for(my $i=1; ($i < $nv); $nu++, $i++) {
      if ($nu > 12) {
	next if (int(rand($xn)));
	my $pos = 1+int(rand(11)); # 11+1=12
	$u->[$pos] = $v->[$i];
      } else {
	push(@$u,$v->[$i]);
      }
    }
  }
}

sub td_display {
  my $data = shift;
  my $simple = shift;
  my $min = $simple ? 0 : 10;
  foreach my $w (sort {$data->{ngrams}{$b}[0] <=> $data->{ngrams}{$a}[0]} 
		 grep {$data->{ngrams}{$_}[0] > $min}
		 keys %{$data->{ngrams}}) {
    my @u = @{$data->{ngrams}{$w}};
    my @l = sort map {"$_->[0]:E$_->[1]"} @u[1 .. $#u];
    print "$w\t$u[0]\t@l\n";
  }
}

package Corpus::DepXML;
use List::Util qw{min max first};
use XML::Twig;

my %keep_xcat = ( 'N2' => 1,
		  'adjP' => 1,
		  'PP' => 1
		);

sub collect {
  my ($handle,$data,$seg,$sid) = @_;
  my $ngrams = {};
  my $crossed = {};
  my $cas = XML::Twig->new(
			   twig_handlers => {
					     op => sub {
					       my $op = $_;
					       my @span = split(/\s+/,$op->att('span'));
					       @span == 4 and return;	# don't keep adjoining op
					       my $left = $span[0];
					       my $right = $span[1];
					       $ngrams->{$right} ||= {};
					       $crossed->{$_} = 1 foreach (($left+1) .. ($right-1));
					       my $xcat = $op->att('cat');
					       $keep_xcat{$xcat} or return;
					       my $current = $ngrams->{$left} ||= {};
					       $current->{"xcat=$xcat"}{$right} = 1;
					       $op->purge;
					     },
					     node => sub {
					       my $node = $_;
					       my $cat = $node->att('cat') || '_';
					       my $lemma = $node->att('lemma');
					       $lemma eq '_' and $lemma = $node->att('form');
					       my ($left,$right) = $node->att('cluster') =~ /c_(\d+)_(\d+)$/;
					       $right > $left or return;
					       $ngrams->{$right} ||= {};
					       my $current = $ngrams->{$left} ||= {};
					       my $label = $lemma;
					       (grep {$cat eq $_} qw{_ poncts ponctw}) or $label .= "_$cat";
					       $lemma eq '_' or $current->{"lemma=$label"}{$right} = 1;
					       (grep {$cat eq $_} qw{_ ponctw poncts ilimp})
						 or $current->{"cat=$cat"}{$right} = 1;
					       $node->purge;
					     }
					    }
			  );
  eval {
    $cas->parse($handle);
  };
  $cas->purge;
  my $max = max keys %$ngrams;
  $ngrams->{start}{'^'}{0} = 1;
  $ngrams->{$max}{'$'}{end} = 1;
  $crossed->{$max} = 1;
  $crossed->{$_} = 1 foreach (qw{start 0 end});

  my $bigrams = {};
  my $bigrams2 = {};
  foreach my $p1 (keys %$ngrams) {
    my $r1 = (exists $crossed->{$p1}) ? '' : ":: ";
    my $n1 = $ngrams->{$p1};
    foreach my $l1 (keys %$n1) {
      my ($c1) = $l1 =~ /^(\w+)=/o;
      foreach my $p2 (keys %{$n1->{$l1}}) {
	my $r2 = (exists $crossed->{$p2}) ? '' : ":: ";
	my $n2 = $ngrams->{$p2};
	foreach my $l2 (keys %$n2) {
	  my ($c2) = $l2 =~ /^(\w+)=/o;
	  ($c1 eq 'xcat' && $c2 eq 'xcat') and next;
	  ($c1 eq 'lemma' && $c2 eq 'lemma') and next;
	  my $l = "$r1$l1 $r2$l2";
	  my $c = [$c1,$c2];
	  $bigrams->{$p1}{$l}{$_} = $c foreach (keys %{$n2->{$l2}});
	  $bigrams2->{$p1}{$l} = 1;
	}
      }
    }
  }

  my $trigrams = {};
  foreach my $p1 (keys %$bigrams) {
    my $n1 = $bigrams->{$p1};
    foreach my $l1 (keys %$n1) {
      foreach my $p3 (keys %{$n1->{$l1}}) {
	my ($c1,$c2) = @{$n1->{$l1}{$p3}};
	my $r3 = (exists $crossed->{$p3}) ? '' : ":: ";
	my $n3 = $ngrams->{$p3};
	foreach my $l3 (keys %$n3) {
	  my ($c3) = $l3 =~ /^(\w+)=/o;
	  ($c2 eq 'xcat' && $c3 eq 'xcat') and next;
	  my $nlemma = grep {$_ eq 'lemma'} $c1,$c2,$c3;
#	  ($nlemma == 1) or next;
	  $nlemma or next;
#	  $trigrams->{$p1}{"$l1 $r3$l3"}{$_} = 1 foreach (keys %{$n3->{$l3}});
	  $trigrams->{$p1}{"$l1 $r3$l3"}=1;
	}
      }
    }
  }

  foreach my $p1 (keys %$trigrams) {
    foreach my $l (keys %{$trigrams->{$p1}}) {
      my $u = $data->{ngrams}{$l} ||= [0];
      $u->[0]++;
      my @v = @$u;
      shift @v;
      (first {$seg eq $_->[0] && $sid eq $_->[1]} @v) and next;
      if (@$u < 12) {
	push(@$u,[$seg,$sid]);
      } elsif (!int(rand(20))) {
	# we may decide to keep the sentence and remove another one !
	my $pos = 1+int(rand(12));
	$u->[$pos][0] = $seg; 
	$u->[$pos][1] = $sid; 
      }
    }
  }

  foreach my $p1 (keys %$bigrams2) {
    foreach my $l (keys %{$bigrams2->{$p1}}) {
      my $u = $data->{ngrams}{$l} ||= [0];
      $u->[0]++;
      my @v = @$u;
      shift @v;
      (first {$seg eq $_->[0] && $sid eq $_->[1]} @v) and next;
      if (@$u < 12) {
	push(@$u,[$seg,$sid]);
      } elsif (!int(rand(20))) {
	# we may decide to keep the sentence and remove another one !
	my $pos = 1+int(rand(12));
	$u->[$pos][0] = $seg; 
	$u->[$pos][1] = $sid; 
      }
    }
  }



}

1;
