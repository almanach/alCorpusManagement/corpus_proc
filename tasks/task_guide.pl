#!/usr/bin/env perl

## test a dispatcher task for providing guiding data

use strict;
use List::Util qw{min max first};
use DBI;

sub td_collect {
  my ($data,$config) = @_;
  if ($config->simple) {
    Corpus::DepXML::collect(\*STDIN,$data,'dummy',1);
  } else {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      next unless ($name =~ /\.dep\.xml/);
      my $cref = $file->get_content_by_ref;
      my $io = IO::String->new($$cref);
      my ($seg,$sid) = ($name =~ m{([\w-]+)\.E(\d+)});
      Corpus::DepXML::collect($io,$data,$seg,$sid);
    }
  }
}

sub td_reduce {
  my ($data,$server) = @_;
  foreach my $sid (keys %{$data->{guide}}) {
    $server->{data}{guide}{$sid} = $data->{guide}{$sid};
  }
}

sub td_display {
  my $data = shift;
  my $guide = "guide.dat";
  -f $guide and unlink $guide;
  my $dbh = DBI->connect("dbi:SQLite:$guide","","",{RaiseError => 1, AutoCommit => 0});
  $dbh->do(<<EOF);
CREATE TABLE node (
  sid TEXT,
  left INTEGER,
  right INTEGER,
  form TEXT,
  lemma TEXT,
  cat TEXT,
  tree TEXT
)
EOF
  $dbh->do(<<EOF);
CREATE TABLE op (
  sid TEXT,
  left INTEGER,
  right INTEGER,
  xcat TEXT,
  tree TEXT,
  caller TEXT
)
EOF
  $dbh->commit;
  my $node_insert = $dbh->prepare(<<EOF);
insert into node (sid,left,right,form,lemma,cat,tree) values (?,?,?,?,?,?,?);
EOF
  my $op_insert = $dbh->prepare(<<EOF);
insert into op (sid,left,right,xcat,tree,caller) values (?,?,?,?,?,?);
EOF
  foreach my $sid ( sort {$a <=> $b}
		    keys %{$data->{guide}}
		  ) {
    my $entry = $data->{guide}{$sid};
    my $xsid = $entry->{id};
    0 and print <<EOF;
guide.
EOF
    foreach my $node (sort {$a->{left} <=> $b->{left}} @{$entry->{node}}) {
      my ($form,$cat,$tree) = map {quote($_)} @{$node}{qw{form cat tree}};
      0 and print <<EOF;
#guide!node($node->{left},$node->{right},$form,$cat,$tree).
EOF
      $node_insert->execute($xsid,@{$node}{qw{left right form lemma cat tree}});
    }
    foreach my $op (sort {$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1]} @{$entry->{op}}) {
      my $xcat = quote($op->[2]);
      my $tree = quote($op->[3]);
      my $caller = quote($op->[4]);
      0 and print <<EOF;
guide!op($op->[0],$op->[1],$xcat,$tree,$caller).
EOF
      $op_insert->execute($xsid,$op->[0],$op->[1],$op->[2],$op->[3],$op->[4]);
    }
    0 and print "\n\n";
  }
  $dbh->commit;
  $dbh->disconnect;
}

sub quote {
  my $string = shift || "";
  return $string if ($string =~ /^\d+$/);
  return $string if ($string =~ /^[a-z][a-zA-Z��������������������������������������]*$/o);
  $string =~ s/\'/\'\'/og;
  return "\'$string\'";
}

package Corpus::DepXML;
use List::Util qw{min max first};
use XML::Twig;

sub collect {
  my ($handle,$data,$seg,$sid) = @_;
  my $entry = $data->{guide}{$sid} ||={ node => [], op => [], xop => {}};
  my $cas = XML::Twig->new(
			   ignore_elts => { # edge => 'discard',
					    hypertag => 'discard',
					    cluster => 'discard',
					    narg => 'discard',
					    cost => 'discard'
					  },
			   start_tag_handlers => {
						 dependencies => sub {
						   $entry->{id} = $_->att('id');
						 }
						},
			   twig_handlers => {
					     op => sub {
					       my $op = $_;
					       my @span = split(/\s+/,$op->att('span'));
					       push(@{$entry->{op}},
						    [ $span[0], # left
						      $span[1], # right
						      $op->att('cat'), # xcat
						      $entry->{deriv}{$op->att('deriv')}, # tree
						      $entry->{xop}{$op->att('id')} || '_' # caller tree
						    ]
						   );
					       $op->purge;
					     },
					     node => sub {
					       my $node = $_;
					       my $tree = $node->att('tree');
					       my $deriv = $node->att('deriv');
					       my ($left,$right) = $node->att('cluster') =~ /c_(\d+)_(\d+)$/;
					       ($right > $left) and
						 push(@{$entry->{node}},
						      { left => $left,
							right => $right,
							cat => $node->att('cat') || '_',
							lemma => $node->att('lemma'),
							form => $node->att('form'),
							tree => $tree
						      }
						     );
					       ($deriv && $tree && $node->att('xcat')) and $entry->{deriv}{$deriv} = $tree;
					       $node->purge;
					     },
					     deriv => sub {
					       my $deriv = $_;
					       my $xderiv = $deriv->att('names');
					       my $target_op = $deriv->att('target_op');
					       $entry->{xop}{$target_op} = $entry->{deriv}{$xderiv};
					       $deriv->purge;
					     }
					    }
			  );
  eval {
    $cas->parse($handle);
  };
  $cas->purge;
}

1;
