This directory is to be used for storing tasks for task_dispatcher.pl.

The various tasks should be documented in this files

    - task_xngrams.pl : collected extended ngrams from DepXML files,
      with specific marks at segment boundaries for the sentences
      parsed in robust mode.

