#!/usr/bin/env perl

## a task for collecting apposition relations in PASSAGE files
## another variant should be done for DepXML files

use strict;
use List::Util qw{min max first};

sub td_collect {
  my ($data,$config,$task_data) = @_;
  if ($config->simple) {
    Corpus::Passage::collect(\*STDIN,$data,'dummy','E1');
  } else {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      next unless ($name =~ /\.passage\.xml/);
      my $cref = $file->get_content_by_ref;
      my $io = IO::String->new($$cref);
      my ($seg,$sid) = ($name =~ m{([\w-]+)\.E(\d+)});
      Corpus::Passage::collect($io,$data,$seg,$sid);
    }
  }
}

sub td_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{appos}}) {
    my $u = $server->{data}{appos}{$w} ||= [0];
    my $v = $data->{appos}{$w};
    my $n = $u->[0] += $v->[0];
    my $nu = scalar(@$u);
    my $nv = scalar(@$v);
    my $xn = int(sqrt($n));
    for(my $i=1; ($i < $nv); $nu++, $i++) {
      if ($nu > 12) {
	next if (int(rand($xn)));
	my $pos = 1+int(rand(11)); # 11+1=12
	$u->[$pos] = $v->[$i];
      } else {
	push(@$u,$v->[$i]);
      }
    }
  }
}

sub td_display {
  my $data = shift;
  my $simple = shift;
#  my $min = $simple ? 0 : 10;
  my $min = 0;
  foreach my $w (sort {$data->{appos}{$b}[0] <=> $data->{appos}{$a}[0]} 
		 grep {$data->{appos}{$_}[0] > $min}
		 keys %{$data->{appos}}) {
    my @u = @{$data->{appos}{$w}};
    my @l = sort map {"$_->[0]:E$_->[1]"} @u[1 .. $#u];
    print "$w\t$u[0]\t@l\n";
  }
}

package Corpus::Passage;
use List::Util qw{min max first};
use XML::Twig;
use IO::String;

sub collect {
  my ($io,$data,$seg,$sid) = @_;

  my $groups = {};
  my $tokens = {};
  my $ws={};
  my $current;
  my $rel;
  my $chains = {};
  my $inchain = {};
  my $has_role = {};

  my $in_appos;
  my $appos = {};

  while (my $line=<$io>) {

    if ($line =~ m{<G } ) {
      $line =~ m{id="(.+?)"};
      $current = $1;
      my ($type) = $line =~ m{type="(.+?)"};
      $groups->{$current} = { type => $type,
			      ws => [],
			      ctx => {}
			    };
      next;
    }

    if ($current && $line =~ m{</G>}) {
      $current = undef;
      next;
    }

    if ($line =~ m{<T }) {
      my ($id) = $line =~ m{id="(.+?)"};
      my $content;
      if ($line =~ m{>(.*?)</T>}) {
	$content = $1;
      } else {
	$line = <$io>;
	chomp($line);
	$line =~ s/^\s+//og;
	$line =~ s/\s+$//og;
	$content = $line;
      }
      $tokens->{$id} = $content;
      next;
    }

    if ($current && $line =~ m{<W.*id="(.+?)"}) {
      my $wid = $1;
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($form) = $line =~ /form="(.*?)"/;
      my ($toks) = $line =~ /tokens="(.*?)"/;
      my $content =
	join(' ',
	     map {$tokens->{$_}}
	     split(/\s+/,$toks));
      if ($pos eq 'np' && $lemma =~ /^_[A-Z]/) {
	$lemma = "$content:$lemma";
      }
      my $xw = $ws->{$wid} = {
			      fid => $wid,
			      lemma => $lemma,
			      pos => $pos,
			      group => $current,
			      content => $content
			     };
      if ($current) {
	push(@{$groups->{$current}{ws}},$xw);
      };
      next;
    }

    if ($line =~ m{<R.*id="(.+?)"}) {
      my ($type) = $line =~ /type="(.*?)"/;
      if ($type eq 'APPOS') {
	my $rid = $1;
	my ($tid) = <$io> =~ /ref="(.+?)"/; # premier
	my ($sid) = <$io> =~ /ref="(.+?)"/; # appos
	my $entry = $appos->{$rid} = { premier => $ws->{$tid}{group},
				       appos => $ws->{$sid}{group}
				     };
##	print STDERR "Registered appos $rid  premier=$entry->{premier} appos=$entry->{appos}\n";
	next;
      }
      next unless ( grep {$type eq $_} 
		    qw{MOD-N MOD-A MOD-R CPL-V}
		  );
      $rel = $1;
      next;
    }
    
    if ($line =~ m{</R>}) {
      $rel = undef;
      next;
    }

    if ($rel 
	&& $line =~ m{<(modifieur|complement).*ref="(.+?)"}
       ) {
      my $tid = $2;
      $line = <$io>;
      $line =~ /ref="(.+?)"/;	# governor
      my $sid = $1;
      my $xtid = $ws->{$tid};
      my $xsid = $ws->{$sid};
      next unless ($xtid && $xsid && $xtid->{group} && $xsid->{group});
      next if ($xtid->{group} eq $xsid->{group});
      my $spos = $xsid->{pos};
      my $tpos = $xtid->{pos};
      next unless (grep {$spos eq $_} qw/nc np adj adv advneg/);
      next unless (grep {$tpos eq $_} qw/nc np adj v adv advneg/);
      next if ($tpos eq 'v' && !exists $xtid->{mood});
      push(@{$chains->{$xsid->{group}}},$xtid->{group});
##      print "add chain $sid/$xsid->{group} $tid/$xtid->{group}\n";
      $inchain->{$xtid->{group}} = 1;
    }

    if ($line =~ m{<(sujet|cod|attribut).*ref="(.+?)"/>}) {
      my $role = $1;
      my $tid = $2;
      my $xtid = $ws->{$tid};
      next unless ($xtid && $xtid->{group});
      my $tpos = $xtid->{pos};
      next unless (grep {$tpos eq $_} qw/nc np adj v adv advneg/);
      if (1) {
	$line = <$io>;		# the verb
	$line =~ m{<verbe .*ref="(.+?)"/>};
	my $sid = $1;
	my $xsid = $ws->{$sid};
      }
      $has_role->{$xtid->{group}} = $role;
      next;
    }

    if ($line =~ m{</Sentence>}) {
      keys %$appos or next;
      my $build_chain = sub {
	my $start = shift;
	my $res;
	$start or return;
	my $type = $groups->{$start}{type};
	($type eq 'GN' || $type eq 'GP') or return;
	my $np = ($type eq 'GN') ? 1 : 0;
	my $flat = [];
	flat_chain($chains,$start,$flat);
	$flat = [ sort {gid_compare($a,$b)} @$flat ];
	my $lastfid;
	my $lastform;
	## should have a chain of adjacent groups
	foreach my $gid (@$flat) {
	  my $g = $groups->{$gid};
	  if (defined $lastfid) {
	    my ($first) = ($g->{ws}[0]{fid} =~ /F(\d+)$/);
	    return if ($first > (1+$lastfid));
	  }
	  ($lastfid) = ($g->{ws}[-1]{fid} =~ /F(\d+)$/);
	}
	my $l = scalar(@$flat);
	my $sflat = [];
	foreach my $i (1 .. $l) {
	  push(@$sflat,$flat->[$i-1]);
	  my @flat = @$sflat;
	  shift @flat;
	  next if $groups->{$flat[-1]}{type} eq 'GR';
	  ##	  next if (scalar(@flat) > 3);
	  my @forms = (); 
	  foreach my $gid (@$sflat) {
	    my $g = $groups->{$gid};
	    my @ws = @{$g->{ws}};
	    my $type = $g->{type};
	    my @content = ();
	    foreach my $w (@ws) {
	      my $pos = $w->{pos};
	      ## remove starting det and prep
	      next if (($gid eq $start) and grep {$pos eq $_} qw{det prep});
	      ## remove EPSILON
	      next if ($w->{lemma} eq '_EPSILON');
	      push(@content,"$w->{lemma}/$pos");
	      my $form = $w->{content};
	      $form = lc($form) unless ($pos eq 'np');
	      push(@forms,$form);
	    }
	    $type = 'GN' if ($gid eq $start);
	  }
	  my @xforms = ();
	  my $last;
	  while (@forms) {
            my $f = shift @forms;
	    next if (@forms && lc($forms[0]) eq $f);
	    push(@xforms,$f) unless (##$f =~ /'/ &&
                                     (
				      ## for contractions
				      ($last && $last eq $f)
				      ## following is for np with contraction as in "d'Air France"
				      ## || (@forms && lc($forms[0]) eq $f)
                                     )
				    );
	    $last=$f;
	  }
	  my $forms = join(' ',@xforms);
	  $forms =~ s/^[ld]'//io;	# remove contracted det
	  $res = $forms;
	}
	return $res;
      };
      foreach my $entry (values %$appos) {
	my $base_form = $build_chain->($entry->{premier});
	my $appos_form = $build_chain->($entry->{appos});
	($base_form && $appos_form) or next;
	my $label = "<$base_form>\t<$appos_form>";
	my $u = $data->{appos}{$label} ||= [0];
##	print STDERR "%% $label\n";
	$u->[0]++;
	my @v = @$u;
	shift @v;
	(first {$seg eq $_->[0] && $sid eq $_->[1]} @v) and next;
	if (@$u < 12) {
	  push(@$u,[$seg,$sid]);
	} elsif (!int(rand(20))) {
	  # we may decide to keep the sentence and remove another one !
	  my $pos = 1+int(rand(12));
	  $u->[$pos][0] = $seg; 
	  $u->[$pos][1] = $sid; 
	}
      }
    }
  }
}

1;
