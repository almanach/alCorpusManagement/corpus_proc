#!/usr/bin/env perl

## a task for collecting the unknown words with their POS in PASSAGE files
## another variant should be done for DepXML files

use strict;
use List::Util qw{min max first};

sub td_collect {
  my ($data,$config,$task_data) = @_;
  if ($config->simple) {
#    Corpus::Passage::collect(\*STDIN,$data,'dummy','1');
    Corpus::DepXML::collect(\*STDIN,$data,'dummy','1');
  } else {
    my $corpus = $data->{corpus};
    my $where = $data->{where} || $config->results;
    my $archive = Corpus::Archive->new($where,$corpus);
    while (my $file=$archive->next) {
      my $name = $file->name;
      if ($name =~ /\.passage\.xml/) {
	my $cref = $file->get_content_by_ref;
	my $io = IO::String->new($$cref);
	my ($seg,$sid) = ($name =~ m{([\w-]+)\.E(\d+)});
	Corpus::Passage::collect($io,$data,$seg,$sid);
      } elsif ($name =~ /\.dep\.xml/) {
	my $cref = $file->get_content_by_ref;
	my $io = IO::String->new($$cref);
	my ($seg,$sid) = ($name =~ m{([\w-]+)\.E(\d+)});
	Corpus::DepXML::collect($io,$data,$seg,$sid);
      }
    }
  }
}

sub td_reduce {
  my ($data,$server) = @_;
  foreach my $w (keys %{$data->{uw}}) {
    my $u = $server->{data}{uw}{$w} ||= [0];
    my $v = $data->{uw}{$w};
    my $n = $u->[0] += $v->[0];
    my $nu = scalar(@$u);
    my $nv = scalar(@$v);
    my $xn = int(sqrt($n));
    for(my $i=1; ($i < $nv); $nu++, $i++) {
      if ($nu > 12) {
	next if (int(rand($xn)));
	my $pos = 1+int(rand(11)); # 11+1=12
	$u->[$pos] = $v->[$i];
      } else {
	push(@$u,$v->[$i]);
      }
    }
  }
}

sub td_display {
  my $data = shift;
  my $simple = shift;
#  my $min = $simple ? 0 : 10;
  my $min = 0;
  foreach my $w (sort {$data->{uw}{$b}[0] <=> $data->{uw}{$a}[0]} 
		 grep {$data->{uw}{$_}[0] > $min}
		 keys %{$data->{uw}}) {
    my @u = @{$data->{uw}{$w}};
    my @l = sort map {"$_->[0]:E$_->[1]"} @u[1 .. $#u];
    print "$w\t$u[0]\t@l\n";
  }
}

package Corpus::Passage;
use List::Util qw{min max first};
use XML::Twig;
use IO::String;

sub collect {
  my ($io,$data,$seg,$sid) = @_;

  my $refl=0;
  my $tokens={};

  my $ldata = $data->{uw} ||= {};

  while (my $line=<$io>) {

    if ($line =~ m{<T }) {
      my ($id) = $line =~ m{id="(.+?)"};
      my $content;
      if ($line =~ m{>(.*?)</T>}) {
	$content = $1;
      } else {
	$line = <$io>;
	chomp($line);
	$line =~ s/^\s+//og;
	$line =~ s/\s+$//og;
	$content = $line;
      }
      $tokens->{$id} = $content;
      next;
    }

    if ($line =~ m{<G } ) {
      $refl = 0;
      next;
    }

    if ($line =~ m{<W\s}) {
      my ($lemma) = $line =~ /lemma="(.*?)"/;
      $lemma eq "uw" or next;
      my ($pos) = $line =~ /pos="(.*?)"/;
      my ($form) = $line =~ /form="(.*?)"/;
      my ($toks) = $line =~ /tokens="(.*?)"/;
      $refl ||=  ($pos eq 'clr' && ($form eq 'se' || $form eq "s'"));
      $lemma = "se_$lemma" if ($pos eq 'v' && $refl);
      my $label = "${form}\t${pos}";
      my $u = $ldata->{$label} ||= [0];
      $u->[0]++;
      my @v = @$u;
      shift @v;
      (first {$seg eq $_->[0] && $sid eq $_->[1]} @v) and next;
      if (@$u < 12) {
	push(@$u,[$seg,$sid]);
      } elsif (!int(rand(20))) {
	# we may decide to keep the sentence and remove another one !
	my $pos = 1+int(rand(12));
	$u->[$pos][0] = $seg; 
	$u->[$pos][1] = $sid; 
      }
    }
  }
}

package Corpus::DepXML;
use Forest::Dependency::Reader;
use Forest::Tagger::Writer;
use XML::Twig;
use DepXML;
use DepXML::Query::Compile;
use List::Util qw{min max first};

sub collect {
  my ($io,$data,$seg,$sid) = @_;
  my $ldata = $data->{uw} ||= {};
  while (<$io>) {
    if (m{<node\s+.*lemma="uw"}) {
      my ($form) = / form="(.+?)"/;
      my ($cat) = / cat="(\S+?)"/;
      ($form && $cat) or next;
      my $label = "$form\t$cat";
      my $u = $ldata->{$label} ||= [0];
      $u->[0]++;
      my @v = @$u;
      shift @v;
      (first {$seg eq $_->[0] && $sid eq $_->[1]} @v) and next;
      if (@$u < 12) {
	push(@$u,[$seg,$sid]);
      } elsif (!int(rand(20))) {
	# we may decide to keep the sentence and remove another one !
	my $pos = 1+int(rand(12));
	$u->[$pos][0] = $seg; 
	$u->[$pos][1] = $sid; 
      }
    }
  }
}

sub collect_slow {
  my ($io,$data,$seg,$sid) = @_;
  my $cas = DepXML->new();
  eval {
    $cas->parse($io);
  };
  if ($@) {
    print "**pb\n";
    $cas->reset;
    return;
  }
  my $ldata = $data->{uw} ||= {};
  foreach my $node ($cas->find_nodes(dpath lemma_in_uw)) {
    my $lemma = $node->lemma;
#    print "lemma $lemma\n";
    $lemma eq 'uw' or next;
    my $form = $node->form;
    my $cat = $node->cat;
    ($form && $cat) or next;
    my $label = "$form\t$cat";
    my $u = $ldata->{$label} ||= [0];
    $u->[0]++;
    my @v = @$u;
    shift @v;
    (first {$seg eq $_->[0] && $sid eq $_->[1]} @v) and next;
    if (@$u < 12) {
      push(@$u,[$seg,$sid]);
    } elsif (!int(rand(20))) {
      # we may decide to keep the sentence and remove another one !
      my $pos = 1+int(rand(12));
      $u->[$pos][0] = $seg; 
      $u->[$pos][1] = $sid; 
    }
  }
}

1;
