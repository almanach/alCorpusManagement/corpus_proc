#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Carp;
use POSIX qw(strftime floor);
use POSIX ":sys_wait_h";

## use IPC::Run qw(start finish run pump timeout);

use AppConfig qw/:argcount :expand/;

##use File::Temp qw/tempfile/;
##use File::Basename;

##use XML::Parser;

my $config = AppConfig->new(
			    'verbose!'        => {DEFAULT => 1},
			    'results=f'       => {DEFAULT => 'results'},
			    'dest=f'          => {DEFAULT => 'concat'},
			    "corpus=f@"       => {DEFAULT => [] },
			    "dagdir=f"        => {DEFAULT => 'dag'},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "set=s"           => {DEFAULT => "frmg"},
			    "strict!"           => {DEFAULT => 0},
                            "compress!"        => {DEFAULT => 1},
                            "logdir=f",
			    "tagset=f",
			    "tmpdir=f" # to be used when untaring in $results is too  slow (if nfs dir)
			   );


my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;
my $dest = $config->dest;
my $dagdir = $config->dagdir;
my $dagext = $config->dagext;
my $annotset = $config->set;
my $log;
my $tmpdir = $config->tmpdir || $results;

my %corpora = ( 'general'     =>  '.sgml',
                'litteraire'  => '.txt',
                'mail'        => '',
                'medical'     => '.txt',
                'oral_delic'  => '.xml',
                'oral_elda'   => '.xml',
                'questions'   => '.sgml'
              );

my @corpus = @{$config->corpus};

if (!@corpus) {

  open(INFO,"<$dagdir/info.txt") || die "can't open '$dagdir/info.txt': $!";
  while(<INFO>) {
    next if /^\#/;
    my ($corpus) = split(/\s+/,$_);
    push(@corpus,$corpus)
  }
  close(INFO);
}

my $count = 0;

system("mkdir -p $dest");

if ($verbose) {
  $log = "$dest/concat.log";
  open(VERBOSE,">$log") or die "can't open log $log: $!";
  verbose("Activate log in '$log'");
}

verbose("Start EASY concat from $results");

foreach my $corpus (@corpus) {
  
  my ($base) = ($corpus =~ /^(\w+)_/);
  my $dagfile = "$dagdir/$corpus.${dagext}";
  my $destfile = "$dest/$corpus$corpora{$base}.ph2.xml";

  my $tar = 0;

  if (!-d "$results/$corpus" && (-f "$results/$corpus.tar.bz2")) {
    $tar = 1;
    verbose("untar from archive $results/$corpus");
    system("cd $tmpdir && tar xjf $corpus.tar.bz2");
  } elsif (!-d "$results/$corpus" && (-f "$results/$corpus.tar.gz")) {
    $tar = 1;
    verbose("untar from archive $results/$corpus in $tmpdir/");
    my $where = `pwd`;
    chomp $where;
    system("cd $tmpdir && tar xzf $where/$results/$corpus.tar.gz"); # warning: assume $results is relative
  }

  verbose("Missing corpus $corpus"), next unless (-d "$results/$corpus" || -d "$tmpdir/$corpus" );

  verbose("Concatenating $corpus");

  open(DEST,">$destfile") || die "can't open destfile '$destfile': $!";

  print DEST <<EOF;
<?xml version="1.0"  encoding="ISO-8859-1"?>
<DOCUMENT fichier="$corpus$corpora{$base}" id="$annotset" xmlns:xlink="http://www.w3.org/1999/xlink">
EOF

   my $tried = 0;
   my $added = 0;

  if (-r $dagfile) {
    open(DAG,"<$dagfile") || die "can't open dag for $corpus '$dagfile': $!";
  } elsif (-r "${dagfile}.bz2") {
    open(DAG,"bunzip2 -c ${dagfile}.bz2|") || die "can't open dag for $corpus '$dagfile': $!";
  } else {
    die "can't find DAG file '$dagfile'";
  }
  while (<DAG>) {
    next unless /^##\s*DAG\s+BEGIN/oi;
    my $line = <DAG>;
    my ($sid) = ($line =~ /E(\d+(?:\.\d+)?)F\d+/);
    ++$tried;
    my $forms = {};
    while(1) {
      while ($line =~ m{\G.*?(<F id="E\d+(?:\.\d+)?F\d+">[^<>]+</F>)}og) {
	my $form = $1;
	##	  print STDERR "Adding $form\n";
	my ($fid) = ($form =~ /id="(E\d+(?:\.\d+)?F\d+)"/o);
	$form =~ s/>([^<>]+)</>\n\t  $1\n\t</o;
	$form =~ s/\\(["?+])/$1/og; ## remove escaping added by dag2udag
	$form = "\t$form";
	$forms->{$fid} = $form;
      }
      $line = <DAG>;
      last if ($line =~ /^##\s*DAG\s+END/);
    }
    if (handle_sentence($corpus,$sid,$forms)) {
      $added++;
    } else {
      print DEST <<EOF;
<!-- added from dag -->
<E id="E$sid">
EOF
      foreach my $fid (sort {sort_fid($a,$b)} keys %$forms) {
      print DEST <<EOF;
$forms->{$fid}
EOF
      }
      print DEST <<EOF;
   <relations>
   </relations>
</E>
EOF
    }
  }
  close(DAG);

  print DEST <<EOF;
</DOCUMENT>
EOF

  close(DEST);
  system("bzip2 $destfile") if ($config->compress);

  if ($tar) {
    system("rm -Rf $tmpdir/$corpus");
  }

  my $ratio = 100 * ($added / $tried);
  my $msg = sprintf("(added %3i tried %3i ratio %6.2f%%)",$added,$tried,$ratio);
  verbose("Done $corpus $msg");
}

verbose("Done EASY concatenation");

sub sort_fid {
  my $a = shift;
  my $b = shift;
  my ($sida,$fida) = ($a =~ /E(\d+(?:\.\d+)?)F(\d+)/);
  my ($sidb,$fidb) = ($b =~ /E(\d+(?:\.\d+)?)F(\d+)/);
  return ($sida <=> $sidb) || ($fida <=> $fidb);
}

## print "FILES @easy\n";

sub handle_sentence {
  my ($corpus,$sid,$forms) = @_;
  my $l1 = floor($sid / 10000);
  my $l2 = floor(($sid - ($l1 * 10000)) / 1000);
  my $l3 = floor(($sid - ($l1 * 10000) - ($l2*1000)) / 100);
  my $file = "$results/$corpus/$l1/$l2/$l3/$corpus.E$sid.easy.xml";
  my $xfile = "$tmpdir/$corpus/$l1/$l2/$l3/$corpus.E$sid.passage.xml";
  if (-r $file && -s $file) {
    open(FILE,"<$file") || die "can't open $file: $!";
  } elsif (-r "$file.bz2" && -s "$file.bz2") {
    $file .= ".bz2";
    open(FILE,"bunzip2 -c $file|") || die "can't open $file: $!";
  } elsif (-r $xfile && -s $xfile) {
    open(FILE,"<$xfile") || die "can't open $xfile: $!";
  } elsif (-r "$xfile.bz2" && -s "$xfile.bz2") {
    $xfile .= ".bz2";
    open(FILE,"bunzip2 -c $xfile|") || die "can't open $xfile: $!";
  } else {
    verbose("*** missing $corpus:$sid");
    return 0;
  }

  my $in = 0;
  my $in_f = 0;
  my $out = '';
  my $in_group;
  my @fids = sort {sort_fid($a,$b)} keys %$forms;
  while (<FILE>) {
    $in = 1 if (m{^\s*<E .*>});
    next unless ($in);
    if (m{^\s*<Groupe.*>}) {
      $in_group = $_;
      next;
    }
    if (m{^\s*<F.*>} && /id="(E\d+(?:\.\d+)?F\d+)"/) {
      my $fid=$1;
      $in_f = 1;
      while (@fids && sort_fid($fids[0],$fid) == -1) {
	my $tmp = shift @fids;
	$out .= <<EOF;
    <!-- added form from dag -->
$forms->{$tmp}
EOF
      }
      shift @fids;
    }
    $in_f = 0 if (m{^\s*</F>});
    if ($in_f) {
      $_ =~ s/&/&amp;/og 
    }
    if (m{^\s*<relations>} || m{^\s*</E>}) {
      while(@fids) {
	## emits pending missing forms before relations
	my $tmp = shift @fids;
	$out .= <<EOF;
    <!-- added from dag -->
$forms->{$tmp}
EOF
      }
    }
    if ($in_group) {
      $out .= $in_group;
      undef $in_group;
    }
    $out .= $_;
    $in = 0, last if m{^\s*</E>};
  }
  close(FILE);
  if ($in || $in_f) {
    verbose("*** bad XML file $corpus:$sid");
    return 0;
  } else {
    print DEST $out;
  }
  return 1;
}

sub verbose {
  return unless ($config->verbose);
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
  print VERBOSE "$date @_\n" if (defined $log);
}

=head1 NAME

easyconcat.pl -- Concat sentences results into one big file per corpus

=head1 SYNOPSIS 	 
  	 
./easyconcat.pl

The resulting files are sent to directory F<concat> [default]. To send
the files to another directory, use option --dest.

./easyconcat.pl --dest=somewhere

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
