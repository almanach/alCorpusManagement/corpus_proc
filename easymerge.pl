#!/usr/bin/env perl

# Copyright (C) 2007, 2008, 2009, 2010, 2011 by Eric de la Clergerie -- INRIA

# merge ref and hyp EASy information found in *.ph2.xml files
# emit various log information

use strict;
use Carp;
use Data::Dumper;
use XML::Twig;
use CGI::Pretty qw/:standard *table *ul/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
			    "ref|r=f"  => {DEFAULT => ""},
			    "hyp|h=f"  => {DEFAULT => "./"},
			    "html=f",
			    "nohyp!"   => {DEFAULT => 0}
			   );

my $conffile = 'merge.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my @base = @ARGV;

@base = qw{general_elda
	   general_lemonde
	   general_mlcc
	   general_senat
	   litteraire_1
	   litteraire_2
	   litteraire_3
	   litteraire_4
	   mail_9
	   mail_10
	   medical_1
	   medical_2
	   medical_3
	   medical_4
	   medical_6
	   oral_delic_1
	   oral_delic_2
	   oral_delic_3
	   oral_delic_4
	   oral_delic_5
	   oral_delic_6
	   oral_delic_7
	   oral_delic_8
	   oral_delic_9
	   questions_amaryllis
	   questions_trec
	} unless (@base);

my $verbose = $config->verbose();
my $refdir  = $config->ref();
my $hypdir  = $config->hyp();
my $html    = $config->html() || 0;
my $nohyp   = $config->nohyp();

my $suffix  = $nohyp ? 'ref' : 'merged';

sub empty_stats {
  return {all => 0, inter => 0};
}

sub empty_stats2 {
  return { hyp => empty_stats(), ref => empty_stats() };
}

my @groups = qw/GA GN GP GR NV PV/;
my @rels   = qw/SUJ-V AUX-V COD-V CPL-V MOD-V COMP ATB-SO MOD-N MOD-A MOD-R MOD-P COORD APPOS JUXT/;

my $stats  = { groups => { map {$_ => empty_stats2()} @groups }, 
	       rels   => { map {$_ => empty_stats2()} @rels },
	       whole  => { groups  => empty_stats2(),
			   rels    => empty_stats2(),
		       },
	       corpus => {
	       },
	       sentences => {
			   },
	       confusion => { groups => {},
			      rels => {}
			    },
	       full => { groups => empty_stats2(),
			 rels => empty_stats2()
		       },
	       robust => { groups => empty_stats2(),
			   rels => empty_stats2()
			 },
	    };

my $catalogue = {};

## Emit relations as edges
my @lines = ();

if ($html) {
  open(HTML,">$html") || die "can't open $html: $!";
  print HTML header,
    start_html('EASy files and stats'),
      h1('EASy files and stats');
}

foreach my $base (@base) {
  $stats->{corpus}{$base} = 
    {
     groups => { map {$_ => empty_stats2()} @groups },
     rels => { map {$_ => empty_stats2()} @rels }
    };
  handle_corpus($base);
}

print HTML ul(li([map( a({href=>"$_.$suffix.html"},$_),  @base)]));

emit_stats() unless ($nohyp);
emit_catalogue() unless ($nohyp);

if ($html) {
  print HTML end_html;
  close(HTML);
}

sub find_hyp_file {
  my $base = shift;
  my @ext1 = ("",qw{.sgml .txt .xml});
  my @ext2 = ("",qw{.ph2});
  my @ext3 = (qw{.xml .xml.bz2});
  foreach my $ext1 (@ext1) {
    foreach my $ext2 (@ext2) {
      foreach my $ext3 (@ext3) {
	my $f = "$hypdir/$base$ext1$ext2$ext3";
	return $f if (-r $f);
      }
    }
  }
}

sub handle_corpus {
  my $base = shift;
  my $reffile = "$refdir$base.xml";

  print "** ref: $base not found\n", return unless (-r $reffile);

  my $hypfile;

  unless ($nohyp) {
    $hypfile = find_hyp_file($base);
    print "** hyp: $base not found\n", return unless ($hypfile);
    print "handling base=$base ref=$reffile hyp=$hypfile\n";
  }

  my $ref = load_easy($base,$reffile,'ref');# info about references
  my $hyp = load_easy($base,$hypfile,'hyp');# info about hypotheses
  diff($base,$ref,$hyp);
  emit_new_xml($ref,$hyp);
}

#exit;
#print Dumper($hyp->{info}{s});

sub load_easy {
  my $base = shift;
  my $file = shift;
  my $kind = shift;
  my $xml = XML::Twig->new(
			   twig_handlers => {
					     'E' => \&sentence_handler,
					     'hypconsts' => sub { $_->delete },
					     'hyprels' => sub { $_->delete },
					    },
			   pretty_print => 'indented',
			  );
  $xml->{info} = { s         => {}, 
		   f         => {},
		   groups    => {},
		   relations => {},
		   base      => $base,
		   kind      => $kind,
		   stats     => { groups => {},
				  rels => {}
			      }
		 };
  if (-r $file) {
    if ($file =~ /\.bz2$/) {
      open(FILE,"bzcat $file|") || die "can't open $file: $!";
    } else {
      open(FILE,"<$file") || die "can't open $file: $!";
    }
    print "Handling '$file'\n";
    $xml->parse(\*FILE);
    close(FILE);
  }
  return $xml;
}

sub sentence_handler {
  my $twig      = shift;
  my $e         = shift;
  my $info      = $twig->{info};
  my $base      = $info->{base};
  my $mode      = $info->{kind};
  my $sid       = $e->att('id');
  my $fs        = {};
  my $groups    = {};
  my $relations = {};
  print "$base: ** $mode: read sentence $sid\n";
  print "$base: ** $mode: duplicate sentence $sid\n" if (exists $twig->{info}{s}{$sid});
  $stats->{sentences}{$base}{$sid} ||= { groups => empty_stats2(),
					rels => empty_stats2()
				      };  
  my $sentence = $info->{s}{$sid}
    = { sid       => $sid,
	f         => $fs,
	groups    => $groups,
	relations => $relations,
	xml       => $e,
	stats     => { groups => {},
		       rels => {},
		   }
      };
  if (defined $e->att('mode')) {
    my $smode = $e->att('mode');
    $smode eq 'corrected' and $smode = 'full';
    $sentence->{mode} = $smode;
  }
  my $const = $e->first_child('constituants');
  unless (defined $const) {
    $const = XML::Twig::Elt->new('constituants');
    foreach my $x ($e->children) {
      $x->move( last_child => $const ) unless ($x->gi eq 'relations');
    }
    $const->paste( first_child => $e);
  }
  foreach my $f ($const->children('F')) {
    my $fid = $f->att('id');
    print "$base: $mode: read form $fid\n";
    print "$base: ** $mode: bad form id $fid\n" unless ($fid =~ /E\d+F\d+/);
    print "$base: ** $mode: duplicate form $fid\n" if (exists $info->{f}{$fid});
    $fs->{$fid} 
      =  $info->{f}{$fid} 
	= { fid     => $fid,
	    xml     => $f, 
	    r       => {},
	    content => $f->text()
	  };
  }
  foreach my $g ($const->children('Groupe')) {
      my $gid = $g->att('id');
      print "$base: ** $mode: missing gid in sentence $sid\n" unless (defined $gid);
      print "$base: $mode: read group $gid\n";
      print "$base: ** $mode: bad group id $gid\n" unless ($gid =~ /E\d+G\d+/);
      print "$base: ** $mode: duplicate group $gid\n" if (exists $info->{groups}{$gid});
      my $ginfo = $groups->{$gid} 
	= $info->{groups}{$gid}
	  = { gid  => $gid, 
	      f    => {}, 
	      xml  => $g, 
	      r    => {},
	      type => $g->att('type')
	    };
      foreach my $f ($g->children('F')) {
	my $fid = $f->att('id');
	print "$base: $mode: read form $fid\n";
	print "$base: ** $mode: bad form id $fid\n" unless ($fid =~ /E\d+F\d+/);
	print "$base: ** $mode: duplicate form $fid\n" if (exists $info->{f}{$fid});
	$ginfo->{f}{$fid} = 1;
	$sentence->{f}{$fid} 
	  = $info->{f}{$fid}
	    = { fid     => $fid, 
		xml     => $f, 
		gid     => $gid,
		r       => {},
		content => $f->text()
	      };
      }
  }
  if (!defined $e->first_child('relations')) {
      print STDERR "$base: ** $mode: missing relations in $sid\n";
  } else {
  foreach my $r ($e->first_child('relations')->children('relation')) {
      my $rid = $r->att('id');
      print "$base: $mode: read relation $rid\n";
      print "$base: ** $mode: bad relation id $rid\n" unless ($rid =~ /E\d+R\d+/);
      print "$base: ** $mode: duplicate relation $rid\n" if (exists $info->{relations}{$rid});
      my $t = $r->att('type');
      my $rinfo = $relations->{$rid} = $info->{relations}{$rid}
	= { rid   => $rid,
	    xml   => $r,  
	    const => {},
	    type  => $t
	  };
      my %intra   = ();
      my $hasform = 0;
      my $first   = 1000;
      my $last    = -1;
      my $comp    = 0;
      foreach my $role ($r->children()) {
	next unless (defined $role->att('xlink:href'));
	my $cid = $role->att('xlink:href');
	my $tag = $role->gi;
	$rinfo->{const}{$tag} = $cid;
	my $cinfo;
	if ($cid =~ /^(E\d+)[FG]\d+/o) {
	  my $lsid = $1;
	  if ($lsid ne $sid) {
	    print "$base: ** $mode: cross sentence relation $rid: type=$t $tag=$cid\n";
	    $rinfo->{cross} = 1;
	  }
	  if (exists $info->{f}{$cid}) {
	    $comp++;
	    $info->{f}{$cid}{r}{$t}{$tag}{$rid} = 1;
	    if (exists $info->{f}{$cid}{gid}) {
	      $hasform = 1;
	      $intra{$info->{f}{$cid}{gid}} = 1;
	    }
	    my ($x) = ($cid =~ /F(\d+)/);
	    $first=$x if ($x < $first);
	    $last=$x if ($x > $last);
	  } elsif (exists $info->{groups}{$cid}) {
	    $comp++;
	    $info->{groups}{$cid}{r}{$t}{$tag}{$rid} = 1;
	    my @l = keys %{$info->{groups}{$cid}{f}};
	    my $l = @l;
	    foreach my $fid (@l) {
	      $info->{f}{$fid}{r}{$t}{$tag}{$rid} = $l;
	      my ($x) = ($fid =~ /F(\d+)/);
	      $first=$x if ($x < $first);
	      $last=$x if ($x > $last);
	    }
	    $intra{$cid} = 1;
	  }
	} else {
	  print "$base: ** $mode: bad relation locator $rid: type=$t $tag=$cid\n";
	}
      }
      $rinfo->{first}  = $first;
      $rinfo->{last}   = $last;
      $rinfo->{nroles} = $comp;
      if (0 & $hasform && (keys %intra > 1)) {
	# inter group relation with a form belonging to a group
	# => we remap F to corresponding G
	print "$base: ** $mode: remapping relation $rid type=$t\n";
	foreach my $role (keys %{$rinfo->{const}}) {
	  my $cid = $rinfo->{const}{$role};
	  if ($cid =~ /^(E\d+)F/o) {
	    my $lsid = $1;
	    if (exists $info->{f}{$cid}{gid}) {
	      my $ncid = $info->{f}{$cid}{gid};
	      print "$base: ** $mode: remapping relation $rid type=$t $role=$cid->$ncid\n";
	      $rinfo->{const}{$role} = $ncid;
	      $info->{f}{$ncid}{r}{$t}{$role}{$rid} = 1;
	    }
	  }
	}
      }
    }
}
}

sub xid {
  my $id = shift;
  $id =~ /(\d+)$/og;
  return $1;
}

sub sentence_map {
  my $base   = shift;
  my $s      = shift;
  my $kind   = shift;
  my $map    = shift;
  my $prefix = "Found";
  if ($map) {
    set_map($s,$map->{sid});
    set_map($map,$s->{sid});
  } else {
    set_map($s,0);
    $prefix = "** $kind: Extra";
  }
  print "$base: $prefix sentence $s->{sid}\n";
}

sub form_map {
  my $base   = shift;
  my $f      = shift;
  my $kind   = shift;
  my $map    = shift;
  my $prefix = "Found";
  if ($map) {
    set_map($f,$map->{fid});
    set_map($map,$f->{fid});
  } else {
    set_map($f,0);
    $prefix = "** $kind: Extra";
  }
  print "$base: $prefix form $f->{fid} '$f->{content}'\n";
}

sub group_map {
  my $base   = shift;
  my $sid    = shift;
  my $g      = shift;
  my $kind   = shift;
  my $map    = shift;
  my $kind2  = shift;
  my $smode = shift;
  my $t      = $g->{type};
  my @fs     = sort {xid($a) <=>xid xid($b)} keys %{$g->{f}};
  my $prefix = "Found";
  my $remap  = "";

  $stats->{corpus}{$base}{groups}{$kind}{all}++;
  $stats->{groups}{$t}{$kind}{all}++;
  $stats->{whole}{groups}{$kind}{all}++;

  $smode and $stats->{$smode}{groups}{$kind}{all}++;
  
  $stats->{sentences}{$base}{$sid}{groups}{$kind}{all}++;

  if ($map) {
    set_map($g,$map->{gid}); 
    set_map($map,$g->{gid}); 
    $remap = "->$g->{map}";

    $stats->{corpus}{$base}{groups}{$kind}{inter}++;
    $stats->{groups}{$t}{$kind}{inter}++;
    $stats->{whole}{groups}{$kind}{inter}++;
    $smode and $stats->{$smode}{groups}{$kind}{inter}++;
    $stats->{corpus}{$base}{groups}{$kind2}{all}++;
    $stats->{groups}{$t}{$kind2}{all}++;
    $stats->{whole}{groups}{$kind2}{all}++;
    $smode and $stats->{$smode}{groups}{$kind2}{all}++;
    $stats->{corpus}{$base}{groups}{$kind2}{inter}++;
    $stats->{groups}{$t}{$kind2}{inter}++;
    $stats->{whole}{groups}{$kind2}{inter}++;
    $smode and $stats->{$smode}{groups}{$kind2}{inter}++;

    $stats->{sentences}{$base}{$sid}{groups}{$kind}{inter}++;
    $stats->{sentences}{$base}{$sid}{groups}{$kind2}{all}++;
    $stats->{sentences}{$base}{$sid}{groups}{$kind2}{inter}++;

  } else {
    set_map($g,0);
    $g->{gid} =~ /^(E\d+)/;
    $catalogue->{$kind}{groups}{$t}{$base}{$1} = 1;
    $prefix = "** $kind: Extra";
  }
  print "$base: $prefix group $g->{gid}$remap type=$t f={@fs}\n";
}

sub relation_map {
  my $base    = shift;
  my $sid     = shift;
  my $r       = shift;
  my $kind    = shift;
  my $map     = shift;
  my $kind2   = shift;
  my $overlap = shift || {};
  my $smode = shift || "";
  my $prefix  = "Found";
  my $t       = $r->{type};
  my $remap   = "";

  $stats->{corpus}{$base}{rels}{$kind}{all}++;
  $stats->{rels}{$t}{$kind}{all}++;
  $stats->{whole}{rels}{$kind}{all}++;
  $smode and $stats->{$smode}{rels}{$kind}{all}++;

  $stats->{sentences}{$base}{$sid}{rels}{$kind}{all}++;

  if ($map) {
    set_map($r,$map->{rid});
    set_map($map,$r->{rid});
    $remap = "->$r->{rid}";

    set_overlap($r,$overlap);
    set_overlap($map,$overlap);

    $stats->{corpus}{$base}{rels}{$kind}{inter}++;
    $stats->{rels}{$t}{$kind}{inter}++;
    $stats->{whole}{rels}{$kind}{inter}++;
    $smode and $stats->{$smode}{rels}{$kind}{inter}++;
    $stats->{corpus}{$base}{rels}{$kind2}{all}++;
    $stats->{rels}{$t}{$kind2}{all}++;
    $stats->{whole}{rels}{$kind2}{all}++;
    $smode and $stats->{$smode}{rels}{$kind2}{all}++;
    $stats->{corpus}{$base}{rels}{$kind2}{inter}++;
    $stats->{rels}{$t}{$kind2}{inter}++;
    $stats->{whole}{rels}{$kind2}{inter}++;
    $smode and $stats->{$smode}{rels}{$kind2}{inter}++;
    $stats->{sentences}{$base}{$sid}{rels}{$kind}{inter}++;    
    $stats->{sentences}{$base}{$sid}{rels}{$kind2}{inter}++;    
    $stats->{sentences}{$base}{$sid}{rels}{$kind2}{all}++;  
    
  } else {
    set_map($r,0);
    $prefix = "** $kind: Extra";
    $r->{rid} =~ /^(E\d+)/;
    $catalogue->{$kind}{relations}{$t}{$base}{$1} = 1;
  }
  print "$base: $prefix relation $r->{rid}$remap type=$t";
  foreach my $k (keys %{$r->{const}}) {
    print " $k=$r->{const}{$k}";
  }
  print "\n";
}

sub set_map {
  my $x   = shift;
  my $map = shift;
  $x->{map} = $map;
  $x->{xml}->set_att('map',$map) if ($map);
}

sub set_overlap {
  my $r = shift;
  my $overlap = shift;
  $r->{overlap} = $overlap;
  my $xml = $r->{xml};
  foreach my $role (keys %$overlap) {
    my $xrole = $xml->first_child($role);
    $xrole->set_att(overlap => sprintf("%.0f",100*$overlap->{$role}));
  }
}

sub content_compare {		
  # assume $f2 is hyp
  my $f1 = shift;
  my $f2 = shift;
  my $c1 = $f1->{content};
  my $c2 = $f2->{content};
  return 1 if ($c1 eq $c2);
  $c2 =~ s/ /_/og;
  if ($c1 eq $c2) {
    $f2->{content} = $f1->{content};
    return 1;
  }  
  $c1 = lc($c1);
  $c2 = lc($c1);
  if ($c1 eq $c2) {
    $f2->{content} = $f1->{content};
    return 1;
  } 
  print "** form content mismatch $f1->{fid}='$f1->{content}' $f2->{fid}='$f2->{content}'\n";
  return 0;
}

sub diff {
  my $base  = shift;
  my $x1    = shift;
  my $x2    = shift;
  my $info1 = $x1->{info};
  my $info2 = $x2->{info};
  my $k1    = $info1->{kind};
  my $k2    = $info2->{kind};

  foreach my $sid (sort {xid($a) <=> xid($b)} keys %{$info1->{s}}) {
    my $s1 = $info1->{s}{$sid};
    sentence_map($base,$s1,$k1,0), next unless (exists $info2->{s}{$sid});
    my $s2 = $info2->{s}{$sid};
    sentence_map($base,$s1,$k1,$s2);

    my $smode = $s2->{mode} || $s1->{mode} || "";

    foreach my $fid (sort {xid($a) <=> xid($b)} keys %{$s1->{f}}) {
      my $f1 = $s1->{f}{$fid};
      form_map($base,$f1,$k1,0,$smode), next unless (exists $s2->{f}{$fid});
      my $f2 = $s2->{f}{$fid};
      form_map($base,$f1,$k1,0,$smode), next unless (content_compare($f1,$f2));
      form_map($base,$f1,$k1,$f2,$smode);
    }

    GROUP: foreach my $gid (sort {xid($a) <=> xid($b)} keys %{$s1->{groups}}) {
      my $g1   = $s1->{groups}{$gid};
      my $t1   = $g1->{type};
      my @fs1  =  sort {xid($a) <=>xid xid($b)} keys %{$g1->{f}};
      my $gid2 = 0;
      my $fail = sub { my $t2 = shift;
		       $stats->{confusion}{groups}{$t1}{$t2}++;
		       group_map($base,$sid,$g1,$k1,0,$k2,$smode);
		       next GROUP;
		     };
      foreach my $fid (@fs1) {
	my $f2 = $s2->{f}{$fid};
	$fail->('void') unless (exists $f2->{gid});
	my $lgid2 = $f2->{gid};
	$fail->('overlap') if ($gid2 && ($gid2 ne $lgid2));
	$gid2 ||= $lgid2;
      }
      $fail->('void') unless ($gid2 && exists $s2->{groups}{$gid2});
      my $g2 = $s2->{groups}{$gid2};
      $fail->($g2->{type}) unless($g1->{type} eq $g2->{type});
      foreach my $fid (keys %{$g2->{f}}) {
	$fail->($t1) unless (exists $g1->{f}{$fid});
      }
      group_map($base,$sid,$g1,$k1,$g2,$k2,$smode);
    }

  REL: foreach my $rid (sort {xid($a) <=> xid($b)} keys %{$s1->{relations}}) {
      my $r1   = $s1->{relations}{$rid};
      my $t1   = $r1->{type};
##      my $confusion = $stats->{confusions}{rels}{$t1} ||= {};
      my $fail = sub { my $t2 = shift;
		       $stats->{confusions}{rels}{$t1}{$t2 || 'misc'}++;
		       relation_map($base,$sid,$r1,$k1,0,$k2,{},$smode); 
		       next REL;
		     };
      my %rid2 = ();
      foreach my $k (keys %{$r1->{const}}) {
	my $xid   = $r1->{const}{$k};
	my %lrid2 = ();
	next unless ($xid =~ /^(E\d+)/);
	my $lsid = $1;
	$fail->() unless (exists $info1->{s}{$lsid});
	$fail->() unless (exists $info2->{s}{$lsid});
	if ($xid =~ /F\d+$/) {
	  $fail->() unless (exists $info1->{f}{$xid}{map});
	  my $fid2 = $info1->{f}{$xid}{map};
	  $fail->('void') unless (exists $info2->{f}{$fid2});
	  $fail->('void') unless (exists $info2->{f}{$fid2}{r}{$t1});
	  $fail->($t1) unless (exists $info2->{f}{$fid2}{r}{$t1}{$k});
	  my $tmp = $info2->{f}{$fid2}{r}{$t1}{$k};
	  foreach my $rid2 (keys %{$tmp}) {
	    my $x = 2 / (1+$tmp->{$rid2});
	    $lrid2{$rid2} = $x if ($x >= 0.25);
	  };
	} elsif ($xid =~ /G\d+$/) {
	  $fail->() unless (exists $info1->{groups}{$xid});
	  my @l = keys %{$info1->{groups}{$xid}{f}};
	  my $l = scalar(@l);
	  my %length2 = ();
	  foreach my $lfid (@l) {
	    next unless (exists $info2->{f}{$lfid}{r});
	    next unless (exists $info2->{f}{$lfid}{r}{$t1});
	    next unless (exists $info2->{f}{$lfid}{r}{$t1}{$k});
	    my $tmp = $info2->{f}{$lfid}{r}{$t1}{$k};
	    foreach my $rid2 (keys %{$tmp}) {
	      $lrid2{$rid2} += 2;
	      $length2{$rid2} = $tmp->{$rid2};
	    }
	  }
	  foreach my $rid2 (keys %lrid2) {
	    my $x = $lrid2{$rid2} / ($l + $length2{$rid2});
	    if ($x >= .25) {
	      $lrid2{$rid2} = $x;
	    } else {
	      delete $lrid2{$rid2};
	    }
	  }
	} else {
	  next;
	}
	$fail->('void') unless (%lrid2);
	if (%rid2) {
	  foreach my $xid2 (keys %rid2) {
	    delete $rid2{$xid2}, next unless (exists $lrid2{$xid2});
	    $rid2{$xid2}{$k} = $lrid2{$xid2};
	  }
	} else {
	  foreach my $xid2 (keys %lrid2) {
	    $rid2{$xid2}{$k} = $lrid2{$xid2};
	  }
	}
	$fail->($t1) unless (%rid2);
      }
      
      my @rid2 = keys %rid2;
      my $rid2 = $rid2[0];
      my $r2   = $s2->{relations}{$rid2};
      relation_map($base,$sid,$r1,$k1,$r2,$k2,$rid2{$rid2},$smode);
    }
  }
   
  foreach my $sid (sort {xid($a) <=> xid($b)} keys %{$info1->{s}}) {
      my $s1 = $info1->{s}{$sid};
      sentence_map($base,$s1,$k1,0) unless (exists $s1->{map});
      my $smode = $s1->{mode} | "";
      foreach my $fid (sort {xid($a) <=> xid($b)} keys %{$s1->{f}}) {
	my $f1=$s1->{f}{$fid};
	form_map($base,$f1,$k1,0,$smode) unless (exists $f1->{map});
      }
      foreach my $gid (sort {xid($a) <=> xid($b)} keys %{$s1->{groups}}) {
	my $g1 = $s1->{groups}{$gid};
	next if (exists $g1->{map});
	group_map($base,$sid,$g1,$k1,0,$k1,$smode);
##	$stats->{confusion}{groups}{void}{$g1->{type}};
      }
      foreach my $rid (sort {xid($a) <=> xid($b)} keys %{$s1->{relations}}) {
	my $r1 = $s1->{relations}{$rid};
	relation_map($base,$sid,$r1,$k1,0,$k1,{},$smode) unless (exists $r1->{map});
      }
    }

  foreach my $sid (sort {xid($a) <=> xid($b)} keys %{$info2->{s}}) {
      my $s2 = $info2->{s}{$sid};
      my $smode = $s2->{mode} || "";
      sentence_map($base,$s2,$k2,0) unless (exists $s2->{map});
      foreach my $fid (sort {xid($a) <=> xid($b)} keys %{$s2->{f}}) {
	my $f2=$s2->{f}{$fid};
	form_map($base,$f2,$k2,0,$smode) unless (exists $f2->{map});
      }
      foreach my $gid (sort {xid($a) <=> xid($b)} keys %{$s2->{groups}}) {
	my $g2 = $s2->{groups}{$gid};
	group_map($base,$sid,$g2,$k2,0,$k1,$smode) unless (exists $g2->{map});
      }
      foreach my $rid (sort {xid($a) <=> xid($b)} keys %{$s2->{relations}}) {
	my $r2 = $s2->{relations}{$rid};
	relation_map($base,$sid,$r2,$k2,0,$k1,{},$smode) unless (exists $r2->{map});
      }
    }
  
  foreach my $sid (keys %{$info1->{s}}) {
    my $s1 = $info1->{s}{$sid};
    print "$base: $k1: unseen sentence $sid\n" unless (exists $s1->{map});
    foreach my $fid (keys %{$s1->{f}}) {
      my $f1=$s1->{f}{$fid};
      print "$base: $k1: unseen form $fid\n" unless (exists $f1->{map});
    }
    foreach my $gid (keys %{$s1->{groups}}) {
      my $g1=$s1->{groups}{$gid};
      print "$base: $k1: unseen group $gid\n" unless (exists $g1->{map});
    }
    foreach my $rid (keys %{$s1->{relations}}) {
      my $r1=$s1->{relations}{$rid};
      print "$base: $k1: unseen relation $rid\n" unless (exists $r1->{map});
    }
  }
  
  foreach my $sid (keys %{$info2->{s}}) {
    my $s2 = $info2->{s}{$sid};
    print "$base: $k2: unseen sentence $sid\n" unless (exists $s2->{map});
    foreach my $fid (keys %{$s2->{f}}) {
      my $f2=$s2->{f}{$fid};
      print "$base: $k2: unseen form $fid\n" unless (exists $f2->{map});
    }
    foreach my $gid (keys %{$s2->{groups}}) {
      my $g2=$s2->{groups}{$gid};
      print "$base: $k2: unseen group $gid\n" unless (exists $g2->{map});
    }
    foreach my $rid (keys %{$s2->{relations}}) {
      my $r2=$s2->{relations}{$rid};
      print "$base: $k2: unseen relation $rid\n" unless (exists $r2->{map});
    }
  }
  
}

sub fmeasure {
  my ($inter, $hyp, $ref) = @_;
  return 2 * $inter / (($hyp + $ref) || 1);
}

sub get_stats {
  my $stats    = shift;
  my $hypall   = $stats->{hyp}{all};
  my $hypinter = $stats->{hyp}{inter};
  my $refall   = $stats->{ref}{all};
  my $refinter = $stats->{ref}{inter};
  print "** stats: mismatch hypinter=$hypinter refinter=$refinter\n" unless ($hypinter == $refinter);
  my $prec = $hypinter / ($hypall || 1); 
  my $rec  = $refinter / ($refall || 1);
  my $f    = fmeasure($hypinter,$hypall,$refall);
  return (100*$prec,100*$rec,100*$f,$refall);
}

sub print_stats {
  my $prefix = shift;
  my ($prec,$rec,$f) = @_;
  printf("$prefix prec=%5.2f%% rec=%5.2f%% f=%5.2f%%\n", $prec, $rec, $f);
}

sub html_stats {
  my $h = shift;
  my ($prec,$rec,$f,$ref) = @_;
  return td([$h,,map(sprintf("%.2f%%",$_),$prec,$rec,$f),$ref]);
}

sub emit_stats {
  print HTML h2("Statistics") if ($html);
  # emit global
  my ($gprec,$grec,$gf,$gall) = get_stats($stats->{whole}{groups});
  my ($rprec,$rrec,$rf,$rall) = get_stats($stats->{whole}{rels});

  my ($full_gprec,$full_grec,$full_gf,$full_gall) = get_stats($stats->{full}{groups});
  my ($full_rprec,$full_rrec,$full_rf,$full_rall) = get_stats($stats->{full}{rels});

  my ($robust_gprec,$robust_grec,$robust_gf,$robust_gall) = get_stats($stats->{robust}{groups});
  my ($robust_rprec,$robust_rrec,$robust_rf,$robust_rall) = get_stats($stats->{robust}{rels});

  print_stats("stats: groups",$gprec,$grec,$gf);
  print_stats("stats: relations",$rprec,$rrec,$rf);
  print HTML table({-border => 1},
		   Tr(
		      [th(['','precision','recall','f','#ref']),
		       html_stats('groups',$gprec,$grec,$gf,$gall),
		       html_stats('relations',$rprec,$rrec,$rf,$rall),

		       html_stats('full groups',$full_gprec,$full_grec,$full_gf,$full_gall),
		       html_stats('full relations',$full_rprec,$full_rrec,$full_rf,$full_rall),

		       html_stats('robust groups',$robust_gprec,$robust_grec,$robust_gf,$robust_gall),
		       html_stats('robust relations',$robust_rprec,$robust_rrec,$robust_rf,$robust_rall),

		      ])) if $html;
  # emit per corpus
  print HTML h3("Statistics per corpus") if ($html);
  my @info = ();
  foreach my $base (@base) {
    my ($prec,$rec,$f,$all) = get_stats($stats->{corpus}{$base}{groups});
    push(@info,html_stats($base,$prec,$rec,$f,$all)) if $html;
    print_stats("stats $base: groups",$prec,$rec,$f);
  }
  print HTML table({-border => 1},
		   Tr([th(['groups per corpus','precision','recall','f','#ref']),
		       @info])) if $html;
  @info = ();
  foreach my $base (@base) {
    my ($prec,$rec,$f,$all) = get_stats($stats->{corpus}{$base}{rels});
    push(@info,html_stats($base,$prec,$rec,$f,$all)) if $html;
    print_stats("stats $base: rels",$prec,$rec,$f);
  }
  if ($html) {
    print HTML table({-border => 1},
		     Tr([th(['rels per groups','precision','recall','f','#ref']),
			 @info]));
    @info  = ();
    # emit per group type
    print HTML h3("Statistics per groups");
  }
  foreach my $t (@groups) {
    my ($prec,$rec,$f,$all) = get_stats($stats->{groups}{$t});
    push(@info,html_stats($t,$prec,$rec,$f,$all)) if $html;
    print_stats("stats: groups type=$t",$prec,$rec,$f);
  }
  if ($html) {
    print HTML table({-border => 1},
		     Tr([th(['groups','precision','recall','f','#ref']),
			 @info]));
    print HTML h3("Statistics per relations");
    @info  = ();
  }
  # emit per relation type
  foreach my $t (@rels) {
    my ($prec,$rec,$f,$all) = get_stats($stats->{rels}{$t});
    push(@info,html_stats($t,$prec,$rec,$f,$all)) if $html;
    print_stats("stats: relations type=$t",$prec,$rec,$f);
  }
  print HTML table({-border => 1},
		   Tr([th(['groups','precision','recall','f','#ref']),
		       @info])) if ($html);
  ## emit confusion table for the groups
  if ($html) {
    my @info = ();
    foreach my $t1 ('void','overlap',@groups) {
      my @td = ();
      foreach my $t2 ('void','overlap',@groups) {
	push(@td,$stats->{confusion}{groups}{$t1}{$t2} || 0);
      }
      push(@info,td([$t1,@td]));
    }
    print HTML h3("Confusion table");
    print HTML table({-border => 1},
		     Tr([th(['','void','overlap',@groups]),@info]));
    @info = ();
    foreach my $t1 ('void','misc',@rels) {
      my @td = ();
      foreach my $t2 ('void','misc',@rels) {
	push(@td,$stats->{confusion}{rels}{$t1}{$t2} || 0);
      }
      push(@info,td([$t1,@td]));
    }
    print HTML table({-border => 1},
		     Tr([th(['','void','misc',@rels]),@info]));
  }
  ## emit per sentence
  foreach my $base (@base) {
    foreach my $sid (sort {xid($a) <=> xid($b)} keys %{$stats->{sentences}{$base}}) {
      my ($gprec,$grec,$gf,$gall) = get_stats($stats->{sentences}{$base}{$sid}{groups});
      my ($rprec,$rrec,$rf,$rall) = get_stats($stats->{sentences}{$base}{$sid}{rels});
      print_stats("stats sentence $base.$sid: groups",$gprec,$grec,$gf);
      print_stats("stats sentence $base.$sid: rels",$rprec,$rrec,$rf);
    }
  }
}

sub emit_catalogue {
  return unless ($html);
  my @catalogue = ();
  print HTML h1("Catalogue");
  print HTML start_ul;
  foreach my $t (@groups) {
    print HTML li("Groupe type $t"), start_ul;
    foreach my $c (@base) {
      next unless exists ($catalogue->{ref}{groups}{$t}{$c});
      my @k = sort {xid($a) <=> xid($b)} keys %{$catalogue->{ref}{groups}{$t}{$c}};
      print HTML li("$c: ",map( a({href=>"$c.$suffix.html#$_"},$_),@k));
    }
    print HTML end_ul;
  }
  print HTML end_ul;

  print HTML start_ul;
  foreach my $t (@rels) {
    print HTML li("Relation type $t"), start_ul;
    foreach my $c (@base) {
      next unless exists ($catalogue->{ref}{relations}{$t}{$c});
      my @k = sort {xid($a) <=> xid($b)} keys %{$catalogue->{ref}{relations}{$t}{$c}};
      print HTML li("$c: ",map( a({href=>"$c.$suffix.html#$_"},$_),@k));
    }
    print HTML end_ul;
  }
  print HTML end_ul;

}

sub emit_new_xml {
  my $twig1 = shift;		# assume twig1 is ref
  my $twig2 = shift; # hyp
  my $info1 = $twig1->{info};
  my $info2 = $twig2->{info};
  my $base  = $info1->{base};

  foreach my $sid (keys %{$info1->{s}}) {
    my $s1 = $info1->{s}{$sid};
    next unless ($s1->{map});
    my $s2 = $info2->{s}{$sid};
    $s1->{xml}->set_att( mode => $s2->{xml}->att('mode') );
    my $hypconsts = $s2->{xml}->first_child("constituants");
    if ($hypconsts) {
      $hypconsts = $hypconsts->cut;
      $hypconsts->set_gi("hypconsts");
    } else {
      $hypconsts = XML::Twig->new("hypconsts");
    }
    $hypconsts->paste(last_child => $s1->{xml} );
 
    my $hyprels = $s2->{xml}->first_child("relations");
    if ($hyprels) {
      $hyprels = $hyprels->cut;
      $hyprels->set_gi("hyprels");
    } else {
      $hyprels = XML::Twig::Elt->new("hyprels");
    }
    $hyprels->paste(last_child => $s1->{xml} );
   }

  foreach my $sid (keys %{$info1->{s}}) {
    my $s1 = $info1->{s}{$sid};
    my $s2 = $info2->{s}{$sid};
    @lines=();
    foreach my $r1 (sort { $a->{first} <=> $b->{first}
			     || $a->{last} <=> $b->{last}
			}
		    values %{$s1->{relations}}) {
#      rel2line($s1,$r1,($r1->{map} || $nohyp) ? 'both' : 'ref');
      rel2line($s1,$r1,($r1->{map}) ? 'both' : 'ref');
    }
    if (defined $s2) {
      foreach my $r2 (sort { $a->{first} <=> $b->{first}
			       || $a->{last} <=> $b->{last}
			     } values %{$s2->{relations}}) {
	next if ($r2->{map});
	rel2line($s2,$r2,'hyp');
      }
    }
    if (@lines) {
      my $lines = XML::Twig::Elt->new(lines => {n => scalar(@lines) });
      for (my $l=0; $l <= $#lines; $l++) {
	my $line = XML::Twig::Elt->new(line => {l => $l });
	my $x = $lines[$l];
	my %rels = ();
	foreach my $i (sort {$a <=> $b} keys %$x) {
	  my $r = $x->{$i};
	  my $rid = $r->{rid};
	  my $status = $r->{status};
	  my $key = "$status$rid";
	  next if (exists $rels{$key});
	  $rels{$key}=1;
	  XML::Twig::Elt->new( edge => { left   => $r->{first}, 
					 right  => $r->{last},
					 rid    => $r->{rid},
					 type   => $r->{type},
					 status => $r->{status}
				       } )
	      ->paste( last_child => $line);
	}
	$line->paste( last_child => $lines );
      }
      $lines->paste( last_child => $s1->{xml} );
    }
  }

  my $out = "$base.$suffix.xml";
  open(OUT,">$out") || die "can't open $out: $!";
  $twig1->flush(\*OUT);
  close(OUT);
}

sub rel2line {
  my $s = shift;
  my $r = shift;
  my $status = shift;
  return if (exists $r->{cross});
  $r->{status} = $status;
  my $rid   = $r->{rid};
  my $const = $r->{const};
  my $first = $r->{first};
  my $last  = $r->{last};
  my $line  = -1;
 LOOP: for (my $l=0; $l <= $#lines; $l++) {
    ## Search for a line
    for (my $i=$first; $i <= $last; $i++) {
      next LOOP if (exists $lines[$l]{$i});
    };
    ## Found a line
    $line=$l;
    last;
  }
  if ($line == -1 ) {
    push(@lines,{});
    $line = $#lines;
  }
  # toggle (l,i) cases to state they are busy
  for (my $i=$first; $i <= $last; $i++) {
    $lines[$line]{$i}=$r;
  };
}

1;

__END__

=head1 NAME

easymerge.pl -- Merge EASy reference and hypothese XML annotation files

=head1 SYNOPSIS

    easymerge.pl  [-verbose] [-ref=<refdir>] [-hyp=<hypdir>] [-html] [-nohyp]

=head1 DESCRIPTION

easymerge.pl is a Perl script that can be used to merge EASy xml set
of files, one of them denoting the REFerence files and the other the
HYPothesis files.

Merged files with extension .merged.xml are created. They follow an
extension of the EASy DTD to incorporate REF and HYP annotations +
attributes providing information about mapping.

The easyalt2html.xsl XSTL template may be used to build HTML files
from the merged XML files.

In verbose mode, easymerge.pl also provides a lot of information +
warnings.

With the option -html, an index file called 'merge.html' is created
with statistical information and links to the HTML merged files.

The option -nohyp disables merging and only requires reference files.
New XML files are created with extension 'ref.html'. This option is
useful to enrich the original XML file with extra layout
information.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
