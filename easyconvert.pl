#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Carp;
use POSIX qw(strftime floor);
use POSIX ":sys_wait_h";

use IPC::Run qw(start finish run pump timeout);

use AppConfig qw/:argcount :expand/;

use File::Temp qw/tempfile/;
use File::Basename;
use Net::Ping;

my $config = AppConfig->new(
			    'verbose!'             => {DEFAULT => 1},
			    'results=f'            => {DEFAULT => 'results'},
			    'dest=f'               => {DEFAULT => 'easyxml'},
			    "corpus=f@"            => {DEFAULT => [] },
			    "easyforest=f"         => {DEFAULT => 'easyforest'},
			    "easyforest_options=s" => {DEFAULT => ''},
			    "timeout=i"            => {DEFAULT => 100},
			    "run!"                 => {DEFAULT => 1},
			    "dagdir=s"             
			   );

my $conffile = 'convert.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results    = $config->results;
my $verbose    = $config->verbose;
my $dest       = $config->dest;
my $easyforest = $config->easyforest;
my $dagdir = $config->dagdir;
my $log;

my @corpus     = @{$config->corpus};

if (!@corpus) {
  open(INFO,"<$dagdir/info.txt") || die "can't open info: $!";
  while(<INFO>) {
    next if (/^\#/);
    my ($corpus) = split(/\s+/,$_);
    push(@corpus,$corpus)
  }
  close(INFO);
}

my $count = 0;

verbose("Preparing results directory '$dest'");
system("mkdir -p $dest");

if ($verbose) {
  $log = "$dest/convert.log";
  open(VERBOSE,">$log") or die "can't open log $log: $!";
  verbose("Activate log in '$log'");
}

verbose("Start EASY conversion from $results to $dest");
system("mkdir -p $dest");

foreach my $corpus (@corpus) {
  process_corpus($corpus);
}

verbose("Done EASY conversion");

sub process_corpus {
  my ($corpus) = @_;
  system("mkdir -p $dest/$corpus");
  verbose("start converting $corpus");
  $count = 0;

  foreach my $d1 (<$results/$corpus/[0-9]>) {
    my ($l1) = ($d1 =~ /(\d+)$/);
    system("mkdir -p $dest/$corpus/$l1");
    foreach my $d2 (<$d1/[0-9]>) {
      my ($l2) = ($d2 =~ /(\d+)$/);
      system("mkdir -p $dest/$corpus/$l1/$l2");
      my $ldest = "$dest/$corpus/$l1/$l2";
      my @xmldep = <$d2/$corpus*.dep.xml.bz2>;
      foreach my $file (sort { sid($a) <=> sid($b) } @xmldep) {
	dep2easy_handler($file,$corpus,$ldest);
      }
    }
  }
  verbose("done converting $corpus");
}

sub sid {
  my $file = shift;
  $file =~ /E(\d+(?:\.\d+)?)(?:\.\w+)+(.bz2)?$/;
  return $1;
}

sub dep2easy_handler {
  my ($file,$corpus,$ldest) = @_;
  my $sid = sid($file);
  my $destfile = "$ldest/$corpus.E$sid.ph2.xml.bz2";
  return if (-e $destfile && -s $destfile);
  my $cmd  = "bunzip2 -c $file | $easyforest -- -e $sid | xmllint -format - | bzip2 > $destfile";
  if (-z $file) {
    verbose("** empty $corpus:$sid");
    return;
  }  
  if (!$config->get('run')) {
    verbose("\tSimulating $cmd");
    return;
  }
  my $pid;
  if ($pid = fork) {
    eval {
      local $SIG{ALRM} = sub { die "Timed Out!\n" };
      alarm($config->get('timeout'));
      waitpid($pid,0);
      alarm(0);
    };
    if ($@ =~/timed out/i ){
      verbose("***timeout $corpus:$sid");
      kill KILL => $pid;
      waitpid($pid,0);
    } elsif ($@) {
      verbose("***pb $sid: '$@' and '$!'");
      kill KILL => $pid;
      waitpid($pid,0);
    }
  } else {
    exec($cmd);
  };
  $count++;
  verbose("$corpus $count") unless ($count % 100);
}

sub verbose {
  return unless ($config->get('verbose'));
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
  print VERBOSE "$date @_\n" if (defined $log);
}

=head1 NAME

easyconvert.pl -- Convert results from xmldep to XML Easy format

=head1 SYNOPSIS 	 
  	 
./easyconvert.pl --results <resultsdir> --dest <destdir> --easyforest_options="-weights"

=head1 DESCRIPTION

B<easyconvert.pl> converts results in xmldep format (dep.xml) to XML
Easy format (ph2.xml) using easyforest, which is provided with frmg
package.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
