#!/usr/bin/env perl

use strict;
use XML::Twig;

my $file = shift;

my $xml = XML::Twig->new(
			 twig_handlers => {
					   'Sentence' => \&sentence_handler
					  },
			 pretty_print => 'indented'
			);

if ($file =~ /\.bz2$/) {
  open(FILE,"bzcat $file|") || die "can't open $file: $!";
} else {
  open(FILE,"<$file") || die "can't open $file: $!";
}
$xml->parse(\*FILE);
$xml->root->set_name('DOCUMENT');
$xml->flush;
close(FILE);

sub sentence_handler {
  my ($t,$s) = @_;
  my $mapping = {};
  my $tokens = {};
  $s->set_name('E');
  my @tokens = ();
  foreach my $tok ($s->children('T')) {
    $tok->cut;
    my $tid = $tok->att('id');
    my $fid = $tid;
    $fid =~ s/T(\d+)/F$1/;
    my $u = $1;
    $tok->set_name('F');
    $tok->set_att(id => $fid);
    $tokens->{$tid} = { xml => $tok,
			emitted => 0,
			ws => [],			
			uid => $u
		      };
    push(@tokens,$tid);
  }
  my $ws={};
  foreach my $w ($s->descendants('W')) {
    my @toks = split(/\s+/,$w->att('tokens'));
    my $wid = $w->att('id');
    my $info = $ws->{$wid} = { tokens => [@toks], xml => $w, id => $wid };
    push(@{$tokens->{$_}{ws}},$info) foreach (@toks);
  }
  foreach my $tid (@tokens) {
    my $info = $tokens->{$tid};
    my @ws = @{$info->{ws}};
    my $first = shift @ws;
    next unless (@ws);
    next if (grep {@{$_->{tokens}}==1} @ws);
    ## try to some something for Ts in several Ws
    my @toks = @{$first->{tokens}};
    my $u = shift @toks;
    if (@toks && $u == $tid) {
      ## T belong to a multi-T W and to several W
      ## we keep T in the first W and remove it from the other Ws
      my @debug = map {$_->{id}} $first,@ws;
      print STDERR "Distributing tokens in muti Ws: $tid in @debug\n";
      $first->{tokens} = [$u];
      $info->{ws} = [$first];
      shift @{$tokens->{$_}{ws}} foreach (@toks);
      shift @{$_->{tokens}} foreach (@ws);
    }
  }

  foreach my $w ($s->descendants('W')) {
    ## my @tokens = split(/\s+/,$w->att('tokens'));
    my $wid = $w->att('id');
    my @tokens = @{$ws->{$wid}{tokens}};
    unless (@tokens) {
      print STDERR "No tokens for $wid\n";
    }
    my $last;
    my $in_group = $w->parent('G');
    foreach my $tid (@tokens) {
      my $tok = $tokens->{$tid};
      if ($tok->{emitted}) {
	my $txt = $tok->{xml}->trimmed_text;
	my $in_group2 = $tok->{xml}->parent('G');
	if ($in_group2 && $in_group) {
	  print STDERR "Token $tid txt=$txt in two distinct groups\n" unless ($in_group2 == $in_group);
	  next;
	} elsif (!$in_group2 && $in_group) {
	  print STDERR "Token $tid txt=$txt in F and in group\n";
	  $tok->{xml}->cut;
	} else {
	  print STDERR "Token $tid txt=$txt in two distinct Fs\n";
	  next;
	}
      }
      $tok->{xml}->paste( before => $w );
      $tok->{emitted} = 1;
      $last = $tok;
    }
    if (!defined $last) {
      my $form = $w->att('form');
      print STDERR "Pb with form $wid form='$form' tokens=@tokens\n";
    } 
    $mapping->{$wid} = $tokens->{$tokens[-1]}{xml}->att('id');
    $w->cut;
  }
  foreach my $g ($s->children('G')) {
    $g->set_name('Groupe');
    if (!$g->children('F')) {
      my $gid = $g->att('id');
      print STDERR "Empty group $gid\n";
      $g->cut;
    }
  }
  my $relations = XML::Twig::Elt->new('relations');
  foreach my $r ($s->children('R')) {
    $r->set_name('relation');
    $r->set_att('xlink:type' => "extended" );
    my %seen=();
    foreach my $role ($r->children) {
      next unless ($role->att('ref'));
      $role->set_att('xlink:type' => "locator" );
      my $ref = $role->att('ref');
      my $xref = $mapping->{$ref} || $ref;
      $seen{$xref}++;
      $role->set_att('xlink:href' => $xref);
      $role->del_att('ref');
    }
    ## relation with same head and target: we discard
    $r->cut;
    next if (grep {$seen{$_} > 1} keys %seen);
    $r->paste(last_child => $relations);
  }
  foreach my $c ($s->children('cost')) {
    $c->cut;
  }
  $relations->paste(last_child => $s);
}

