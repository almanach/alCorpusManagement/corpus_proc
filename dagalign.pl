#!/usr/bin/perl

use strict;

my $ref = shift;
open(REF,"<$ref") || die "can't open ref $ref: $!";

my $last = undef;
my $shift = 0;

my %map = ();

while (<>) {
  s{<F\s+id="(E\d+F\d+)">(.*?)</F>}{&emit_fid($1,$2)}eg;
  print $_;
}

sub emit_fid {
  my $fid = shift;
  my $content = shift;
  ($fid,$content) = get_reference_id($fid,$content);
  return '<F id="'.$fid.'">'.$content.'</F>';
}

sub get_reference_id {
  my $fid = shift;
  my ($sid,$id) = ($fid =~ /(E\d+)F(\d+)/);
  if ($shift > 0 && $id > $shift) {
    $fid = $sid."F".($id-$shift);
  } else {
    $shift = 0;
  }
  my $content = shift;
  unless (exists $map{$fid}) {
    if (defined $last) {
      my $lcontent = $last->{content};
      my $x        = $last->{x};
      my $lfid     = $last->{fid};
      my $nx       = $x.$content;
      if ($nx eq $lcontent || $lcontent eq ucfirst($nx)) {
	print STDERR "*** $fid -> $lfid: completed content '$content' in '$lcontent'\n";
	$last = undef;
	$map{$fid} = [$lfid,$lcontent];
	return @{$map{$fid}};
      } elsif (index($lcontent,$nx,0) == 0 ||index($lcontent,ucfirst($nx),0) == 0) {
	print STDERR "*** $fid -> $lfid: continued content '$content' in '$lcontent'\n";
	$last->{x} = $nx;	
	$map{$fid} = [$lfid,$lcontent];
	return @{$map{$fid}};
      }
      print STDERR "*** $fid -> $lfid: can't complete '$lcontent' with '$content'\n";
      $last = undef;
    }
    my ($lfid,$lcontent) = next_ref_form();
    $map{$fid} = [$lfid,$lcontent];
    my $xcontent = $content;
    $xcontent =~ s/_/ /og;
    if (($content ne $lcontent) && (ucfirst($content) ne $lcontent) && index($lcontent,$content,0) == 0) {
      print STDERR "*** $fid -> $lfid: unfilled content '$content' in '$lcontent'\n";
      $last = { x => $content, 
		content => $lcontent, 
		fid => $lfid };
    } elsif (($content ne $lcontent) && (ucfirst($content) ne $lcontent)
	     && ($xcontent ne $lcontent) && (ucfirst($xcontent) ne $lcontent)
	     && $content =~ /\w+\s*\d+/
	    ) {
      ## to handle cases like G7 that add an extra form
      ## very rare but induce index shift in remaining sentences
      print STDERR "*** $fid -> $lfid: unexpected content '$content' instead of '$lcontent'\n";
      $shift++;
      $last = undef;
    } else {
      $last = undef;
    }
  }
  return @{$map{$fid}};
}

sub next_ref_sentence {
  while(<REF>) {
    return $1 if /<E\s+id="E(\d+)"/;
  }
  return undef;
}

sub next_ref_form {
  while(<REF>) {
    return ($1,$2) if m{<F\s+id="(E\d+F\d+)">(.*?)</F>};
  }
  return undef;
}


close(REF);

=head1 NAME

dagalign.pl -- Normalize dags created by sxpipe-easy

=head1 SYNOPSIS

cat file.udag | ./dagalign.pl tokenizedfile.xml > normalizedfile.udag

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
