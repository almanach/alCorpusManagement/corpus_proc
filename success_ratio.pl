#!/usr/bin/perl

use strict;
use File::Basename;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "verbose!"        => {DEFAULT => 1},
			    "results=f"       => {DEFAULT => 'results'},
			    "host=s@"         => {DEFAULT => []},
                            "parse_options=s" => {DEFAULT => ''},
			    "dag=f"           => {DEFAULT => 'dag'},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "callparser=f"    => {DEFAULT => ''},
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;

my %info = ();

open(INFO,"<info.txt") or die "can't open info.txt: $!";

my $all_n       = 0;
my $all_success = 0;
my $all_fail    = 0;

my @corpus      = ();

while(<INFO>) {
  chomp;
  my ($file,$n) = split(/\s+/,$_);
  $info{$file}  = $n;
  $all_n += $n;
  push(@corpus,$file);
}

close(INFO);

## print results for each corpus
foreach my $log (<$results/*.log>) {

  my $base = basename($log,'.log');

  next unless ($base =~ /_/);

  my $success = 0;
  my $fail    = 0;
  
  open(LOG,"$log") || die "can't open logfile $log";

  while(<LOG>) {
    $success++, next if (/^ok/);
    $fail++, next if (/^fail/);
  }

  close(LOG);

  my $host = '';
  
  if (-r "$results/lock/$base") {
    open(LOCK,"<$results/lock/$base") or die "Can't open lock for $base";
    $host = <LOCK>;
    chomp $host;
    close(LOCK);
  }

  $all_success += $success;
  $all_fail    += $fail;

  my $tried     = $success+$fail || 0.001;

  my $done      = 0;
  $done         = 100*($tried/$info{$base}) if ($info{$base});

  printf "%-20s Tried: %7i Success: %6.2f%% Done: %6.2f%% %10s\n", 
    $base, $tried, 100*$success / ($tried), $done, $host;
  
}


## print Total
my $all_tried = $all_success+$all_fail;
my $all_done  = 0;
$all_done     = 100*($all_tried/$all_n) if ($all_n);

print '-'x70,"\n";
printf "%-20s Tried: %7i Success: %6.2f%% Done: %6.2f%%\n", 
  'Total', $all_tried, 100*$all_success / ($all_tried), $all_done;




__END__


=head1 NAME

success_ratio.pl -- Print success ratio

=head1 SYNOPSIS 	 
  	 
./success_ratio.pl

=head1 DESCRIPTION

For each corpus (one per line) B<success_ratio.pl> displays the number
of sentences tried, the success ratio and the percentage of processed
sentences according to info.txt.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
