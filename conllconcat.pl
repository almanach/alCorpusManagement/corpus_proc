#!/usr/bin/env perl

use 5.006;
use strict;
use warnings;
use Carp;
use POSIX qw(strftime floor);
use POSIX ":sys_wait_h";

use locale;
use Encode;
use List::Util qw(max min);
use String::Approx 'amatch';

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    { CREATE => 1 },
			    'verbose!'        => {DEFAULT => 1},
			    'results=f'       => {DEFAULT => 'results'},
			    'dest=f'          => {DEFAULT => 'conll'},
			    'refdir=f',
			    "corpus=f@"       => {DEFAULT => [] },
			    "dagdir=f"        => {DEFAULT => 'dag'},
			    "dagext=s"        => {DEFAULT => "udag"},
			    "set=s"           => {DEFAULT => "frmg"},
                            "compress!"        => {DEFAULT => 1},
                            "logdir=f",
			    "tagset=f",
			    "tmpdir=f", # to be used when untaring in $results is too  slow (if nfs dir)
			    "cost!"           => {DEFAULT => 0}
			   );


my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $verbose = $config->verbose;
my $dest = $config->dest;
my $dagdir = $config->dagdir;
my $dagext = $config->dagext;
my $log;


my $costopt = $config->cost;

my $tmpdir = $config->tmpdir || $results;

my $refdir = $config->refdir;

-d $refdir 
  || die "missing refdir '$refdir'";

-d $results
  || die "missing result dir '$results'";

-d $dagdir
  || die "missing dagdir '$dagdir'";


my %altforms = (
		'&lt;' => '<',
		'&gt;' => '>',
		'&amp;' => '&',
	       );

my @corpus = @{$config->corpus};

if (!@corpus) {

  open(INFO,"<$dagdir/info.txt") || die "can't open '$dagdir/info.txt': $!";
  while(<INFO>) {
    next if /^\#/;
    my ($corpus) = split(/\s+/,$_);
    push(@corpus,$corpus)
  }
  close(INFO);
}

my $count = 0;

system("mkdir -p $dest");

if ($verbose) {
    my $logdir = $config->logdir || $dest;
    $log = "$logdir/conll.log";
    open(VERBOSE,">>$log") or die "can't open log $log: $!";
    verbose("Activate log in '$log'");
}

verbose("Start CONLL concat from $results");

foreach my $corpus (@corpus) {
  
  my ($base) = ($corpus =~ /^(\w+)_/);
  my $dagfile = "$dagdir/$corpus.${dagext}";
  my $destfile = "$dest/$corpus.conll";
  my $xdestfile = "$dest/$corpus.xconll";
  my $reffile = "$refdir/$corpus.l1.conll";
##  my $reffile = "$refdir/$corpus.conll";

  my $tar = 0;

  if (!-d "$results/$corpus" && (-f "$results/$corpus.tar.bz2")) {
    $tar = 1;
    verbose("untar from archive $results/$corpus in $tmpdir");
    system("cd $tmpdir && tar xjf $results/$corpus.tar.bz2");
  } elsif (!-d "$results/$corpus" && (-f "$results/$corpus.tar.gz")) {
    $tar = 1;
    verbose("untar from archive $results/$corpus in $tmpdir/");
    my $where = `pwd`;
    chomp $where;
    system("cd $tmpdir && tar xzf $where/$results/$corpus.tar.gz"); # warning: assume $results is relative
  }

  verbose("Missing corpus $corpus"), next unless (-d "$results/$corpus" || -d "$tmpdir/$corpus");

  verbose("Concatenating $corpus");

  open(DEST,">:utf8","$destfile") || die "can't open destfile '$destfile': $!";
  open(XDEST,">$xdestfile") || die "can't open xdestfile '$xdestfile': $!";

  my $tried = 0;
  my $added = 0;

  -r $reffile
    || die "can find reffile '$reffile'";

  my $refforms = {};
#  open(REF,"<:encoding(utf8)","$reffile")
  open(REF,"<","$reffile")
    || die "can't open reference file for $corpus: '$reffile': $1";
  my $refsid = 1;
  my $sid = "E1";
  while (<REF>) {
    ## REF is in UTF8 => convert to latin1
    if (/^\s*$/) {
      $refsid++;
      $sid = "E$refsid";
      next;
    }
    my ($pos,$form,$lemma,$cat,$fullcat,$head,$deprel) = split(/\t/,$_);
    my $xform = $form;
    $xform =~ s/_-_/-/og;
    $xform =~ s/'_/'/og;
    my $fid = "${sid}F$pos";
##    verbose("register $sid $pos $form");
    push(@{$refforms->{$refsid}},
	 { id => $fid,
	   sid => $sid,
	   form => $form,
	   xform => $xform,
	   lemma => $lemma,
	   cat => $cat,
	   fullcat => $fullcat,
	   head => $head,
	   deprel => $deprel,
	   pos => $pos
	 }
	);
  }
  close(REF);

  if (-r $dagfile) {
    open(DAG,"<$dagfile") || die "can't open dag for $corpus '$dagfile': $!";
  } elsif (-r "${dagfile}.bz2") {
    open(DAG,"bunzip2 -c ${dagfile}.bz2|") || die "can't open dag for $corpus '$dagfile': $!";
  } else {
    die "can't find DAG file '$dagfile'";
  }
  my $forms = {};
  my $pos = 0;
  my $sentences = [];
  while (<DAG>) {
    next unless /^##\s*DAG\s+BEGIN/oi;
    my $line = <DAG>;
    my ($sid) = ($line =~ /(E\d+(?:\.\d+)?)F\d+/);
    my ($xsid) = $sid =~ /(E\d+)/;
    my ($nsid) = $sid =~ /E(\d+(?:\.\d+)?)/;
    ($xsid eq $sid) and $pos = 0;
    ++$tried;
    push(@$sentences,$nsid);
    while(1) {
      while ($line =~ m{\G.*?(<F id="E\d+(?:\.\d+)?F\d+">[^<>]+</F>)}og) {	
	my $content = $1;
	##	  print STDERR "Adding $form\n";
	my ($fid) = $content =~ /id="(E\d+(?:\.\d+)?F\d+)"/o;
	my ($nfid) = $fid =~ /F(\d+)$/;
	my ($form) = $content =~ />([^<>]+)</o;
	$form =~ s/\\(["?+()])/$1/og; ## remove escaping added by dag2udag
	$form =~ s/_underscore/_/og;
	$form =~ s/&gt;/>/og;
	$form =~ s/&lt;/</og;
	$form =~ s/&#194;//og;
	if ($form =~ s/&#(\d+);/chr($1)/oeg) {
	    verbose("+++ encoding $corpus.$sid form=$form");
	  }
#	$form = "\t$form";
	$fid =~ s/\.\d+//o;
	my $entry = $forms->{$xsid}{$fid} ||= { form => $form, 
						id => $fid,
						sid => $xsid,
						dagsid => $sid,
						nfid => $nfid
					      };
      }
      if ($line =~ /^\#\#\s*OFFSET\s+(\S+)\s+(\d+)\s+(\d+)/) {
	my $fid = $1;
	my $start = $2;
	my $end = $3;
	$forms->{$xsid}{$fid}{start} = $start;
	$forms->{$xsid}{$fid}{end} = $end;
      }
      $line = <DAG>;
      unless ($line) {
	  # this case should not arise
	  verbose("*** Unfinished DAG in '$corpus' sid=$sid");
	  last;
      }
      last if ($line =~ /^##\s*DAG\s+END/);
    }
  }
  close(DAG);

  foreach my $nsid (sort {$a <=> $b} keys %$refforms) {
    ##    verbose("Align $corpus.E$nsid");
    align("$corpus.E$nsid",$refforms->{$nsid},$forms->{"E$nsid"});
  }
  
  foreach my $nsid (@$sentences) {
    my $xsid = "E$nsid";
    $xsid =~ s/\.\d+//o;
    if (handle_sentence($corpus,$nsid,$forms->{$xsid})) {
      $added++;
    } else {
      ## to be filled
    }
  }

  my @cost=();
  my @bcost=();
  foreach my $nsid (sort {$a <=> $b} keys %$refforms) {
    ##    verbose("Emit $corpus.E$nsid");
    my $s = $refforms->{$nsid};
    foreach my $t (@$s) {
      my $pos = $t->{pos};
      next unless (defined $pos);
      my $form = $t->{form};
      my $lemma = $t->{lemma};
      my $cat = $t->{cat};
      my $fullcat = $t->{fullcat};
      my $head = 0;
      my $deprel = 'missinghead';
      my $mode = 'fail';
      my $sid = "E$nsid";
      my $xrule = "_";
      my $eid = -1;
      my $mstag = '_';
      if (my $align2 = $t->{align2}) {
	foreach my $hyp (@$align2) {
	  defined $hyp->{head} or next;
	  $lemma = $hyp->{info}[2];
	  $head = $hyp->{head};
	  $deprel = $hyp->{info}[7];
	  $mode = $hyp->{mode};
	  $sid = $hyp->{sid};
	  $fullcat = $hyp->{fullcat};
	  $cat = $hyp->{cat};
	  $xrule = $hyp->{info}[10];
	  $eid = $hyp->{info}[11];
	  $mstag = $hyp->{info}[5];
	  scalar(keys %{$hyp->{cost}}) and push(@cost,$hyp);
	  scalar(keys %{$hyp->{bcost}}) and push(@bcost,$hyp);
	}
      }
      my $xhead = $head ? "${sid}F$head" : 0;
      print DEST "$pos\t$form\t$lemma\t$cat\t$fullcat\t$mstag\t$head\t$deprel\t_\t_\n";
      print XDEST "$corpus.${sid}F$pos\t$mode\t$form\t$lemma\t$fullcat\t$xhead\t$deprel\t$xrule\t$eid\n";
    }
    print DEST "\n";
    foreach my $t (grep {$_->{align2}} @$s) {
	foreach my $hyp (grep {defined $_->{head}} @{$t->{align2}}) {
           print XDEST $_ foreach (keys %{$hyp->{cost}});
	   print XDEST $_ foreach (keys %{$hyp->{bcost}});
        }
    }
    delete $refforms->{$nsid};
    print XDEST "\n";
  }

  close(DEST);
  ($config->compress) and system("bzip2 $destfile");


  close(XDEST);
#  system("bzip2 $xdestfile") if ($config->compress);
  ($config->compress) and system("gzip $xdestfile");

  if ($tar) {
    system("rm -Rf $tmpdir/$corpus");
  }

  my $ratio = 100 * ($added / $tried);
  my $msg = sprintf("(added %3i tried %3i ratio %6.2f%%)",$added,$tried,$ratio);
  verbose("Done $corpus $msg");
}

verbose("Done CONLL concatenation");


## print "FILES @easy\n";

sub handle_sentence {
  my ($corpus,$nsid,$forms) = @_;

  my $sid = "E$nsid";
  my $xsid = $sid;
  $xsid =~ /\.\d+/og;

  my $l1 = floor($nsid / 10000);
  my $l2 = floor(($nsid - ($l1 * 10000)) / 1000);
  my $l3 = floor(($nsid - ($l1 * 10000) - ($l2*1000)) / 100);
  my $file = "$results/$corpus/$l1/$l2/$l3/$corpus.$sid.conll";
  my $xfile = "$tmpdir/$corpus/$l1/$l2/$l3/$corpus.$sid.conll";
  if (-r $file && -s $file) {
    open(FILE,"<$file") || die "can't open $file: $!";
  } elsif (-r "$file.bz2" && -s "$file.bz2") {
    $file .= ".bz2";
    open(FILE,"bunzip2 -c $file|") || die "can't open $file: $!";
  } elsif (-r $xfile && -s $xfile) {
    open(FILE,"<$xfile") || die "can't open $xfile: $!";
  } elsif (-r "$xfile.bz2" && -s "$xfile.bz2") {
    $xfile .= ".bz2";
    open(FILE,"bunzip2 -c $xfile|") || die "can't open $xfile: $!";
  } else {
    verbose("*** missing $corpus:$sid");
    return 0;
  }

  my @rforms = sort {$a->{nfid} <=> $b->{nfid}} grep {$_->{dagsid} eq $sid} values %$forms;
  my $out = '';
  ##  verbose("handling sentence corpus=$corpus sid=$sid fids=@fids");
  my %tmp = ();
  my $mode;
  while (<FILE>) {
    next if /^\s*$/;
    if (/^\#\#\s+(cost|bcost)/) {		# CONLL extension: cost in comment
      $costopt or next;
      my $kind = $1;
      my ($tpos) = /tpos=(\d+)/;
      $tpos or next;
      $tmp{$tpos}{$kind}{$_} = 1;
      next;
    }
    if (/^\#/) {		# CONLL extension: comment
      /mode=(\S+)/ and $mode=$1;
      chomp;
      ## log CONLL conversion error messages (from easyforest)
      ## they have the form "## *** <msg>"
      /\*{3}\s+(.*)$/ and verbose("*** $corpus:$sid $1");
      next;
    }
    chomp;
    my @info = split(/\t+/,$_);
    my ($pos,$form) = @info[0,1];
    $form =~ s/\\(["?+()])/$1/og; ## remove escaping added by dag2udag
    $form =~ s/\\(["?+()])/$1/og; ## remove escaping added by dag2udag
    $form =~ s/_underscore/_/og;
    $form =~ s/&amp;/&/og;	# somewhere in the process & was transformed into &amp; (need to track it)
    $form =~ s/&gt;/>/og;
    $form =~ s/&lt;/</og;
    $form =~ s/&#194;//og;
    if ($form =~ s/&#(\d+);/chr($1)/oeg) {
      verbose("+++ encoding2 $corpus.$sid form=$form");
    }
    my $entry = $tmp{$pos} = { pos => $pos,
			       form => $form,
			       info => \@info,
			       tokens => [],
			       mode => $mode,
			       sid => $sid,
			       cat => $info[3],
			       fullcat => $info[4],
			       cost => {},
			       bcost => {},
			     };
    my @xform = ();
    while (1) {
      @rforms 
	or verbose("**** not enough tokens in $corpus.$sid"), 
	  last;
      my $nextform = shift @rforms;
      push(@xform,$nextform->{form});
      push(@{$entry->{tokens}},$nextform);
      my $u = join('_',@xform);
#      ($u eq $form) and last;
#      (lc($u) eq $form) and last;
      form_cost($u,$form) and last;
      verbose("*** bad sync between dag and conll output $corpus.$sid pos=$pos form=$form dag=$u");
      unless (index(lc($form),lc($u),0) == 0) {
	unshift @rforms, $nextform;
	last;
      }
    }
    if (my @t = @{$entry->{tokens}}) {
      my $max = 0;
      foreach my $t (@t) {
	foreach my $r (@{$t->{align}}) {
	  push(@{$r->{align2}},$entry);
	  push(@{$entry->{align}},$r->{pos});
	}
      }
    }
    ## to be fillled
  }
  close(FILE);
  foreach my $entry (values %tmp) {
    my $head = $entry->{info}[6];
    if ($head) {
      my $hentry = $entry->{head} = $tmp{$head};
      $head = max @{$hentry->{align}};
    }
    $entry->{head} = $head;
  }
  return 1;
}


sub verbose {
  return unless ($config->verbose);
  my $date = strftime "[%F %H:%M:%S]", localtime;
  print "$date @_\n";
  print VERBOSE "$date @_\n" if (defined $log);
}

sub align {
  my ($sid,$ref,$dag) = @_;
  # my $ref = shift;		# a list of CONLL tokens
  # my $dag = shift;            # a hash of SXPIPE tokens (but following CONLL tokens)
  my $hyp = [ map {$dag->{$_}} sort {$dag->{$a}{nfid} <=> $dag->{$b}{nfid}} keys %$dag ];
  if (find_best($ref,$hyp)
      && !grep {!exists $_->{align}} @$ref) {
  } else {
    verbose("*** Alignment mismatch in $sid");
  }
}

sub form_eq {
  my ($cform,$pform,$partial) = @_;
  $cform =~ /^-\S/ and $partial = 1;
  $cform =~ s/_-_/-/og;
  $pform =~ s/'(\S)/'_$1/og;
  $pform =~ s/\\([%+()])/$1/og;
  ##  $cform =~ /^\d+,\d+/ and $cform =~ s/,/_,_/og;
  my $pform2 = $pform;
  $pform2 =~ s/_/$1$2/og;
  return ($cform eq $pform) 
    || ($cform eq $pform2)
      || ($partial && index($pform,$cform) != -1);
}

sub form_cost {
  my ($rform,$hform) = @_;
#  print "form cost '$rform' '$hform'\n";
  $rform =~ s/_-_/-/og;
##  $hform =~ s/'(\S)/'_$1/og;
##  $hform =~ s/'(\S)/'$1/og;
#  my $hform2 = $hform;
#  $hform2 =~ s/_/$1$2/og;
  return 3 if (($rform eq $hform) 
	       ## || ($rform eq $hform2) 
	       || (lc($rform) eq lc($hform)));
  return 2 if (index(lc($hform),lc($rform)) != -1);
  return 2 if (index(lc($rform),lc($hform)) != -1);
  return 2 if ($rform eq 'des' && $hform eq 'les');
  return 2 if ($rform eq 'du' && $hform eq 'le');
  return 2 if ($rform eq 'de' && $hform eq 'le');
  return 1 if (amatch(lc($hform),lc($rform)));
  return 1 if ($hform =~ /^_[A-Z]+/); # entities
##  verbose("--- no match $rform $hform") if ($rform =~ /carte/i || $hform =~ /carte/i);
  return 0;
}

sub find_best {
  ## reftokens: CONLL ones
  ## hyptokens: SxPipe ones
  my ($rtokens,$htokens) = @_;
  my $align={};
  my $rmax=scalar(@$rtokens);
  my $hmax=scalar(@$htokens);
  my $best = { v => -1000 };
  ## initialize table
  ##  print "init align rmax=$rmax hmax=$hmax\n";
  for (my $i=0; $i <= $rmax; $i++) {
    $align->{$i}{0}=0;
  }
  for (my $j=0; $j <= $hmax; $j++) {
    $align->{0}{$j}=0;
  }
  ## fill table
  for (my $i=1; $i <= $rmax; $i++) {
    my $rtok = $rtokens->[$i-1];
    for (my $j=1; $j <= $hmax; $j++) {
      my $htok = $htokens->[$j-1];
      my $x = $align->{$i-1}{$j-1};
      ## comparison
      my $d = form_cost($rtok->{xform},$htok->{form});
      $x += $d;
      my $y = $align->{$i}{$j-1};
      my $z = $align->{$i-1}{$j};
      my $v = max(0,$x,$y,$z);
      $align->{$i}{$j} = $v;
##      print "align $i '$rtok->{form}' $j '$htok->{form}': d=$d x=$x y=$y x=$z v=$v\n";
      ($v > $best->{v}) and $best = { v => $v, i => $i, j => $j};
    }
  }
  ## extract best mapping
  my $i = $best->{i};
  my $j = $best->{j};
  my @map = ([$i,$j]);
  while ($align->{$i}{$j} > 0) {
    my $max = max($align->{$i-1}{$j-1},$align->{$i}{$j-1},$align->{$i-1}{$j}
);
    if ($align->{$i-1}{$j-1} == $max) {
      $i--;
      $j--;
      unshift(@map,[$i,$j]);
    } elsif ($align->{$i}{$j-1} == $max) {
      $j--;
      unshift(@map,[$i,$j]);
    } else {
      $i--;
      unshift(@map,[$i,$j]);
    }
  }
  foreach my $sync (@map) {
    ($i,$j) = @$sync;
    next unless ($i > 0 && $j > 0 && $align->{$i}{$j} > 0);
    ##    next unless ($matches->{$i}{$j});
    last;
  }
  unless ($align->{$i}{$j} > 0) {
#    print "### *** pb sync\n";
#    print "### best is v=$best->{v} i=$best->{i} j=$best->{j} rmax=$rmax hmax=$hmax\n";
#    print '### local align mapping '.join(' ',map {"$_->[0]:$_->[1]"} @map)."\n";
    return 0;
  }
  ## we got an alignment
  ##  print "found alignment @map\n";
  foreach my $sync (@map) {
    ($i,$j) = @$sync;
    next unless ($i > 0 && $j > 0);
    my $rtok=$rtokens->[$i-1];
    my $htok=$htokens->[$j-1];
    ##    print "\ttid=$rtok->{id} wid=$htok->{id}\n";
    push(@{$rtok->{align}},$htok);
    push(@{$htok->{align}},$rtok);
  }
  return 1;
}

=head1 NAME

easyconcat.pl -- Concat sentences results into one big file per corpus

=head1 SYNOPSIS 	 
  	 
./easyconcat.pl

The resulting files are sent to directory F<concat> [default]. To send
the files to another directory, use option --dest.

./easyconcat.pl --dest=somewhere

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=cut
