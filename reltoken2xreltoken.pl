#!/usr/bin/env perl

use strict;

## add new columns to reltoken 
##   POS of ref and hyp governors

my @lines = ();
my $sid;
my $ref = {};
my $hyp = {};

while (<>) {
  if (/^##/) {
    print $_;
    next;
  }
  chomp;
  my @info = split(/\s+/,$_);
  my ($tmpsid) = $info[0] =~ /(\S+)F\d+$/;
  if (defined $sid && $sid ne $tmpsid) {
    flush_sentence();
    $sid = $tmpsid;
    @lines = ();
    $ref = {};
    $hyp = {};
  }
  my ($fid) = $info[0] =~ /(E\S+?F\d+)$/;
  push(@lines,\@info);
  unless ($info[1] eq '###') {
    $ref->{$fid} = $info[4];
    $hyp->{$fid} = $info[4];
  }
}

flush_sentence();

sub flush_sentence {
  foreach my $info (@lines) {
    if ($info->[1] eq '###') {
      my $refhead = $info->[8];
      my $hyphead = $info->[9];
      my $delta = 0;
      if ($refhead && $hyphead) {
	my ($xfid) = $info->[0] =~ /F(\d+)$/;
	my ($xrefhead) = $refhead =~ /F(\d+)$/;
	my ($xhyphead) = $hyphead =~ /F(\d+)$/;
	$delta = (($xrefhead le $xhyphead) && ($xhyphead le $xfid)) ? "rh*"
	  : (($xhyphead le $xrefhead) && ($xrefhead le $xfid)) ? "hr*"
	    : (($xfid le $xhyphead) && ($xhyphead le $xrefhead)) ? "*hr"
	      : (($xfid le $xrefhead) && ($xrefhead le $xhyphead)) ? "*rh"
		: (($xhyphead le $xfid) && ($xfid le $xrefhead)) ? "h*r"
		  : "r*h";
	($refhead eq $hyphead) and $delta = uc($delta);
      } 
      push(@$info, 
	   $refhead ? ($ref->{$refhead} || "_") : "_", 
	   $hyphead ? ($hyp->{$hyphead} || "_") : "_", 
	   $delta
	  );
    }
    print "@$info\n";
  }
}
