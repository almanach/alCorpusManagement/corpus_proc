#!/usr/bin/perl

# Align DAG  with text
# adding offsets to DAGs

use strict;
use warnings;
use Encode;
use utf8;
use List::Util qw(max min);

## binmode STDIN , ":encoding(utf8)"; 
## binmode STDOUT , ":encoding(utf8)"; 

my $file = shift;

## Dag is read on input

if ($file =~ /\.gz$/) {
  open(FILE,"zcat $file|") || die "can't open $file: $!";
} elsif ($file =~ /\.bz2$/) {
  open(FILE,"bzcat $file|") || die "can't open $file: $!";
} else {
  open(FILE,"<$file") || die "can't open $file: $!";
}

##binmode FILE, ":utf8";
##binmode STDIN, ":utf8";

my $tokens = {};
my @tokens = ();

my $buffer = ();
my $buffer_end = 0;
my $start = 0;
my $end;
my $shift = 0;
my $line = 0;
my @last = ();
my $end_reached = 0;
my @lookahead = ();


my $all=0;
my $fail=0;
my $align=0;
my $dagskip=0;

my @doctokens = ();

my %altforms = (
		'&lt;' => '<',
		'&gt;' => '>',
		'&amp;' => '&',
		'...' => "…",
		'....' => "….",
		'o' => "•",
		'-' => '—'
	       );


get_next_dag();
buffer_load();

align();

verbose(sprintf("## STATS tokens=%d pb=%d ratio=%.3f%% align=%d dagskip=%d ratio=%.3f%%",$all,$fail,$fail/($all || 1)*100,$align,$dagskip,$dagskip/($align || 1)*100));

sub get_next_dag {
##    verbose("start reading next dag");
    my %ltokens = ();
    my @dag = ();
    while (my $x = <>) {
	while ($x =~ m{<F\s+id="(.+?)">\s*(.*?)\s*</F>}go) {
	    my ($tid,$form) = ($1,$2);
	    $form =~ s/^\\{2,4}([+?*-])/$1/o;
	    $form =~ s/^\\([%?()\|])/$1/o;
	    $form =~ s/\\{2}/\\/og;
	    $form =~ s/_ACC_O/\{/o;
	    $form =~ s/_ACC_F/\}/o;
	    my $token = { form => $form, 
			  id => $tid 
	    };
	    add_alt_forms($token);
	    $tokens->{$tid} = $token;
	    $ltokens{$tid} = 1;
	}
	if ($x =~ /^##\s*DAG END/) {
	    
	    my @ltokens = sort {token_sort($a,$b)} keys %ltokens;
	    
	    push(@tokens,@ltokens);
	    
	    $tokens->{$ltokens[0]}{is_first} = [@dag];
	    $tokens->{$ltokens[-1]}{is_last} = [$x];
	    $tokens->{$ltokens[-1]}{to_clean} = [@ltokens];

##	    verbose("Get next dag first=$ltokens[0] last=$ltokens[-1]");
##	    verbose("DAG is ".join(' ',map {"$_:$tokens->{$_}{form}"} @ltokens));
	    return 1;
	}
	next if ($x =~ /^##\s*OFFSET/);
	push(@dag,$x);
    }
    return 0;
}

sub my_decode {
  my $s = shift;
  $s = Encode::decode("latin1", $s);
##  $s = Encode::encode("utf8", $s);
  $s =~ s/&#(\d+);/chr($1)/gse;
  return $s;
}

sub verbose {
  my $msg = shift;
  print STDERR "$msg\n";
##  print "### $msg\n";
}

sub buffer_load {
  if (@lookahead) {
    use_lookahead();
    return 1;
  } elsif (!$end_reached
	   && ($buffer = <FILE>)) {
    $line++;
    ##    $buffer = encode('latin1',$buffer);
    ##    print STDERR "line $line:";
    ##    print STDERR $buffer;
    ##    $buffer =~ /^/sgc;
    pos($buffer) = 0;
    my $n = 100;
    add_lookahead() while ($n--);
    $shift += $buffer_end;
    $buffer_end = length($buffer);
    $start = 0;
    $buffer =~ /^/sgc;
    return 1;
  } else {
    $buffer = undef;
    $end_reached = 1;
    return 0;
  }
}

sub buffer_advance {
  my $l = shift;
  my $pos = shift;
  while ($l >= 0) {
    use_lookahead();
    $l--;
  }
  $start = $pos;
  $buffer =~ /^/sgc;
  pos($buffer) = $start;
}

sub add_lookahead {
  return if ($end_reached);
  if (my $x = <FILE>) {
    push(@lookahead,$x);
  } else {
    $end_reached = 1;
  }
}

sub use_lookahead {
  if (@lookahead) {
    $buffer = shift @lookahead;
    $shift += $buffer_end;
    $buffer_end = length($buffer);
    $start = 0;
    $line++;
##    verbose("Lookahead Shift line=$line");
    add_lookahead();
    $buffer =~ /^/sgc;
##    pos($buffer) = 0;
  }
}

sub find_in_recent {
  my $form = shift;
  foreach my $last (@last) {
    if ($last->{form} eq $form) {
      return $last;
    }
  }
  return 0;
}

sub push_recent {
  my $token = shift;
  unshift(@last,$token);
  if (@last > 3) {
    pop(@last);
  }
}

sub add_alt_forms {

    my $token = shift;

    my $form = $token->{form};

    my $altform;
    my $altform2;

    if ($form =~ /^PONCT(.+)$/o) {
	$altform = $1;
    } elsif ($form =~ /(oe|OE|ae|AE|'|-|&#\d+;)/o) {
	$altform = $form;
	$altform =~ s/oe/œ/og;
	$altform =~ s/OE/Œ/og;
	$altform =~ s/ae/æ/og;
	$altform =~ s/AE/Æ/og;
	$altform =~ s/'/’/og;
	$altform =~ s/-/–/og;
	$altform =~ s/&#(\d+);/chr($1)/ge;
    } elsif (my $tmp = $altforms{$form}) {
	$altform = $tmp;
    } elsif ($form =~ /\.\.\./) {
	$altform = $form;
	$altform =~ s/\.\.\./…/og;
    } elsif ($form =~ /\&(gt,lt|amp);/) {
	$altform = $form;
	$altform =~ s/(\&(?:gt|lt|amp);)/$altforms{$1}/ge;
    }
    
    if (($form =~ /[:'"-]/o) && ($form !~ m{\\$})) {
      ## \x{EFBFBD}
      $altform2 = $form;
      $altform2 =~ s/\*/\\*/og;
      $altform2 =~ s/\+/\\+/og;
      $altform2 =~ s/\?/\\?/og;
      $altform2 =~ s/\./\\./og;
      $altform2 =~ s/\(/\\(/og;
      $altform2 =~ s/\)/\\)/og;
      $altform2 =~ s/\[/\\]/og;
      $altform2 =~ s/'/['’‘\x{EFBFBD}](\\s*)/og;
      $altform2 =~ s/-/(\\s*)[—−–-](\\s*)/og;
      $altform2 =~ s/:/(\\s*):(\\s*)/og;
      $altform2 =~ s/oe/(œ\|oe)/og;
      $altform2 =~ s/OE/(Œ\|OE)/og;
      $altform2 =~ s/ae/(æ\|ae)/og;
      $altform2 =~ s/AE/(Æ\|AE)/og;
      $altform2 =~ s/&#(\d+);/chr($1)/ge;
      $altform2 =~ s/"/(\\s*)["“”„‟‘’](\\s*)/og;
##      verbose("altform2 '$form' => '$altform2'");
    }

    $token->{altform} = $altform if ($altform);
    $token->{altform2} = $altform2 if ($altform2);

}


sub align {

##  @tokens = sort {token_sort($a,$b)} keys %$tokens;

TOKENS:
  while (1) {

  unless ($buffer) {
    buffer_load();
  }

  unless (@tokens) {
      get_next_dag();
  }

  return unless (@tokens);

    my $k = shift @tokens;
    my $token = $tokens->{$k};
    #    my $form = encode("latin1",$token->att('form'));
    my $form = $token->{form};
##    verbose("try align token $k $form");
    my $altform = $token->{altform};
    my $altform2 = $token->{altform2};


##  verbose("Processing $k '$form' at $line $start");
  
  L:
    ## skip whitespaces
    $buffer =~ /\G(\s|[       ])*/sgc;
    $start = pos($buffer);
##    print STDERR "test with pos=$start l=$buffer_end form=$form and str='$buffer'\n";
    ## if at end of buffer, we load a new slice
    if ($start == $buffer_end) {
      if (buffer_load()) {
	goto L;
      } else {
	## at the end of the document with remaining unaligned tokens
	## big problem !!
	verbose("*** Pb in aligment on $k '$form'");
	last;
      }
    }

    ## if next token is "" we skip everything because of a bug in sxpipe
    ## we need to swap the token with the next one until reaching point
    my $next = @tokens ? $tokens->{$tokens[0]} : undef;
    if ($next && ($next->{form} eq '""')) {
      verbose("**** Fixing sxpipe bug \"\" for $k '$form' at pos $line $start");
      $end = $start;
      assign($token,'pb');
      assign($next,'pb');
      shift @tokens;
      goto NOASSIGN;
    }
    
    unless (($buffer =~ /\G\Q$form\E/sgci)
	    || ($altform && ($buffer =~ /\G\Q$altform\E/sgci))
	    || ($altform2 && ($buffer =~ /\G$altform2/sgci))
	   ) {
      ## failed direct alignement
      my $area = substr($buffer,$start,15);
      verbose("*** Pb in aligment on $k '$form' at pos $line $start '$area'");
      
      ## we try to skip some punctuations
      if (($buffer =~ /\G([,;.:+­-]\s*)\Q$form\E/sgci)
	  || ($altform2 && ($buffer =~ /\G([,;.:]\s*)$altform2/sgci))) {
	$start += length($1);
	verbose("*** Skip punctuation for $k '$form' til pos $line $start");
	$end = pos($buffer);
	assign($token,'pb');
	goto NOASSIGN;
      }

      ## we look in recent tokens in case of duplication by sxpipe
      if (my $last = find_in_recent($form)) {
	verbose("*** Reusing previous token $last->{id} for $k");
	$token->{start} = $last->{start};
	$token->{end} = $last->{end};
	$token->{line} = $last->{line};
	$token->{badalign} = 'pb';
	handle_token($token);
	goto NOASSIGN;
      }

      my @chars = split(/\s*/,$form);
      foreach my $char (@chars) {
	$buffer =~ /\G\s*/sgci;
	if ($char eq "'" || $char eq '-' || $char eq '_') {
	  $buffer =~ /\G./sgci;
	} elsif ($buffer !~ /\G[­—-]?\Q$char\E/sgci) {
	  verbose("*** Need to skip $k '$form' at pos $line $start '$area'");

	  unshift(@tokens,$k);
	  my $info;
	  my $xn = 100;
	  verbose("Try local align for $k '$form' at pos $line $start on $xn tokens"); 
	  while ((scalar(@tokens) < $xn) && get_next_dag()) {}
	  my $yn = min(20,scalar(@tokens))-1;
	  verbose("tokens ".join(' ',map {"$_:$tokens->{$_}{form}"} @tokens[0..$yn]));
	  tokenize($xn);
	  $align++;
	  $info = local_align($xn);
          if ($info) {
	      $k = $info->{sync};
	      shift @tokens;
	      $token = $tokens->{$k};
	      $form = $token->{form};
	      verbose("found best resync point token $k '$form' at lookahed=$info->{l} start=$info->{start} end=$info->{end} form='$info->{form}'");
	      
	      foreach my $ignore (@{$info->{ignored}}) {
		  verbose("alignment ignore $ignore->{id} '$ignore->{form}': setting default value");
		  ## we should try to resynchronized ignored tokens
		  ## between old $start and current $start
		  assign($ignore,'pb');
	      }
	      
	      ## we advance in the string
	      if ($info->{l} == -1) {
		  pos($buffer) = $info->{start};
	      } else {
		  buffer_advance($info->{l},$info->{start});
	      }
	      pos($buffer) = $info->{end};
	      goto ASSIGN;
          } else {
              ## no sync in current DAG
	      verbose("*** skip end of dag");
	      $dagskip++;
	      while (@tokens) {
		  my $n = shift @tokens;
		  my $ignore = $tokens->{$n};
		  verbose("alignment ignore $ignore->{id} '$ignore->{form}': setting default value");
		  ## we should try to resynchronized ignored tokens
		  ## between old $start and current $start
		  my $is_last = $ignore->{is_last};
		  assign($ignore,'pb');
		  last if ($is_last);
	      }
	      next TOKENS;
          }
	}
      }
    }
  ASSIGN:
    $end = pos($buffer);
    assign($token);
  NOASSIGN:
  }
}

sub assign {
  my $token = shift;
  my $status = shift || 'ok';
  my $id = $token->{id};
##  verbose("ASSIGN id=$id $shift $start $end line=$line status=$status form=$token->{form}");
  my $lstart = $token->{start} = $shift+$start;
  my $lend = $token->{end} = $shift+$end;
  $token->{line} = $line;
  $token->{badalign} = $status;
  push_recent($token);

  if (my $dag = $token->{is_first}) {
      foreach my $x (@$dag) {
	  print $x;
      }
      delete $token->{is_first};
  }

  print "##OFFSET $id $lstart $lend $status $line\n";
  
  if (my $dag = $token->{is_last}) {
      foreach my $x (@$dag) {
	  print $x;
      }
      delete $token->{is_last};
  }

  if (my $erase = $token->{to_clean}) {
      foreach my $tid (@$erase) {
	  delete $tokens->{$tid};
      }
  }

  $all++;
  $fail++ unless ($status eq 'ok');

##  delete $tokens->{$id};
  
}


sub handle_token {

  my $token = shift;
  my $status = $token->{badalign};
  my $id = $token->{id};
  my $lstart = $token->{start};
  my $lend = $token->{end};
  my $line = $token->{line};

  if (my $dag = $token->{is_first}) {
      foreach my $x (@$dag) {
	  print $x;
      }
      delete $token->{is_first};
  }

  print "##OFFSET $id $lstart $lend $status $line\n";
  
  if (my $dag = $token->{is_last}) {
      foreach my $x (@$dag) {
	  print $x;
      }
      delete $token->{is_last};
  }

  if (my $erase = $token->{to_clean}) {
      foreach my $tid (@$erase) {
	  delete $tokens->{$tid};
      }
  }

  $all++;
  $fail++ unless ($status eq 'ok');

}


sub token_sort {
  my ($a,$b) = @_;
  my ($a1,$a2) = ($a =~ /E(\d+)F(\d+)/);
  my ($b1,$b2) = ($b =~ /E(\d+)F(\d+)/);
  return ($a1 <=> $b1) || ($a2 <=> $b2);
}

sub tokenize {
    ## simple tokenizing of current buffer and lookahed lines up to 10 next tokens
    my $n = shift;
    @doctokens = ();
    my $i = 0;
    my $l = -1;
    my $line = $buffer;
    my $pos = $start;
    pos($line) = $pos;
    my $end = length($line);
    while ($i < $n) {
START_TOK:
	$line =~ /\G\s*/og;
	$pos = pos($line);
	if ($pos == $end) {
	    $l++;
	    add_lookahead();	# just to be sure a lookahead is available
	    $line = $lookahead[$l];
	    $pos = 0;
	    unless ($line) {
		verbose("*** end of document reached while tokenizing");
		return;
	    }
	    pos($line) = 0;
	    $end = length($line);
	    goto START_TOK;
	}
	if ($line =~ /\G(\.\.\.\.)/sogc ||
	    $line =~ /\G(\.\.\.)/sogc ||
	    $line =~ /\G(\.)/sogc
	    || $line =~ /\G(-)/sogc
	    || $line =~ /\G([[:punct:]]+)/sogc
	    || $line =~ /\G((?:http|ftp|mailto):\S+)/sogc
	    || $line =~ /\G([[:alpha:]]+(?:['-][[:alpha:]]+)*)/sogc
	    || $line =~ /\G(\d+(?:\.\d+)?)/sogc
	    || $line =~ /\G(\S+)/og
	    ) {
	    push_token($i,$1,$l,$pos,pos($line));
	    $i++;
	    next;
	} 
    }
    verbose("Tokenize n=$n: ".join(' ',map {"$_->{i}='$_->{form}'"} @doctokens));
}

sub push_token {
    my ($i,$form,$l,$start,$end) = @_;
    push(@doctokens, 
	 { i => $i, 
	   l => $l,
	   form => $form,
	   start => $start,
	   end => $end
	 });
}

sub local_align {
    ## use a Dynamic Programming algo to find the local best alignement
    my $n = shift;
    my $table = {};
    my $matches = {};
    my $best = { v => -10000 };
    my @map = ();
    ## initialize $table
    for (my $i = 0; $i < $n; $i++) {
	$table->{$i}{0} = 0;
	$table->{0}{$i} = 0;
    }
    ## fill $table
    for (my $i = 1; $i < $n; $i++) {
	my $xk = $tokens[$i-1];
	last unless ($xk);
	my $xtoken = $tokens->{$xk};
	my $xform = $xtoken->{form};
	my $xaltform = $xtoken->{altform};
	my $xaltform2 = $xtoken->{altform2};
	my $xw = length($xform);
	for (my $j = 1; $j < $n; $j++) {
	    my $form = $doctokens[$j-1]{'form'};
	    my $w = length($form);
##	    verbose("Local align xk=$xk xtok=$xtoken xform=$xform altform=$xaltform altform2=$xaltform2");
	    my $x = $table->{$i-1}{$j-1};
	    if ($form eq $xform) {
	      $x += $w;
	      $matches->{$i}{$j} = 1;
	    } elsif (lc($form) eq lc($xform)) {
		$x += $w;
		$matches->{$i}{$j} = 1;
	    } elsif ($xaltform && ($xaltform eq $form)) {
		$x += $w;
		$matches->{$i}{$j} = 1;
	    } elsif ($xaltform2 && ($form =~ /$xaltform2/i)) {
		$x += $w;
		$matches->{$i}{$j} = 1;
	    } elsif (($w > 2) && ($xw > $w) && (index($xform,$form) >= 0)) {
		$x += $w;
		$matches->{$i}{$j} = 1;
	    } elsif (($xw > 2) && ($w > $xw) && (index($form,$xform) >= 0)) {
		$x += $xw;
		$matches->{$i}{$j} = 1;
	    } else {
		$x -= max($w,$xw);
	    }
	    my $y = $table->{$i}{$j-1}-$w;
	    my $z = $table->{$i-1}{$j}-$w;
	    my $v = max(0,$x,$y,$z);
##	    verbose("matrice i=$i j=$j v=$v xform='$xform' docform='$form' w=$w");
	    $table->{$i}{$j} = $v;
	    $best = { v => $v, i => $i, j => $j} if ($v > $best->{v});
	}
    }
    ## extract best mapping
    my $i = $best->{i};
    my $j = $best->{j};
##    verbose("best is v=$best->{v} i=$best->{i} j=$best->{j}");
    @map = ([$i,$j]);
    while ($table->{$i}{$j} > 0) {
	my $max = max($table->{$i-1}{$j-1},$table->{$i}{$j-1},$table->{$i-1}{$j});
	if ($table->{$i-1}{$j-1} == $max) {
	    $i--;
	    $j--;
	    unshift(@map,[$i,$j]);
	} elsif ($table->{$i}{$j-1} == $max) {
	    $j--;
	    unshift(@map,[$i,$j]);
	} else {
	    $i--;
	    unshift(@map,[$i,$j]);
	}
    }
    verbose('local align mapping '.join(' ',map {"$_->[0]:$_->[1]"} @map));
    ## get first aligned tokens in the best mapping
    ## these two tokens will be re-syncronized
    foreach my $sync (@map) {
	($i,$j) = @$sync;
	next unless ($i > 0 && $j > 0 && $table->{$i}{$j} > 0);
##	verbose("compare $i:'$tokens->{$tokens[$i-1]}{form}' with $j:'$doctokens[$j-1]{form}'");
	next unless ($matches->{$i}{$j});
	last;
    }
    ## not found any sync
    return undef unless ($i > 0 && $j > 0 && $table->{$i}{$j} > 0);
    ## we got a sync point
    $i--;
    $j--;
    ## we resync at $i for $tokens and $j for $doctokens
    my @ignored = ();
    for (my $k = 0; $k < $i; $k++) {
	my $u = shift @tokens;
	push(@ignored,$tokens->{$u});
    }
    my $sync = $tokens[0];
    my $dtok = $doctokens[$j];
    $dtok->{sync} = $sync;
    $dtok->{ignored} = [@ignored];
    return $dtok;
}
