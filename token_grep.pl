#!/usr/bin/perl

# to search with multi-lines patterns into tokens.log

## example: 
## token_grep.pl -p ' cat:adj ' -p 'form:que mode:robust ref:void hyp:mismatch'

use strict;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "pattern|p=s@" => {DEFAULT => []}
                           );

$config->args();

my %map = (
	   'mode' => '$mode',
	   'cat' => '$cat',
	   'form' => '$form',
	   'hyp' => '$thyp',
	   'ref' => '$tref',
	   'id' => '$id',
);

my @patterns = map {compile_pattern($_)} @{$config->pattern};

##print "PATTERN @patterns\n";

push(@ARGV,'tokens.log') unless (@ARGV);

my @pending = ();
my @tmp = @patterns;

my $n = 0;

while (my $line = <>) {
 TEST: 
  if (@tmp) {
    if ($tmp[0]->($line)) {
      shift @tmp;
      push(@pending,$line);
      unless (@tmp) {
	$n++;
	print "-"x30,$n,"-"x30,"\n";
	print $_ foreach (@pending);
	@pending = ();
	@tmp = @patterns;
      }
    } elsif (@pending) {
      @pending = ();
      @tmp = @patterns;
      goto TEST;
    }
  }
}

sub compile_pattern {
  my $s = shift;
  $s =~ s/^\s+//o;
  $s =~ s/\s+$//o;
  my $pattern = <<EOF;
sub {
  my \$line = shift;
  my (\$id,\$form,\$cat,\$mode,\$tref,\$thyp) = \$line =~ /^(\\S+F\\d+)\\s+(.+?)\\s+(\\S+)\\s+(robust|full)\\s+(\\S+)\\s+(\\S+)/o;
EOF
  my @tests = split(/\s+/,$s);
  foreach my $test (@tests) {
    my ($f,$t) = split(/[:=]/,$test);
##    print "test f='$f' t='$t'\n";
    if ($f eq 'form') {
      $t =~ s/_/ /og;
      $pattern .= "\n\treturn 0 unless (\$form =~ /$t/);";
    } elsif (($f eq 'hyp') && $t eq 'mismatch') {
      $pattern .= "\n\treturn 0 unless (\$tref ne \$thyp);";
    } elsif (($f eq 'hyp') && $t eq 'match') {
      $pattern .= "\n\treturn 0 unless (\$tref eq \$thyp);";
    } else {
      $pattern .= "\n\treturn 0 unless ($map{$f} =~ /$t/);";
    }
  }
  $pattern .= "\n\treturn 1; }";
  my $fun = eval($pattern);
##  print "pat=$pattern fun=$fun\n";
  return $fun;
}

