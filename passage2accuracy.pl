#!/usr/bin/env perl

use strict;
use XML::Twig;
use File::Basename;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "ref|r=f"  => {DEFAULT => "/Users/clerger/Work/EASy/ref290109"},
			    "freqform=f" => {DEFAULT => "concat1/freqform.txt"},
			    "freqth=i" => {DEFAULT => 50},
			    "full!" => {DEFAULT => 1},
			    "robust!" => {DEFAULT => 1},
			    "html=f" => {DEFAULT => "accuracy.html"},
			    "passageref!" => {DEFAULT => 0}
                           );

$config->args();

##print "Handling $corpus\n";
my $refdir = $config->ref;

my $freqform = $config->freqform;
my $freqth=$config->freqth;

my $keepfull = $config->full;
my $keeprobust = $config->robust;

my $html = $config->html;

my $passageref = $config->passageref;

my %forms=();
my $corpus;

my @files = @ARGV;

my @gtypes = qw/GN GA GR GP NV PV/;
my @allgtypes = ('?','mis','void');

foreach my $gt (@gtypes) {
  push(@allgtypes,"B_$gt",$gt,"E_$gt","BE_$gt");
}

my $accinfo = { n => 0, 
		correct => 0,
		full => { n => 0, correct => 0, confusion => {}},
		robust => { n => 0, correct => 0, confusion => {}},
		confusion => {},
		corpus => {}
	      };

my $refinfo = {};
my $reftokens = {};

open(TOK,">tokens.log") || die "can't open 'tokens.log': $!";

foreach my $file (@files) {
  $corpus = fileparse($file,qr{.xml},qr{.xml.bz2});
  $accinfo->{corpus}{$corpus} = { n => 0,
				  correct => 0,
				  full => { n => 0, correct => 0 },
				  robust => { n => 0, correct => 0 }
				};
  my $ref = "$refdir/$corpus.xml";
  print TOK "## Processing corpus $corpus\n";
  process_ref_corpus($ref);
  process_hyp_corpus($file);
}

close(TOK);

open(HTML,">$html") || die "can't open $html: $!";
print HTML header,
  start_html('EASy files and stats'),
  h1('Accuracy');


sub perc {
  my ($a,$b) = @_;
  return '-' if ($b == 0);
  return sprintf("%.2f%%",$a / $b * 100);
}

my $acc = perc($accinfo->{correct},$accinfo->{n});
my $facc = perc($accinfo->{full}{correct},$accinfo->{full}{n});
my $racc = perc($accinfo->{robust}{correct},$accinfo->{robust}{n});

my @table = (td(['all',$acc,$facc,$racc,$accinfo->{n}]));

foreach my $c (sort keys %{$accinfo->{corpus}}) {
  my $info = $accinfo->{corpus}{$c};
  my $acc = perc($info->{correct},$info->{n});
  my $facc = perc($info->{full}{correct},$info->{full}{n});
  my $racc = perc($info->{robust}{correct},$info->{robust}{n});
  push(@table,td([$c,$acc,$facc,$racc,$info->{n}]));
}

print HTML table( {-border => 1},
		  Tr( [th(['','all','full','robust','#tok']),
		       @table
		      ] ) );

print HTML h1("General confusion table");
confusion_table($accinfo->{confusion});

print HTML h1("Confusion table for mode=full");
confusion_table($accinfo->{full}{confusion});

print HTML h1("Confusion table for mode=robust");
confusion_table($accinfo->{robust}{confusion});

sub confusion_table {
  my $table = shift;
  my $tb = [th(['classified as =>', @allgtypes,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  foreach my $t (@allgtypes) {
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    foreach my $r (@allgtypes) {
      $total += $info->{$r} || 0;
      $sum->{$r} += $info->{$r} || 0;
    }
    foreach my $r (@allgtypes) {
      my $n = $info->{$r} || 0;
      my $x = perc($n,$total);
      $n = "<span style='color: red;'>$n</span>" if ($r eq $t);
      $n = "<span style='background-color: blue;'>$n</span>" if ($n > 150 && $r ne $t);
      push(@x,"<span title='$t $r'>$n<br/>$x</span>");
    }
    push(@$tb,td([$t,@x,$total,$t]));
    $ttotal += $total;
  }
  my @x = ('total');
  my $rtotal = 0;
  foreach my $r (@allgtypes) {
    push(@x,$sum->{$r});
    $rtotal += $sum->{$r};
  }
  push(@x,"$rtotal/$ttotal");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['classified as =>', @allgtypes,'total','']));
  print HTML table({-border => 1}, Tr($tb));
}

print HTML end_html;
close(HTML);

sub process_ref_corpus {
  my $ref = shift;
  $refinfo = {};
  $reftokens = {};

  my $refxml = XML::Twig->new(
			      twig_handlers => {
						'E' => \&ref_sentence_handler,
						'Sentence' => \&passageref_sentence_handler
					       }
			     );
  
  $refxml->parsefile($ref);
  $refxml->purge;
}

sub ref_sentence_handler {
  my ($t,$s) = @_;
  foreach my $t (qw/hypconsts hyprels/) {
    if (my $x = $s->first_child($t)) {
      $x->delete;
    }
  }
  foreach my $w ($s->descendants('F')) {
    my $txt = $w->trimmed_text();
    my $group = $w->parent('Groupe');
    my $gtype = 'void';
    my $id = $w->att('id');
    next if (exists $refinfo->{$id});
    if (defined $group) {
      my $first = $group->first_child('F')->att('id');
      my $last = $group->last_child('F')->att('id');
      my $prefix = "";
      $prefix .= "B" if ($first eq $id);
      $prefix .= "E" if ($last eq $id);
      $prefix .= "_" if ($prefix);
      $gtype = $prefix.$group->att('type');
    }
    $refinfo->{$id} = { form => $txt, 
			gtype => $gtype };
  }
  $t->purge;
}


sub passageref_sentence_handler {
  my ($t,$s) = @_;
  my $tokens = {};
  foreach my $tok ($s->children('T')) {
    my $txt = $tokens->{$tok->id} = $tok->trimmed_text();
    $reftokens->{$tok->id} = { form => $txt, w => [] };
  }
  foreach my $w ($s->descendants('W')) {
    my @toks = split(/\s+/,$w->att('tokens'));
    my $txt = join(' ',map {$tokens->{$_}} @toks);
    my $group = $w->parent('G');
    my $gtype = 'void';
    my $id = $w->att('id');
    next if (exists $refinfo->{$id});
    if (defined $group) {
      my $first = $group->first_child('W')->att('id');
      my $last = $group->last_child('W')->att('id');
      my $prefix = "";
      $prefix .= "B" if ($first eq $id);
      $prefix .= "E" if ($last eq $id);
      $prefix .= "_" if ($prefix);
      $gtype = $prefix.$group->att('type');
    }
    my $x = $refinfo->{$id} = { form => $txt, 
				gtype => $gtype };
    push(@{$reftokens->{$_}{w}},$x) foreach (@toks);
  }
  $t->purge;
}


sub process_hyp_corpus {
  my $file = shift;
##  print "Process hyp file $file\n";
  my $xml = XML::Twig->new(
			   twig_handlers => {
					     'Sentence' => \&sentence_handler
					    }
			  );
  if ($file =~ /\.bz2$/) {
    open(FILE,"bzcat $file|") || die "can't open $file: $!";
  }   elsif ($file =~ /\.gz$/) {
    open(FILE,"gzcat $file|") || die "can't open $file: $!";
  } else {
    open(FILE,"<$file") || die "can't open $file: $!";
  }
  $xml->parse(\*FILE);
  $xml->purge;
  close(FILE);
}

sub sentence_handler {
  my ($t,$s) = @_;
  unless ($s->children('G')) {
    $t->purge;
    return;
  }
  my $sid = $s->id;
  my $mode = $s->att('mode');
  $mode eq 'corrected' and $mode = 'full';
  return unless (($keepfull && ($mode eq 'full')) || ($keeprobust && ($mode eq 'robust')));
  my $tokens = {};
  my @tokens = ();
  my $forms = {};
  my $groups = {};
  my $state = 'init';
  foreach my $tok ($s->children('T')) {
    my $id = $tok->id;
    my $refid = $tok->id;
    $passageref or $refid =~ s/T/F/;
    $refid =~ s/\.\d+//;
    my $ref = $passageref ? $reftokens->{$refid} : $refinfo->{$refid};
    my $gtype = 'mis';
    unless (defined $ref) {
      print "NO REF $corpus $id refid=$refid\n";
    }
    if ($ref) {
      if ($passageref) {
	$gtype = $ref->{w}[0]{gtype};
      } else {
	$gtype = $ref->{gtype};
      }
    }
    my $tt = $tokens->{$tok->id} 
      = { form => quote($ref->{form} || $tok->trimmed_text()), 
	  w => [],
	  id => "${corpus}.$refid",
	  gtype => $gtype,
	  uid => $tok->id,
	  ws => []
	};
    push(@tokens,$tt);
  }

  # reused from passage2easy, to handle some mutiple-use of tokens
  my $ws={};
  foreach my $w ($s->descendants('W')) {
    my @toks = split(/\s+/,$w->att('tokens'));
    my $wid = $w->att('id');
    my $info = $ws->{$wid} = { tokens => [@toks], xml => $w, id => $wid };
    push(@{$tokens->{$_}{ws}},$info) foreach (@toks);
  }
  foreach my $tt (@tokens) {
    my @ws = @{$tt->{ws}};
    my $first = shift @ws;
    next unless (@ws);
    next if (grep {@{$_->{tokens}}==1} @ws);
    ## try to some something for Ts in several Ws
    my @toks = @{$first->{tokens}};
    my $u = shift @toks;
    if (@toks && $u == $tt->{uid}) {
      ## T belong to a multi-T W and to several W
      ## we keep T in the first W and remove it from the other Ws
      my @debug = map {$_->{id}} $first,@ws;
      $first->{tokens} = [$u];
      shift @{$tokens->{$_}{ws}} foreach (@toks);
      shift @{$_->{tokens}} foreach (@ws);
    }
  }

  foreach my $w ($s->descendants('W')) {
    #    my @toks = split(/\s+/,$w->att('tokens'));
    my $group = $w->parent('G');
    my $id = $w->id;
    my @toks = @{$ws->{$id}{tokens}};
    my $gtype = 'void';
    if (defined $group) {
      my $first = $group->first_child('W')->id;
      my $last = $group->last_child('W')->id;
      my $prefix = "";
      $prefix .= "B" if ($first eq $id);
      $prefix .= "E" if ($last eq $id);
      $prefix .= "_" if ($prefix);
      $gtype = $prefix.$group->att('type');
    }
    my $pos = $w->att('pos');
    if (defined $pos && ($pos eq 'v')) {
      if (my $mstag = $w->att('mstag')) {
	$mstag =~ /mode\.(\S+)/;
	my $mode = $1;
	if ($mode && grep {$_ eq $mode} qw/participle infinitive gerundive/) {
	  $pos .= ".$mode";
	}
      }
    }
    my ($tpref,$tbase) = gtype2elem($gtype);
    foreach my $tid (@toks) {
      my $tt = $tokens->{$tid};
      my $is_first = ($tid eq $toks[0]);
      my $is_last = ($tid eq $toks[-1]);
      my $lgtype = $gtype;
      if (!$tpref) {
	# nothing to do
      } elsif ($is_first && $is_last) {
	# nothing to do
      } elsif ($tpref eq 'B') {
	$lgtype = "$tbase" unless ($is_first);
      } elsif ($tpref eq 'E') {
	$lgtype = "$tbase" unless ($is_last);
      } elsif ($tpref eq 'BE') {
	if ($is_first) {
	  $lgtype = "B_$tbase"
	} elsif ($is_last) {
	  $lgtype = "E_$tbase"
	} else {
	  $lgtype = "$tbase";
	}
      }
      my $info = { cat => $pos || '?',
		   gtype => $lgtype,
		   lemma => quote($w->att('lemma') || '?'),
		   form => $w->att('form') || ""
		 };
      push(@{$tt->{w}}, $info);
    }
  }
  while (@tokens) {
    my $tok = shift @tokens;
    my $atts = get_atts($tok);
    my $refgtype = $tok->{gtype};
    my $hypgtype;
    foreach my $w (@{$tok->{w}}) {
      my $lgtype = $w->{gtype};
      $hypgtype = $lgtype, next unless (defined $hypgtype);
      my ($lpref,$lbase) = gtype2elem($lgtype);
      my ($hpref,$hbase) = gtype2elem($hypgtype);
##      print "TEST local=$lgtype hyp=$hypgtype : ($lpref,$lbase) vs ($hpref,$hbase)\n";
      if ($hypgtype eq 'void') {
	$hypgtype = $lgtype;
	next;
      } elsif ($lgtype eq 'void') {
	# nothing to do
	next;
      } elsif ($lgtype eq $hypgtype) {
	# nothing to do
	next;
      } elsif ($hpref eq 'BE') {
	$hypgtype = $lgtype;
	next;
      } elsif ($lpref eq 'BE') {
	# nothing to do
	next;
      } elsif ($lbase ne $hbase) {
	$hypgtype = '?'; last;
      } elsif (($hpref eq 'B') && !$lpref) {
	next;
      } elsif (($hpref eq '') && !$lpref) {
	next;
      } elsif (($hpref eq 'B') && ($lpref eq 'E')) { 
	$hypgtype = "BE_$hbase"; 
	next;
      } elsif (!$hpref && ($lpref eq 'E')) {
	$hypgtype = $lgtype;
	next;
      } else {
	$hypgtype = '?';
	last;
      }
    }
    if ($hypgtype eq '?') {
      my @gtypes = map {$_->{gtype}} @{$tok->{w}};
##      print "*** $tok->{id} ref=$refgtype hyp=@gtypes\n";
      print TOK "## *** $tok->{id} $tok->{form} ref=$refgtype hyp=@gtypes\n";
      $hypgtype = "E_$state" unless ($state eq 'init');
    }
    my ($hpref,$hbase) = gtype2elem($hypgtype);
    if ($state eq 'init') {
      # nothing to do
      print TOK "## *** $tok->{id} $tok->{form} transition pb B_$state $hypgtype\n" if (!$hpref && !(grep {$hbase eq $_} qw/? void/));
    } elsif ($hbase ne $state) {
      print TOK "## *** $tok->{id} $tok->{form} transition pb B_$state $hypgtype\n";
    } elsif (($hpref eq 'B') || ($hpref eq 'BE')) {
      print TOK "## *** $tok->{id} $tok->{form} transition pb B_$state $hypgtype\n";
    }
    $state = 'init' if ($hpref eq 'E' || $hpref eq 'BE');
    $state = $hbase if ($hpref eq 'B');
    my $cat = join('_',map {$_->{cat}} @{$tok->{w}});
    print TOK "$tok->{id} $tok->{form} $cat $mode $refgtype $hypgtype\n";
    $accinfo->{n}++;
    $accinfo->{$mode}{n}++;
    $accinfo->{corpus}{$corpus}{n}++;
    $accinfo->{corpus}{$corpus}{$mode}{n}++;
    if ($refgtype eq $hypgtype) {
      $accinfo->{correct}++;
      $accinfo->{$mode}{correct}++;
      $accinfo->{corpus}{$corpus}{correct}++;
      $accinfo->{corpus}{$corpus}{$mode}{correct}++;
    }
    $accinfo->{confusion}{$refgtype}{$hypgtype}++;
    $accinfo->{$mode}{confusion}{$refgtype}{$hypgtype}++;
  }
  $t->purge;
}
 
sub gtype2elem {
  my $gtype = shift;
  my ($prefix,$base) = ($gtype =~ /^(BE|E|B|)_?(.*)/);
##  print "DEC $gtype: pref='$prefix' base='$base'\n";
  return ($prefix,$base);
}

sub get_atts {
  my $tok = shift;
  my @cats = @{$tok->{w}};
  my $cat = join('+',map {$_->{cat}} @cats);
  $cat ||= '?';
  my $lemma = join('+',map {$_->{lemma}} @{$tok->{w}});
  my $gtype = join('+',map {$_->{gtype}} @{$tok->{w}});
  my $form = $tok->{w}[0]{form};
  if (defined $form && exists $forms{$form}) {
    $form = quote($form);
    $form = "$form";
  } else {
    $form = '$$';
  }
  return [$form,"$cat"];
}

sub quote {
  my $s = shift;
##  $s =~ s/'/\\'/og;
  return $s;
}
