<?xml version="1.0"?>
<!-- Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr" -->
<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="xml"
        indent="yes"
        encoding="ISO-8859-1"/>

<xsl:template match="E">
  <E id="{@id}">
    <xsl:apply-templates/>
  </E>
</xsl:template>

<xsl:template match="F">
  <F id="{@id}">
    <xsl:apply-templates/>
  </F>
</xsl:template>

<xsl:template match="constituants">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="*">
  <xsl:copy>
    <xsl:copy-of select="@*"/>
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
