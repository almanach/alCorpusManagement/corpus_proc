#!/usr/bin/perl

use strict;
use File::Basename;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "corpus=f@" => {DEFAULT => []},
                            "results=f" => {DEFAULT => 'results'},
                            "dest=f" => {DEFAULT => 'results'},
			    "maxiter=i" => {DEFAULT => 40},
			    "dump!" => {DEFAULT => 0},
			    "beta=i" => {DEFAULT => 0.1},
			    'distrib=f'
                           );

my $conffile = 'dispatch.conf';

if (@ARGV && $ARGV[0] =~ m/^--config/) {
  shift @ARGV;
  $conffile = shift @ARGV;
  print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
  $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

my $results = $config->results;
my $dest = $config->dest;
my $verbose = $config->verbose;

my @files = @ARGV;
@files = glob("*.xml.bz2") unless (@files);
@files = glob("*.xml") unless (@files);

my $rules = {};			# info on rules
my $sentences = {};		# info on full sentences

my @rules = ();                 # list of all rules
my @sentences = ();		# list of full sentences

my $all = 0;			# number of rule occurrences

my $full = 0;			# number of fully parsed sentences

my $rcount = 0;		# rule counter

my $allw =0;

my $avg;			# avg weight per rule

my $maxiter=$config->maxiter;
my $beta = $config->beta;

my $minr = 0;

my $exclude = { '-RANK' => 1,
		'-LONG' => 1,
		'+CLOSED' => 1,
		'+CatPref' => 1,
		'-VIRTUAL' => 1
	      };


loader();

for (my $iter=1; $iter <= $maxiter; $iter++) {
  process($iter);
}

emit_rules();

sub emit_rules {

  open(RULES1,">rules.freq.db")
    || die "can't open rules.freq.db: $!";
  
  open(RULES2,">rules.proba2.db")
    || die "can't open rules.proba2.db: $!";
  
  open(RULES3,">rules.proba3.db")
    || die "can't open rules.proba3.db: $!";

  open(RULES4,">rules.iter.db")
    || die "can't open rules.iter.db: $!";

  
  print <<EOF;
## all=$all

EOF

  foreach my $rentry (sort {$b->[2] <=> $a->[2]} @rules) {
    my $rule = $rentry->[3];
    my $info = $rules->{$rule};
    my $avg = $info->{w} / $info->{n};
    my $freq = 	 200000 * ($info->{relw} / $full);
    my $rep = 	 10000 * ($info->{relw} / $info->{s});
    my $repfreq = 	$rep * exp($info->{n} / $all);
    my $iter = $rentry->[0] + $minr;
    my $total = $info->{n}+$info->{del};
    my $proba = (log($info->{n} / $total))*1000;
    my $beta = 0.2*(1-exp(-$total/100));
    my $probabar = (1-$beta)*$avg+$beta*$proba;
    my $total2 = $info->{n2}+$info->{del2};
##    my $proba2 = $total2 ? (log($info->{n2} / $total2)+log(2))*1000 : -10000;
    my $proba2 = (log($info->{n2} / $info->{ntree})+log(2))*1000;
    my $probabar2 = (1-$beta)*$avg+$beta*$proba2;
    my $proba3 = (log($info->{p} / $info->{s}))*1000;
    my $probax = (log($info->{px} / $total))*1000;
    my $multiplicity = $total / $info->{edges};
    printf("%25s: %12.2f %6.0f %6.0f %6.0f %6.0f %6.0f %d/%d %d %.1f\n",
	   $rule,
	   ##	 $info->{w},
	   $avg-2000,
	   10000 * ($info->{n} / $all) * ($info->{w} > 0 ? 1 : -1),
	   $freq,
##	   $rep,
##	   $repfreq,
##	   $iter,
	   $proba,
##	   $probabar,
##	   $proba2,
##	   $probabar2,
	   $proba,
	   $probax,
	   $info->{n},
	   $info->{del},
	   $info->{s},
	   $multiplicity
	  );
    
    print RULES1 db_rule($rule,$freq,$avg);
    print RULES2 db_rule($rule,$probabar2,$avg);
    print RULES3 db_rule($rule,$proba3,$avg);
    print RULES4 db_rule($rule,$iter,$avg);
    
  }

  close(RULES1);
  close(RULES2);
  close(RULES3);
  close(RULES4);
}

sub db_rule {
  my ($r,$v,$avg) =@_;
  $v = sprintf("%.0f",$v);
  $avg = sprintf("%.0f",$avg);
  my $prefix = ($exclude->{$r}) ? '%% ' : '';
  return <<EOF;
${prefix}rule_weight('$r',$v). %% default=$avg
EOF
}

sub loader {
  open(LINE,">$dest/easylines.dat") || die "can't open easylines.dat";
  foreach my $file (@files) {
    process_file($file);
  }
  close(LINE);
  

  foreach my $rentry (@rules) {
    $rentry->[5] -= $rentry->[2] * $minr;
    $rentry->[0] = ($rentry->[5] / $rentry->[2]);
  }

  foreach my $sentry (@sentences) {
    $sentry->{w} -= $sentry->{n} * $minr;
  }

}

sub process_file {
  my $file = shift;
  my $corpus = fileparse($file,qr{.xml},qr{.xml.bz2});
##  print "processing $file\n";
  if ($file =~ /\.bz2/) {
    open(FILE,"bzcat $file|") || die "can't open $file: $!";
  } else {
    open(FILE,"<$file") || die "can't open $file: $!";
  }
  my $is_full = 0;
  my $lrules = {};
  my $lw = 0;
  my $sentry;
  while (<FILE>) {
    my $line = $_;
    if ($line =~ /<Sentence/) {
      $is_full = ($line =~ /mode="full"/) ? 1 : 0;
      my ($sid) = ($line =~ /id="(E.*?)"/);
      $lrules = {};
      $lw = 0;
      $sentry = { rules => [],
		  n => 0,
		  corpus => $corpus,
		  id => $sid,
		  line => $.,
		  clusters => {}
		};
      print LINE "$corpus $sid $.\n";
      if ($is_full) {
	$sentry->{sid} = $full++;
	push(@sentences,$sentry);
      }
      next;
    }
    if ($is_full && ($line =~ m{</Sentence})) {
      ## the number of parses exponentally depends on the
      ## number of rejected edges
      ## a lot of deleted edges means we have less confidence
      ## in the selected rules
      ## could be refined to take into account if the edge mandatory
      ## in all parses or not (this info should be available soon)
      my $length = $sentry->{edgekept};
      next unless ($length > 0);
      my $alpha = ($sentry->{edgedel}+$sentry->{edgekept}) / $length;
      my $cc = $alpha * (1-$alpha);
      my $allparses = 1;
      foreach my $c (keys %{$sentry->{clusters}}) {
	$allparses *= $sentry->{clusters}{$c};
      }
      foreach my $rule (keys %$lrules) {
	my $info = $rules->{$rule};
	my $linfo = $lrules->{$rule};
	$info->{relw} += ($linfo->{w} / $lw);
	$info->{s}++;
	my $cx = (1-($linfo->{n}+$linfo->{del})/($alpha*$length));
	$info->{ntree} = ($alpha < 1) ? $alpha**$length*(1-$cx**$length) : 1;
	$info->{n2}++ if ($linfo->{n});
	my $p = ($cc > 0) ? ($linfo->{n} / $cc) : $linfo->{n};
	$p =  1 if ($p > 1);
	$info->{p} += $p;
	foreach my $c1 (keys %{$linfo->{clusters}}) {
	  my $lparses = 1;
	  foreach my $c (keys %{$sentry->{clusters}}) {
	    unless ($c1 eq $c) {
	      $lparses *= $sentry->{clusters}{$c};
	    }
	  }
	  $info->{px} += $lparses / $allparses;
	}
      }
      next;
    }
    if ($is_full && ($line =~ /<cost/)) {
      my ($kept) = ($line =~ /kept="(yes|no)"/);
      my ($ws) = ($line =~ /ws="\[\s*(.*?)\s*\]"/);
      my ($extra) = ($line =~ /info="\[\s*(.*?)\s*\]"/);
      my ($source) = ($line =~ /source="(.*?)"/);
      my ($target) = ($line =~ /target="(.*?)"/);
      my @extra = split(/\s*,\s*/,$extra);
      my @ws = split(/\s*,\s*/,$ws);
      if ($kept && ($kept eq 'no')) {
	$sentry->{edgedel}++;
      } else {
	$sentry->{edgekept}++;
      }
      $sentry->{clusters}{$target}++;
      my %seen = ();
      foreach my $x (@ws) {
	my ($rule,$w) = split(/\s*:\s*/,$x);
	$rule = join('_',$rule,@extra);
	##	next if ($exclude->{$rule});
	$seen{$rule}++;
	my $info = $rules->{$rule} ||= { n => 0, 
					 w => 0, 
					 relw => 0, 
					 s => 0,
##					 id => $rcount++,
					 del => 0,
				       };

	my $linfo = $lrules->{$rule} ||= { w => 0,
					   n => 0,
					   del => 0,
					   edges => 0
					 };

	if ($kept && ($kept eq 'no')) {
	  $info->{del}++;
	  $sentry->{del}++;
	  $linfo->{del}++;
	  next;
	}

	$info->{id} = $rcount++ unless (exists $info->{id}); 
	$info->{n}++;
	$info->{w} += $w;

	$linfo->{clusters}{$target}++;

	my $rid = $info->{id};
	## wentry: 0=rate 1=tmprate 2=occ 3=$name 4=confidence 5=weight 6=sentences 
	## 7=spring 8=history 9=barycentre

	my $rentry = $rules[$rid] ||= [1,0,0,$rule,0,0,[],0,"",1];
	push(@{$sentry->{rules}},$rentry); # push rule in current sentence
	## update current sentence
	$sentry->{n}++;		# increase $rules in current sentence
	$sentry->{w} += $w;

	## update current rule
	$rentry->[2]++;		# increase #occ for this rule
	$rentry->[5] += $w;	# cumulated weight for this rule
	$all++;			# total number of all rule occurrences
	$allw += $w;

	$linfo->{w} += $w;
	$linfo->{n}++;
	$lw += abs($w);
	
	if ($w < $minr) {
	  $minr = $w;
	}

      }

      foreach my $rule (keys %seen) {
	$rules->{$rule}{edges}++;
      }

    }
  }
  close(FILE);
}


sub process {
  my $iter = shift;

  print STDERR "Processing round $iter...\n";

  foreach my $rentry (@rules) {
    $rentry->[6] = [];
  }

  print STDERR "\tUpdating rule occurrences in sentences\n";
  foreach my $sentry (@sentences) {
    next if ($sentry->{w} == 0);
    my $n = 0;
    my $max=0;
    $n += $_->[0] foreach (@{$sentry->{rules}});
    $n += 0.01 unless ($n);
    my $m = $sentry->{w} / $n;
    foreach my $rentry (@{$sentry->{rules}}) {
      my $delta = $rentry->[0] * $m;
      $rentry->[1] += $delta;
      if ($delta > $max) {
	$max=$delta;
      }
    }
    $sentry->{max} = $max;
    if ($iter == $maxiter ) {
      if ($max == 0) {
	print STDERR "*** plus max=0 $sentry->{corpus} $sentry->{id}\n";
      } else {
	my $thresdhold=0.6*$max;
	foreach my $rentry (@{$sentry->{rules}}) {
	  my $delta = ($rentry->[0] * $m);
	  next unless ($delta > $thresdhold);
	  my @l = ([$delta,$sentry],@{$rentry->[6]});
	  @l = sort {$b->[0] <=> $a->[0]} @l;
	  @l = @l[0..20] if (@l > 20);
	  $rentry->[6] = [@l];
	}
      }
    }
  }

  print STDERR "\tUpdating rules\n";

  foreach my $rentry (@rules) {
    my $tmp = $rentry->[0] = $rentry->[1] / $rentry->[2];
    ##    my $avg = $rentry->[10][1];
    $rentry->[1] = 0;
    ##    $rentry->[9] = $avg + $rentry->[7]*($tmp - $avg);
    ##    $wentry->[4] = $wentry->[9] * $wentry->[2];
  }
  
  ## sorting @rules
  unless (($iter % 5) && ($iter < ($maxiter -1))) {
    @rules = sort {$b->[0] <=> $a->[0]} @rules;
    my $rank=0;
    # handling history on words
    foreach my $rentry (@rules) {
##      next unless ($rentry->[2] > 5);
##      last unless ($rentry->[9] > 1.5*$avg);
      $rank++;
      $rentry->[8] .= sprintf(" %d:%d:%.2f",$iter,$rank,$rentry->[0]+$minr);
    }
  }

  mydump($iter) if ($iter == $maxiter);

}

sub mydump {
  my $iter = shift;
  ##  return if ($iter % 5);
  print STDERR "Dumping round $iter ...\n";
  print STDERR "\tsorting\n";
  print STDERR "\twriting to $dest/rules$iter.csv\n";
  open(DUMP,">$dest/rules$iter".".csv") || die "can't dump iter $iter";
#  print "#AVERAGE AMB RATE=$avg\n";
#  print DUMP "#AVERAGE AMB RATE=$avg\n";
  print DUMP <<EOF;
# $minr = $minr
#rule\t#occ\tweight\tconfidence\trate

EOF
  my $rank=0;
  foreach my $rentry (@rules) {
    ## next unless ($wentry->[2] > 5);
    ## last unless ($wentry->[9] > 1.5*$avg);
    ## next unless ($iter != $maxiter || @{$wentry->[6]});
    $rank++;
    printf DUMP "%s\t%d\t%.2f\t%.2f\t%.2f\n",
      $rentry->[3],
##	$rentry->[9],
	  $rentry->[2],
	    $rentry->[5]+$minr*$rentry->[2],
	      $rentry->[4],
		$rentry->[0]+$minr;
    print DUMP "\thistory $rentry->[8]\n";
    foreach my $info (@{$rentry->[6]}) {
      my $sentry = $info->[1];
      print DUMP "\t$sentry->{corpus}#$sentry->{id}\t$sentry->{line}\n";
    }
  }
  close(DUMP);
}

1;

__END__
