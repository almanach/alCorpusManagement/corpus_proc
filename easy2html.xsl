<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:exslt="http://exslt.org/common"
  >

<xsl:output
  method="html"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:variable name="colors">
  <colors>
    <color id="GN" value="#CC0000"/>
    <color id="NV" value="#009900"/>
    <color id="GP" value="#3333FF"/>
    <color id="GR" value="#CC6600"/>
    <color id="PV" value="#999999"/>
    <color id="GA" value="#993399"/>
  </colors>
</xsl:variable>

<xsl:variable name="relations">
  <relations>
    <relation type="SUJ-V" title="Sujet - Verbe" color="#ffff99">
      <arg name="sujet"/>
      <arg name="verbe"/>
    </relation>
    <relation type="AUX-V" title="Auxiliaire - Verbe" color="#88dd88">
      <arg name="auxiliaire"/>
      <arg name="verbe"/>
    </relation>
    <relation type="COD-V" title="COD - Verbe" color="#ffff99">
      <arg name="COD"/>
      <arg name="verbe"/>
    </relation>
    <relation type="CPL-V" title="Compl�ment - Verbe"  color="#88dd88">
      <arg name="compl�ment"/>
      <arg name="verbe"/>
    </relation>
    <relation type="MOD-V" title="Modifieur - Verbe" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="verbe"/>
    </relation>
    <relation type="COMP" title="Compl�menteur"  color="#88dd88">
      <arg name="compl�menteur"/>
      <arg name="NV de la proposition subordonn�e"/>
    </relation>
    <relation type="ATB-SO" title="Attribut - Sujet / Objet" color="#ffff99">
      <arg name="attribut"/>
      <arg name="verbe"/>
      <arg name="sujet / objet"/>
    </relation>
    <relation type="MOD-N" title="Modifieur - Nom"  color="#88dd88">
      <arg name="modifieur"/>
      <arg name="nom"/>
      <arg name="� propager"/>
    </relation>
    <relation type="MOD-A" title="Modifieur - Adjectif" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="adjectif"/>
    </relation>
    <relation type="MOD-R" title="Modifieur - Adverbe"  color="#88dd88">
      <arg name="modifieur"/>
      <arg name="adverbe"/>
    </relation>
    <relation type="MOD-P" title="Modifieur - Pr�position" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="pr�position"/>
    </relation>
    <relation type="COORD" title="Coordination"  color="#88dd88">
      <arg name="coordonnant"/>
      <arg name="coordonn� gauche"/>
      <arg name="coordonn� droit"/>
    </relation>
    <relation type="APPOS" title="Apposition" color="#ffff99">
      <arg name="premier �l�ment"/>
      <arg name="deuxi�me �l�ment"/>
    </relation>
    <relation type="JUXT" title="Juxtaposition"  color="#88dd88">
      <arg name="premier �l�ment"/>
      <arg name="deuxi�me �l�ment"/>
    </relation>
  </relations>
</xsl:variable>

<xsl:template match="DOCUMENT">
  <html>
    <head>
<!--      <link rel="STYLESHEET" type="text/css" href="file:///home/vagabond/clerger/Grammars/frmg/easy.css"/> -->
      <title> HTML EASy Format </title>
    </head>
    <body bgcolor="linen">
      
      <h1> Annotations EASY</h1>

      More information <a href="http://www.limsi.fr/Recherche/CORVAL/easy/">here</a>

      <p>
      For the following sentences, the first group line provides the
      reference groups and the second group line the hypothese groups.
      </p>

      <p> The color code for relations is:
      <ul>
	<li> [black] : hyp. relations that are OK</li>
	<li> [<font color="red">red</font>] : hyp. relations that are not OK</li>
	<li> [<font color="blue">blue</font>] : missing reference relations (using ref. groups)</li>
      </ul>
      </p>

      <xsl:apply-templates/>

    </body>
  </html>
</xsl:template>

<xsl:template match="E">

  <xsl:variable name="strip" select="1+string-length(@id)"/>
  <hr/>
  <p> <strong>No answer returned for Enonc� <xsl:value-of select="substring(@id,2)"/></strong>
  </p>

</xsl:template>

<xsl:template match="E[F|Groupe/F]">

  <xsl:variable name="strip" select="1+string-length(@id)"/>

  <hr/>
      
      <h2> Constituants </h2>
      <table class="constituants" border="1">
	<!-- Enonc� -->
	<tr>
	  <td colspan="{count(F|Groupe/F)}" bgcolor="#99CCCC">
	    <font size="+1">
	      Enonc� <xsl:value-of select="substring(@id,2)"/>
	    </font>
	  </td>
	</tr>
	<!-- Ref groups -->
	<xsl:if test="refconst">
	  <tr class="groups">
	    <xsl:apply-templates select="refconst/*" mode="groupe">
	      <xsl:with-param name="strip" select="$strip"/>
	    </xsl:apply-templates>
	  </tr>
	</xsl:if>
        <!-- Groups -->
        <tr class="groups">
          <xsl:apply-templates select="Groupe|F" mode="groupe">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
        <!-- Words -->
        <tr class="words" bgcolor="lightBlue">
          <xsl:apply-templates select="F|Groupe/F" mode="word">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
        <!-- F -->
        <tr class="Fs" bgcolor="#FFFFFF">
          <xsl:apply-templates select="F|Groupe/F" mode="F">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
      </table>

      <xsl:if test="Groupe[not(F)]">
	<p>
	  <strong> Bogus empty groups:
	  </strong>
	  <xsl:apply-templates select="Groupe[not(F)]" mode="bogus">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
	</p>
      </xsl:if>

      <h2> Relations </h2>
      
      <table>

	<tr valign="top">
	  <xsl:variable name="current" select="."/>
	  <xsl:for-each select="exslt:node-set($relations)/relations/relation">
	    <xsl:variable name="type" select="@type"/>
	    <xsl:if test="$current/relations/relation[@type=$type] or $current/missingrelations/refrelation[@type=$type] ">
	      <td align="center">
		<h3><xsl:value-of select="position()"/>. <xsl:value-of select="@title"/></h3>
		
		<table border="1" bgcolor="{@color}">
		  <tbody>
		    <tr class="header">
		      <xsl:for-each select="arg">
			<td><font size="+1"><xsl:value-of select="@name"/></font></td>
		      </xsl:for-each>
		    </tr>
		    <xsl:apply-templates select="$current/relations/relation[@type=$type and @status='OK']">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		    <xsl:apply-templates select="$current/relations/relation[@type=$type and not(@status)]">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		    <xsl:apply-templates select="$current/missingrelations/refrelation[@type=$type]">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		  </tbody>
		</table>
		
	      </td>
	    </xsl:if>
	  </xsl:for-each>

	</tr>
      </table>

</xsl:template>

<xsl:template match="Groupe[F]" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./F)}" class="{$type}" bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}">
    <font color="white">
      <xsl:value-of select="@type"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="substring(@id,$strip+1)"/>
    </font>
  </td>
</xsl:template>

<xsl:template match="Groupe[@status='OK']" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./F)}" class="{$type}" 
      bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}"
  >
    <xsl:value-of select="@type"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="Groupe" mode="bogus">
  <xsl:param name="strip"/>
  <xsl:variable name="type" select="@type"/>
  <span style="background: {exslt:node-set($colors)/colors/color[@id=$type]/@value};">
    <xsl:value-of select="@type"/>
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </span>
  <xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="F" mode="groupe">
  <xsl:param name="strip"/>
  <td colspan="1"/>
</xsl:template>

<xsl:template match="refgroup" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./refF)}" class="{$type}" bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}">
      <xsl:value-of select="@type"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="refF" mode="groupe">
  <xsl:param name="strip"/>
  <td colspan="1"/>
</xsl:template>


<xsl:template match="F" mode="word">
  <xsl:param name="strip"/>
  <td>
    <xsl:value-of select="."/>
  </td>
</xsl:template>

<xsl:template match="F" mode="F">
  <xsl:param name="strip"/>
  <td class="F">
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="relation">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<font color="red">
	  <xsl:apply-templates select="." mode="relarg"> 
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
	</font>
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template match="relation[@status='OK']">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<xsl:apply-templates select="." mode="relarg"> 
	  <xsl:with-param name="strip" select="$strip"/>
	</xsl:apply-templates>	
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template match="refrelation">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<font color="blue">
	  <xsl:apply-templates select="." mode="relarg"> 
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
	</font>
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template match="*" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:value-of select="substring(./@xlink:href,$strip)"/>
</xsl:template>

<xsl:template match="s-o" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:value-of select="@valeur"/>
</xsl:template>

<xsl:template match="�-propager[@booleen='vrai']" mode="relarg">
  <xsl:param name="strip"/>
  X
</xsl:template>

</xsl:stylesheet>
