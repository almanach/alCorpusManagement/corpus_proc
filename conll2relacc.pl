#!/usr/bin/env perl

use strict;
use File::Basename;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;
use List::Util qw/max min sum first/;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
                            "refdir|r=f"  => {DEFAULT => "/Users/clergeri/Work/Corpus/FTB6/"},
			    "refversion=f" => {DEFAULT => ""},
			    "html=f" => {DEFAULT => "relaccuracy.html"},
			    "log=f",
			    "th=d" => {DEFAULT => 35},
			    "ratio=d" => {DEFAULT => 14},
			    "size=d" => {DEFAULT => 50},
			    'features=f' => {DEFAULT => 'features.conf'},
			    'oracle=f',
			    'perceptron!' => {DEFAULT => 0},
			    'min_undecided=d' => {DEFAULT => 15}
                           );

$config->args();

##print "Handling $corpus\n";
my $refdir = $config->refdir;
my $refversion = $config->refversion;

my $html = $config->html;

my $perceptron = $config->perceptron;
my $min_undecided = $config->min_undecided;

my $oracle = {};

my %selected_parent = ();		# hash of selected parent edges (because of the selection of an edge)

my %eid2tpos=();		# reset at each sentence

if (my $file=$config->oracle) {
  print "Processing oracle '$file'\n";
  if ($file =~ /\.gz/) {
    open(ORACLE,"-|","gunzip -c $file") ||
      die "die can't read oracle '$file': $!";
  } else {
    open(ORACLE,"<",$file) ||
      die "die can't read oracle '$file': $!";
  }
  my $found = 1;
  while(<ORACLE>) {
    s/^##\s+<yes>\s+//o or next;
    chomp;
    my ($fid,$eid,$tnid,$snid,$label,$type,$seid,@features) = split(/\t+/,$_);
    @features = map {(split(/=/,$_))[1]} @features;
    ##    print "register oracle eid=$eid '$fid $label $type @features'\n";
    $found++;
# only accept exact match
#    $oracle->{"$fid $label $type @features"} = 1;
    $oracle->{"$fid $eid $tnid $snid $seid"} = 1;
  }
  close(ORACLE);
  print "Registered $found entries from oracle\n";
}

sub oracle_is_ok {
  my ($cinfo,$fid) = @_;
  $cinfo or return 0;
  my @key = @{$cinfo}{qw{eid tnid snid}};
  my $seid = exists $cinfo->{seid} ? $cinfo->{seid} : -1;
  $oracle->{"$fid @key $seid"} and return 1;
# only accept exact match
#  @key = map {feature_decode($_)} @{$cinfo}{qw{label type sleft sright stoken tleft tright ttoken stree ttree scat tcat slemma tlemma}};
#  return exists $oracle->{"$fid @key"};
  return 0;
}

my %forms=();
my $corpus;

my @files = @ARGV;

my %hypdeprel = ();
my %refdeprel = ();
my %allrules = ();

my %hypcat = ();
my %refcat = ();

my $cost = {};
my $costctr = 1;

my $bcost = {};
my $bcostctr = 1;

my %tmpcost = ();
my %btmpcost=();

my $undecided = 0;
my $distrib = {};
my $dkeyctr = 1;
my $dkey_registry = {};

my $bundecided = 0;
my $bdistrib = {};
my $bdkeyctr = 1;
my $bkey_registry = {};

my $fskeyctr = 1;
my $fskey_registry = {};

my $labelmap = {};
my $catmap = {};

my $accinfo = { n => 0, 
		correct => 0,
		confusion => {},
		full => { n => 0, correct => 0, confusion => {}},
		robust => { n => 0, correct => 0, confusion => {}},
		fail => { n => 0, correct => 0, confusion => {}},
		corrected => { n => 0, correct => 0, confusion => {}},
		corpus => {},
		cat => { correct => 0,
			 confusion => {}
		       },
		rules => {
			 }
	      };


my %map = ( "1yes" => 3,
	    "1no" => 4,
	    "1samehead" => 4,
	    "1sametype" => 4,
	    "1almostyes" => 4,
	    "0yes" => 5,
	    "0no" => 6,
	    "0samehead" => 6,
	    "0sametype" => 6,
	    "0almostyes" => 6,
	    );

my %dmap = ( "n" => 0,
	     "should_keep" => 1,
	     "1yes" => 2,
	     "1no" => 3,
	     "1samehead" => 3,
	     "1sametype" => 3,
	     "0yes" => 4,
	     "0no" => 5,
	     "0samehead" => 5,
	     "0sametype" => 5
	    );

my %lexicon = ();
my $lexctr=0;

my $refinfo = {};

my @features=();
my @bfeatures=();
my %feature=();

# non integer valued features
my %foreign = (
	       'label' => 'vocabulary',
	       'type' => 'vocabulary',
	       'status' => 'vocabulary',
	       'stoken' => 'vocabulary',
	       'ttoken' => 'vocabulary'
	      );


my $fsetctr=0;

if ($config->features && -f $config->features) {
  my $features = $config->features;
  open(F,"<$features") || die "can't open features file '$features': $!";
  while(<F>) {
    /^\#foreign\s+(\S+)\s+(\S+)/ and $foreign{$1} = $2;
    (/^\s*\#/ || /^\s*$/) and next; # comments and blank lines
    chomp;
    s/^\s+//o;
    s/\#.*$//o;			# comment on end of line
    s/\s+$//o;
    my @tmp = split(/\s+/,$_);
    if ($tmp[0] eq 'empty') {
      push(@features,prepare_fset_entry([]));	# empty feature set
      next;
    }
    $feature{$_} = 1 foreach (@tmp);
    if (grep {/^b/} @tmp) {
      push(@bfeatures,prepare_fset_entry(\@tmp));
    } else {
      push(@features,prepare_fset_entry(\@tmp));
    }
  }
  close(F);
}

sub prepare_fset_entry {
  my $fs = shift;
  my $id = $fsetctr++;
  my $entry = { fs => $fs, id => $id };
  my $test = join(' && ',map {"(exists \$_[0]->{$_})"} @$fs);
  $test ||= 1;
  my $key = join(' ',map {"\$_[0]->{$_}"} @$fs);
  my $key2 = "sub{ $test and fset_key(\$fskey_registry,\$_[0],\$_[1],\"$id \$_[0]->{label} \$_[0]->{type} $key\")}";
  my $value = join(',',map {$foreign{$_} ? "\"+$_=\${\$_[0]->{$_}}\"" : "\"+$_=\$_[0]->{$_}\""} @$fs);
  my $value2 = "sub{ join(\"\\t\",\"label=\${\$_[0]->{label}}\",\"type=\${\$_[0]->{type}}\",$value)}";
  0 and print <<EOF;
prepare fs id=$id @$fs
        check=$key2
        value=$value2
EOF
  $entry->{check} = eval $key2;
  $entry->{value} = eval $value2;
  0 and print <<EOF;
         check eval=$entry->{check}
         value eval=$entry->{value}
EOF
  return $entry;
}

my $log = $config->log || "reltokens$refversion.log";

open(TOK,">$log") || die "can't open '$log': $!";

my @allctxset = ("ctx0" .. "ctx6");

my $oldcost = {};

foreach my $file (@files) {
  $corpus = fileparse($file,qr{.xconll},qr{.xconll.bz2},qr{.xconll.gz},qr{.conll});
  $accinfo->{corpus}{$corpus} = { n => 0,
				  correct => 0,
				};
  my $ref = "$refdir/$corpus$refversion.l1.conll";
  print TOK "## Processing corpus $corpus\n";
  process_ref_corpus($corpus,$ref);
  $cost = {};
  $undecided = 0;
  $distrib = {};
  $dkey_registry = {};
  $dkeyctr = 1;
  process_hyp_corpus($corpus,$file);
  print STDERR "cost processing $corpus\n";
  open(COST,">","$corpus.cost") || die "can't open cost file '$corpus.cost': $!";
#  my $th = $config->th;
#  my $ratio = $config->ratio;
#  my $size = $config->size;
#  my $date = `date`;
  my $minsize = $config->size;
#  print STDERR "\temit rules\n";
#  foreach  my $rule ( sort {$cost->{$b}[1] <=> $cost->{$a}[1]} 
#		      keys %$cost) {
#    printf COST "rule\t%s\t%d\t%.0f\n", $rule,$cost->{$rule}[1],$cost->{$rule}[2] / $cost->{$rule}[1];
#  }
  my $nrules = scalar(keys %$cost);
  print STDERR "\temit ctx costctr=$costctr rules=$nrules\n";
  my $nusedrules=0;
  my $nusedctx=0;
  foreach  my $rule ( sort {$cost->{$b}[1] <=> $cost->{$a}[1]} 
		      grep {$cost->{$_}[1] > $minsize}
		      keys %$cost
		    ){		# rules
    my $info = $cost->{$rule}[0];
    $nusedrules++;
    foreach my $ctx (	sort {$info->{$b}[1] <=> $info->{$a}[1]}
			grep {$info->{$_}[1] > $minsize} 
			keys %$info
		    ) {		# ctx
      $nusedctx++;
      my $ctxinfo = $info->{$ctx};
      my $id = $ctxinfo->[0];
      my $n = $ctxinfo->[1];
      my $v = $ctxinfo->[2] / $n;
      my $keptgood = 100*$ctxinfo->[3] / $n; # yes-yes
      my $keptbad = 100*$ctxinfo->[4] / $n ; # yes-no
      my $delgood = 100*$ctxinfo->[5] / $n;	# no-yes
      my $delbad = 100*$ctxinfo->[6] / $n; # no-no
      
      my $shouldkeep = 100 * ( $keptgood / ($keptgood + $keptbad || 1) - 0.5);
      ($keptgood+$keptbad) or $shouldkeep = 50;
      my $shoulddel = 100 * ($delgood / ($delgood + $delbad || 1) - 0.5);
      ($delgood+$delbad) or $shoulddel = 50;
      my $prefix="";
      ## try to establish a typology of the rule effects
      if ($shouldkeep > 40 && $shoulddel > 40) {
	$prefix = "##good ";
      } elsif (abs($shouldkeep) < 5 && abs($shoulddel) < 5) {
	$prefix = "##noeffect ";
      } elsif ($shouldkeep < 0 && $shoulddel < 0) {
	$prefix = "##strange ";
      } elsif ( !( ($shouldkeep > 5 && $keptgood > 10)
		   || ($shoulddel > 5 && $delgood > 10) ) ) {
	$prefix = "##weak ";
      } 
      printf COST "%s%s\t%s\t%d\t%+.1f\t%.1f\t%.1f\t{%+.1f}\t%.1f\t%.1f\t{%+.1f}\t%d\t%d\t%s\n",
	$prefix,
	  'ctx0',  # $ctxset,
	    $rule,
	      $n,
		$v,
		  $keptgood,
		    $keptbad,
		      $shouldkeep,
			$delgood,
			  $delbad,
			    $shoulddel,
			      $id,
				0,  # $previd,
				  $ctxinfo->[7][2]->($ctxinfo->[7][1]);
				    
    }
  }
  ## brother cost
  my $nusedbctx=0;
  foreach my $ctx ( sort {$bcost->{$b}[1] <=> $bcost->{$a}[1]}
		    grep {$bcost->{$_}[1] > $minsize}
		    keys %$bcost
		  ) {
    $nusedbctx++;
    my $ctxinfo = $bcost->{$ctx};
    my $id = $ctxinfo->[0];
    my $n = $ctxinfo->[1];
    my $keptgood = 100*$ctxinfo->[3] / $n; # yes-yes
    my $keptbad = 100*$ctxinfo->[4] / $n ; # yes-no
    my $delgood = 100*$ctxinfo->[5] / $n;	# no-yes
    my $delbad = 100*$ctxinfo->[6] / $n; # no-no
    
    my $shouldkeep = 100 * ( $keptgood / ($keptgood + $keptbad || 1) - 0.5);
    ($keptgood+$keptbad) or $shouldkeep = 50;
    my $shoulddel = 100 * ($delgood / ($delgood + $delbad || 1) - 0.5);
    ($delgood+$delbad) or $shoulddel = 50;


    printf COST "%s\t%s\t%d\t%+.1f\t%.1f\t%.1f\t{%+.1f}\t%.1f\t%.1f\t{%+.1f}\t%d\t%d\t%s\n",
	'ctx0',  # $ctxset,
	  'brother',
	    $n,
	      0,
		$keptgood,
		  $keptbad,
		    $shouldkeep,
		      $delgood,
			$delbad,
			  $shoulddel,
			    $id,
			      0,  # $previd,
				$ctxinfo->[7][2]->($ctxinfo->[7][1]);
  }
  print "processed rules=$nusedrules ctx=$nusedctx bctx=$nusedbctx\n";
  close(COST);
}
close(TOK);

my @allreftypes = sort {$refdeprel{$b} <=> $refdeprel{$a}} keys %refdeprel;
my @allhyptypes = (@allreftypes,
		   grep {!exists $refdeprel{$_}} 
		   sort {$hypdeprel{$b} <=> $hypdeprel{$a}} 
		   keys %hypdeprel);


my @allrefcat = sort {$refcat{$b} <=> $refcat{$a}} keys %refcat;
my @allhypcat = (@allrefcat,
		   grep {!exists $refcat{$_}} 
		   sort {$hypcat{$b} <=> $hypcat{$a}} 
		   keys %hypcat);


my @allrules = sort {$allrules{$b} <=> $allrules{$a}} keys %allrules;

my $css = <<CSS;
<!--
td .diag {
    background-color: yellow;
}
-->
CSS


open(HTML,">$html") || die "can't open $html: $!";
print HTML header,
  start_html( -title => 'CONLL files and stats',
	      -style =>  { -code => $css }
	    ),
  h1('POS accuracy');

my $catacc = perc($accinfo->{cat}{correct},$accinfo->{n});
my @cattable = (td(['all corpus',$catacc,$accinfo->{n}]));

print HTML table( {-border => 1},
		  Tr( [th(['','all','#tok']),
		       @cattable
		      ] ) );

print HTML
  h1('Relation accuracy');


sub perc {
  my ($a,$b) = @_;
  return '-' if ($b == 0);
  return sprintf("%.2f%%",$a / $b * 100);
}

my $acc = perc($accinfo->{correct},$accinfo->{n});
my $facc = perc($accinfo->{full}{correct},$accinfo->{full}{n});
my $cacc = perc($accinfo->{corrected}{correct},$accinfo->{corrected}{n});
my $racc = perc($accinfo->{robust}{correct},$accinfo->{robust}{n});
my $nacc = perc($accinfo->{fail}{correct},$accinfo->{fail}{n});

my @table = (td(['all corpus',$acc,$facc,$cacc,$racc,$nacc,$accinfo->{n}]));

foreach my $c (sort keys %{$accinfo->{corpus}}) {
  my $info = $accinfo->{corpus}{$c};
  my $acc = perc($info->{correct},$info->{n});
  my $facc = perc($accinfo->{full}{corpus}{$c}{correct},$accinfo->{full}{corpus}{$c}{n});
  my $cacc = perc($accinfo->{corrected}{corpus}{$c}{correct},$accinfo->{corrected}{corpus}{$c}{n});
  my $racc = perc($accinfo->{robust}{corpus}{$c}{correct},$accinfo->{robust}{corpus}{$c}{n});
  my $nacc = perc($accinfo->{fail}{corpus}{$c}{correct},$accinfo->{fail}{corpus}{$c}{n});
  push(@table,td([$c,$acc,$facc,$cacc,$racc,$nacc,$info->{n}]));
}

print HTML table( {-border => 1},
		  Tr( [th(['','all','full','corrected','robust','fail','#tok']),
		       @table
		      ] ) );

print HTML h1("General POS confusion table");
confusion_cattable($accinfo->{cat}{confusion});

print HTML h1("General dependency confusion table");
confusion_table($accinfo);

print HTML h1("Dependency confusion table for mode=full");
confusion_table($accinfo->{full});

print HTML h1("Dependency confusion table for mode=corrected");
confusion_table($accinfo->{corrected});

print HTML h1("Dependency confusion table for mode=robust");
confusion_table($accinfo->{robust});

print HTML h1("Dependency confusion table for mode=fail");
confusion_table($accinfo->{fail});

print HTML h1("Accuracy of CONLL conversion rules");
rule_table($accinfo);

print HTML end_html;
close(HTML);


sub confusion_table {
  my $xtable = $_[0];
  my $table = $xtable->{confusion};
  my $all = $xtable->{n};
  my $tb = [th(['classified as =>', @allhyptypes,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  my $rtotal = {};
  foreach my $r (@allreftypes) {
    foreach my $t (@allhyptypes) {
      my $info = $table->{$t}{$r} || {};
      foreach my $map (keys %$info) {
	$rtotal->{$r} += $info->{$map};
      }
    }
  }
  foreach my $t (@allreftypes) {
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    foreach my $r (@allhyptypes) {
      foreach my $map (keys %{$info->{$r}}) {
	$total += $info->{$r}{$map};
	$sum->{$r} += $info->{$r}{$map};
      }
    }
    foreach my $r (@allhyptypes) {
      my @y = ();
      foreach my $map (keys %{$info->{$r}}) {
	my $n = $info->{$r}{$map};
	my $rec = 100*$n/$total;
	my $prec = 100*$n/($rtotal->{$r} || 1);
	my $xrec = sprintf("%.1f%%",$rec);
	my $xprec = sprintf("%.1f%%",$prec);
##	my $x = perc($n,$total);
	my $label = "$map:$n ($xrec $xprec)";
	if ($map eq 'match') {
	  $label = "<span style='color: red;'>$label</span>";
	} elsif ($rec > 10 || $prec > 10 || $n/$all > 0.005) {
	  $label = "<span style='color: blue;'>$label</span>";
	}
	push(@y,"$label");
      }
      my $class = ($t eq $r) ? "class='diag'" : "";
      push(@x,"<span title='$t $r' $class>".join('<br/>',@y)."</span>");
    }
    push(@$tb,td([$t,@x,$total,$t]));
    $ttotal += $total;
  }
  my @x = ('total');
  my $rtotal = 0;
  foreach my $r (@allhyptypes) {
    push(@x,$sum->{$r});
    $rtotal += $sum->{$r};
  }
  push(@x,"$rtotal/$ttotal");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['classified as =>', @allhyptypes,'total','']));
  print HTML table({-border => 1}, Tr($tb));
}

sub confusion_cattable {
  my $table = $_[0];
  my $tb = [th(['classified as =>', @allhypcat,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  my $rtotal = {};
  foreach my $r (@allrefcat) {
    foreach my $t (@allhypcat) {
      my $info = $table->{$t}{$r} || {};
      foreach my $map (keys %$info) {
	$rtotal->{$r} += $info->{$map};
      }
    }
  }
  foreach my $t (@allrefcat) {
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    foreach my $r (@allhypcat) {
      foreach my $map (keys %{$info->{$r}}) {
	$total += $info->{$r}{$map};
	$sum->{$r} += $info->{$r}{$map};
      }
    }
    foreach my $r (@allhypcat) {
      my @y = ();
      foreach my $map (keys %{$info->{$r}}) {
	my $n = $info->{$r}{$map};
	my $rec = 100*$n/$total;
	my $prec = 100*$n/($rtotal->{$r} || 1);
	my $xrec = sprintf("%.1f%%",$rec);
	my $xprec = sprintf("%.1f%%",$prec);
##	my $x = perc($n,$total);
	my $label = "$map:$n ($xrec $xprec)";
	if ($map eq 'match') {
	  $label = "<span style='color: red;'>$label</span>";
	} elsif ($rec > 10 || $prec > 10) {
	  $label = "<span style='color: blue;'>$label</span>";
	}
	push(@y,"$label");
      }
      push(@x,"<span title='$t $r'>".join('<br/>',@y)."</span>");
    }
    push(@$tb,td([$t,@x,$total,$t]));
    $ttotal += $total;
  }
  my @x = ('total');
  my $rtotal = 0;
  foreach my $r (@allhypcat) {
    push(@x,$sum->{$r});
    $rtotal += $sum->{$r};
  }
  push(@x,"$rtotal/$ttotal");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['classified as =>', @allhypcat,'total','']));
  print HTML table({-border => 1}, Tr($tb));
}

sub rule_table {
  my $xtable = $_[0];
  my $table = $xtable->{rules};
  my $all = $xtable->{n};
  my $tb = [th(['ref deprel =>',@allreftypes,'total',''])];
  my $sum = {};
  my $ttotal = 0;
  my $rtotal = {};
  foreach my $r (keys %$table) {
    foreach my $t (@allreftypes) {
      my $info = $table->{$t}{$r} || {};
      foreach my $map (keys %$info) {
	$rtotal->{$r} += $info->{$map};
      }
    }
  }
  foreach my $t (@allrules) {
    my $info = $table->{$t} ||= {};
    my @x = ();
    my $total = 0;
    foreach my $r (@allreftypes) {
      foreach my $map (keys %{$info->{$r}}) {
	$total += $info->{$r}{$map};
	$sum->{$r} += $info->{$r}{$map};
      }
    }
    my $match = 0;
    foreach my $r (@allreftypes) {
      my @y = ();
      foreach my $map (keys %{$info->{$r}}) {
	my $n = $info->{$r}{$map};
	my $rec = 100*$n/$total;
	my $prec = 100*$n/($rtotal->{$r} || 1);
	my $xrec = sprintf("%.1f%%",$rec);
	my $xprec = sprintf("%.1f%%",$prec);
	$map eq 'match' and $match += $n;
##	my $x = perc($n,$total);
##	my $label = "$map:$n ($xrec $xprec)";
	my $label = "$map:$n ($xrec)";
	if ($map eq 'match') {
	  $label = "<span style='color: red;'>$label</span>";
	} elsif ($rec > 10 || $prec > 10 || $n/$all > 0.005) {
	  $label = "<span style='color: blue;'>$label</span>";
	}
	push(@y,"$label");
      }
      push(@x,"<span title='$t $r'>".join('<br/>',@y)."</span>");
    }
    my $xmatch = sprintf("%.1f%%",100 * $match / ($total || 1));
    push(@$tb,td([$t,@x,"$total (match=$xmatch)",$t]));
    $ttotal += $total;
  }
  my @x = ('total');
  my $rtotal = 0;
  foreach my $r (@allreftypes) {
    push(@x,$sum->{$r});
    $rtotal += $sum->{$r};
  }
  push(@x,"$rtotal/$ttotal");
  push(@$tb,td([@x,'']));
  push(@$tb,th(['ref deprel =>', @allreftypes,'total','']));
  print HTML table({-border => 1}, Tr($tb));
}

sub process_ref_corpus {
  my ($corpus,$ref) = @_;
  $refinfo = {};
  print "Process ref file $ref\n";
#  open(FILE,"<:encoding(utf8)","$ref") ||
  open(FILE,"<","$ref") ||
    die "can't open ref '$ref': $!";
  my $nsid = 1;
  my $s = $refinfo->{$nsid} ||= [];
  while (<FILE>) {
    if (/^\s*$/) {
      $nsid++;
      $s = $refinfo->{$nsid} ||= [];
      next;
    }
    my @info = split(/\t/,$_);
    my $deprel = $info[7];
    my $entry = { pos => $info[0],
		  ref => { head => $info[6],
			   deprel => $deprel,
			   form => $info[1],
			   fullcat => $info[4]
			 },
		  match => 'no'
		};
    $refdeprel{$deprel}++;
    $refcat{$info[4]}++;
    push(@$s,$entry);
    $accinfo->{n}++;
    $accinfo->{corpus}{$corpus}{n}++;
  }
  close(FILE);
}

sub process_hyp_corpus {
  my ($corpus,$file) = @_;
  print "Process hyp file $file\n";
  if ($file =~ /\.bz2$/) {
    open(FILE,"bzcat $file|") || die "can't open $file: $!";
  }   elsif ($file =~ /\.gz$/) {
    open(FILE,"gunzip -c $file|") || die "can't open $file: $!";
  } else {
    open(FILE,"<$file") || die "can't open $file: $!";
  }

  my $format = $file =~ /\.xconll/ ? 'xconll' : 'conll';

  ($format eq 'conll') 
    and die "'conll' is a deprecated format. please use an xconll file";

  my $nsid = 1;
  my @s = @{$refinfo->{$nsid}};
  my @scost = @s;
  my %tmprels = ();

  my $smode;
  my $slength;
  my $smatch;
  my $skeepcost;
  my $sid;
  %eid2tpos=();

  # we buid some kind of oracle for the undecided cases
  open(ORACLE,">","$corpus.oracle") || die "can't open oracle file for $corpus: $!";

  open(INFO,">","$corpus.info") || die "can't open info file for $corpus: $!";

  while (<FILE>) {
    chomp($_);
    if (/^\s*$/) { ## separation line after a sentence

      ($nsid % 1000) or print "\tsid=$nsid\n";
      
      if (keys %tmpcost) {
	my $maxpos = scalar(@scost);
	foreach my $tpos (keys %tmpcost) {
	  my $tentry = $scost[$tpos-1];
	  my $match = $tentry->{match};
	  
	  my $tcost = $tmpcost{$tpos};
	  my $tform2 = feature_code('tform2' => $tentry->{hyp}{form});

	  my $refcat = $tentry->{ref} ? $tentry->{ref}{fullcat} : '-';
	  my $hypcat = $tentry->{hyp} ? $tentry->{hyp}{fullcat} : '-';
	  
	  my $samecat = ($refcat eq $hypcat);

##	  $tentry->{empty_target} = [grep {$_->{tempty}} @$tcost];

	  my $fid= $tentry->{hyp} ? $tentry->{hyp}{fid} : '-';

	  my $pos;
	  if ($tpos == 1) {
	    $pos = feature_code('pos' => 'start');
	  } elsif ($tpos == $maxpos) {
	    $pos = feature_code('pos' => 'end');
	  } else {
	    $pos = feature_code('pos' => 'middle');
	  }

	  foreach my $cinfo (@$tcost) {

	    my $imatch = $match;

	    ## for edges with empty target, we use indirection to a lexicalized target, whenever possible
	    ## in order to get a more plausible status
 	    if ($cinfo->{tempty} && $cinfo->{itpos}) {
	      $imatch = $cinfo->{tmatch} = $scost[$cinfo->{itpos}-1]{match};
	    }

	    (oracle_is_ok($cinfo,$fid) 
	     && ($cinfo->{kept} || $imatch ne 'yes'))
	      and oracle_emit_status('yes',$tentry,$cinfo);

	    if ($fid && $cinfo->{kept}) {

	      print INFO <<EOF;
$fid\t$imatch\t$cinfo->{backup}
EOF
	      delete $cinfo->{backup};

	    }

	    my $spos = spos($cinfo);

	    my $type = feature_decode($cinfo->{type});
	    my $label = feature_decode($cinfo->{label});

	    ## add reroot information for some well-identified cases
	    if ($type eq "adj" && $label  eq "V") {
	      # modal verbs: some edges reaching the verb should be rerooted to the modal
	      my $snid = $cinfo->{snid};
	      if ($match eq 'sametype') {
		push(@{$cinfo->{rerooted_edges}},
		     grep {$_->{tnid} == $snid && !$_->{kept}} @{$tmpcost{$spos}}
		    );
		$tentry->{rerooted_edges} = 1;
	      }
	      push(@{$_->{reroot}},$tpos) foreach (grep {$_->{tnid} == $snid} @{$tmpcost{$spos}});
	    }

	    ## we also compute some info about the status of the source of the edge
	    $spos or next;
	    $scost[$spos-1]{match} eq 'yes' or next;
	    my $sx = first {$_->{kept} && !$_->{tempty}} @{$tmpcost{$spos}};
	    if ($sx &&
		( (exists $cinfo->{ssnid} 
		   && ($sx->{tnid} == $cinfo->{ssnid}))
		  || ($sx->{tnid} == $cinfo->{snid})
		)
	       ) {
	      $cinfo->{smatch} = 1;
	    } 

	  }			# end of loop on @$tcost
	  
	  my $should_have_kept=0;
	  
	  my @unkept = grep {!$_->{kept}} @$tcost;
	  my $nunkept = scalar(@unkept);
	  
	  unless ($match eq 'yes' || $nunkept) {
	    ## only one edge, and a bad one !
	    ## maybe the trace of some missing construction in FRMG
	    my $hyp = $tentry->{hyp};
	    my $ref = $tentry->{ref};
	    print TOK <<EOF;
## <badedge> $hyp->{fid} $hyp->{form} $hyp->{fullcat} $ref->{deprel} $hyp->{deprel} $ref->{head} $hyp->{head}
EOF
	  }
	  
	  unless ($nunkept) {
	    ## no alternative left
	    ## we emit oracle info but don't use the edge to build stats
	    foreach my $cinfo (@$tcost) {
	      my $imatch = $cinfo->{tmatch} || $match;
	      my $oracle_status = ($imatch eq 'yes') ? 'yes' :'no';
	      oracle_emit_status($oracle_status,$tentry,$cinfo);
	    }
	    next;
	  }
	  
	  ## for dependencies d that are not kept in competition with a badly kept dependency
	  ## we are not sure if not keeping d was or was not a good decision
	  ## we only know that one of them should have been kept (but which one)
	  ## therefore we use a crude probability to distribute the decision between good or bad
	  unless ($match eq 'yes') {
	    $should_have_kept = ($nunkept == 1 && !exists $tentry->{rerooted_edges});
	    unless ($should_have_kept) {
	      ## we have more than one alternative to a badly kept edge, so we can't yet decide
	      $tentry->{cost} = $tcost; # add to undecided
	      $undecided++;
	    }
	  }
	  
	  foreach my $cinfo (@$tcost) {
	    
	    my $kept =  $cinfo->{kept};

	    $cinfo->{pos} = $pos;
	    $cinfo->{tform2} = $tform2;
	    
	    ## for edges with empty target, we use indirection to a lexicalized target, whenever possible
	    ## in order to get a more plausible status
	    my $imatch = $cinfo->{tmatch} || $match;

	    if ($kept && ($imatch eq 'yes' || $imatch eq 'sametype' || $imatch eq 'almostyes')) {
	      $labelmap->{label_key($cinfo)}{$tentry->{hyp}{deprel}}++;
	    }

	    if ($kept && $imatch eq 'yes') {
	      $catmap->{label_key($cinfo)}{$hypcat}++;
	    }

	    ( 
	     $kept
	     || $should_have_kept
	     || $imatch eq 'yes' 
	    ) or next;		# undecided for this edge
	    
	    my $oracle_status = ($imatch eq 'yes' && $kept) ? 'yes' : 'no';
	    oracle_emit_status($oracle_status,$tentry,$cinfo);
	    if (1) {
	      my $key = distrib_key($dkey_registry,
				    @{$cinfo}{qw{label type delta tcat scat tlemma slemma}}
				   );
	      my $entry = $cinfo->{dentry} = $distrib->{$key} ||= [ 0, # 0:n
								    0, # 1:should_keep
								    0, # 2: yes yes
								    0, # 3: yes no
								    0, # 4: no yes
								    0  # 5: no no
								  ];
	      
	      $entry->[0]++;
	      $entry->[$dmap{"$kept$imatch"}] ++;
	    }
	    handle_cinfo( $cinfo,
			  $imatch,
			  0,	# the exact value has no importance because we are not in undecided case
			  1	# always decided: $cinfo is the kept edge or the kept edge was the good one or there are no other alternatives
			);
	  }
	}
	
      }

      if (keys %btmpcost) {
	foreach my $tpos (keys %btmpcost) {
	  my $tentry = $scost[$tpos-1];
	  my $match = $tentry->{match};
	  my $btcost = $btmpcost{$tpos};
	  my $should_have_kept = 0;
	  my @unkept = grep {!$_->{kept}} @$btcost;
	  my $nunkept = scalar(@unkept);
	  $nunkept or next;

	  my @has_fullmatch = ();
	  my $has_fullmatch;
	  my @bmatch = ();

	  foreach my $cinfo (@$btcost) {
	    my $btpos = $cinfo->{btpos};
	    $btpos or next;
	    my $btentry = $scost[$btpos-1];
	    my $bmatch = $cinfo->{bmatch} = $btentry->{match};
	    ($bmatch eq 'yes') and push(@bmatch,$cinfo);
	    $cinfo->{match} = $match;
	    if ($cinfo->{fullmatch} = ($match eq 'yes' && $bmatch eq 'yes')) {
	      push(@has_fullmatch,$cinfo);
	      $cinfo->{kept} and $has_fullmatch = $cinfo;
	    }
	  }

	  ## if no fullmatch for the selected edge pair (eid,beid)
	  ## try to find the best non selected edge pair
	  ## eid matches or beid matches
	  my $selected;
	  unless ($has_fullmatch) {
	    my @candidates = grep {($_->{kept1} && $match eq 'yes') || ($_->{kept2} && $_->{bmatch} eq 'yes')} @unkept;
	    @candidates and $selected = $candidates[0];
	  }

	  foreach my $cinfo (grep {$_->{kept1} || $_->{kept2}} @$btcost) {
	    handle_bcinfo($cinfo,
			  $cinfo->{fullmatch} ? 'yes' : 'no',
			  $has_fullmatch || (defined $selected && $cinfo == $selected),
			  $nunkept
			 );
	  }

	}
      }

      ## reset for next sentence
      $nsid++;
      @s = @{$refinfo->{$nsid}};
      @scost = @s;
      %tmprels = ();
      %tmpcost = ();
      %btmpcost = ();
      undef $smode;
      $slength=0;
      $smatch=0;
      $skeepcost=undef;
      $sid=undef;
      %eid2tpos = ();
      next;
    }
    ## entry lines
    ## cost lines come after the conll lines (=> slength and smatch are known)
    if (s/^\#\#\s+cost\s+//o) {
      ## collect all cost info, for sentences whose success rate is over 10%
      defined $skeepcost or $skeepcost = ($slength < 10 || $smatch / $slength > 0.10);
      ($skeepcost && ! /^tpos=0/) and register_cost(\$_);
      next;
    }
    if (s/^\#\#\s+bcost\s+//o) {
      ## collect all cost info, for sentences whose success rate is over 10%
      defined $skeepcost or $skeepcost = ($slength < 10 || $smatch / $slength > 0.10);
      ($skeepcost && ! /^tpos=0/) and register_bcost(\$_);
      next;
    }
    ## conll entry line
#    my @info = split(/\t/,$_);
    my $entry = shift @s;
    my $ref = $entry->{ref};
    my ($fid,$mode,$form,$lemma,$fullcat,$xhead,$deprel,$rule,$eid) = split(/\t/,$_);
    $slength++;
    $hypcat{$fullcat}++;
    $hypdeprel{$deprel}++;
    $smode ||= $mode;
    my $head = ($xhead && $xhead =~ /F(\d+)/) ? $1 : 0;
    my $hyp = $entry->{hyp} = {
			       xhead => $xhead,
			       head => $head,
			       deprel => $deprel,
			       fid => $fid,
			       mode => $mode,
			       fullcat => $fullcat,
			       lemma => $lemma,
			       form => $form,
			       rule => $rule, # the name of the CONLL conversion rule
			      };
    if (defined $eid && $eid ge 0) {
      # the id of the main edge used to trigger $rule when converting to CONLL
      $hyp->{eid} = $eid;
      $eid2tpos{$eid} = $entry->{pos};
    }
    my $modeacc = $accinfo->{$mode} ||= { n => 0, corpus => {} };
    $modeacc->{n}++;
    $modeacc->{corpus}{$corpus}{n}++;
    my $agr = 'match';
    my $refcat = $ref->{fullcat};
    if ($head == $ref->{head}) {
      if ($deprel eq $ref->{deprel}) {
	$accinfo->{correct}++;
	$accinfo->{corpus}{$corpus}{correct}++;
	$accinfo->{confusion}{$deprel}{$deprel}{match}++;
	$modeacc->{correct}++;
	$modeacc->{corpus}{$corpus}{correct}++;
	$modeacc->{confusion}{$deprel}{$deprel}{match}++;
#	$entry->{match} = ($fullcat eq $refcat) ? 'yes' : 'almostyes';
	$entry->{match} = 'yes';
	$smatch++;
      } else {
	$accinfo->{confusion}{$ref->{deprel}}{$deprel}{samehead}++;
	$modeacc->{confusion}{$ref->{deprel}}{$deprel}{samehead}++;
	$agr = 'samehead';
	$entry->{match} = 'samehead';
      }
    } elsif ($deprel eq $ref->{deprel}) {
      $accinfo->{confusion}{$deprel}{$deprel}{sametype}++;
      $modeacc->{confusion}{$deprel}{$deprel}{sametype}++;
      $agr = 'sametype';
      $entry->{match} = 'sametype';
    } else {
      $accinfo->{confusion}{$ref->{deprel}}{$deprel}{mismatch}++;
      $modeacc->{confusion}{$ref->{deprel}}{$deprel}{mismatch}++;
      $agr = 'mismatch';
    }
    ##    $accinfo->{rules}{$hyp->{rule}}{$ref->{deprel}}{n}++;
    $allrules{$hyp->{rule}}++;
    $accinfo->{rules}{$hyp->{rule}}{$ref->{deprel}}{$agr}++;
    my $catagr='match';
    my $catacc = $accinfo->{cat} ||= { correct => 0, confusion => {}};
    if ($fullcat eq $ref->{fullcat}) {
      $catacc->{correct}++;
      $catacc->{confusion}{$ref->{fullcat}}{$fullcat}{match}++;
    } else {
      $catagr='mismatch';
      $catacc->{confusion}{$ref->{fullcat}}{$fullcat}{mismatch}++;
    }
    defined $sid or ($sid) = $fid =~ /(E.+?)F\d+/;
    my $refxhead = $ref->{head};
    $refxhead and $refxhead = "${sid}F$refxhead";
    my $hypheadform = $head ? $refinfo->{$nsid}[$head-1]{ref}{form} : '*';
    my $refheadform = $ref->{head} ? $refinfo->{$nsid}[$ref->{head}-1]{ref}{form} : '*';
    print TOK "$hyp->{fid} $hyp->{form} _ $mode $ref->{fullcat} $hyp->{fullcat}\n";
    print TOK "$hyp->{fid} ### $mode $hyp->{form} $hyp->{fullcat} $agr $ref->{deprel} $deprel $refxhead $xhead $refheadform $hypheadform $hyp->{rule}\n";
  }
  close(FILE);

  ######################################################################
  ## post hyp file parsing
  ## handling undecided cost cases
  ## my $k = scalar(@$undecided);
  print "Handling undecided cases k=$undecided\n";
  #  print "Handling undecided cases k=$k bk=$bk\n";
  # at this point, we can safely remove the registry of distrib keys
  $dkey_registry = {};
  $dkeyctr = 1;

  ## normalize data for label conversion
  open(DEPREL,">","deprel.log") || die "can't open deprel.log: $!";
  foreach my $label (keys %$labelmap) {
    my $info = $labelmap->{$label};
    my $in = 1 / sum values %$info;
    $info->{$_} *= $in foreach (keys %$info);
    foreach (sort {$info->{$b} <=> $info->{$a}} keys %$info) {
      printf DEPREL " %s %s %.2f%%\n", $label, $_, 100 * $info->{$_};
    }
  }
  close(DEPREL);

  ## normalize data for POS conversion
  open(CATMAP,">","catmap.log") || die "can't open catmap.log: $!";
  foreach my $label (keys %$catmap) {
    my $info = $catmap->{$label};
    my $in = 1 / sum values %$info;
    $info->{$_} *= $in foreach (keys %$info);
    foreach (sort {$info->{$b} <=> $info->{$a}} keys %$info) {
      printf CATMAP " %s %s %.2f%%\n", $label, $_, 100 * $info->{$_};
    }
  }
  close(CATMAP);



  ## first, compute distrib on edge
  { 
    my $yesyes = $dmap{'1yes'};
    my $nono = $dmap{'0no'};
    foreach (grep {$_->[0]} values %$distrib) { # undecided cost
      $_->[1] = ($_->[$yesyes] + $_->[$nono]) / $_->[0];
    }
  }

  # try to free some memory
  $distrib={};

  ## second: for each undecided set of edges, choose the edge with max 'should_keep'
  foreach my $tmps (values %$refinfo) { # sentence level
    foreach my $entry (grep {exists $_->{cost}} @$tmps) {
      ## each pos in sentence
      ## among undecided edges with target tpos=pos, we can distinguish 2 classes
      ## - the edges with a non empty target, at most one per tpos (modulo maybe issues on compounds)
      ## - the edges with an empty target, zero or more of them
      ## we find the best alternative in the non-empty target ones
      ## then we can also try to identify which empty target ones to keep
      my $x = $entry->{cost};
      delete $entry->{cost};
      ($entry->{ref}{deprel} eq 'ponct') and next; # skip punctuation edges
      my @unkept = 
	sort { $b->{dentry}[1] <=> $a->{dentry}[1]
		 || $b->{total} <=> $a->{total}
	       } 
#	  grep {(!$_->{kept}) && (!$_->{tempty})} 
	  grep {!$_->{kept}} 
	    @$x;
      push(@unkept,
	   grep {$_}
	   map {@{$_->{rerooted_edges}}} 
	   grep {exists $_->{rerooted_edges}}
	   @$x
	  );
      my $n = scalar(@unkept);
      if (0) {
	push(@{$entry->{empty_target}},
	     grep {(!$_->{kept}) && $_->{tempty}} 
	     @$x
	    ); # to be handled later
      }
      my @kept = grep {$_->{kept}} @$x;
      my $kept = first {!$_->{tempty}} @kept;
      $kept or $kept = $kept[0];
      my $tcat = $kept->{tcat};
      my $status = $entry->{match};
      my $label = $kept->{label};	# encoded label
      my $spos = spos($kept);
      my $refspos = $entry->{ref}{head};
      my $candidate;
      my $fid = $entry->{hyp}{fid};
      my $oracle_status = 'selected';
      if (1) {
	my $hyp = $entry->{hyp};
	my $ref = $entry->{ref};
	print ORACLE <<EOF;
## <badedge2> $hyp->{fid} $hyp->{form} $hyp->{fullcat} $ref->{deprel} $hyp->{deprel} $ref->{head} $hyp->{head} $status
EOF
	if (0) {
	  foreach my $bad (@unkept) {
	    my $xspos= spos($bad);
	    print ORACLE <<EOF;
\tchoice eid=$bad->{eid} tpos=$bad->{tpos} spos=$bad->{spos} xspos=$xspos tcat=${$bad->{tcat}} scat=${$bad->{scat}} label=${$bad->{label}} type=${$bad->{type}}
EOF
	  }
	}
	#      $candidate = $unkept[0];
      }
      my @filtered;
      my $filtered = \@unkept;
      if ($oracle 
	  && $fid
	  && scalar(@filtered = grep { oracle_is_ok($_,$fid) } @unkept)
	 ) {
	my $nfilter = scalar(@filtered);
	print ORACLE <<EOF;
\treused from oracle $entry->{hyp}{fid} eid=$filtered[0]->{eid} size=$nfilter/$n
EOF
	$oracle_status = 'yes';
	$filtered = \@filtered;
	(@filtered == 1) and $candidate = $filtered[0];
      }
      if (1) { 
	if ($status eq 'almostyes') {
	  $candidate ||= first {(spos($_) == $spos) 
				  && exists $_->{smatch} 
				    && ($tcat != $_->{tcat})
				} @$filtered;
	  $candidate ||= first { (spos($_) == $spos) 
				   && ($tcat != $_->{tcat})
				 } @$filtered;
	  $candidate ||= first { (spos($_) == $spos) 	
			       } @$filtered;
	  #	  $candidate or next;	# don't try to find another candidate 
	} elsif ($status eq 'sametype') {
	  $refspos and $candidate ||= first { ($_->{label} == $label || exists $_->{reroot}) 
						&& check_spos($_,$refspos)
						  && exists $_->{smatch} 
						} @$filtered;
	  $refspos and $candidate ||= first { ($_->{label} == $label || exists $_->{reroot}) 
						&& check_spos($_,$refspos)
					      } @$filtered;
	  $candidate ||= 
	    first {(($_->{label} == $label || exists $_->{reroot}) && (spos($_) != $spos)) 
		     || check_spos($_,$refspos)
		   }
	      @$filtered;
	} elsif ($status eq 'samehead') {
	  $candidate ||= first {(spos($_) == $spos) 
				  && ($_->{label} != $label) 
				    && exists $_->{smatch} 
				  } @$filtered;
	  $candidate ||= first { (spos($_) == $spos) 
				   && ($_->{label} != $label) 
				 } @$filtered;
	  #  $candidate or next;	# don't try to find another candidate 
	} elsif ($status eq 'no') {
	  $refspos and $candidate ||= first {  check_spos($_,$refspos)
					       && ($_->{label} != $label)
						 && exists $_->{smatch} 
					       } @$filtered;
	  $refspos and $candidate ||= first { check_spos($_,$refspos)
						&& ($_->{label} != $label)} @$filtered;
	  $candidate ||= first {(spos($_) != $spos) && ($_->{label} != $label)} @$filtered;
	} else {
	  my $hyp = $entry ? $entry->{hyp} : undef;
	  my $fid = $hyp ? $hyp->{fid} : 'unknown';
	  warn "unexpected status value '$status' at $fid";
	}
      }
      if (!$candidate) {
	my $deprel = $entry ? $entry->{ref}{deprel} : undef;
	my $refcat = $entry ? $entry->{ref}{fullcat} : undef;
	my $max = -10;
	my $k = 1;
	foreach my $unkept (@$filtered) {
	  my $score = 1 / $k++;
	  (exists $unkept->{smatch}) and $score += 20;
	  my $lkey = label_key($unkept);
	  if ($deprel && exists $labelmap->{$lkey} && exists $labelmap->{$lkey}{$deprel}) {
	    $score += 5 * $labelmap->{$lkey}{$deprel};
	  }
	  if ($refcat && exists $catmap->{$lkey} && exists $catmap->{$lkey}{$refcat}) {
	    $score += 5 * $catmap->{$lkey}{$refcat};
	  }
	  my $uspos = spos($unkept);
	  ($uspos == $refspos) and $score += 10;
	  ($uspos == $spos) and $score += ($status eq 'samehead' || $status eq 'almostyes') ? +5 : -5;
	  ($unkept->{label} == $label) and $score += ($status eq 'sametype') ? +5 : -5;
	  ($status eq 'almostyes') and $score += ($tcat != $unkept->{tcat}) ? +5 : -5;
	  ($score > $max) or next;
	  $max = $score;
	  $candidate = $unkept;
	}
      }
      $candidate ||= $filtered->[0];
      if ($candidate && $fid && exists $candidate->{reroot}) {
	print ORACLE <<EOF;
\tused rerooted edge $fid $candidate->{eid}
EOF
      }
      oracle_emit_status($oracle_status,$entry,$candidate);
      my $scandidate;
      if ($candidate && $candidate->{sempty}) {
	my $nid = $candidate->{snid};
	my $seid = $candidate->{seid};
	$seid and $scandidate = first {$_->{eid} == $seid} @$x;
	$scandidate or $scandidate = first {$_->{tnid}} @$x;
	$scandidate ||= $candidate;
      }
      my $rank=0;
      foreach (@unkept) {
	##	($i++ > 15 && $candidate != $_ && $scandidate != $_) and next;
	handle_cinfo($_, 
		     'no',
		     $n+1,
		     ($candidate == $_ || $scandidate == $_) ? 1 : 0,
		     ++$rank
		    );
      }
    }
    
    ## dealing with undecided edges with empty targets
    foreach my $entry (grep 
		       {exists $_->{empty_target} && @{$_->{empty_target}}} 
		       @$tmps
		      ) {
      my $fid = $entry->{hyp}{fid};
      my $n = scalar(@{$entry->{empty_target}});
      foreach my $candidate (@{$entry->{empty_target}}) {
	my $oracle_status = 'no';
	if ($oracle && oracle_is_ok($candidate,$entry->{hyp}{fid})) {
	  $oracle_status = 'yes';
	  print ORACLE <<EOF;
\treused from oracle $fid eid=$candidate->{eid} size=$n empty_target_edge
EOF
	}
	($oracle_status eq 'no' && exists $selected_parent{$fid}{$candidate->{eid}}) and $oracle_status = 'selected';
	($oracle_status eq 'no') or oracle_emit_status($oracle_status,$entry,$candidate);
	handle_cinfo($candidate,
		     $candidate->{tmatch},
		     $n,
		     ($oracle_status ne 'no') ? 1 : 0
		    );
      }
    }

  }

  close(ORACLE);
  close(INFO);

}

sub check_spos {
  my ($candidate,$refspos) = @_;
  return spos($candidate) == $refspos
    || (exists $candidate->{reroot} && grep {$_ == $refspos} @{$candidate->{reroot}})
      ;
}

sub oracle_emit_status {
  my ($oracle_status,$tentry,$cinfo) = @_;
  exists $cinfo->{oracle_emitted} and return;
  $cinfo->{oracle_emitted} = 1;
  $cinfo->{$_} ||= feature_code($_ => '_') foreach (qw{stoken ttoken type label stree ttree scat tcat tlemma slemma});
  my $seid = exists $cinfo->{seid} ? $cinfo->{seid} : -1;
  my $fid = $tentry->{hyp}{fid};
  print ORACLE <<EOF;
## <$oracle_status>\t$fid\t$cinfo->{eid}\t$cinfo->{tnid}\t$cinfo->{snid}\t${$cinfo->{label}}\t${$cinfo->{type}}\t$seid\tsleft=$cinfo->{sleft}\tsright=$cinfo->{sright}\tstoken=${$cinfo->{stoken}}\ttleft=$cinfo->{tleft}\ttright=$cinfo->{tright}\tttoken=${$cinfo->{ttoken}}\tstree=${$cinfo->{stree}}\tttree=${$cinfo->{ttree}}\tscat=${$cinfo->{scat}}\ttcat=${$cinfo->{tcat}}\tslemma=${$cinfo->{slemma}}\ttlemma=${$cinfo->{tlemma}}
EOF
  ## selecting eid involve selecting parent edge seid when mentioned (empty source node)
  ($oracle_status ne 'no' && $cinfo->{seid}) and $selected_parent{$fid}{$cinfo->{seid}} = 1;
  if ($oracle && $oracle_status eq 'yes' && !oracle_is_ok($cinfo,$fid)) {
      print ORACLE <<EOF;
\tnot in previous oracle $fid eid=t$cinfo->{eid}
EOF
  }
}

sub decompose_fv {
  $_[0] =~ s/=$/=_/o; 
  my ($f,$v) = split(/=/,$_[0]);
  $f eq 'kept' and $v = ($v eq 'yes') ?  1 : 0;
  return $f => feature_code($f,$v)
}

sub spos {
  my $cinfo = $_[0];
  exists $cinfo->{sspos} and return $cinfo->{sspos};
  return $cinfo->{spos};
}

sub label_key {
  my $cinfo = $_[0];
  my @key = ($cinfo->{scat},$cinfo->{tcat},$cinfo->{label},$cinfo->{type});
  exists $cinfo->{slabel} and push(@key,$cinfo->{sscat},$cinfo->{slabel},$cinfo->{stype});
  return join(' ',map {feature_decode($_)} @key);
}

sub register_cost {
  ##  tie my %h, 'MyHash';
  my %rules = ();
  my %h = (rules => \%rules, kept => 0, backup => ${$_[0]});
  my $total = 0;
  foreach (split(/\t/,${$_[0]})) {
    # unless (/=/) {
    #   # this case should not arise 
    #   warn "bad fv '$_' in '${$_[0]}'";
    #   next;
    # }
    my ($f,$v) = split(/=/,$_,2);
    if ($f eq 'kept') {
      ($v eq 'yes') and $h{kept} = 1;
    } elsif ($foreign{$f}) {
      $v ||= '_';
      $h{$f} = $lexicon{$v} ||= \$v; # foreign coding
    } elsif ($f =~ /^</) {	     # rules
      $total += $rules{$f} = $v;
    } else {
      $h{$f} = $v;
    }
  }

  ($h{tempty} && !$h{itpos}) and return; # we skip edges with an empty target and no potential indirect target

  ##  $h{tempty} and return; 	# test if tempty edges are useful

  $h{total} = $h{tw} + $total;		# total weight of all rules

  my $tpos = $h{tpos};

  $h{$_} ||= feature_code($_,'_') foreach (qw{tcat scat tlemma slemma tform sform});
  my $delta = abs($tpos-$h{spos}); # spos is an integer feature
  if ($delta < 7) {
    # noop
  } elsif ($delta < 16) {
    $delta = 6;
  } else {
    $delta = 15;
  }
  ## delta is an integer feature
  $h{delta} = cinfo_get(\%h,'dir') eq 'left' ? - $delta : $delta;

  ## rank is an integer feature
  my $rank = $h{rank};
  if ($rank < 6) {
    # noop
  } elsif ($rank < 11) {
    $h{rank} = 5;
  } else {
    $h{rank} = 10;
  }

  my $xtpos = $tpos;
  (exists $h{itpos} && $h{itpos}) and $xtpos = $h{itpos};

  my $eid = $h{eid};
  $eid2tpos{$eid} and $xtpos = $eid2tpos{$eid};

  push(@{$tmpcost{$xtpos}},\%h);
}

sub register_bcost {
  my %rules = ();
  my %h = (kept => 0, kept1 => 0, kept2 => 0, backup => ${$_[0]});
  foreach (split(/\s/,${$_[0]})) { # to be corrected into tabs !
    my ($f,$v) = split(/=/,$_,2);
    if ($f =~ /^kept/) {
      ($v eq 'yes') and $h{$f} = 1;
    } elsif ($foreign{$f}) {
      $v ||= '_';
      $h{$f} = $lexicon{$v} ||= \$v; # foreign coding
    } else {
      $h{$f} = $v;
    }
  }

  my $tpos = $h{tpos};

  $h{$_} ||= feature_code($_,'_') foreach (qw{tcat scat tlemma slemma tform sform});
  
##  print "register bcost $tpos\n";

  my $xtpos = $tpos;
  my $eid = $h{eid};
  my $beid = $h{beid};

  $eid2tpos{$eid} and $xtpos = $eid2tpos{$eid};

  push(@{$btmpcost{$xtpos}},\%h);
}

sub handle_cinfo {
  my ($cinfo,$match,$n,$decided,$rank) = @_;

  $rank ||= 1;

  ## testing perceptron:
  ## we only register info about kept edges (good or bad) and unkept edges selected by the oracle (the edges that should have been selected)
  ($perceptron && !$cinfo->{kept} && !$decided) and return;

  my $status = $map{"$cinfo->{kept}$match"};

#  my @preset = ("label=${$cinfo->{label}}","type=${$cinfo->{type}}");
  
  my @labels = ();
  foreach my $fsentry (@features) {
    #    (first {!exists $cinfo->{$_}} @$fs) and next; # all features have to be defined to keep the set
    #    push(@labels, join("\t",@preset, map {"+$_=".feature_decode($cinfo->{$_})} @$fs));
    if (my $key = $fsentry->{check}($cinfo,$fsentry->{value})) {
      push(@labels,$key);
    }
  }

  my $d1 =  1;
  my $d2 = 0;

  unless ($decided) {
#    $d1 =  min(1, 1 / max(1,min($n-1,14)));
    $d1 =  min(1, 1 / max(1,min($n-1,13)));
#    $d1 =  min(1, 1 / max(1,min($n-1,12)));
#    $d1 =  min(1, 1 / max(1,min(0.5 * ($n+$rank)-1,14)));
    $d2 = 1 - $d1;
  }
  while (my ($rule,$v) = each %{$cinfo->{rules}}) {
    my $crule = $cost->{$rule} ||= [ {}, # ctx0
				     0, # n
				     0, # v
				   ];
      
    $crule->[1] ++;
    $crule->[2] += $v;
    my $ctx0 = $crule->[0];
      
    foreach (@labels) {
      my $x = $ctx0->{$_->[0]} 
	||= [$costctr++, # id = 0
	     0, # n = 1
	     0, # v = 2
	     0, # yes-yes = 3 = keepgood
	     0, # yes-no = 4 = keepbad
	     0, # no-yes = 5 = delgood
	     0,  # no-no = 6 = delbad
	     $_, # value to emit = 7
	    ];
      $x->[1] ++;               # n
      $x->[2] += $v;            # v
      if ($decided) {
	$x->[$status] ++;	# no-no (delbad) for oracle edge (ie unkept but should have been kept)
      } else {
	$x->[4] += $d1;     # yes-no
	$x->[6] += $d2;     # no-no
	## maybe we should try instead $x->[5] += 1 for edges not oracle and not kept, but alternative to some oracle edge
	## could be a way to be closer from a perceptron !?
	## tried on April 2013: doesn't work ! but no real idea why not ...
	## also tried $x->[4] ++ : doesn't work either
      }
    }
  }

}

sub handle_bcinfo {
  my ($cinfo,$match,$decided,$n) = @_;
  
  my $status = $map{"$cinfo->{kept}$match"};

  ##  print "bcinfo tpos=$cinfo->{tpos} btpos=$cinfo->{btpos} label=$cinfo->{label} btype=$cinfo->{btype} match=$match\n";
  my @blabels = ();
  foreach my $fsentry (@bfeatures) {
    if (my $key = $fsentry->{check}($cinfo,$fsentry->{value})) {
      push(@blabels,$key);
    }
  }
   
  my $d1 =  1;
  my $d2 = 0;
  unless ($decided) {
#    $d1 =  min(1, 1 / max(1,min($n-1,14)));
    $d1 =  min(1, 1 / max(1,min($n-1,13)));
#    $d1 =  min(1, 1 / max(1,min($n-1,12)));
#    $d1 =  min(1, 1 / max(1,min(0.5 * ($n+$rank)-1,14)));
    $d2 = 1 - $d1;
  }
   
  foreach (@blabels) {
##    print "register bcinfo $_->[0]\n";
    my $x = $bcost->{$_->[0]} 
      ||= [$bcostctr++, # id = 0
	   0, # n = 1
	   0, # v = 2
	   0, # yes-yes  = 3 = keepgood
	   0, # yes-no  = 4 = keepbad
	   0, # no-yes  = 5 = delgood
	   0, # no-no  = 6 = delbad
	   $_, # value to emit = 7
	  ];
    $x->[1] ++;               # n
    if ($decided) {
      $x->[$status]++;
    } else {
      $x->[4] += $d1;     # yes-no
      $x->[6] += $d2;     # no-no
    }
  }
}

sub distrib_key {
  my ($where,@keys) = @_;
  return $where->{"@keys"} ||= $dkeyctr++;
}

sub fset_key {
  my ($where,$cinfo,$emitfun,@keys) = @_;
  return $where->{"@keys"} ||= [$fskeyctr++,$cinfo,$emitfun];
}

sub feature_code {
  ## $_[0] is feature
  ## $_[1] is value
  return exists $foreign{$_[0]} ? ($lexicon{$_[1]} ||= \$_[1]) : $_[1];
}

sub feature_decode {
  ## $_[0] is a coded value
  return ref($_[0]) eq 'SCALAR' ? ${$_[0]} : $_[0];
}

sub cinfo_set {
  $_[0]->{$_[1]} = feature_code($_[1] => $_[2]);
}

sub cinfo_get {
  my $v = $_[0]->{$_[1]};
  return ref($v) eq 'SCALAR' ? $$v : $v;
}

package MyHash;

## a mix implementation of hash embbeding arrays for fast access

our $map = {};
our $fctr=0;

sub TIEHASH {
  my $class = $_[0];
  $map ||= {};
  $fctr ||= 0;
  my $obj = [$map,(undef) x $fctr];
#  my @k = keys %$map;
#  print "tie hash fctr=$fctr @k\n";
  return bless $obj, $class;
}

sub FETCH {
#  print "fetch f=$_[1] fctr=$fctr\n";
#  exists $map->{$_[1]} or print "fetch new f=$_[1]\n";
  $_[0]->[$map->{$_[1]} ||= ++$fctr];
}

sub STORE {
#  exists $map->{$_[1]} or print "store new $fctr f=$_[1]\n";
  $_[0]->[$map->{$_[1]} ||= ++$fctr] = $_[2];
}

sub DELETE {
  $_[0]->[$map->{$_[1]} ||= ++$fctr] = undef;
}

sub CLEAR {
  $_[0]->[$_] = undef for (0..$fctr);
}

sub EXISTS {
  defined $_[0]->[$map->{$_[1]} ||= ++$fctr];
}

sub FIRSTKEY {
  my $a = keys %{$_[0]->[0]};
  my ($f,$i) = each(%{$_[0]->[0]});
  $f and return ($f,$_[0]->[$i]);
}

sub NEXTKEY {
  my ($f,$i) = each %{$_[0][0]};
  $f and return ($f,$_[0]->[$i]);
}

sub SCALAR {
  return scalar(grep {defined $_} @{$_[0]});
}

1;

