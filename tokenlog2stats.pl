#!/usr/bin/env perl

## build stats from tokens.log and reltokens.log

use strict;

use AppConfig qw/:argcount :expand/;
use Text::Table;
use CGI::Pretty qw/:standard *table *ul/;
use List::Util qw/min max/;

my $config = AppConfig->new(
                            "verbose!" => {DEFAULT => 1},
			    "tokens=f" => {DEFAULT => "tokens.log"},
			    "reltokens=f" => {DEFAULT => "reltokens.log"}, 
                            "html=f" => {DEFAULT => "stats.html"}
                           );

$config->args();


my $tokenfile = $config->tokens;
my $reltokenfile = $config->reltokens;
my $html = $config->html;

my $sentences = {};
my $errors = {};

open(RELTOK,"<$reltokenfile") 
  or die "can't open '$reltokenfile': $!";
while (<RELTOK>) {
  if (/^(\S+?)F(\d+)\s+\#{3}\s+(\S+)/) {
    my $sid = $1;
    my $fid = $2;
    my $status = $3;
    my $match = / match / ? 1 : 0;
    my ($corpuskind) = $sid =~ /^(\S+?)[_.]/;
    my $entry = $sentences->{$sid} ||= { n => 0, failed => 0, sid => $sid, s=> [], rels => [],
					 corpuskind => $corpuskind || 'other'
				       };
    $entry->{status} = $status;
    $entry->{n}++;
    unless ($match) {
      $entry->{failed}++;
      unless ($match eq 'onlyref') {
	$entry->{hyp}++;
      }
      unless ($match eq 'onlyhyp') {
	$entry->{ref}++;
      }
      my ($xsid) = $sid =~ /(E\S+)$/;
      my $xfid = "$xsid.$fid";
      my ($reft,$hypt,$refhead,$hyphead) = /(\S+)\s+(\S+)\s+(0|E\S+F\d+)\s+(0|E\S+F\d+)/;
      if ($refhead) {
	$refhead =~ s/^E\S+F//;
#	print "\tregister refrel $reft F$refhead F$fid\n";
	push(@{$entry->{rels}},{status => 'ref', type => $reft, tail => $fid, head => $refhead, 
				first => min($fid,$refhead),
				last => max($fid,$refhead),
			       });
      }
      if ($hyphead) {
	$hyphead =~ s/^E\S+F//;
	push(@{$entry->{rels}},{status => 'hyp', type => $hypt, tail => $fid, head => $hyphead, 
				first => min($fid,$hyphead),
				last =>  max($fid,$hyphead),
			       });
      }
    }
  } elsif (/^(\S+?)F\d+\s+(.+?)\s+(\S+)\s+(full|robust|fail|corrected)/) {
    my $sid = $1;
    my $w = $2;
    my $status = $4;
    my ($corpuskind) = $sid =~ /^(\S+?)[_.]/;
    my $entry = $sentences->{$sid} ||= { status => $status, 
					 n => 0, 
					 failed => 0, 
					 sid => $sid, 
					 s => [], 
					 rels => [],
					 corpuskind => $corpuskind || 'other'
				       };
    push(@{$entry->{s}},$w);
  }
}
close(RELTOK);

my $ns = 0;

my $corpuskind = {};

foreach my $s (values %$sentences) {
  $s->{size} = scalar(@{$s->{s}});
  $s->{sflat} = join(' ',@{$s->{s}});
  push(@{$errors->{$s->{failed}}},$s);
  $ns++;
  ## compute lines for relations
  ## reuse from EasyRef
  $s->{lines} = [];
  my @lines = ();

  my $entry = $corpuskind->{$s->{corpuskind}} ||= { n => 0, failed => 0 };
  $entry->{n} += $s->{n};
  $entry->{failed} += $s->{failed};

  my $status = $s->{status};
  $entry->{"${status}_n"} += $s->{n};
  $entry->{"${status}_failed"} += $s->{failed};

  foreach my $r (sort { $a->{first} <=> $b->{first}
			  || $a->{last} <=> $b->{last}
			}
		 @{$s->{rels}}) {
    ## rel2line
    my $first = $r->{first};
    my $last = $r->{last};
    my $line = -1;
  LOOP: for (my $l=0; $l <= $#lines; $l++) {
      ## Search for a line
      for (my $i=$first; $i <= $last; $i++) {
	exists $lines[$l]{$i} and next LOOP;
      };
      ## Found a line
      $line=$l;
      last LOOP;
    }
    if ($line == -1 ) {
      push(@lines,{});
      $line = $#lines;
    }
    # toggle (l,i) cases to state they are busy
    for (my $i=$first; $i <= $last; $i++) {
      $lines[$line]{$i}=$r;
    };
  }

  if (@lines) {
    for (my $l=0; $l <= $#lines; $l++) {
      my $last = 0;
      my $prevr;
      my $line = [];
      my $x = $lines[$l];
      foreach my $i (sort {$a <=> $b} keys %$x) {
	my $r = $x->{$i};
	next if (defined $prevr && $prevr == $r);
	$prevr = $r;
	my $status = $r->{status};
	my $edge =
	  { left => $r->{first},
	    right => $r->{last},
	    type => $r->{type},
	    status => $status,
	    size => 1+$r->{last}-$r->{first},
	    rel => $r
	  };
	## print "edge register left=$edge->{left} right=$edge->{right} type=$edge->{type} size=$edge->{size}\n";
	if ($edge->{left} > $last+1) {
	  push(@$line, { left => $last+1, 
			 right => $edge->{left}-1,
			 size => $edge->{left}-$last-1 });
	}
	push(@$line,$edge);
	$last = $edge->{right};
      }
      push(@{$s->{lines}},$line);
    }
  }

}

print STDERR "processed $ns sentences\n";


my $k = 0;
my $stats = [];
foreach my $n (sort {$a <=> $b} keys %$errors) {
  my $v = scalar(@{$errors->{$n}});
  my $vfull = scalar( grep {$_->{status} eq 'full'} @{$errors->{$n}} );
  my $vcorrected = scalar( grep {$_->{status} eq 'corrected'} @{$errors->{$n}} );
  my $vrobust = $v - $vfull -$vcorrected;
  push(@$stats, { i => $k++, 
		  errors => $n, 
		  full => $vfull, 
		  corrected => $vcorrected,
		  robust => $vrobust 
		});
}

my $i = 0;
my $stats2 = [];
my $acc = 0;
my $okacc = 0;
my $n2 = 0;
foreach my $s (sort {$sentences->{$a}{failed} <=> $sentences->{$b}{failed}} keys %$sentences) {
  $acc += $sentences->{$s}{failed};
  $okacc += ($sentences->{$s}{n}-$sentences->{$s}{failed});
  unless ($i % 500) {
    push(@$stats2, { n => $n2++, i => $i, acc => $acc, okacc => $okacc });
  }
  $i++;
}
push(@$stats2, { n => $n2++, i => $i, acc => $acc, okacc => $okacc });

use Template;

my $tt=Template->new({});

$tt->process(\*DATA,             
	     { 
	      stats => $stats,
	      nrecords => $k,
	      nsentences => $ns,
	      errors => $errors,
	      stats2 => $stats2,
	      nrecords2 => scalar(@$stats2),
	      corpus => $corpuskind
             },
	     $html
            )
  or die "can't process template: $!";

__END__

[% USE date %]
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Error distribution in sentences</title>
    <style type="text/css">
     <!--
       .sid { color: red; }
       .hilight { background: lightgreen; }

.Fs {
    background-color: #FFFFFF;
}
.F {
    text-align: center;
}
.word {
    background-color: lightBlue;
    min-width: 10px;
}

.void {
        border-style: none;
 }

.SUJ-V, .suj {
     background-color: lightgreen;
}

.AUX-V, .aux_tps, .aux_pass, .aux_caus {
        background-color: lightblue;
}

.COD-V, .obj, .aff {
        background-color: orange;
}

.CPL-V, .de_obj, .a_obj, .p_obj {
        background-color: yellow;
}

.MOD-V, .mod, .mod_rel {
        background-color: #FF99FF;
}

.COMP, .det {
        background-color: #66FFFF;
}

.ATB-SO, .ato, .ats {
        background-color: #00FFFF;
}

.MOD-N, .dep {
        background-color: #FFCC99;
}

.MOD-A {
        background-color: #CC9900;
}

.MOD-R {
        background-color: #CCCC00;
}

.MOD-P {
        background-color: #33FFFF;
}

.COORD, .coord, .dep_coord {
        background-color: #CCFFFF;
}

.COORD-D {
        background-color: #CCFFFF;
}

.COORD-G {
        background-color: #CCFFFF;
}

.APPOS, .root {
        background-color: #00FF00;
}

.JUXT, .ponct {
        background-color: #00FFCC;
}

.ref {
    color: blue;
    font-weight: bolder;
}

.hyp {
    color: red;
    font-weight: bolder;
}

.line {
    text-align: center;
    font-size: smaller;
    border-width: 0pt;
}

.edge {
    border-style: none;
    border-width: 2pt;
}

     -->
   </style>
   <script type="text/javascript" language="javascript">
<!--
        function showhide(obj) {
           var el=document.getElementById(obj);
           if (el.style.display == "none") {
              el.style.display = "block";
           } else {
              el.style.display = "none";
           }
        }
        function show(obj) {
           var el=document.getElementById(obj);
           el.style.display = "block";
        }
-->
  </script>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      data.addColumn('string', '#errors');
      data.addColumn('number', '#full sentences');
      data.addColumn('number', '#corrected sentences');
      data.addColumn('number', '#robust sentences');
      data.addRows([% nrecords %]);
[% FOREACH k IN stats %]
     data.setValue([% k.i %], 0, '[% k.errors %]');
     data.setValue([% k.i %], 1, [% k.full %]);
     data.setValue([% k.i %], 2, [% k.corrected %]);
     data.setValue([% k.i %], 3, [% k.robust %]);
[% END %]
      chart.draw(data, { 
                         height: 500,
                         width: 1000,
                         isStacked: 1,
                         title: 'bad tokens per sentence',
                         hAxis: {title: '#errors', titleTextStyle: {color: 'red'}}
                         });

      // Create the data table.
      var data2 = new google.visualization.DataTable();

      var chart2 = new google.visualization.AreaChart(document.getElementById('chart2_div'));
      data2.addColumn('string','#sentences');
      data2.addColumn('number','#errors');
      data2.addColumn('number','#ok');
      data2.addRows([% nrecords2 %]);
[% FOREACH k IN stats2 %]
     data2.setValue([% k.n %], 0, '[% k.i %]');
     data2.setValue([% k.n %], 1, [% k.acc %]);
     data2.setValue([% k.n %], 2, [% k.okacc %]);
[% END %]
      chart2.draw(data2, { 
                         height: 500,
                         width: 1000,
                         isStacked: 1,
                         title: 'accumulated errors',
                         hAxis: {title: '#errors', titleTextStyle: {color: 'red'}}
                         });

[% FOREACH c IN corpus.keys.sort %]
    data = new google.visualization.DataTable();
    chart = new google.visualization.PieChart(document.getElementById('chart_div_[%- c %]'));
    data.addColumn('string','Status');
    data.addColumn('number','#rels');
    data.addRows(4);
    data.setValue(0,0,'failure in full');
    data.setValue(0,1,[% corpus.$c.full_failed %]);
    data.setValue(1,0,'failure in robust');
    data.setValue(1,1,[% corpus.$c.robust_failed %]);

    data.setValue(2,0,'success in robust');
    data.setValue(2,1,[% corpus.$c.robust_n - corpus.$c.robust_failed %]);
    data.setValue(3,0,'success in full');
    data.setValue(3,1,[% corpus.$c.full_n - corpus.$c.full_failed %]);

    chart.draw(data, { 
                         height: 300,
                         width: 300
                      });
[% END %]

    }


 </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <h1>Reltoken failure distribution for [% nsentences %] sentences</h1>
    <div id="chart_div"></div>
    <h1>Accumulated errors</h1>
    <div id="chart2_div"></div>
   <h1> Failure rate per corpus </h1>
   <table>
      <tr>
[% FOREACH c IN corpus.keys.sort %]
         <td><div id="chart_div_[%- c %]"></div></td>
[% END %]
      </tr>
      <tr>
[% FOREACH c IN corpus.keys.sort %]
         <td>[%- c %]</td>
[% END %]
      </tr>
    </table>
    <h1>Sentences</h1>
<p> Cliquer sur les labels pour voir ou non les phrases exemples </p>
<ul>
[% FOREACH e IN errors.keys.nsort %]
  <li> <a name="errors_[%- e %]" onclick="showhide('errors_[%- e %]')">[%- e %] errors</a>
  ([%- errors.$e.size %] sentences)
    <div id="errors_[%- e %]" style="display:none;">
      <ul>
       [% FOREACH s IN errors.$e.sort('size','status','sid') %]
            <li> <span class="sid"> <a name="[%- s.sid %]" onclick="showhide('[%- s.sid %]')">[% s.sid %]</a></span> ([% s.status %], [% s.n %] rels)
                   <span class="s">[% s.sflat %]</span> 
                 <div id="[%- s.sid %]" style="display:none;">

<table>
  <tr border="1" class="words">
[% FOREACH w IN s.s %]
      <td class="word">[%- w %]</td>
[% END %]
  </tr>
  <tr class="Fs">
[%- f = 0 %]
[% FOREACH w IN s.s %]
[%- f = f+1 %]
      <td class="F">F[%- f %]</td>
[% END %]
  </tr>

 <!-- lines -->
  [%- FOREACH line IN s.lines %]
  <tr class="line">
    [% last = 0 %]
    [%- FOREACH edge IN line %]
      [%- IF edge.left > last + 1 -%]
        <td colspan="[%- edge.left - 1 - last %]" class="void"/>
      [%- END -%]
      [%- IF edge.type %]
        <td colspan="[%- edge.size %]"
            class="edge [%- edge.type %]"
            status="[%- edge.status %]">
          <span class="[%- edge.status %]">[% edge.type %]</span>
        </td>
      [%- ELSE %]
        <td colspan="[%- edge.size %]" class="void"/>
      [%- END %]
      [%- last = edge.right %]
    [%- END %]
  </tr>
  [%- END %]


</table>

                 </div>
            </li>
       [% END %]
      </ul>
    </div>
  </li>
[% END %]

</ul>

  </body>

<html>

