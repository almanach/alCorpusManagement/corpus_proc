<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--

     convert from old easy format to new easy format
     removed tag constituants
     add tag id
     delete attribute mode

-->

<xsl:output
  encoding="ISO-8859-1"
  method="xml"
  indent="yes"/>

<!-- copy original content -->

<xsl:template match="*">
  <xsl:element name="{name()}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="@*">
  <xsl:copy/>
</xsl:template>

<!-- add attribute id -->

<xsl:template match="DOCUMENT">
  <xsl:copy>
    <xsl:copy-of select="@*"/>
    <xsl:attribute name="id">frmg</xsl:attribute>
    <xsl:apply-templates select="E"/>
  </xsl:copy>
</xsl:template>

<!-- delete tag constituants -->
<xsl:template match="constituants">
  <xsl:apply-templates/>
</xsl:template>

<!-- delete attribute mode -->
<xsl:template match="@mode">
</xsl:template>

<!-- delete attribute cat -->
<xsl:template match="@cat">
</xsl:template>

</xsl:stylesheet>
